import React, { Component } from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    StatusBar,
    TouchableOpacity,
    View,
    Image,
    AsyncStorage
} from 'react-native';
import { Icon } from 'native-base';
import { createStackNavigator, createMaterialTopTabNavigator, createDrawerNavigator } from 'react-navigation';

import { colors, normalize, dimens } from '../resources/'

import SplashScreen from '../screens/SplashScreen';
import LoginScreen from '../screens/Login/LoginScreen';
import ForgotScreen from '../screens/Forgot/ForgotScreen';
import RegistrationScreen from '../screens/Registration/RegistrationScreen';
import OtpScreen from '../screens/Registration/OtpScreen';
import HomeScreen from '../screens/Home/HomeScreen';
import CandidateScreen from '../screens/Candidates/CandidateListScreen';
import CandidateProfileScreen from '../screens/Candidates/CandidateProfileScreen';
import ElectedProfileScreen from '../screens/Officials/ElectedProfileScreen';
import BillScreen from '../screens/Candidates/BillScreen';
import BillDetailScreen from '../screens/Candidates/BillDetailScreen';
import BillCreationScreen from '../screens/Candidates/BillCreationScreen';
import ContributionScreen from '../screens/Candidates/ContributionScreen';
import ContributionDetailScreen from '../screens/Candidates/ContributionDetailScreen';
import ContributionCreateScreen from '../screens/Candidates/ContributionCreateScreen';
import DonateScreen from '../screens/Candidates/DonateScreen';
import ProjectScreen from '../screens/Candidates/ProjectScreen';
import ProjectDetailScreen from '../screens/Candidates/ProjectDetailScreen';
import ProjectCreateScreen from '../screens/Candidates/ProjectCreateScreen';
import OfficialScreen from '../screens/Officials/OfficialScreen';
import OfficialProfileScreen from '../screens/Officials/OfficialProfileScreen';
import IssueIdeaScreen from '../screens/IssuesIdea/IssueIdeaScreen';
import IssueScreen from '../screens/IssuesIdea/IssueScreen';
import IdeaScreen from '../screens/IssuesIdea/IdeaScreen';
import IssuesIdeaCreateScreen from '../screens/IssuesIdea/IssuesIdeaCreateScreen';
import PollScreen from '../screens/Polls/PollScreen';
import PollRecentScreen from '../screens/Polls/PollRecentScreen';
import PollCompleteScreen from '../screens/Polls/PollCompleteScreen';
import PollDetailScreen from '../screens/Polls/PollDetailScreen';
import PollCreateScreen from '../screens/Polls/PollCreateScreen';
import ChatScreen from '../screens/Chat/ChatScreen';
import ChatAll from '../screens/Chat/ChatAll';
import ChatDetailScreen from '../screens/Chat/ChatDetailScreen';
import ForumScreen from '../screens/Forum/ForumScreen';
import ForumDetailScreen from '../screens/Forum/ForumDetailScreen';
import ForumCreationScreen from '../screens/Forum/ForumCreationScreen';
import PaymentScreen from '../screens/Payment/WalletScreen';

import ProfileScreenEdit from '../screens/Officials/ProfileScreenEdit';
import FollowerScreen from '../screens/Officials/FollowerScreen';
import FollowerProfileScreen from '../screens/Officials/FollowerProfileScreen';
import DrawerLeftConfig from '../screens/DrawerLeftScreen';
import News from '../screens/News/News';


export const PollTab = createMaterialTopTabNavigator({
    PollRecentScreen: {
        screen: PollRecentScreen,
        navigationOptions: {
            tabBarLabel: 'Recent'
        }
    },
    PollCompleteScreen: {
        screen: PollCompleteScreen,
        navigationOptions: {
            tabBarLabel: 'Completed',
        }
    },
},
    {
        tabBarPosition: 'top',
        tabBarOptions: {
            upperCaseLabel: false,
            labelStyle: {
                fontSize: 16,
                color: colors.primary,
            },
            style: {
                backgroundColor: '#fff',
            },
            indicatorStyle: {
                borderBottomColor: colors.primary,
                borderBottomWidth: 2,
            }
        }
    }
);


const HomeStack = createStackNavigator({
    HomeScreen: {
        screen: HomeScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    CandidateScreen: {
        screen: CandidateScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    CandidateProfileScreen: {
        screen: CandidateProfileScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    DonateScreen: {
        screen: DonateScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    BillScreen: {
        screen: BillScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    BillDetailScreen: {
        screen: BillDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    BillCreationScreen: {
        screen: BillCreationScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ContributionScreen: {
        screen: ContributionScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ContributionDetailScreen: {
        screen: ContributionDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ContributionCreateScreen: {
        screen: ContributionCreateScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ProjectScreen: {
        screen: ProjectScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ProjectDetailScreen: {
        screen: ProjectDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ProjectCreateScreen: {
        screen: ProjectCreateScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    OfficialScreen: {
        screen: OfficialScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    OfficialProfileScreen: {
        screen: OfficialProfileScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    FollowerScreen: {
        screen: FollowerScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    FollowerProfileScreen:{
        screen: FollowerProfileScreen,
        navigationOptions: {
            header: () => null,
        }
    },   
    IssueIdeaScreen: {
        screen: IssueIdeaScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    IssueScreen: {
        screen: IssueScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    IdeaScreen: {
        screen: IdeaScreen,
        navigationOptions: {
            header: () => null,
        }
    },

    NewsScreen: {
        screen: News,
        navigationOptions: {
            header: () => null,
        }
    },

    IssuesIdeaCreateScreen: {
        screen: IssuesIdeaCreateScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    PollScreen: {
        screen: PollTab,
        navigationOptions: ({ navigation }) => ({

            headerLeft: (
                <TouchableOpacity onPress={() => { navigation.openDrawer() }}>
                    <Image style={{ marginLeft: 10, width: 25, height: 25 }} source={require('../resources/images/Home_page/Menu_icon/Menu-icon.png')} />
                </TouchableOpacity>
            ),
            title: 'Polls',
            headerTitleStyle: {
                flex: 1, textAlign: 'left',
                fontWeight: 'bold', fontSize: normalize(dimens.headerTitle), alignItems: 'flex-start'
            },
            headerRight: (
                <TouchableOpacity onPress={() => { 
                    navigation.navigate('PollCreateScreen') 
                    }}>
                     {(navigation.state.params.block_status === 0) ? null : 
                    <View style={{
                        flexDirection: 'row', marginRight: 10,
                        backgroundColor: colors.primary, borderRadius: 5, justifyContent: 'space-between', alignItems: 'center', padding: 5
                    }}>
                        <Image style={{ margin: 5, width: 15, height: 15 }} source={require('../resources/images/04IssueIdea/Edit-Icon/Edit-Icon.png')} />
                        <Text uppercase={false} style={{ margin: 5, color: colors.white, fontSize: normalize(dimens.headerButtonText) }}>
                            Create Poll</Text>
                    </View>
                    } 
                </TouchableOpacity>
            ),
        })
    },
    PollDetailScreen: {
        screen: PollDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    PollCreateScreen: {
        screen: PollCreateScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ProfileScreenEdit: {
        screen: ProfileScreenEdit,
        navigationOptions: {
            header: () => null,
        }
    }
});


const ChatStack = createStackNavigator({
    ChatScreen: {
        screen: ChatScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ChatAll: {
        screen: ChatAll,
        navigationOptions: {
            header: () => null,
        }
    },
    ChatDetailScreen: {
        screen: ChatDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    FollowerProfileScreen:{
        screen: FollowerProfileScreen,
        navigationOptions: {
            header: () => null,
        }
    }, 
});

const ForumStack = createStackNavigator({
    ForumScreen: {
        screen: ForumScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ForumDetailScreen: {
        screen: ForumDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ForumCreationScreen: {
        screen: ForumCreationScreen,
        navigationOptions: {
            header: () => null,
        }
    },
});

const PaymentStack = createStackNavigator({
    PaymentScreen: {
        screen: PaymentScreen,
        navigationOptions: {
            header: () => null,
        }
    }
});

const ElectedStack = createStackNavigator({
    ElectedProfileScreen: {
        screen: ElectedProfileScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    BillScreen: {
        screen: BillScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    BillDetailScreen: {
        screen: BillDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    BillCreationScreen: {
        screen: BillCreationScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ContributionScreen: {
        screen: ContributionScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ContributionDetailScreen: {
        screen: ContributionDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ContributionCreateScreen: {
        screen: ContributionCreateScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ProjectScreen: {
        screen: ProjectScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ProjectDetailScreen: {
        screen: ProjectDetailScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ProjectCreateScreen: {
        screen: ProjectCreateScreen,
        navigationOptions: {
            header: () => null,
        }
    },
});
const DrawerStack = createDrawerNavigator(
    {
        'Home': { screen: HomeStack },
        'Chat': { screen: ChatStack },
        'Profile': { screen: ElectedStack },
        'Forum': { screen: ForumStack },
        'Payment': { screen: PaymentStack },
    }, {
        contentOptions: {
            inactiveTintColor: 'white',
            activeTintColor: 'white',
            activeBackgroundColor: '#28262b',
            style: {
                flex: 1
            }
        },
        contentComponent: props => <DrawerLeftConfig {...props} />,
        initialRouteName: 'Home'
    }
);

const RootStack = createStackNavigator({
    SplashScreen: {
        screen: SplashScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    LoginScreen: {
        screen: LoginScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    ForgotScreen: {
        screen: ForgotScreen,
        navigationOptions: {
            header: () => null
        }
    },
    RegistrationScreen: {
        screen: RegistrationScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    OtpScreen: {
        screen: OtpScreen,
        navigationOptions: {
            header: () => null,
        }
    },
    HomeScreen: {
        screen: DrawerStack,
        navigationOptions: {
            header: () => null,
        }
    },


}, {
        initialRouteName: 'SplashScreen',
    });


export default createStackNavigator(
    {
        SplashScreen: {
            screen: RootStack,
        },
    }, {
        headerMode: 'none'
    }
);