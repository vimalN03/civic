import {
    CONTRIBUTION_DETAIL_ACTION, CONTRIBUTION_DETAIL_SUCCESS, CONTRIBUTION_DETAIL_FAILED,
    CONTRIBUTION_DELETE_ACTION, CONTRIBUTION_DELETE_SUCCESS, CONTRIBUTION_DELETE_FAILED, CLEAR_STATE
} from '../actions/contributionDetail-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    contributionDetailResponse: {},
    contributionDeleteResponse: {},
    isLoading: true
};

const contributionDetailReducer = (state = INIT, action) => {
    switch (action.type) {
        case CONTRIBUTION_DETAIL_ACTION:
            return { ...state, isLoading: false };
        case CONTRIBUTION_DETAIL_SUCCESS:
            return { ...state, error: '', contributionDetailResponse: action.payload, isLoading: true };
        case CONTRIBUTION_DETAIL_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CONTRIBUTION_DELETE_ACTION:
            return { ...state, isLoading: false };
        case CONTRIBUTION_DELETE_SUCCESS:
            return { ...state, error: '', contributionDeleteResponse: action.payload, isLoading: true };
        case CONTRIBUTION_DELETE_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CLEAR_STATE:
            return { ...state, error: '', contributionDetailResponse: {}, contributionDeleteResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default contributionDetailReducer;