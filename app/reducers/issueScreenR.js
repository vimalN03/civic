import {
    GET_ALL_ISSUE_ACTION, GET_ALL_ISSUE_SUCCESS, ISSUE_LIST_FAILED, ISSUE_CLEAR_STATE
    
} from '../actions/issue-screen-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getResponseData: {},
    isLoading: true,
};



const issueReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_ALL_ISSUE_ACTION:
            return { ...state, isLoading: true };
        case GET_ALL_ISSUE_SUCCESS:
            return { ...state, error: '', getResponseData: action.payload, isLoading: true };
        case ISSUE_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case ISSUE_CLEAR_STATE:
            return { ...state, error: '', getResponseData: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default issueReducer;