import {
    GET_OFFICIAL_PROFILE_ACTION, GET_OFFICIAL_PROFILE_SUCCESS, OFFICIAL_PROFILE_LIST_FAILED, CLEAR_STATE
    
} from '../actions/official-profile-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    officialProfileResponse: {},
    isLoading: true,
};



const profileOfficialReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_OFFICIAL_PROFILE_ACTION:
            return { ...state, isLoading: true };
        case GET_OFFICIAL_PROFILE_SUCCESS:
            return { ...state, error: '', officialProfileResponse: action.payload, isLoading: true };
        case OFFICIAL_PROFILE_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', officialProfileResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default profileOfficialReducer;