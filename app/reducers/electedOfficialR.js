import {
    GET_ALL_OFFICIAL_LIST_ACTION, GET_ALL_OFFICIAL_LIST_SUCCESS, OFFICIAL_LIST_FAILED, CLEAR_STATE
    
} from '../actions/elected-official-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getResponseData: [],
    isLoading: true,
};



const electedOfficialReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_ALL_OFFICIAL_LIST_ACTION:
            return { ...state, isLoading: true };
        case GET_ALL_OFFICIAL_LIST_SUCCESS:
            return { ...state, error: '', getResponseData: action.payload, isLoading: true };
        case OFFICIAL_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', getResponseData: [], isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default electedOfficialReducer;