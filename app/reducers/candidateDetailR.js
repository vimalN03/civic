import {
    CANDIDATE_DETAIL_ACTION, CANDIDATE_DETAIL_SUCCESS, CANDIDATE_DETAIL_FAILED,
    CANDIDATE_DETAIL_RATING_ACTION, CANDIDATE_DETAIL_RATING_SUCCESS, CANDIDATE_DETAIL_RATING_FAILED, CLEAR_STATE
} from '../actions/candidateDetail-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    candidateDetailResponse: {},
    candidateDetailRatingResponse: {},
    isLoading: true
};

const candidateDetailReducer = (state = INIT, action) => {
    switch (action.type) {
        case CANDIDATE_DETAIL_ACTION:
            return { ...state, isLoading: false };
        case CANDIDATE_DETAIL_SUCCESS:
            return { ...state, error: '', candidateDetailResponse: action.payload, isLoading: true };
        case CANDIDATE_DETAIL_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CANDIDATE_DETAIL_RATING_ACTION:
            return { ...state, isLoading: false };
        case CANDIDATE_DETAIL_RATING_SUCCESS:
            return { ...state, error: '', candidateDetailRatingResponse: action.payload, isLoading: true };
        case CANDIDATE_DETAIL_RATING_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', candidateDetailResponse: {}, candidateDetailRatingResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default candidateDetailReducer;