import {
    GOOGLE_ACTION, GOOGLE_SUCCESS, GOOGLE_FAILED, CLEAR_STATE
} from '../actions/social-action';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    googleResponse: {},
    fbResponse: {},
    isLoading: true
};

const googleReducer = (state = INIT, action) => {
    switch (action.type) {
        case GOOGLE_ACTION:
            return { ...state, isLoading: false };
        case GOOGLE_SUCCESS:
            return { ...state, error: '', googleResponse: action.payload, isLoading: true };
        case GOOGLE_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', googleResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default googleReducer;