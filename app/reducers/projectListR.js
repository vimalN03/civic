import {
    PROJECT_LIST_ACTION, PROJECT_LIST_SUCCESS, PROJECT_LIST_FAILED, CLEAR_STATE
} from '../actions/projectList-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    projectListResponse: {},
    isLoading: true
};

const projectListReducer = (state = INIT, action) => {
    switch (action.type) {
        case PROJECT_LIST_ACTION:
            return { ...state, isLoading: false };
        case PROJECT_LIST_SUCCESS:
            return { ...state, error: '', projectListResponse: action.payload, isLoading: true };
        case PROJECT_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', projectListResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default projectListReducer;