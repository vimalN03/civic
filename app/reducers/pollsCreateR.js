import {
    POST_POLLS_CREATION_ACTION, POST_POLLS_CREATION_SUCCESS, POST_POLLS_CREATION_FAILED, POST_POLLS_CREATION_STATE
} from '../actions/pollsCreation-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getPollsCreateResData: {},
    isLoading: true,
};


const pollsCreateReducer = (state = INIT, action) => {
    switch (action.type) {
        case POST_POLLS_CREATION_ACTION:
            return { ...state, isLoading: true };
        case POST_POLLS_CREATION_SUCCESS:
            return { ...state, error: '', getPollsCreateResData: action.payload, isLoading: true };
        case POST_POLLS_CREATION_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case POST_POLLS_CREATION_STATE:
            return { ...state, error: '', getPollsCreateResData: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default pollsCreateReducer;