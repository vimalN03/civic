import {
    CHAT_MSG_POST_ACTION, CHAT_MSG_POST_SUCCESS, CHAT_MSG_POST_FAILED, CHAT_MSG_POST_CLEAR_STATE
} from '../actions/chatPostMsg-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    chatMsgerror: '',
    getChatMsgRes: {},
    chatMsgisLoading: true,
};

const chatPostMsgReducer = (state = INIT, action) => {
    switch (action.type) {
        case CHAT_MSG_POST_ACTION:
            return { ...state, chatMsgisLoading: true };
        case CHAT_MSG_POST_SUCCESS:
            return { ...state, chatMsgerror: '', getChatMsgRes: action.payload, chatMsgisLoading: true };
        case CHAT_MSG_POST_FAILED:
            return { ...state, chatMsgerror: action.payload, chatMsgisLoading: true };
        case CHAT_MSG_POST_CLEAR_STATE:
            return { ...state, chatMsgerror: '', getChatMsgRes: {}, chatMsgisLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default chatPostMsgReducer;