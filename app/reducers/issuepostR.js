import {
    GET_POST_ISSUE_ACTION, GET_POST_ISSUE_SUCCESS, POST_ISSUE_LIST_FAILED, POST_ISSUE_CLEAR_STATE
    
} from '../actions/issue-post-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getIssuePostResponseData: {},
    isLoading: true,
};



const issueReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_POST_ISSUE_ACTION:
            return { ...state, isLoading: true };
        case GET_POST_ISSUE_SUCCESS:
            return { ...state, error: '', getIssuePostResponseData: action.payload, isLoading: true };
        case POST_ISSUE_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case POST_ISSUE_CLEAR_STATE:
            return { ...state, error: '', getIssuePostResponseData: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default issueReducer;