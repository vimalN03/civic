import { combineReducers } from 'redux';
import LoginR from './loginR';
import RegistrationR from './registrationR';
import ForgotPasswordR from './forgotPasswordR';
import CandidateListR from './candidateListR';
import CandidateDetailR from './candidateDetailR';
import ProjectListR from './projectListR';
import ProjectDetailR from './projectDetailR';
import ProjectCreationR from './projectCreationR';
import ContributionListR from './contributionListR';
import ContributionDetailR from './contributionDetailR';
import ContributionCreationR from './contributionCreationR';
import BillsListR from './billsListR';
import BillDetailR from './billDetailR';
import BillCreationR from './billCreationR';
import ElectedOfficialR from './electedOfficialR';
import ProfileOfficialR from './officialProfileR';
import IssueIdeasR from './issueIdeasR';
import IssueR from './issueScreenR';
import IssuePostR from './issuepostR';
import CreateIssueR from './issueCreateR';
import WalletR from './walletR';
import IdeaR from './ideaScreenR';
import PollsR from './pollsRecentR';
import PollsCompletedR from './pollsCompletedR';
import PollsDetailsR from './pollsDetailsR';
import PollsCreateR from './pollsCreateR';
import PollsQuestionR from './pollsQuestionsR';
import HomeR from './homeR';
import ProfileEditR from './profileEditR';
import ForumListR from './forumListR';
import ForumDetailR from './forumDetailR';
import ForumCreationR from './forumCreationR';
import ForumPostR from './forumPostR';
import ChatListR from './chatListR';
import ChatListAllR from './chatListAllR';
import ChatMsgPostR from './chatMsgPostR';
import ChatConverstionR from './chatConversationR';
import ChatMemberR from './chatMemberR';
import OtpSendR from './optSendR';
import OtpVerifyR from './otpVerifyR';
import ChangeUserTypeR from './changeUserStatusR';
import SocialLoginR from './socialactionR';
import fbR from './fbR';
import NewsR from './newsR';

export default combineReducers({
    LoginR, RegistrationR, ForgotPasswordR, OtpSendR, OtpVerifyR, ChangeUserTypeR,
    CandidateListR, CandidateDetailR,
    ProjectListR, ProjectDetailR, ProjectCreationR,
    ContributionListR, ContributionDetailR, ContributionCreationR,
    BillsListR, BillDetailR, BillCreationR,
    ElectedOfficialR, ProfileOfficialR,
    IssueIdeasR, IssueR, IssuePostR, CreateIssueR,
    IdeaR,
    PollsR, PollsCompletedR, PollsDetailsR, PollsCreateR, PollsQuestionR,
    WalletR,
    HomeR,ProfileEditR,
    ForumListR, ForumDetailR, ForumCreationR, ForumPostR,
    ChatListR, ChatListAllR, ChatConverstionR, ChatMsgPostR, ChatMemberR,
    SocialLoginR, fbR, NewsR
});