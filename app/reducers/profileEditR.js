import {
    PROFILE_EDIT_ISSUE_ACTION, PROFILE_EDIT_ISSUE_SUCCESS, PROFILE_EDIT_LIST_FAILED, PROFILE_EDIT_CLEAR_STATE,
    GET_LGA_STATE_ID_ACTION, GET_LGA_STATE_ID_SUCCESS, GET_LGA_STATE_ID_FAILED, GET_LGA_STATE_ID_CLEAR,
    USER_PROFILE_ACTION, USER_PROFILE_SUCCESS, USER_PROFILE_LIST_FAILED, USER_CLEAR_STATE
} from '../actions/profile-edit-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getProfileEditResponseData: {},
    isLoading: true,
    getLgaStateRes: {},
    getUserDetails: {}
};



const profileEditReducer = (state = INIT, action) => {
    switch (action.type) {
        case PROFILE_EDIT_ISSUE_ACTION:
            return { ...state, isLoading: true };
        case PROFILE_EDIT_ISSUE_SUCCESS:
            return { ...state, error: '', getProfileEditResponseData: action.payload, isLoading: true };
        case PROFILE_EDIT_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case PROFILE_EDIT_CLEAR_STATE:
            return { ...state, error: '', getProfileEditResponseData: {}, isLoading: true };

        case GET_LGA_STATE_ID_ACTION:
            return { ...state, isLoading: true };
        case GET_LGA_STATE_ID_SUCCESS:
            return { ...state, error: '', getLgaStateRes: action.payload, isLoading: true };
        case GET_LGA_STATE_ID_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case GET_LGA_STATE_ID_CLEAR:
            return { ...state, error: '', getLgaStateRes: {}, isLoading: true };

        case USER_PROFILE_ACTION:
            return { ...state, isLoading: true };
        case USER_PROFILE_SUCCESS:
            return { ...state, error: '', getUserDetails: action.payload, isLoading: true };
        case USER_PROFILE_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case USER_CLEAR_STATE:
            return { ...state, error: '', getUserDetails: {}, isLoading: true };

        default:
            return commonErrorReducer(state, action);
    }
};

export default profileEditReducer;