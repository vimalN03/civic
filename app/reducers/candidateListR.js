import {
    CANDIDATE_LIST_ACTION, CANDIDATE_LIST_SUCCESS, CANDIDATE_LIST_FAILED,
    CANDIDATE_FOLLOW_ACTION, CANDIDATE_FOLLOW_SUCCESS, CANDIDATE_FOLLOW_FAILED, 
    FOLLOWER_LIST_ACTION, FOLLOWER_LIST_SUCCESS, FOLLOWER_LIST_FAILED,
    CLEAR_STATE
} from '../actions/candidatelist-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    candidateListResponse: {},
    candidateFollowResponse: {},
    followerListResponse: {},
    isLoading: true
};

const candidateListReducer = (state = INIT, action) => {
    switch (action.type) {
        case CANDIDATE_LIST_ACTION:
            return { ...state, isLoading: false };
        case CANDIDATE_LIST_SUCCESS:
            return { ...state, error: '', candidateListResponse: action.payload, isLoading: true };
        case CANDIDATE_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CANDIDATE_FOLLOW_ACTION:
            return { ...state, isLoading: false };
        case CANDIDATE_FOLLOW_SUCCESS:
            return { ...state, error: '', candidateFollowResponse: action.payload, isLoading: true };
        case CANDIDATE_FOLLOW_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case FOLLOWER_LIST_ACTION:
            return { ...state, isLoading: false };
        case FOLLOWER_LIST_SUCCESS:
            return { ...state, error: '', followerListResponse: action.payload, isLoading: true };
        case FOLLOWER_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CLEAR_STATE:
            return { ...state, error: '', candidateFollowResponse: {}, candidateListResponse: {}, followerListResponse:{}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default candidateListReducer;