import {
    GET_POLLS_COMPLETED_ACTION, GET_POLLS_COMPLETED_SUCCESS, POLLS_COMPLETED_LIST_FAILED, POLLS_CLEAR_STATE
    
} from '../actions/polls-completed-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getResponseData: [],
    isLoading: true,
};



const pollsCompletedReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_POLLS_COMPLETED_ACTION:
            return { ...state, isLoading: true };
        case GET_POLLS_COMPLETED_SUCCESS:
            return { ...state, error: '', getResponseData: action.payload, isLoading: true };
        case POLLS_COMPLETED_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case POLLS_CLEAR_STATE:
            return { ...state, error: '', getResponseData: [], isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default pollsCompletedReducer;