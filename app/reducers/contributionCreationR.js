import {
    CONTRIBUTION_CREATION_ACTION, CONTRIBUTION_CREATION_SUCCESS, CONTRIBUTION_CREATION_FAILED, CLEAR_STATE
} from '../actions/contributionCreation-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    contributionCreationResponse: {},
    isLoading: true
};

const contributionCreationReducer = (state = INIT, action) => {
    switch (action.type) {
        case CONTRIBUTION_CREATION_ACTION:
            return { ...state, isLoading: false };
        case CONTRIBUTION_CREATION_SUCCESS:
            return { ...state, error: '', contributionCreationResponse: action.payload, isLoading: true };
        case CONTRIBUTION_CREATION_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CLEAR_STATE:
            return { ...state, error: '', contributionCreationResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default contributionCreationReducer;