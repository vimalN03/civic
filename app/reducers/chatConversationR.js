import {
    CHAT_CONVERSATION_ACTION, CHAT_CONVERSATION_SUCCESS, CHAT_CONVERSATION_FAILED, CHAT_CONVERSATION_CLEAR_STATE
} from '../actions/chatConversation-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    getChatConversationRes: {},
    isLoading: true,
};

const chatConversationReducer = (state = INIT, action) => {
    switch (action.type) {
        case CHAT_CONVERSATION_ACTION:
            return { ...state, isLoading: true };
        case CHAT_CONVERSATION_SUCCESS:
            return { ...state, error: '', getChatConversationRes: action.payload, isLoading: true };
        case CHAT_CONVERSATION_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CHAT_CONVERSATION_CLEAR_STATE:
            return { ...state, error: '', getChatConversationRes: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default chatConversationReducer;