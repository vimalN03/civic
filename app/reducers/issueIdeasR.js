import {
    GET_ALL_ISSUEIDEAS_ACTION, GET_ALL_ISSUEIDEAS_SUCCESS, ISSUEIDEAS_LIST_FAILED, ISSUEIDEAS_CLEAR_STATE
    
} from '../actions/issue-ideas-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getResponseData: [],
    isLoading: true,
};



const issueIdeasReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_ALL_ISSUEIDEAS_ACTION:
            return { ...state, isLoading: true };
        case GET_ALL_ISSUEIDEAS_SUCCESS:
            return { ...state, error: '', getResponseData: action.payload, isLoading: true };
        case ISSUEIDEAS_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case ISSUEIDEAS_CLEAR_STATE:
            return { ...state, error: '', getResponseData: [], isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default issueIdeasReducer;