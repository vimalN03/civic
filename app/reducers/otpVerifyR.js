import {
    OTP_VERIFY_ACTION, OTP_VERIFY_SUCCESS, OTP_VERIFY_FAILED, CLEAR_OTP_VERIFY_STATE
} from '../actions/otpVerify-action';
import { commonErrorReducer } from './errorR';

const INIT = {
    errOtpVerify: '',
    otpVerifyResponse: {},
    isLoadotpVerify: true
};

const otpVerifyReducer = (state = INIT, action) => {
    switch (action.type) {
        case OTP_VERIFY_ACTION:
            return { ...state, isLoadotpVerify: true };
        case OTP_VERIFY_SUCCESS:
            return { ...state, errOtpVerify: '', otpVerifyResponse: action.payload, isLoading: false };
        case OTP_VERIFY_FAILED:
            return { ...state, errOtpVerify: action.payload, isLoadotpVerify: false };
        case CLEAR_OTP_VERIFY_STATE:
            return { ...state, errOtpVerify: '', otpVerifyResponse: {}, isLoadotpVerify: false };
        default:
            return commonErrorReducer(state, action);
    }
};

export default otpVerifyReducer;