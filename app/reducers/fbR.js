import {
    CLEAR_STATE,FB_ACTION, FB_SUCCESS, FB_FAILED
} from '../actions/social-action';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
   
    fbResponse: {},
    isLoading: true
};


const fbReducer = (state = INIT, action) => {
    switch (action.type) {
        case FB_ACTION:
            return { ...state, isLoading: false };
        case FB_SUCCESS:
            return { ...state, error: '', fbResponse: action.payload, isLoading: true };
        case FB_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', fbResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default fbReducer;