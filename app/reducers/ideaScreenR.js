import {
    GET_ALL_IDEA_ACTION, GET_ALL_IDEA_SUCCESS, IDEA_LIST_FAILED, IDEA_CLEAR_STATE,
    POST_RATING_IDEAS_ACTION,GET_ALL_RATING_IDEAS_SUCCESS, RATING_IDEAS_LIST_FAILED, RATING_IDEAS_CLEAR_STATE
    
} from '../actions/idea-screen-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getResponseData: {},
    getRatingResponseData:{},
    isLoading: true,
};



const ideaReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_ALL_IDEA_ACTION:
            return { ...state, isLoading: true };
        case GET_ALL_IDEA_SUCCESS:
            return { ...state, error: '', getResponseData: action.payload, isLoading: true };
        case IDEA_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case IDEA_CLEAR_STATE:
            return { ...state, error: '', getResponseData: {}, isLoading: true };

        case POST_RATING_IDEAS_ACTION:
            return { ...state, isLoading: true };
        case GET_ALL_RATING_IDEAS_SUCCESS:
            return { ...state, error: '', getRatingResponseData: action.payload, isLoading: true };
        case RATING_IDEAS_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case RATING_IDEAS_CLEAR_STATE:
            return { ...state, error: '', getRatingResponseData: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default ideaReducer;