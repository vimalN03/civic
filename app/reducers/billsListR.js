import {
    BILL_LIST_ACTION, BILL_LIST_SUCCESS, BILL_LIST_FAILED, CLEAR_STATE
} from '../actions/billsList-action';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    billListResponse: {},
    isLoading: true
};

const billListReducer = (state = INIT, action) => {
    switch (action.type) {
        case BILL_LIST_ACTION:
            return { ...state, isLoading: false };
        case BILL_LIST_SUCCESS:
            return { ...state, error: '', billListResponse: action.payload, isLoading: true };
        case BILL_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CLEAR_STATE:
            return { ...state, error: '', billListResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default billListReducer;