import {
    WALLET_ACTION, WALLET_SUCCESS, WALLET_FAILED, CLEAR_STATE
} from '../actions/wallet-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    walletResponse: {},
    isLoading: true
};

const walletReducer = (state = INIT, action) => {
    switch (action.type) {
        case WALLET_ACTION:
            return { ...state, isLoading: false };
        case WALLET_SUCCESS:
            return { ...state, error: '', walletResponse: action.payload, isLoading: true };
        case WALLET_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', walletResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default walletReducer;