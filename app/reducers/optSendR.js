import {
    OTP_SEND_ACTION, OTP_SEND_SUCCESS, OTP_SEND_FAILED, CLEAR_OTP_SEND_STATE
} from '../actions/optSender-action';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    otpResponse: {},
    isLoading: true
};

const otpSendReducer = (state = INIT, action) => {
    switch (action.type) {
        case OTP_SEND_ACTION:
            return { ...state, isLoading: true };
        case OTP_SEND_SUCCESS:
            return { ...state, error: '', otpResponse: action.payload, isLoading: false };
        case OTP_SEND_FAILED:
            return { ...state, error: action.payload, isLoading: false };
        case CLEAR_OTP_SEND_STATE:
            return { ...state, error: '', otpResponse: {}, isLoading: false };
        default:
            return commonErrorReducer(state, action);
    }
};

export default otpSendReducer;