import {
    CHAT_LIST_ALL_ACTION, CHAT_LIST_ALL_SUCCESS, CHAT_LIST_ALL_FAILED, CHAT_LIST_ALL_CLEAR_STATE
} from '../actions/chatListAll-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    getChatListAllRes: {},
    isLoading: true,
};

const chatListAllReducer = (state = INIT, action) => {
    switch (action.type) {
        case CHAT_LIST_ALL_ACTION:
            return { ...state, isLoading: true };
        case CHAT_LIST_ALL_SUCCESS:
            return { ...state, error: '', getChatListAllRes: action.payload, isLoading: true };
        case CHAT_LIST_ALL_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CHAT_LIST_ALL_CLEAR_STATE:
            return { ...state, error: '', getChatListAllRes: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default chatListAllReducer;