import {
    CHANGE_USERTYPE_ACTION, CHANGE_USERTYPE_SUCCESS, CHANGE_USERTYPE_FAILED, CLEAR_CHANGE_USERTYPE_STATE
} from '../actions/changeUserType-action';
import { commonErrorReducer } from './errorR';

const INIT = {
    changeUserStatusErr: '',
    changeUserStatusResponse: {},
    isLoading: true
};

const changeUserStatusReducer = (state = INIT, action) => {
    switch (action.type) {
        case CHANGE_USERTYPE_ACTION:
            return { ...state, isLoading: true };
        case CHANGE_USERTYPE_SUCCESS:
            return { ...state, changeUserStatusErr: '', changeUserStatusResponse: action.payload, isLoading: false };
        case CHANGE_USERTYPE_FAILED:
            return { ...state, changeUserStatusErr: action.payload, isLoading: false };
        case CLEAR_CHANGE_USERTYPE_STATE:
            return { ...state, changeUserStatusErr: '', changeUserStatusResponse: {}, isLoading: false };
        default:
            return commonErrorReducer(state, action);
    }
};

export default changeUserStatusReducer;