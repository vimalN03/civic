import {
    FORGOT_AUTH_ACTION, FORGOT_AUTH_SUCCESS, FORGOT_FAILED, CLEAR_FORGOT_STATE
} from '../actions/forgot-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    forgotResponse: {},
    isLoading: true
};

const loginReducer = (state = INIT, action) => {
    switch (action.type) {
        case FORGOT_AUTH_ACTION:
            return { ...state, isLoading: true };
        case FORGOT_AUTH_SUCCESS:
            return { ...state, error: '', forgotResponse: action.payload, isLoading: false };
        case FORGOT_FAILED:
            return { ...state, error: action.payload, isLoading: false };
        case CLEAR_FORGOT_STATE:
            return { ...state, error: '', forgotResponse: {}, isLoading: false };
        default:
            return commonErrorReducer(state, action);
    }
};

export default loginReducer;