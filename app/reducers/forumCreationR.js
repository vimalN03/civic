import {
    FORUM_CREATION_ACTION, FORUM_CREATION_SUCCESS, FORUM_CREATION_FAILED, CLEAR_STATE
} from '../actions/forumCreation-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    forumCreationResponse: {},
    isLoading: true
};

const forumCreationReducer = (state = INIT, action) => {
    switch (action.type) {
        case FORUM_CREATION_ACTION:
            return { ...state, isLoading: false };
        case FORUM_CREATION_SUCCESS:
            return { ...state, error: '', forumCreationResponse: action.payload, isLoading: true };
        case FORUM_CREATION_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CLEAR_STATE:
            return { ...state, error: '', forumCreationResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default forumCreationReducer;