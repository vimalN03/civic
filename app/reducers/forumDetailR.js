import {
    FORUM_DETAIL_ACTION, FORUM_DETAIL_SUCCESS, FORUM_DETAIL_FAILED,
    FORUM_CHANGE_STATUS_ACTION, FORUM_CHANGE_STATUS_SUCCESS, FORUM_CHANGE_STATUS_FAILED, CLEAR_STATE
} from '../actions/forumDetail-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    forumDetailResponse: {},
    forumChangeStatusResponse: {},
    isLoading: true
};

const forumDetailReducer = (state = INIT, action) => {
    switch (action.type) {
        case FORUM_DETAIL_ACTION:
            return { ...state, isLoading: false };
        case FORUM_DETAIL_SUCCESS:
            return { ...state, error: '', forumDetailResponse: action.payload, isLoading: true };
        case FORUM_DETAIL_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case FORUM_CHANGE_STATUS_ACTION:
            return { ...state, isLoading: false };
        case FORUM_CHANGE_STATUS_SUCCESS:
            return { ...state, error: '', forumChangeStatusResponse: action.payload, isLoading: true };
        case FORUM_CHANGE_STATUS_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CLEAR_STATE:
            return { ...state, error: '', forumDetailResponse: {}, forumChangeStatusResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default forumDetailReducer;