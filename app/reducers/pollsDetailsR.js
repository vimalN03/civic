import {
    GET_ALL_DETAILS_POLLS_ACTION, GET_ALL_DETAILS_POLLS_SUCCESS, POLLS_DETAILS_LIST_FAILED, POLLS_CLEAR_STATE
    
} from '../actions/polls-details-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getResponseData: {},
    isLoading: true,
};



const pollsDetailsReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_ALL_DETAILS_POLLS_ACTION:
            return { ...state, isLoading: true };
        case GET_ALL_DETAILS_POLLS_SUCCESS:
            return { ...state, error: '', getResponseData: action.payload, isLoading: true };
        case POLLS_DETAILS_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case POLLS_CLEAR_STATE:
            return { ...state, error: '', getResponseData: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default pollsDetailsReducer;