import {
    BILL_DETAIL_ACTION, BILL_DETAIL_SUCCESS, BILL_DETAIL_FAILED,
    BILL_DETAIL_DELETE_ACTION, BILL_DETAIL_DELETE_SUCCESS, BILL_DETAIL_DELETE_FAILED, CLEAR_STATE
} from '../actions/billDetail-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    billDetailResponse: {},
    billDetailDeleteResponse: {},
    isLoading: true
};

const contributionDetailReducer = (state = INIT, action) => {
    switch (action.type) {
        case BILL_DETAIL_ACTION:
            return { ...state, isLoading: false };
        case BILL_DETAIL_SUCCESS:
            return { ...state, error: '', billDetailResponse: action.payload, isLoading: true };
        case BILL_DETAIL_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case BILL_DETAIL_DELETE_ACTION:
            return { ...state, isLoading: false };
        case BILL_DETAIL_DELETE_SUCCESS:
            return { ...state, error: '', billDetailDeleteResponse: action.payload, isLoading: true };
        case BILL_DETAIL_DELETE_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', billDetailResponse: {}, billDetailDeleteResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default contributionDetailReducer;