import {
    FORUM_POST_ACTION, FORUM_POST_SUCCESS, FORUM_POST_FAILED, FORUM_POST_CLEAR_STATE
} from '../actions/forumPost-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    forumError: '',
    getForumPostRes: {},
    forumisLoading: true,
};

const forumPostReducer = (state = INIT, action) => {
    switch (action.type) {
        case FORUM_POST_ACTION:
            return { ...state, forumisLoading: true };
        case FORUM_POST_SUCCESS:
            return { ...state, forumError: '', getForumPostRes: action.payload, forumisLoading: true };
        case FORUM_POST_FAILED:
            return { ...state, forumError: action.payload, forumisLoading: true };
        case FORUM_POST_CLEAR_STATE:
            return { ...state, forumError: '', getForumPostRes: {}, forumisLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default forumPostReducer;