import {
    PROJECT_DETAIL_ACTION, PROJECT_DETAIL_SUCCESS, PROJECT_DETAIL_FAILED,
    PROJECT_DELETE_ACTION, PROJECT_DELETE_SUCCESS, PROJECT_DELETE_FAILED, CLEAR_STATE
} from '../actions/projectDetail-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    projectDetailResponse: {},
    projectDeleteResponse: {},
    isLoading: true
};

const projectDetailReducer = (state = INIT, action) => {
    switch (action.type) {
        case PROJECT_DETAIL_ACTION:
            return { ...state, isLoading: false };
        case PROJECT_DETAIL_SUCCESS:
            return { ...state, error: '', projectDetailResponse: action.payload, isLoading: true };
        case PROJECT_DETAIL_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case PROJECT_DELETE_ACTION:
            return { ...state, isLoading: false };
        case PROJECT_DELETE_SUCCESS:
            return { ...state, error: '', projectDeleteResponse: action.payload, isLoading: true };
        case PROJECT_DELETE_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', projectDetailResponse: {}, projectDeleteResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default projectDetailReducer;