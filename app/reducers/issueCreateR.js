import {
    CREATE_POST_ISSUE_ACTION, CREATE_POST_ISSUE_SUCCESS, CREATE_ISSUE_LIST_FAILED, CREATE_ISSUE_CLEAR_STATE
    
}
from '../actions/issue-create-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getResponseData: {},
    isLoading: true,
};



const issueReducer = (state = INIT, action) => {
    switch (action.type) {
        case CREATE_POST_ISSUE_ACTION:
            return { ...state, isLoading: true };
        case CREATE_POST_ISSUE_SUCCESS:
            return { ...state, error: '', getResponseData: action.payload, isLoading: true };
        case CREATE_ISSUE_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CREATE_ISSUE_CLEAR_STATE:
            return { ...state, error: '', getResponseData: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default issueReducer;