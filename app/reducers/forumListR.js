import {
    FORUM_LIST_ACTION, FORUM_LIST_SUCCESS, FORUM_LIST_FAILED, CLEAR_STATE
} from '../actions/forumList-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    forumListResponse: {},
    isLoading: true
};

const forumListReducer = (state = INIT, action) => {
    switch (action.type) {
        case FORUM_LIST_ACTION:
            return { ...state, isLoading: false };
        case FORUM_LIST_SUCCESS:
            return { ...state, error: '', forumListResponse: action.payload, isLoading: true };
        case FORUM_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };

        case CLEAR_STATE:
            return { ...state, error: '', forumListResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default forumListReducer;