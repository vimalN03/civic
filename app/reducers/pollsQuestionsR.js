import {
    POLLS_QUESTIONS_ACTION, POLLS_QUESTIONS_SUCCESS, POLLS_QUESTIONS_FAILED, POLLS_QUESTIONS_STATE
} from '../actions/pollsQuestionSubmit-actions';
import { commonErrorReducer } from './errorR';



const INIT = {
    pollQueserror: '',
    getPollSubmitRes: {},
    pollQuesisLoading: true,
};


const pollsQuestionReducer = (state = INIT, action) => {
    switch (action.type) {
        case POLLS_QUESTIONS_ACTION:
            return { ...state, pollQuesisLoading: true };
        case POLLS_QUESTIONS_SUCCESS:
            return { ...state, pollQueserror: '', getPollSubmitRes: action.payload, pollQuesisLoading: true };
        case POLLS_QUESTIONS_FAILED:
            return { ...state, pollQueserror: action.payload, pollQuesisLoading: true };
        case POLLS_QUESTIONS_STATE:
            return { ...state, pollQueserror: '', getPollSubmitRes: {}, pollQuesisLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default pollsQuestionReducer;