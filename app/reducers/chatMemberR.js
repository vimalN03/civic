import {
    CHAT_CHOOSE_MEMBER_ACTION, CHAT_CHOOSE_MEMBER_SUCCESS, CHAT_CHOOSE_MEMBER_FAILED, CHAT_CHOOSE_MEMBER_CLEAR_STATE
} from '../actions/chatChooseMember-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    chatMembererror: '',
    getchatMemberRes: {},
    chatMemberisLoading: true,
};

const chatPostMemberReducer = (state = INIT, action) => {
    switch (action.type) {
        case CHAT_CHOOSE_MEMBER_ACTION:
            return { ...state, chatMemberisLoading: true };
        case CHAT_CHOOSE_MEMBER_SUCCESS:
            return { ...state, chatMembererror: '', getchatMemberRes: action.payload, chatMemberisLoading: true };
        case CHAT_CHOOSE_MEMBER_FAILED:
            return { ...state, chatMembererror: action.payload, chatMemberisLoading: true };
        case CHAT_CHOOSE_MEMBER_CLEAR_STATE:
            return { ...state, chatMembererror: '', getchatMemberRes: {}, chatMemberisLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default chatPostMemberReducer;