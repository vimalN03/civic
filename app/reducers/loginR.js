import {
    LOGIN_AUTH_ACTION, LOGIN_AUTH_SUCCESS, LOGIN_FAILED, LOGGED_IN, ON_LOGOUT, CLEAR_STATE,
    LOGOUT_AUTH_ACTION, LOGOUT_AUTH_SUCCESS, LOGOUT_FAILED
} from '../actions/login-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    loginResponse: {},
    logoutResponse: {},
    isLoading: true,
    isLoggedIn: false,
};

const loginReducer = (state = INIT, action) => {
    switch (action.type) {
        case LOGIN_AUTH_ACTION:
            return { ...state, isLoading: false };
        case LOGIN_AUTH_SUCCESS:
            return { ...state, error: '', isLoggedIn: true, loginResponse: action.payload, isLoading: true };
        case LOGIN_FAILED:
            return { ...state, isLoggedIn: false, error: action.payload, isLoading: true };
        case LOGGED_IN:
            return { ...state, error: '', isLoading: true, isLoggedIn: true };
        case ON_LOGOUT:
            return { ...state, error: '', isLoading: true, isLoggedIn: false };
        case LOGOUT_AUTH_ACTION:
            return { ...state, isLoading: false };
        case LOGOUT_AUTH_SUCCESS:
            return { ...state, error: '', logoutResponse: action.payload, isLoading: true };
        case LOGOUT_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', loginResponse: {}, logoutResponse: {}, isLoading: true, isLoggedIn: false };
        default:
            return commonErrorReducer(state, action);
    }
};

export default loginReducer;