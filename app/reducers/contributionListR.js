import {
    CONTRIBUTION_LIST_ACTION, CONTRIBUTION_LIST_SUCCESS, CONTRIBUTION_LIST_FAILED, CLEAR_STATE
} from '../actions/contributionList-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    contributionListResponse: {},
    isLoading: true
};

const contributionListReducer = (state = INIT, action) => {
    switch (action.type) {
        case CONTRIBUTION_LIST_ACTION:
            return { ...state, isLoading: false };
        case CONTRIBUTION_LIST_SUCCESS:
            return { ...state, error: '', contributionListResponse: action.payload, isLoading: true };
        case CONTRIBUTION_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', contributionListResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default contributionListReducer;