import {
    HOME_ACTION, HOME_SUCCESS, HOME_FAILED, CLEAR_STATE
} from '../actions/home-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    recentIssuesPollsResponse: {},
    isLoading: true
};

const homeReducer = (state = INIT, action) => {
    switch (action.type) {
        case HOME_ACTION:
            return { ...state, isLoading: true };
        case HOME_SUCCESS:
            return { ...state, error: '', recentIssuesPollsResponse: action.payload, isLoading: false };
        case HOME_FAILED:
            return { ...state, error: action.payload, isLoading: false };
        case CLEAR_STATE:
            return { ...state, error: '', recentIssuesPollsResponse: {}, isLoading: false };
        default:
            return commonErrorReducer(state, action);
    }
};

export default homeReducer;