import {
    CHAT_LIST_ACTION, CHAT_LIST_SUCCESS, CHAT_LIST_FAILED, CHAT_LIST_CLEAR_STATE
} from '../actions/chatList-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    getChatListRes: {},
    isLoading: true,
};

const chatListReducer = (state = INIT, action) => {
    switch (action.type) {
        case CHAT_LIST_ACTION:
            return { ...state, isLoading: true };
        case CHAT_LIST_SUCCESS:
            return { ...state, error: '', getChatListRes: action.payload, isLoading: true };
        case CHAT_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CHAT_LIST_CLEAR_STATE:
            return { ...state, error: '', getChatListRes: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default chatListReducer;