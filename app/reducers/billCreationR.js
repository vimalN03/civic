import {
    BILL_CREATION_ACTION, BILL_CREATION_SUCCESS, BILL_CREATION_FAILED, CLEAR_STATE
} from '../actions/billCreation-action';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    billCreationResponse: {},
    isLoading: true
};

const billCreationReducer = (state = INIT, action) => {
    switch (action.type) {
        case BILL_CREATION_ACTION:
            return { ...state, isLoading: false };
        case BILL_CREATION_SUCCESS:
            return { ...state, error: '', billCreationResponse: action.payload, isLoading: true };
        case BILL_CREATION_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', billCreationResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default billCreationReducer;