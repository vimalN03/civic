import {
    E400_ERROR,
    E401_ERROR,
    E404_ERROR,
    E500_ERROR
} from '../actions/error-actions';

export function commonErrorReducer(state, action) {
    switch (action.type) {
        case E400_ERROR:
            return {
                ...state,
                error: action.error,
                isLoading: false
            };
        case E401_ERROR:
            return {
                ...state,
                error: action.error,
                isLoading: false
            };
        case E404_ERROR:
            return {
                ...state,
                error: action.error,
                isLoading: false
            };
        case E500_ERROR:
            return {
                ...state,
                error: action.error,
                isLoading: false
            };
        default:
            return state;
    }
}