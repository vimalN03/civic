import {
    NEWS_ACTION, NEWS_SUCCESS, NEWS_FAILED, CLEAR_STATE
} from '../actions/news-action';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    newsFeedResponse: {},
    isLoading: true
};

const newsReducer = (state = INIT, action) => {
    switch (action.type) {
        case NEWS_ACTION:
            return { ...state, isLoading: true };
        case NEWS_SUCCESS:
            return { ...state, error: '', newsFeedResponse: action.payload, isLoading: false };
        case NEWS_FAILED:
            return { ...state, error: action.payload, isLoading: false };
        case CLEAR_STATE:
            return { ...state, error: '', newsFeedResponse: {}, isLoading: false };
        default:
            return commonErrorReducer(state, action);
    }
};

export default newsReducer;