import {
    REGISTRATION_AUTH_ACTION, REGISTRATION_AUTH_SUCCESS, REGISTRATION_FAILED, CLEAR_STATE,
    GET_ALL_STATE_LIST_ACTION, GET_ALL_STATE_LIST_SUCCESS, GET_ALL_STATE_LIST_FAILED, GET_ALL_STATE_LIST_CLEAR,
    GET_ALL_WARD_LIST_ACTION, GET_ALL_WARD_LIST_SUCCESS, GET_ALL_WARD_LIST_FAILED, GET_ALL_WARD_LIST_CLEAR,
    GET_LGA_BY_STATE_ID_ACTION, GET_LGA_BY_STATE_ID_SUCCESS, GET_LGA_BY_STATE_ID_FAILED, GET_LGA_BY_STATE_ID_CLEAR,
    GET_WARD_BY_STATE_LGA_ID_ACTION, GET_WARD_BY_STATE_LGA_ID_SUCCESS, GET_WARD_BY_STATE_LGA_ID_FAILED, GET_WARD_BY_STATE_LGA_ID_CLEAR
} from '../actions/registration-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    registrationResponse: {},
    allStateResponse: {},
    allLgaResponse: {},
    allWardResponse: {},
    isLoading: true,
};



const registrationReducer = (state = INIT, action) => {
    switch (action.type) {
        case REGISTRATION_AUTH_ACTION:
            return { ...state, isLoading: false };
        case REGISTRATION_AUTH_SUCCESS:
            return { ...state, error: '', registrationResponse: action.payload, isLoading: true };
        case REGISTRATION_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', registrationResponse: {}, isLoading: true };

        case GET_ALL_STATE_LIST_ACTION:
            return { ...state, isLoading: false };
        case GET_ALL_STATE_LIST_SUCCESS:
            return { ...state, error: '', allStateResponse: action.payload, isLoading: true };
        case GET_ALL_STATE_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case GET_ALL_STATE_LIST_CLEAR:
            return { ...state, error: '', allStateResponse: {}, isLoading: true };

        case GET_ALL_WARD_LIST_ACTION:
            return { ...state, isLoading: false };
        case GET_ALL_WARD_LIST_SUCCESS:
            return { ...state, error: '', allWardResponse: action.payload, isLoading: true };
        case GET_ALL_WARD_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case GET_ALL_WARD_LIST_CLEAR:
            return { ...state, error: '', allWardResponse: {}, isLoading: true };

        case GET_LGA_BY_STATE_ID_ACTION:
            return { ...state, isLoading: false };
        case GET_LGA_BY_STATE_ID_SUCCESS:
            return { ...state, error: '', allLgaResponse: action.payload, isLoading: true };
        case GET_LGA_BY_STATE_ID_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case GET_LGA_BY_STATE_ID_CLEAR:
            return { ...state, error: '', allLgaResponse: {}, isLoading: true };

        case GET_WARD_BY_STATE_LGA_ID_ACTION:
            return { ...state, isLoading: false };
        case GET_WARD_BY_STATE_LGA_ID_SUCCESS:
            return { ...state, error: '', allWardResponse: action.payload, isLoading: true };
        case GET_WARD_BY_STATE_LGA_ID_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case GET_WARD_BY_STATE_LGA_ID_CLEAR:
            return { ...state, error: '', allWardResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default registrationReducer;