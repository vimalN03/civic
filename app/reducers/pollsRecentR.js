import {
    GET_ALL_POLLS_ACTION, GET_ALL_POLLS_SUCCESS, POLLS_LIST_FAILED, POLLS_CLEAR_STATE
    
} from '../actions/polls-action';
import { commonErrorReducer } from './errorR';



const INIT = {
    error: '',
    getResponseData: [],
    isLoading: true,
};



const pollsRecentReducer = (state = INIT, action) => {
    switch (action.type) {
        case GET_ALL_POLLS_ACTION:
            return { ...state, isLoading: true };
        case GET_ALL_POLLS_SUCCESS:
            return { ...state, error: '', getResponseData: action.payload, isLoading: true };
        case POLLS_LIST_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case POLLS_CLEAR_STATE:
            return { ...state, error: '', getResponseData: [], isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default pollsRecentReducer;