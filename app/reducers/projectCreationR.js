import {
    PROJECT_CREATION_ACTION, PROJECT_CREATION_SUCCESS, PROJECT_CREATION_FAILED, CLEAR_STATE
} from '../actions/projectCreation-actions';
import { commonErrorReducer } from './errorR';

const INIT = {
    error: '',
    projectCreationResponse: {},
    isLoading: true
};

const projectCreationReducer = (state = INIT, action) => {
    switch (action.type) {
        case PROJECT_CREATION_ACTION:
            return { ...state, isLoading: false };
        case PROJECT_CREATION_SUCCESS:
            return { ...state, error: '', projectCreationResponse: action.payload, isLoading: true };
        case PROJECT_CREATION_FAILED:
            return { ...state, error: action.payload, isLoading: true };
        case CLEAR_STATE:
            return { ...state, error: '', projectCreationResponse: {}, isLoading: true };
        default:
            return commonErrorReducer(state, action);
    }
};

export default projectCreationReducer;