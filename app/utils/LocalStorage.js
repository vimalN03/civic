import { AsyncStorage } from 'react-native';

/**
 * Deals with the local storage of Notes into AsyncStorage
 *
 * @class LocalStorage
 */
class LocalStorage {
    /**
     * Get a single item
     *
     * @param {string} key
     * @memberof LocalStorage
     */
    async getItem(key) {
        var value = await AsyncStorage.getItem(key);
        console.log('------------------------------value', value);
        return JSON.parse(value);
        // .then((json) => {
        //     console.log('------------------------------value', json);
        //     return JSON.parse(json);
        // }).catch((error) => {
        //     console.log(error);
        //     return '';
        // });
    }

    /**
     * Save a single item
     *
     * @param key
     * @param value
     * @memberof LocalStorage
     */
    async setItem(key, value) {
        return AsyncStorage.setItem(key, JSON.stringify(value));
    }

    /**
     * Deletes a single item
     *
     * @memberof LocalStorage
     */
    async deleteItem(key) {
        return AsyncStorage.removeItem(key);
    }

    /**
     * Get all the items
     *
     * @memberof LocalStorage
     */
    async getAllItems() {
        return AsyncStorage.getAllKeys()
            .then((keys) => {
                const fetchKeys = keys.filter((k) => { return k });
                return AsyncStorage.multiGet(fetchKeys);
            })
            .then((result) => {
                return result.map((r) => { return JSON.parse(r[1]) });
            });
    }
};

const localStorage = new LocalStorage();
export default localStorage;