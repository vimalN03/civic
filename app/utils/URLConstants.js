import { Platform, NetInfo } from 'react-native';
import { wp } from '../resources';
import LocalStorage from '../utils/LocalStorage';
import _ from 'lodash';

export var BASE_URL = {
  // ISPRODUCTION = false;
  BARCODE_APP_URL: "http://colanapps.in",

  // production
  // APP_URL: 'http://colanapps.in',

  // development
  APP_URL: "http://colanapps.in",

  API_VERSION: "/api/v1/",

  //Image url
  IMAGE_URL: 'http://colanapps.in/civic/public/'

}


export var CONSTANT_URL = {
  LOGIN_URL: BASE_URL.APP_URL + "/civic/public/api/users/login",
  REGISTRATION_URL: BASE_URL.APP_URL + "/civic/public/api/users/register",
  GET_ALL_STATE_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/states",
  GET_LGA_BY_STATE_ID_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/localGovt",
  GET_WARD_BY_STATE_ID_LGA_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/ward",
  GET_ALL_LGA_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/all/localGovt",
  GET_ALL_WARD_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/all/ward",
  POST_FORGOT_PASSWORD_URL: BASE_URL.APP_URL + "/civic/public/api/users/forgot/password",
  POST_LOGOUT_URL: BASE_URL.APP_URL + "/civic/public/api/users/logout",
  // OTP_SEND_URL: BASE_URL.APP_URL + "/civic/public/api/send/otp",
  OTP_SEND_URL: BASE_URL.APP_URL + "/civic/public/api/get/otp",
  OTP_VERIFY_URL: BASE_URL.APP_URL + "/civic/public/api/verify/otp",
  CHANGE_USER_TYPE: BASE_URL.APP_URL+ "/civic/public/api/users/change/user/type",

  GET_POLITICIANS_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/politicians",
  GET_POLITICIANS_DETAIL_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/politician/detail",
  GET_POLITICIANS_DETAIL_RATING_URL: BASE_URL.APP_URL + "/civic/public/api/users/post/politician/ratings",
  POST_POLITICIANS_FOLLOW_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/follow",
  GET_FOLLOWER_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/followers/list",

  //GET_PROJECT_LIST_URL: BASE_URL.APP_URL + "/civic/public/api/users/project/index",
  POST_PROJECT_DETAIL_URL: BASE_URL.APP_URL + "/civic/public/api/users/project/view",
  POST_PROJECT_CREATION_URL: BASE_URL.APP_URL + "/civic/public/api/users/project/store",
  POST_PROJECT_POLITICIAN_LIST_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/politician/project",
  GET_MY_PROJECT_LIST_URL: BASE_URL.APP_URL + "/civic/public/api/users/view/posted/project",
  POST_MY_PROJECT_UPDATE_URL: BASE_URL.APP_URL + "/civic/public/api/users/project/update",
  POST_MY_PROJECT_DELETE_URL: BASE_URL.APP_URL + "/civic/public/api/users/project/delete",

  POST_CONTRIBUTION_LIST_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/politician/contribution",
  POST_CONTRIBUTION_DETAIL_URL: BASE_URL.APP_URL + "/civic/public/api/users/contribution/view",
  GET_MY_CONTRIBUTION_LIST_URL: BASE_URL.APP_URL + "/civic/public/api/users/view/posted/contribution",
  POST_MY_CONTRIBUTION_UPDATE_URL: BASE_URL.APP_URL + "/civic/public/api/users/contribution/update",
  POST_MY_CONTRIBUTION_DELETE_URL: BASE_URL.APP_URL + "/civic/public/api/users/contribution/delete",
  POST_MY_CONTRIBUTION_CREATION_URL: BASE_URL.APP_URL + "/civic/public/api/users/contribution/store",

  POST_BILL_LIST_URL: BASE_URL.APP_URL + "/civic/public/api/users/get/politician/bills",
  POST_BILL_DETAIL_URL: BASE_URL.APP_URL + "/civic/public/api/users/bills/view",
  POST_BILL_DETAIL_DELETE_URL: BASE_URL.APP_URL + "/civic/public/api/users/bill/delete",
  POST_BILL_DETAIL_UPDATE_URL: BASE_URL.APP_URL + "/civic/public/api/users/bill/update",
  POST_BILL_CREATION_URL: BASE_URL.APP_URL + "/civic/public/api/users/bills/store",

  GET_OFFICIAL_LIST: BASE_URL.APP_URL + "/civic/public/api/users/get/officialslist",
  GET_OFFICIAL_PROFILE: BASE_URL.APP_URL + "/civic/public/api/users/get/profile",
  GET_ISSUE_IDEAS_API: BASE_URL.APP_URL + "/civic/public/api/users/get/issues",
  GET_ISSUE_DETAILS_API: BASE_URL.APP_URL + "/civic/public/api/users/get/issue/detail",
  POST_ISSUE_COMMENT_API: BASE_URL.APP_URL + "/civic/public/api/users/post/issue/comment",
  CREATE_ISSUE_API: BASE_URL.APP_URL + "/civic/public/api/users/post/issue/detail",
  RATING_IDEA_API: BASE_URL.APP_URL + "/civic/public/api/users/post/issue/ratings",

  POLLS_INDEX_API: BASE_URL.APP_URL + "/civic/public/api/users/polls/index",
  POLLS_COMPLETED_API: BASE_URL.APP_URL + "/civic/public/api/users/completed/polls",
  POLLS_DETAILS_API: BASE_URL.APP_URL + "/civic/public/api/users/polls/view",
  POLLS_CREATE_API: BASE_URL.APP_URL + "/civic/public/api/users/polls/store",
  POLLS_QUESTION_SUBMIT_API: BASE_URL.APP_URL + "/civic/public/api/users/voting/polls",

  POST_WALLET_API: BASE_URL.APP_URL + "/civic/public/api/users/wallet/payment",

  GET_HOME_API: BASE_URL.APP_URL + "/civic/public/api/users/recent/posts",

  GET_FORUM_API: BASE_URL.APP_URL + "/civic/public/api/users/all/forum/topics",
  POST_FORUM_DETAIL_API: BASE_URL.APP_URL + "/civic/public/api/users/view/forum",
  POST_FORUM_CREATION_API: BASE_URL.APP_URL + "/civic/public/api/users/create/forum/topic",
  POST_FORUM_CHANGE_STATUS_API: BASE_URL.APP_URL + "/civic/public/api/users/change/forum/status",
  POST_FORUM_REPLY: BASE_URL.APP_URL+ "/civic/public/api/users/send/forum/reply",

  CHAT_LIST_ALL_API: BASE_URL.APP_URL + "/civic/public/api/users/get/all/politicians",
  CHAT_LIST_API: BASE_URL.APP_URL + "/civic/public/api/users/get/chat/list",
  CHAT_CONVERSATION_API: BASE_URL.APP_URL + "/civic/public/api/users/view/conversation",
  CHAT_POST_MSG_API: BASE_URL.APP_URL + "/civic/public/api/users/send/message",
  CHAT_CHOOSE_MEMBER_API: BASE_URL.APP_URL + "/civic/public/api/users/add/chat/member",
  
  PROFILE_EDIT_API: BASE_URL.APP_URL + "/civic/public/api/users/edit/profile",
  GOOGLE_API: BASE_URL.APP_URL + "/civic/public/api/users/google/login"
}

export const header = {
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'Authorization': 'Basic Y2l2aWNfcHJvamVjdDozNjRhNDRjZmU5ZjhkNDYxYjA4YjRiOTk0ZDlkNTg1NDAzOWViYmQ3'
};

export const headerToken = (token) => ({
  'Accept': 'application/json',
  'Content-Type': 'application/json',
  'Authorization': 'Bearer ' + token
});

export const multiPartHeader = {
  'Content-Type': 'multipart/form-data',
  'Authorization': 'Basic Y2l2aWNfcHJvamVjdDozNjRhNDRjZmU5ZjhkNDYxYjA4YjRiOTk0ZDlkNTg1NDAzOWViYmQ3'
};

export const multiPartHeaderToken = (token) => ({
  'Content-Type': 'multipart/form-data',
  'Authorization': 'Bearer ' + token
})

export function getHeaderKey() {
  return new Promise(async function (resolve, reject) {
    var data = await LocalStorage.getItem(consts.USER_LOGIN)
    resolve(data)
  })
}

export function getProfile() {
  return new Promise(async function (resolve, reject) {
    var data = await LocalStorage.getItem(consts.GET_PROFILE)
    resolve(data)
  })
}

export function getDataList(url) {
  return new Promise(async function (resolve, reject) {
    var data = await LocalStorage.getItem(url) //consts.USER_LOGIN
    resolve(data)
  })
}

export var consts = {
  USER_LOGIN: 'user_login',
  GET_PROFILE: 'GET_PROFILE',
  STATE_LIST: 'STATE_LIST'
}

export const getHeaderTitle = () => {
  if (Platform.OS === 'ios') {
    return {}
  } else {
    return { marginStart: wp('10%') }
  }
}

export const getHeaderMainTitle = () => {
  if (Platform.OS === 'ios') {
    return {}
  } else {
    return { marginStart: wp('10%'), flex: 2 }
  }
}

export const getNetInfo = () => (
  NetInfo.isConnected.fetch().then(isConnected => {
    console.log('First, is ' + isConnected);
    return isConnected;
  })
)

export const getUserProfileDummyUrl = (imageUrl) => {
  if (imageUrl == null || imageUrl === '' || imageUrl == undefined) {
    if (getNetInfo()) {
      return { uri: 'http://colanapps.in/civic/public/images/profile/default.jpg' }
    } else {
      return require('../resources/images/Profile/avatar1.png')
    }
  } else {
    return { uri: BASE_URL.IMAGE_URL + imageUrl }
  }
}

export const getBillsDummyUrl = (imageUrl) => {
  var extension = imageUrl.split('.').pop();
  if (imageUrl == null || imageUrl === '' || imageUrl == undefined) {
    if (extension.toUpperCase() === 'PDF') {
      return require('../resources/images/Profile/pdf.png')
    }
  } else {
      if (extension.toUpperCase() === 'PDF') {
        return require('../resources/images/Profile/pdf.png')
      } else {
        return { uri: BASE_URL.IMAGE_URL + imageUrl }
      }
  }
}

export const getErrorMessage = (type) => {
  return {
    type: type,
    // payload: { status: 'error', message: 'Check Network Connection!' }
  }
}

export const getImageDummyUrl = (imageUrl) => {
  if (imageUrl == null || imageUrl === '' || imageUrl == undefined) {
    return require('../resources/images/Profile/avatar1.png');
  } else {
    return { uri: imageUrl }
  }
}