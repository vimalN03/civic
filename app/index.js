import React, { Component } from 'react';
import { createStore, applyMiddleware } from 'redux';
import { Root } from 'native-base';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';

import reducers from './reducers';
import AppNavigator from '../app/config/routes';

console.disableYellowBox = true;

export default class App extends Component {
    render() {
        return (
            <Provider store={createStore(reducers, applyMiddleware(thunk))}>
                <Root>
                    <AppNavigator />
                </Root>
            </Provider>
        );
    }
}
