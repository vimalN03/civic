import axios from 'axios';
import { CONSTANT_URL, headerToken, getErrorMessage } from '../utils/URLConstants';

export const BILL_DETAIL_ACTION = "BILL_DETAIL_ACTION";
export const BILL_DETAIL_SUCCESS = "BILL_DETAIL_SUCCESS";
export const BILL_DETAIL_FAILED = 'BILL_DETAIL_FAILED';

export const BILL_DETAIL_DELETE_ACTION = "BILL_DETAIL_DELETE_ACTION";
export const BILL_DETAIL_DELETE_SUCCESS = "BILL_DETAIL_DELETE_SUCCESS";
export const BILL_DETAIL_DELETE_FAILED = 'BILL_DETAIL_DELETE_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getBillDetailAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: BILL_DETAIL_ACTION });
        axios.post(CONSTANT_URL.POST_BILL_DETAIL_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getBillDetailAction', res)
                dispatch({
                    type: BILL_DETAIL_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(BILL_DETAIL_FAILED));
                console.log('error', error.response)
            });
    };
};

export const getBillDetailDeleteAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: BILL_DETAIL_DELETE_ACTION });
        axios.post(CONSTANT_URL.POST_BILL_DETAIL_DELETE_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getBillDetailDeleteAction', res)
                dispatch({
                    type: BILL_DETAIL_DELETE_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(BILL_DETAIL_DELETE_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}