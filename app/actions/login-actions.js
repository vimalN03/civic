import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { header, CONSTANT_URL, consts,getErrorMessage, headerToken } from '../utils/URLConstants';
//import LocalStorage from '../utils/LocalStorage';

export const LOGIN_AUTH_ACTION = "LOGIN_AUTH_ACTION";
export const LOGIN_AUTH_SUCCESS = "LOGIN_AUTH_SUCCESS";
export const LOGIN_FAILED = 'LOGIN_FAILED';
export const LOGGED_IN = 'LOGGED_IN';
export const ON_LOGOUT = 'ON_LOGOUT';
export const CLEAR_STATE = 'CLEAR_STATE';

export const LOGOUT_AUTH_ACTION = "LOGOUT_AUTH_ACTION";
export const LOGOUT_AUTH_SUCCESS = "LOGOUT_AUTH_SUCCESS";
export const LOGOUT_FAILED = 'LOGOUT_FAILED';

export const getLoginAuthAction = (user) => {
    return (dispatch) => {
        dispatch({ type: LOGIN_AUTH_ACTION });
        axios.post(CONSTANT_URL.LOGIN_URL, JSON.stringify(user), { headers: header })
            .then((res) => {
                console.log('logginRespAction', res)
                if (res.data.status !== 'error') {
                    AsyncStorage.setItem(consts.USER_LOGIN, JSON.stringify(res.data))
                }
                dispatch({
                    type: LOGIN_AUTH_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(LOGIN_FAILED));
                console.log('error', error.response)
            });
    };
};

export const LoggedIn = () => {
    return {
        type: LOGGED_IN,
        payload: ''
    }
}
export const onLogout = () => {
    AsyncStorage.removeItem(consts.USER_LOGIN);
    return {
        type: ON_LOGOUT,
        payload: ''
    }
}

export const onClearLoginState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}

export const getLogoutAction = (headerKey) => {
    return (dispatch) => {
        dispatch({ type: LOGOUT_AUTH_ACTION });
        console.log('getLogoutAction', headerKey)
        axios.post(CONSTANT_URL.POST_LOGOUT_URL, '', { headers: headerToken(headerKey) })
            .then((res) => {
                console.log('getLogoutAction', res)
                AsyncStorage.removeItem(consts.USER_LOGIN);
                AsyncStorage.removeItem(consts.GET_PROFILE);                
                dispatch({
                    type: LOGOUT_AUTH_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(LOGOUT_FAILED));
                console.log('error', error.response)
            });
    };
};