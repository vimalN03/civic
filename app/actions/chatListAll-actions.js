import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const CHAT_LIST_ALL_ACTION = "CHAT_LIST_ALL_ACTION";
export const CHAT_LIST_ALL_SUCCESS = "CHAT_LIST_ALL_SUCCESS";
export const CHAT_LIST_ALL_FAILED = 'CHAT_LIST_ALL_FAILED';
export const CHAT_LIST_ALL_CLEAR_STATE = 'CHAT_LIST_ALL_CLEAR_STATE';

export const getChatListAllAction = (token) => {
    
    return (dispatch) => {
        dispatch({ type: CHAT_LIST_ALL_ACTION });
        axios.get(CONSTANT_URL.CHAT_LIST_ALL_API, { headers: headerToken(token) })
        .then((res) => {
            console.log('getChatAllAction', res)
            dispatch({
                type: CHAT_LIST_ALL_SUCCESS,
                payload: res.data
            });
        })
        .catch((error) => {
            dispatch(getErrorMessage(CHAT_LIST_ALL_FAILED));
            console.log('error', error.response)
        });
    };
};

export const onChatAllClearState = () => {
    return {
        type: CHAT_LIST_ALL_CLEAR_STATE,
        payload: ''
    }
}
