import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const GET_ALL_IDEA_ACTION = 'GET_ALL_IDEA_ACTION';
export const GET_ALL_IDEA_SUCCESS = 'GET_ALL_IDEA_SUCCESS';
export const IDEA_LIST_FAILED = 'IDEA_LIST_FAILED';
export const IDEA_CLEAR_STATE = 'IDEA_CLEAR_STATE';

export const POST_RATING_IDEAS_ACTION = 'POST_RATING_IDEAS_ACTION';
export const GET_ALL_RATING_IDEAS_SUCCESS = 'GET_ALL_RATING_IDEAS_SUCCESS';
export const RATING_IDEAS_LIST_FAILED = 'RATING_IDEAS_LIST_FAILED';
export const RATING_IDEAS_CLEAR_STATE = 'RATING_IDEAS_CLEAR_STATE';

export const ideaDetailsApi = (request ,token) => {
    return (dispatch) => {
        dispatch({ type: GET_ALL_IDEA_ACTION });
        axios.post(CONSTANT_URL.GET_ISSUE_DETAILS_API, JSON.stringify(request), { headers: headerToken(token)})
            .then((res) => {
                console.log('ideaApiRes', res.data)
                dispatch({
                    type: GET_ALL_IDEA_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(IDEA_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearIssueIdeas = () => {
    return {
        type: IDEA_CLEAR_STATE,
        payload: ''
    }
}


export const postRatingIdeaApi = (request ,token) => {
    return (dispatch) => {
        dispatch({ type: POST_RATING_IDEAS_ACTION });
        axios.post(CONSTANT_URL.RATING_IDEA_API,  JSON.stringify(request),{ headers: headerToken(token)})
            .then((res) => {
                console.log('ratingIdeasApiRes', res.data)
                dispatch({
                    type: GET_ALL_RATING_IDEAS_SUCCESS,
                    payload: res.data
                });
                
            })
            .catch((error) => {
                dispatch(getErrorMessage(RATING_IDEAS_LIST_FAILED));
                console.log('raterror', error.response)
            });
    };
};

export const onClearRatingIdeas = () =>{
    return{
        type: RATING_IDEAS_CLEAR_STATE,
        payload: ''
    }
}