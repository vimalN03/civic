import axios from 'axios';
import { CONSTANT_URL, headerToken, getErrorMessage } from '../utils/URLConstants';

export const BILL_LIST_ACTION = "BILL_LIST_ACTION";
export const BILL_LIST_SUCCESS = "BILL_LIST_SUCCESS";
export const BILL_LIST_FAILED = 'BILL_LIST_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getBillListAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: BILL_LIST_ACTION });
        axios.post(CONSTANT_URL.POST_BILL_LIST_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getBillListAction', res)
                dispatch({
                    type: BILL_LIST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(BILL_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}