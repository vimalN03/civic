import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const CHAT_MSG_POST_ACTION = "CHAT_MSG_POST_ACTION";
export const CHAT_MSG_POST_SUCCESS = "CHAT_MSG_POST_SUCCESS";
export const CHAT_MSG_POST_FAILED = 'CHAT_MSG_POST_FAILED';
export const CHAT_MSG_POST_CLEAR_STATE = 'CHAT_MSG_POST_CLEAR_STATE';

export const getChatPostMsgAction = (token, data) => {
    return (dispatch) => {
        console.log('====123',CONSTANT_URL.CHAT_POST_MSG_API, JSON.stringify(data), { headers: headerToken(token) })
        dispatch({ type: CHAT_MSG_POST_ACTION });
        axios.post(CONSTANT_URL.CHAT_POST_MSG_API, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getChatPostAction', res)
                dispatch({
                    type: CHAT_MSG_POST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                // dispatch(getErrorMessage(CHAT_MSG_POST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onChatPostMsgClearState = () => {
    return {
        type: CHAT_MSG_POST_CLEAR_STATE,
        payload: ''
    }
}
