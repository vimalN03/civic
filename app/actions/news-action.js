import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const NEWS_ACTION = 'NEWS_ACTION';
export const NEWS_SUCCESS = 'NEWS_SUCCESS';
export const NEWS_FAILED = 'NEWS_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getNewsAction = () => {
    return (dispatch) => {
        dispatch({ type: NEWS_ACTION });
        axios.get('https://newsapi.org/v2/everything?q=nigeria&politics&apiKey=039083353f864b999a3e75428e937044')
            .then((res) => {
                console.log('getNEWSAction', res)
                dispatch({
                    type: NEWS_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(NEWS_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}