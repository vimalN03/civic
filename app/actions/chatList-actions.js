import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const CHAT_LIST_ACTION = "CHAT_LIST_ACTION";
export const CHAT_LIST_SUCCESS = "CHAT_LIST_SUCCESS";
export const CHAT_LIST_FAILED = 'CHAT_LIST_FAILED';
export const CHAT_LIST_CLEAR_STATE = 'CHAT_LIST_CLEAR_STATE';

export const getChatListAction = (token) => {
    return (dispatch) => {
        dispatch({ type: CHAT_LIST_ACTION });
        axios.get(CONSTANT_URL.CHAT_LIST_API, { headers: headerToken(token) })
        .then((res) => {
            console.log('getChatPostAction', res)
            dispatch({
                type: CHAT_LIST_SUCCESS,
                payload: res.data
            });
        })
        .catch((error) => {
            dispatch(getErrorMessage(CHAT_LIST_FAILED));
            console.log('error', error.response)
        });
    };
};

export const onChatClearState = () => {
    return {
        type: CHAT_LIST_CLEAR_STATE,
        payload: ''
    }
}
