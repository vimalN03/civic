import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const GET_ALL_OFFICIAL_LIST_ACTION = 'GET_ALL_OFFICIAL_LIST_ACTION';
export const GET_ALL_OFFICIAL_LIST_SUCCESS = 'GET_ALL_OFFICIAL_LIST_SUCCESS';
export const OFFICIAL_LIST_FAILED = 'OFFICIAL_LIST_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getOfficialListAction = (token) => {
    return (dispatch) => {
        dispatch({ type: GET_ALL_OFFICIAL_LIST_ACTION });
        axios.get(CONSTANT_URL.GET_OFFICIAL_LIST, { headers: headerToken(token) })
            .then((res) => {
                
                dispatch({
                    type: GET_ALL_OFFICIAL_LIST_SUCCESS,
                    payload: res.data
                });
                console.log('getOfficialListAction', res.data)
            })
            .catch((error) => {
                dispatch(getErrorMessage(OFFICIAL_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearElectedOfficial = () =>{
    return{
        type: CLEAR_STATE,
        payload: ''
    }
}