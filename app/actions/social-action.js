import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { header, CONSTANT_URL, getErrorMessage, consts, headerToken } from '../utils/URLConstants';

export const GOOGLE_ACTION = "GOOGLE_ACTION";
export const GOOGLE_SUCCESS = "GOOGLE_SUCCESS";
export const GOOGLE_FAILED = 'GOOGLE_FAILED';

export const FB_ACTION = "FB_ACTION";
export const FB_SUCCESS = "FB_SUCCESS";
export const FB_FAILED = 'FB_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getGoogleAction = (data) => {
    // console.log("google token===>",token);
    console.log("google data====>",data);
    return (dispatch) => {
        dispatch({ type: GOOGLE_ACTION });
        axios.post(CONSTANT_URL.GOOGLE_API, JSON.stringify(data), { headers: header })
            .then((res) => {
                console.log('getgoogleAction', res)
                dispatch({
                    type: GOOGLE_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(GOOGLE_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}

export const getFbAction = (data) => {
    // console.log("google token===>",token);
    console.log("Fb data====>",data);
    return (dispatch) => {
        dispatch({ type: FB_ACTION });
        axios.post(CONSTANT_URL.GOOGLE_API, JSON.stringify(data), { headers: header })
            .then((res) => {
                console.log('fb', res)
                if (res.data.status !== 'error') {
                    AsyncStorage.setItem(consts.USER_LOGIN, JSON.stringify(res.data))
                }
                dispatch({
                    type: FB_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(FB_FAILED));
                console.log('error', error.response)
            });
    };
};
