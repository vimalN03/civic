import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const POLLS_QUESTIONS_ACTION = 'POLLS_QUESTIONS_ACTION';
export const POLLS_QUESTIONS_SUCCESS = 'POLLS_QUESTIONS_SUCCESS';
export const POLLS_QUESTIONS_FAILED = 'POLLS_QUESTIONS_FAILED';
export const POLLS_QUESTIONS_STATE = 'POLLS_QUESTIONS_STATE';

export const pollsQuestionAction = (req,token) => {
    return (dispatch) => {
        dispatch({ type: POLLS_QUESTIONS_ACTION });
        console.log(CONSTANT_URL.POLLS_QUESTION_SUBMIT_API, JSON.stringify(req), { headers: headerToken(token) })
        axios.post(CONSTANT_URL.POLLS_QUESTION_SUBMIT_API, JSON.stringify(req), { headers: headerToken(token) })
            .then((res) => {
                dispatch({
                    type: POLLS_QUESTIONS_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(POLLS_QUESTIONS_FAILED));
                console.log('pollserror', error.response)
            });
    };
};

export const onClearPollsQuestions = () =>{
    return{
        type: POLLS_QUESTIONS_STATE,
        payload: ''
    }
}