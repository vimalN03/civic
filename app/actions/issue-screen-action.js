import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const GET_ALL_ISSUE_ACTION = 'GET_ALL_ISSUE_ACTION';
export const GET_ALL_ISSUE_SUCCESS = 'GET_ALL_ISSUE_SUCCESS';
export const ISSUE_LIST_FAILED = 'ISSUE_LIST_FAILED';
export const ISSUE_CLEAR_STATE = 'ISSUE_CLEAR_STATE';


export const issueDetailsApi = (user ,token) => {
    return (dispatch) => {
        dispatch({ type: GET_ALL_ISSUE_ACTION });
        axios.post(CONSTANT_URL.GET_ISSUE_DETAILS_API, JSON.stringify(user), { headers: headerToken(token)})
            .then((res) => {
                console.log('issueApiRes', res.data)
                dispatch({
                    type: GET_ALL_ISSUE_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(ISSUE_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearIssueIdeas = () => {
    return {
        type: ISSUE_CLEAR_STATE,
        payload: ''
    }
}