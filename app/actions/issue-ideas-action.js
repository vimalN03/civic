import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const GET_ALL_ISSUEIDEAS_ACTION = 'GET_ALL_ISSUEIDEAS_ACTION';
export const GET_ALL_ISSUEIDEAS_SUCCESS = 'GET_ALL_ISSUEIDEAS_SUCCESS';
export const ISSUEIDEAS_LIST_FAILED = 'ISSUEIDEAS_LIST_FAILED';
export const ISSUEIDEAS_CLEAR_STATE = 'ISSUEIDEAS_CLEAR_STATE';

export const getIssueIdeasListAction = (token) => {
    return (dispatch) => {
        dispatch({ type: GET_ALL_ISSUEIDEAS_ACTION });
        axios.get(CONSTANT_URL.GET_ISSUE_IDEAS_API, { headers: headerToken(token) })
            .then((res) => {
                
                dispatch({
                    type: GET_ALL_ISSUEIDEAS_SUCCESS,
                    payload: res.data
                });
                console.log('issueIdeasApiRes', res.data)
            })
            .catch((error) => {
                dispatch(getErrorMessage(ISSUEIDEAS_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearIssueIdeas = () =>{
    return{
        type: ISSUEIDEAS_CLEAR_STATE,
        payload: ''
    }
}