import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const GET_POLLS_COMPLETED_ACTION = 'GET_POLLS_COMPLETED_ACTION';
export const GET_POLLS_COMPLETED_SUCCESS = 'GET_POLLS_COMPLETED_SUCCESS';
export const POLLS_COMPLETED_LIST_FAILED = 'POLLS_LIST_FAILED';
export const POLLS_CLEAR_STATE = 'POLLS_CLEAR_STATE';

export const getpollsCompletedAction = (token) => {
    return (dispatch) => {
        dispatch({ type: GET_POLLS_COMPLETED_ACTION });
        axios.get(CONSTANT_URL.POLLS_COMPLETED_API, { headers: headerToken(token) })
            .then((res) => {
                dispatch({
                    type: GET_POLLS_COMPLETED_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(POLLS_LIST_FAILED));
                console.log('cerror', error.response)
            });
    };
};

export const onClearIssueIdeas = () =>{
    return{
        type: POLLS_CLEAR_STATE,
        payload: ''
    }
}