import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken, consts } from '../utils/URLConstants';

export const GET_OFFICIAL_PROFILE_ACTION = 'GET_OFFICIAL_PROFILE_ACTION';
export const GET_OFFICIAL_PROFILE_SUCCESS = 'GET_OFFICIAL_PROFILE_SUCCESS';
export const OFFICIAL_PROFILE_LIST_FAILED = 'OFFICIAL_LIST_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getOfficialProfileAction = (token, user) => {
   console.log("token123==>",token)
   console.log("user==>",user)
    return (dispatch) => {
        dispatch({ type: GET_OFFICIAL_PROFILE_ACTION });
        axios.post(CONSTANT_URL.GET_OFFICIAL_PROFILE, JSON.stringify(user), { headers: headerToken(token) })
            .then((res) => {
                console.log("res 123===>",res)
                AsyncStorage.setItem(consts.GET_PROFILE, JSON.stringify(res.data))
                
                dispatch({
                    type: GET_OFFICIAL_PROFILE_SUCCESS,
                    payload: res.data
                });
                
            })
            .catch((error) => {
                console.log("error 123===>",error)
                dispatch(getErrorMessage(OFFICIAL_PROFILE_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearElectedOfficial = () =>{
    return{
        type: CLEAR_STATE,
        payload: ''
    }
}