import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const WALLET_ACTION = "WALLET_ACTION";
export const WALLET_SUCCESS = "WALLET_SUCCESS";
export const WALLET_FAILED = 'WALLET_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getWalletAction = (token, data) => {
    console.log("wallet token===>",token);
    console.log("wallet data====>",data);
    return (dispatch) => {
        dispatch({ type: WALLET_ACTION });
        axios.post(CONSTANT_URL.POST_WALLET_API, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getWalletAction', res)
                dispatch({
                    type: WALLET_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(WALLET_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}
