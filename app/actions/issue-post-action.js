import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const GET_POST_ISSUE_ACTION = 'GET_POST_ISSUE_ACTION';
export const GET_POST_ISSUE_SUCCESS = 'GET_POST_ISSUE_SUCCESS';
export const POST_ISSUE_LIST_FAILED = 'POST_ISSUE_LIST_FAILED';
export const POST_ISSUE_CLEAR_STATE = 'POST_ISSUE_CLEAR_STATE';

export const issuePostApi = (post ,token) => {
    return (dispatch) => {
        dispatch({ type: GET_POST_ISSUE_ACTION });
        axios.post(CONSTANT_URL.POST_ISSUE_COMMENT_API, JSON.stringify(post), { headers: headerToken(token)})
            .then((res) => {
                console.log('issuePostRes', res.data)
                dispatch({
                    type: GET_POST_ISSUE_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(POST_ISSUE_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearIssue = () => {
    return {
        type: POST_ISSUE_CLEAR_STATE,
        payload: ''
    }
}