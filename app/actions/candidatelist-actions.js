import axios from 'axios';
import { CONSTANT_URL, headerToken ,getErrorMessage} from '../utils/URLConstants';

export const CANDIDATE_LIST_ACTION = "CANDIDATE_LIST_ACTION";
export const CANDIDATE_LIST_SUCCESS = "CANDIDATE_LIST_SUCCESS";
export const CANDIDATE_LIST_FAILED = 'CANDIDATE_LIST_FAILED';

export const CANDIDATE_FOLLOW_ACTION = "CANDIDATE_FOLLOW_ACTION";
export const CANDIDATE_FOLLOW_SUCCESS = "CANDIDATE_FOLLOW_SUCCESS";
export const CANDIDATE_FOLLOW_FAILED = 'CANDIDATE_FOLLOW_FAILED';

export const FOLLOWER_LIST_ACTION = "FOLLOWER_LIST_ACTION";
export const FOLLOWER_LIST_SUCCESS = "FOLLOWER_LIST_SUCCESS";
export const FOLLOWER_LIST_FAILED = 'FOLLOWER_LIST_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getCandidateListAction = (token) => {
    return (dispatch) => {
        dispatch({ type: CANDIDATE_LIST_ACTION });
        axios.get(CONSTANT_URL.GET_POLITICIANS_URL, { headers: headerToken(token) })
            .then((res) => {
                console.log('getCandidateListAction', res)
                dispatch({
                    type: CANDIDATE_LIST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CANDIDATE_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const getCandidateFollowAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: CANDIDATE_FOLLOW_ACTION });
        axios.post(CONSTANT_URL.POST_POLITICIANS_FOLLOW_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getCandidateFollowAction', res)
                dispatch({
                    type: CANDIDATE_FOLLOW_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CANDIDATE_FOLLOW_FAILED));
                console.log('error', error.response)
            });
    };
};

export const getFollowerListAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: FOLLOWER_LIST_ACTION });
        axios.post(CONSTANT_URL.GET_FOLLOWER_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getFollowerListAction', res)
                dispatch({
                    type: FOLLOWER_LIST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(FOLLOWER_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}