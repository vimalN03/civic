import { CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const PROJECT_CREATION_ACTION = "PROJECT_CREATION_ACTION";
export const PROJECT_CREATION_SUCCESS = "PROJECT_CREATION_SUCCESS";
export const PROJECT_CREATION_FAILED = 'PROJECT_CREATION_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getProjectCreationAction = (token, formData, isUpdate) => {
    console.log(formData, token, isUpdate);
    return (dispatch) => {
        dispatch({ type: PROJECT_CREATION_ACTION });
        let url = (isUpdate) ? CONSTANT_URL.POST_MY_PROJECT_UPDATE_URL : CONSTANT_URL.POST_PROJECT_CREATION_URL
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            },
            body: formData
        }).then(response => response.json())
            .then(response => {
                console.log("123", response);
                dispatch({
                    type: PROJECT_CREATION_SUCCESS,
                    payload: response
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(PROJECT_CREATION_FAILED));
                console.log('error', error);
            });
    }
}

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}
