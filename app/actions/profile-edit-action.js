import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { header, CONSTANT_URL, consts, headerToken, multiPartHeaderToken, getErrorMessage } from '../utils/URLConstants';

export const PROFILE_EDIT_ISSUE_ACTION = 'PROFILE_EDIT_ISSUE_ACTION';
export const PROFILE_EDIT_ISSUE_SUCCESS = 'PROFILE_EDIT_ISSUE_SUCCESS';
export const PROFILE_EDIT_LIST_FAILED = 'PROFILE_EDIT_LIST_FAILED';
export const PROFILE_EDIT_CLEAR_STATE = 'PROFILE_EDIT_CLEAR_STATE';

export const GET_LGA_STATE_ID_ACTION = 'GET_LGA_STATE_ID_ACTION';
export const GET_LGA_STATE_ID_SUCCESS = 'GET_LGA_STATE_ID_SUCCESS';
export const GET_LGA_STATE_ID_FAILED = 'GET_LGA_STATE_ID_FAILED';
export const GET_LGA_STATE_ID_CLEAR = 'GET_LGA_STATE_ID_CLEAR';

export const USER_PROFILE_ACTION = 'USER_PROFILE_ACTION';
export const USER_PROFILE_SUCCESS = 'USER_PROFILE_SUCCESS';
export const USER_PROFILE_LIST_FAILED = 'USER_PROFILE_LIST_FAILED';
export const USER_CLEAR_STATE = 'USER_CLEAR_STATE';


export const profileUpdateApi = (formData ,token) => {
    return (dispatch) => {
        
        dispatch({ type: PROFILE_EDIT_ISSUE_ACTION });

        fetch(CONSTANT_URL.PROFILE_EDIT_API, {
            method: 'POST',
            headers: multiPartHeaderToken(token),
            body: formData
        }).then(response => response.json())
        .then(response => {
            console.log('editRes', response)
            dispatch({
                type: PROFILE_EDIT_ISSUE_SUCCESS,
                payload: response
            });
        })
        .catch((error) => {
            dispatch({
                type: PROFILE_EDIT_LIST_FAILED,
                payload: ''
            });
            console.log('error', error);
        });
    };
    // return (dispatch) => {
    //     dispatch({ type: PROFILE_EDIT_ISSUE_ACTION });
    //     axios.post(CONSTANT_URL.PROFILE_EDIT_API, formData, { headers: multiPartHeaderToken(token) })
    //         .then((res) => {
    //             console.log('editRes', res)
    //             dispatch({
    //                 type: PROFILE_EDIT_ISSUE_SUCCESS,
    //                 payload: res.data
    //             });
    //         })
    //         .catch((error) => {
    //             dispatch(getErrorMessage(PROFILE_EDIT_LIST_FAILED));
    //             console.log('error', error.response)
    //         });
    // };
};

export const onClearprofileUpdate = () => {
    return {
        type: PROFILE_EDIT_CLEAR_STATE,
        payload: ''
    }
}

export const getLgaStateIdAction = (data) => {
    return (dispatch) => {
        dispatch({ type: GET_LGA_STATE_ID_ACTION });
        axios.post(CONSTANT_URL.GET_LGA_BY_STATE_ID_URL, JSON.stringify(data), { headers: header })
            .then((res) => {
                console.log('getLgaStateIdAction111', res)
                dispatch({
                    type: GET_LGA_STATE_ID_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(GET_LGA_STATE_ID_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearLgaStateId = () => {
    return {
        type: GET_LGA_STATE_ID_CLEAR,
        payload: ''
    }
}

export const getUserProfileAction = (token, user) => {
   
    return (dispatch) => {
        dispatch({ type: USER_PROFILE_ACTION });
        axios.post(CONSTANT_URL.GET_OFFICIAL_PROFILE, JSON.stringify(user), { headers: headerToken(token) })
            .then((res) => {
                AsyncStorage.setItem(consts.GET_PROFILE, JSON.stringify(res.data))
                
                dispatch({
                    type: USER_PROFILE_SUCCESS,
                    payload: res.data
                });
                
            })
            .catch((error) => {
                dispatch(getErrorMessage(USER_PROFILE_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearUserOfficial = () =>{
    return{
        type: USER_CLEAR_STATE,
        payload: ''
    }
}