import axios from 'axios';
import { CONSTANT_URL, headerToken, getErrorMessage } from '../utils/URLConstants';

export const FORUM_DETAIL_ACTION = "FORUM_DETAIL_ACTION";
export const FORUM_DETAIL_SUCCESS = "FORUM_DETAIL_SUCCESS";
export const FORUM_DETAIL_FAILED = 'FORUM_DETAIL_FAILED';

export const FORUM_CHANGE_STATUS_ACTION = "FORUM_CHANGE_STATUS_ACTION";
export const FORUM_CHANGE_STATUS_SUCCESS = "FORUM_CHANGE_STATUS_SUCCESS";
export const FORUM_CHANGE_STATUS_FAILED = 'FORUM_CHANGE_STATUS_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getForumDetailAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: FORUM_DETAIL_ACTION });
        axios.post(CONSTANT_URL.POST_FORUM_DETAIL_API, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getForumDetailAction', res)
                dispatch({
                    type: FORUM_DETAIL_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(FORUM_DETAIL_FAILED));
                console.log('error', error.response)
            });
    };
};

export const getForumChangeStatusAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: FORUM_CHANGE_STATUS_ACTION });
        axios.post(CONSTANT_URL.POST_FORUM_CHANGE_STATUS_API, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getForumChangeStatusAction', res)
                dispatch({
                    type: FORUM_CHANGE_STATUS_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(FORUM_CHANGE_STATUS_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}