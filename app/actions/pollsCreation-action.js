import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const POST_POLLS_CREATION_ACTION = 'POST_POLLS_CREATION_ACTION';
export const POST_POLLS_CREATION_SUCCESS = 'POST_POLLS_CREATION_SUCCESS';
export const POST_POLLS_CREATION_FAILED = 'POST_POLLS_CREATION_FAILED';
export const POST_POLLS_CREATION_STATE = 'POST_POLLS_CREATION_STATE';

export const pollsCreateAction = (req,token) => {
    return (dispatch) => {
        dispatch({ type: POST_POLLS_CREATION_ACTION });
        axios.post(CONSTANT_URL.POLLS_CREATE_API, JSON.stringify(req), { headers: headerToken(token) })
            .then((res) => {
                console.log('ddddRes', res.data)                
                dispatch({
                    type: POST_POLLS_CREATION_SUCCESS,
                    payload: res.data
                });

            })
            .catch((error) => {
                dispatch(getErrorMessage(POST_POLLS_CREATION_FAILED));
                console.log('pollserror', error.response)
            });
    };
};

export const onClearPollCreate = () =>{
    return{
        type: POST_POLLS_CREATION_STATE,
        payload: ''
    }
}