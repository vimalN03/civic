import axios from 'axios';
import { CONSTANT_URL, headerToken,getErrorMessage } from '../utils/URLConstants';

export const CANDIDATE_DETAIL_ACTION = "CANDIDATE_DETAIL_ACTION";
export const CANDIDATE_DETAIL_SUCCESS = "CANDIDATE_DETAIL_SUCCESS";
export const CANDIDATE_DETAIL_FAILED = 'CANDIDATE_DETAIL_FAILED';

export const CANDIDATE_DETAIL_RATING_ACTION = "CANDIDATE_DETAIL_RATING_ACTION";
export const CANDIDATE_DETAIL_RATING_SUCCESS = "CANDIDATE_DETAIL_RATING_SUCCESS";
export const CANDIDATE_DETAIL_RATING_FAILED = 'CANDIDATE_DETAIL_RATING_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getCandidateDetailAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: CANDIDATE_DETAIL_ACTION });
        axios.post(CONSTANT_URL.GET_POLITICIANS_DETAIL_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getCandidateDetailAction', res)
                dispatch({
                    type: CANDIDATE_DETAIL_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CANDIDATE_DETAIL_FAILED));
                console.log('error', error.response)
            });
    };
};

export const postCandidateDetailRatingAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: CANDIDATE_DETAIL_RATING_ACTION });
        axios.post(CONSTANT_URL.GET_POLITICIANS_DETAIL_RATING_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('postCandidateDetailRatingAction', res)
                dispatch({
                    type: CANDIDATE_DETAIL_RATING_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CANDIDATE_DETAIL_RATING_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}