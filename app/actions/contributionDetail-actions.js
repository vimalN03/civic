import axios from 'axios';
import { CONSTANT_URL, headerToken,getErrorMessage } from '../utils/URLConstants';

export const CONTRIBUTION_DETAIL_ACTION = "CONTRIBUTION_DETAIL_ACTION";
export const CONTRIBUTION_DETAIL_SUCCESS = "CONTRIBUTION_DETAIL_SUCCESS";
export const CONTRIBUTION_DETAIL_FAILED = 'CONTRIBUTION_DETAIL_FAILED';

export const CONTRIBUTION_DELETE_ACTION = "CONTRIBUTION_DELETE_ACTION";
export const CONTRIBUTION_DELETE_SUCCESS = "CONTRIBUTION_DELETE_SUCCESS";
export const CONTRIBUTION_DELETE_FAILED = 'CONTRIBUTION_DELETE_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getContributionDetailAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: CONTRIBUTION_DETAIL_ACTION });
        axios.post(CONSTANT_URL.POST_CONTRIBUTION_DETAIL_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getContributionDetailAction', res)
                dispatch({
                    type: CONTRIBUTION_DETAIL_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CONTRIBUTION_DETAIL_FAILED));
                console.log('error', error.response)
            });
    };
};

export const getContributionDeleteAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: CONTRIBUTION_DELETE_ACTION });
        axios.post(CONSTANT_URL.POST_MY_CONTRIBUTION_DELETE_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getContributionDeleteAction', res)
                dispatch({
                    type: CONTRIBUTION_DELETE_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CONTRIBUTION_DELETE_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}