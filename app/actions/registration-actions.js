import { AsyncStorage } from 'react-native';
import axios from 'axios';
import { header, CONSTANT_URL, consts,  multiPartHeader, getErrorMessage } from '../utils/URLConstants';

export const REGISTRATION_AUTH_ACTION = "REGISTRATION_AUTH_ACTION";
export const REGISTRATION_AUTH_SUCCESS = "REGISTRATION_AUTH_SUCCESS";
export const REGISTRATION_FAILED = 'REGISTRATION_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const GET_ALL_STATE_LIST_ACTION = 'GET_ALL_STATE_LIST_ACTION';
export const GET_ALL_STATE_LIST_SUCCESS = 'GET_ALL_STATE_LIST_SUCCESS';
export const GET_ALL_STATE_LIST_FAILED = 'GET_ALL_STATE_LIST_FAILED';
export const GET_ALL_STATE_LIST_CLEAR = 'GET_ALL_STATE_LIST_CLEAR';

export const GET_ALL_WARD_LIST_ACTION = 'GET_ALL_WARD_LIST_ACTION';
export const GET_ALL_WARD_LIST_SUCCESS = 'GET_ALL_WARD_LIST_SUCCESS';
export const GET_ALL_WARD_LIST_FAILED = 'GET_ALL_WARD_LIST_FAILED';
export const GET_ALL_WARD_LIST_CLEAR = 'GET_ALL_WARD_LIST_CLEAR';

export const GET_LGA_BY_STATE_ID_ACTION = 'GET_LGA_BY_STATE_ID_ACTION';
export const GET_LGA_BY_STATE_ID_SUCCESS = 'GET_LGA_BY_STATE_ID_SUCCESS';
export const GET_LGA_BY_STATE_ID_FAILED = 'GET_LGA_BY_STATE_ID_FAILED';
export const GET_LGA_BY_STATE_ID_CLEAR = 'GET_LGA_BY_STATE_ID_CLEAR';

export const GET_WARD_BY_STATE_LGA_ID_ACTION = 'GET_WARD_BY_STATE_LGA_ID_ACTION';
export const GET_WARD_BY_STATE_LGA_ID_SUCCESS = 'GET_WARD_BY_STATE_LGA_ID_SUCCESS';
export const GET_WARD_BY_STATE_LGA_ID_FAILED = 'GET_WARD_BY_STATE_LGA_ID_FAILED';
export const GET_WARD_BY_STATE_LGA_ID_CLEAR = 'GET_WARD_BY_STATE_LGA_ID_CLEAR';

export const getRegistrationAuthAction = (formData) => {
    console.log(formData);
    return (dispatch) => {
        dispatch({ type: REGISTRATION_AUTH_ACTION });
        fetch(CONSTANT_URL.REGISTRATION_URL, {
            method: 'POST',
            headers: multiPartHeader,
            body: formData
        }).then(response => response.json())
            .then(response => {
                console.log("123", response);
                dispatch({
                    type: REGISTRATION_AUTH_SUCCESS,
                    payload: response
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(REGISTRATION_FAILED));
                console.log('error', error);
            });
    }
}

export const onClearRegistrationState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}

export const getAllStateListAction = () => {
    return (dispatch) => {
        dispatch({ type: GET_ALL_STATE_LIST_ACTION });
        axios.get(CONSTANT_URL.GET_ALL_STATE_URL, { headers: header })
            .then((res) => {
                console.log('getAllStateListAction', res)
                AsyncStorage.setItem(consts.STATE_LIST, JSON.stringify(res.data))
                dispatch({
                    type: GET_ALL_STATE_LIST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(GET_ALL_STATE_LIST_FAILED));
                console.log('getAllStateListActionerror', error.response)
            });
    };
};

export const onClearAllStateList = () => {
    return {
        type: GET_ALL_STATE_LIST_CLEAR,
        payload: ''
    }
}

export const getLgaByStateIdAction = (data) => {
    return (dispatch) => {
        dispatch({ type: GET_LGA_BY_STATE_ID_ACTION });
        axios.post(CONSTANT_URL.GET_LGA_BY_STATE_ID_URL, JSON.stringify(data), { headers: header })
            .then((res) => {
                console.log('getLgaByStateIdAction', res)
                dispatch({
                    type: GET_LGA_BY_STATE_ID_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(GET_LGA_BY_STATE_ID_FAILED));
                console.log('lga error', error.response)
            });
    };
};

export const onClearLgaByStateId = () => {
    return {
        type: GET_LGA_BY_STATE_ID_CLEAR,
        payload: ''
    }
}

export const getWardByStateLgaAction = (data) => {
    return (dispatch) => {
        dispatch({ type: GET_WARD_BY_STATE_LGA_ID_ACTION });
        axios.post(CONSTANT_URL.GET_WARD_BY_STATE_ID_LGA_URL, JSON.stringify(data), { headers: header })
            .then((res) => {
                console.log('getWardByStateLgaAction', res)
                dispatch({
                    type: GET_WARD_BY_STATE_LGA_ID_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(GET_WARD_BY_STATE_LGA_ID_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearWardByStateLgaList = () => {
    return {
        type: GET_WARD_BY_STATE_LGA_ID_CLEAR,
        payload: ''
    }
}