import axios from 'axios';
import { CONSTANT_URL, headerToken, getErrorMessage } from '../utils/URLConstants';

export const PROJECT_LIST_ACTION = "PROJECT_LIST_ACTION";
export const PROJECT_LIST_SUCCESS = "PROJECT_LIST_SUCCESS";
export const PROJECT_LIST_FAILED = 'PROJECT_LIST_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getProjectListAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: PROJECT_LIST_ACTION });
        axios.post(CONSTANT_URL.POST_PROJECT_POLITICIAN_LIST_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getProjectListAction', res)
                dispatch({
                    type: PROJECT_LIST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(PROJECT_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}