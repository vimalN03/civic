import { CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const CONTRIBUTION_CREATION_ACTION = "CONTRIBUTION_CREATION_ACTION";
export const CONTRIBUTION_CREATION_SUCCESS = "CONTRIBUTION_CREATION_SUCCESS";
export const CONTRIBUTION_CREATION_FAILED = 'CONTRIBUTION_CREATION_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getContributionCreationAction = (token, formData, isUpdate) => {
    console.log(formData, token, isUpdate);
    return (dispatch) => {
        dispatch({ type: CONTRIBUTION_CREATION_ACTION });
        let url = (isUpdate) ? CONSTANT_URL.POST_MY_CONTRIBUTION_UPDATE_URL : CONSTANT_URL.POST_MY_CONTRIBUTION_CREATION_URL
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            },
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                console.log("123", response);
                dispatch({
                    type: CONTRIBUTION_CREATION_SUCCESS,
                    payload: response
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CONTRIBUTION_CREATION_FAILED));
                console.log('error', error);
            });
    }
}

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}
