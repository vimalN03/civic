import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const CHAT_CONVERSATION_ACTION = "CHAT_CONVERSATION_ACTION";
export const CHAT_CONVERSATION_SUCCESS = "CHAT_CONVERSATION_SUCCESS";
export const CHAT_CONVERSATION_FAILED = 'CHAT_CONVERSATION_FAILED';
export const CHAT_CONVERSATION_CLEAR_STATE = 'CHAT_CONVERSATION_CLEAR_STATE';

export const getChatConversationAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: CHAT_CONVERSATION_ACTION });
        axios.post(CONSTANT_URL.CHAT_CONVERSATION_API, JSON.stringify(data), { headers: headerToken(token) })
        .then((res) => {
            console.log('getChatConverAction', res)
            dispatch({
                type: CHAT_CONVERSATION_SUCCESS,
                payload: res.data
            });
        })
        .catch((error) => {
           dispatch(getErrorMessage(CHAT_CONVERSATION_FAILED));
            console.log('error', error.response)
        });
    };
};

export const onChatConversationClearState = () => {
    return {
        type: CHAT_CONVERSATION_CLEAR_STATE,
        payload: ''
    }
}
