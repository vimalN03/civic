import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const FORUM_CREATION_ACTION = "FORUM_CREATION_ACTION";
export const FORUM_CREATION_SUCCESS = "FORUM_CREATION_SUCCESS";
export const FORUM_CREATION_FAILED = 'FORUM_CREATION_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getForumCreationAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: FORUM_CREATION_ACTION });
        axios.post(CONSTANT_URL.POST_FORUM_CREATION_API, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getForumCreationAction', res)
                dispatch({
                    type: FORUM_CREATION_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(FORUM_CREATION_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}
