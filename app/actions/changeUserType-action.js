import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const CHANGE_USERTYPE_ACTION = "CHANGE_USERTYPE_ACTION";
export const CHANGE_USERTYPE_SUCCESS = "CHANGE_USERTYPE_SUCCESS";
export const CHANGE_USERTYPE_FAILED = 'CHANGE_USERTYPE_FAILED';
export const CLEAR_CHANGE_USERTYPE_STATE = 'CLEAR_CHANGE_USERTYPE_STATE';

export const changeUserTypeAction = (data, token) => {
    return (dispatch) => {
        dispatch({ type: CHANGE_USERTYPE_ACTION });
        axios.post(CONSTANT_URL.CHANGE_USER_TYPE, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('changeUserTypeAction', res)
                dispatch({
                    type: CHANGE_USERTYPE_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch({
                    type: CHANGE_USERTYPE_FAILED,
                    payload: error.response.data
                });
                console.log('error11111', error.response)
            });
    };
};

export const onClearChangeUserState = () => {
    return {
        type: CLEAR_CHANGE_USERTYPE_STATE,
        payload: ''
    }
}