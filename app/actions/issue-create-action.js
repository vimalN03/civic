import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const CREATE_POST_ISSUE_ACTION = 'CREATE_POST_ISSUE_ACTION';
export const CREATE_POST_ISSUE_SUCCESS = 'GET_POST_ISSUE_SUCCESS';
export const CREATE_ISSUE_LIST_FAILED = 'CREATE_ISSUE_LIST_FAILED';
export const CREATE_ISSUE_CLEAR_STATE = 'CREATE_ISSUE_CLEAR_STATE';

export const issueCreateApi = (post ,token) => {
    return (dispatch) => {
        dispatch({ type: CREATE_POST_ISSUE_ACTION });
        axios.post(CONSTANT_URL.CREATE_ISSUE_API, JSON.stringify(post), {headers: headerToken(token)})
            .then((res) => {
                console.log('issueCreatetRes', res.data)
                dispatch({
                    type: CREATE_POST_ISSUE_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CREATE_ISSUE_LIST_FAILED));
                console.log('error', error.response)
            });
    };

    
};

export const onClearCreateIssue = () => {
    return {
        type: CREATE_ISSUE_CLEAR_STATE,
        payload: ''
    }
}