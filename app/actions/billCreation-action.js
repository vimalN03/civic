import { CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const BILL_CREATION_ACTION = "BILL_CREATION_ACTION";
export const BILL_CREATION_SUCCESS = "BILL_CREATION_SUCCESS";
export const BILL_CREATION_FAILED = 'BILL_CREATION_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getBillCreationAction = (token, formData, isUpdate) => {
    console.log(formData, token, isUpdate);
    return (dispatch) => {
        dispatch({ type: BILL_CREATION_ACTION });
        let url = (isUpdate) ? CONSTANT_URL.POST_BILL_DETAIL_UPDATE_URL : CONSTANT_URL.POST_BILL_CREATION_URL
        fetch(url, {
            method: 'POST',
            headers: {
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer ' + token
            },
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                console.log("123", response);
                dispatch({
                    type: BILL_CREATION_SUCCESS,
                    payload: response
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(BILL_CREATION_FAILED));
                console.log('error', error);
            });
    }
}

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}
