import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const FORUM_POST_ACTION = "FORUM_POST_ACTION";
export const FORUM_POST_SUCCESS = "FORUM_POST_SUCCESS";
export const FORUM_POST_FAILED = 'FORUM_POST_FAILED';
export const FORUM_POST_CLEAR_STATE = 'FORUM_POST_CLEAR_STATE';

export const getForumPostAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: FORUM_POST_ACTION });
        axios.post(CONSTANT_URL.POST_FORUM_REPLY, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getForumPostAction', res)
                dispatch({
                    type: FORUM_POST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(FORUM_POST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onForumPostClearState = () => {
    return {
        type: FORUM_POST_CLEAR_STATE,
        payload: ''
    }
}
