import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const GET_ALL_POLLS_ACTION = 'GET_ALL_POLLS_ACTION';
export const GET_ALL_POLLS_SUCCESS = 'GET_ALL_POLLS_SUCCESS';
export const POLLS_LIST_FAILED = 'POLLS_LIST_FAILED';
export const POLLS_CLEAR_STATE = 'POLLS_CLEAR_STATE';

export const getpollsListAction = (token) => {
    return (dispatch) => {
        dispatch({ type: GET_ALL_POLLS_ACTION });
        axios.get(CONSTANT_URL.POLLS_INDEX_API, { headers: headerToken(token) })
            .then((res) => {
                dispatch({
                    type: GET_ALL_POLLS_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(POLLS_LIST_FAILED));
                console.log('pollserror', error.response)
            });
    };
};

export const onClearPollsList = () =>{
    return{
        type: POLLS_CLEAR_STATE,
        payload: ''
    }
}