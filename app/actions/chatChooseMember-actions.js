import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const CHAT_CHOOSE_MEMBER_ACTION = "CHAT_CHOOSE_MEMBER_ACTION";
export const CHAT_CHOOSE_MEMBER_SUCCESS = "CHAT_CHOOSE_MEMBER_SUCCESS";
export const CHAT_CHOOSE_MEMBER_FAILED = 'CHAT_CHOOSE_MEMBER_FAILED';
export const CHAT_CHOOSE_MEMBER_CLEAR_STATE = 'CHAT_CHOOSE_MEMBER_CLEAR_STATE';

export const getChatChooseMemberAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: CHAT_CHOOSE_MEMBER_ACTION });
        axios.post(CONSTANT_URL.CHAT_CHOOSE_MEMBER_API, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getChatMemAction', res)
                dispatch({
                    type: CHAT_CHOOSE_MEMBER_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
               dispatch(getErrorMessage(CHAT_CHOOSE_MEMBER_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onChatChooseMembClearState = () => {
    return {
        type: CHAT_CHOOSE_MEMBER_CLEAR_STATE,
        payload: ''
    }
}
