export const E400_ERROR = "E400_ERROR";
export const E401_ERROR = "E401_ERROR";
export const E404_ERROR = "E404_ERROR";
export const E500_ERROR = "E500_ERROR";


export const set400Error = (error) => ({
  type: E400_ERROR,
  error: error
});

export const set401Error = (error) => ({
  type: E401_ERROR,
  error: error
});

export const set404Error = (error) => ({
  type: E404_ERROR,
  error: error
});


export const set500Error = (error) => ({
  type: E500_ERROR,
  error: error
});

export function errorActionSet(error) {
  // console.log("in errorActionSet", error);
  switch (error.status) {

    case 400:
      // console.log("in 400 error call error");
      return set400Error(error);

    case 401:
    case 403:
      // console.log("in 401 error call error");
      return set401Error(error);

    case 404:
      // console.log("in 404 error call error");
      return set404Error(error);

    case 500:
    case 502:
      // console.log("in 500 error call error");
      return set500Error(error);
    case 201:
    case 204:
        // console.log("in 200 error call error");
        return set400Error(error);
    default:
      console.log("in default error call error", error);
      
  }
}
