import axios from 'axios';
import { header, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const FORGOT_AUTH_ACTION = "FORGOT_AUTH_ACTION";
export const FORGOT_AUTH_SUCCESS = "FORGOT_AUTH_SUCCESS";
export const FORGOT_FAILED = 'FORGOT_FAILED';
export const CLEAR_FORGOT_STATE = 'CLEAR_FORGOT_STATE';

export const getForgotAction = (data) => {
    return (dispatch) => {
        dispatch({ type: FORGOT_AUTH_ACTION });
        axios.post(CONSTANT_URL.POST_FORGOT_PASSWORD_URL, JSON.stringify(data), { headers: header })
            .then((res) => {
                console.log('getForgotAction', res)
                dispatch({
                    type: FORGOT_AUTH_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(FORGOT_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearForgotState = () => {
    return {
        type: CLEAR_FORGOT_STATE,
        payload: ''
    }
}