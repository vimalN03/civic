import axios from 'axios';
import { header, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const OTP_SEND_ACTION = "OTP_SEND_ACTION";
export const OTP_SEND_SUCCESS = "OTP_SEND_SUCCESS";
export const OTP_SEND_FAILED = 'OTP_SEND_FAILED';
export const CLEAR_OTP_SEND_STATE = 'CLEAR_OTP_SEND_STATE';

export const getOtpAction = (data) => {
    return (dispatch) => {
        dispatch({ type: OTP_SEND_ACTION });
        axios.post(CONSTANT_URL.OTP_SEND_URL, JSON.stringify(data), { headers: header })
            .then((res) => {
                console.log('getOtpAction', res)
                dispatch({
                    type: OTP_SEND_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(OTP_SEND_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearOtpState = () => {
    return {
        type: CLEAR_OTP_SEND_STATE,
        payload: ''
    }
}