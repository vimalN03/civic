import axios from 'axios';
import { header, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const OTP_VERIFY_ACTION = "OTP_VERIFY_ACTION";
export const OTP_VERIFY_SUCCESS = "OTP_VERIFY_SUCCESS";
export const OTP_VERIFY_FAILED = 'OTP_VERIFY_FAILED';
export const CLEAR_OTP_VERIFY_STATE = 'CLEAR_OTP_VERIFY_STATE';

export const getOtpVerifyAction = (data) => {
    return (dispatch) => {
        dispatch({ type: OTP_VERIFY_ACTION });
        axios.post(CONSTANT_URL.OTP_VERIFY_URL, JSON.stringify(data), { headers: header })
            .then((res) => {
                console.log('verifyOtpAction', res)
                dispatch({
                    type: OTP_VERIFY_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(OTP_VERIFY_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearOtpVerifyState = () => {
    return {
        type: CLEAR_OTP_VERIFY_STATE,
        payload: ''
    }
}