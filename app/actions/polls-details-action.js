import axios from 'axios';
import { CONSTANT_URL, getErrorMessage,headerToken } from '../utils/URLConstants';

export const GET_ALL_DETAILS_POLLS_ACTION = 'GET_ALL_DETAILS_POLLS_ACTION';
export const GET_ALL_DETAILS_POLLS_SUCCESS = 'GET_ALL_DETAILS_POLLS_SUCCESS';
export const POLLS_DETAILS_LIST_FAILED = 'POLLS_DETAILS_LIST_FAILED';
export const POLLS_CLEAR_STATE = 'POLLS_CLEAR_STATE';

export const pollsDetailListAction = (req,token) => {
    return (dispatch) => {
        dispatch({ type: GET_ALL_DETAILS_POLLS_ACTION });
        axios.post(CONSTANT_URL.POLLS_DETAILS_API, JSON.stringify(req), { headers: headerToken(token) })
            .then((res) => {
                
                dispatch({
                    type: GET_ALL_DETAILS_POLLS_SUCCESS,
                    payload: res.data
                });
                console.log('ddddRes', res.data)
            })
            .catch((error) => {
                dispatch(getErrorMessage(POLLS_DETAILS_LIST_FAILED));
                console.log('pollserror', error.response)
            });
    };
};

export const onClearPollDetails = () =>{
    return{
        type: POLLS_CLEAR_STATE,
        payload: ''
    }
}