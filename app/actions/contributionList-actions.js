import axios from 'axios';
import { CONSTANT_URL, headerToken,getErrorMessage } from '../utils/URLConstants';

export const CONTRIBUTION_LIST_ACTION = "CONTRIBUTION_LIST_ACTION";
export const CONTRIBUTION_LIST_SUCCESS = "CONTRIBUTION_LIST_SUCCESS";
export const CONTRIBUTION_LIST_FAILED = 'CONTRIBUTION_LIST_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getContributionListAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: CONTRIBUTION_LIST_ACTION });
        axios.post(CONSTANT_URL.POST_CONTRIBUTION_LIST_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getCONTRIBUTIONListAction', res)
                dispatch({
                    type: CONTRIBUTION_LIST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(CONTRIBUTION_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}