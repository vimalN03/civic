import axios from 'axios';
import { CONSTANT_URL, headerToken ,getErrorMessage} from '../utils/URLConstants';

export const FORUM_LIST_ACTION = "FORUM_LIST_ACTION";
export const FORUM_LIST_SUCCESS = "FORUM_LIST_SUCCESS";
export const FORUM_LIST_FAILED = 'FORUM_LIST_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getForumListAction = (token) => {
    return (dispatch) => {
        dispatch({ type: FORUM_LIST_ACTION });
        axios.get(CONSTANT_URL.GET_FORUM_API, { headers: headerToken(token) })
            .then((res) => {
                console.log('getForumListAction', res)
                dispatch({
                    type: FORUM_LIST_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(FORUM_LIST_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}