import axios from 'axios';
import { headerToken, CONSTANT_URL, getErrorMessage } from '../utils/URLConstants';

export const HOME_ACTION = 'HOME_ACTION';
export const HOME_SUCCESS = 'HOME_SUCCESS';
export const HOME_FAILED = 'HOME_FAILED';
export const CLEAR_STATE = 'CLEAR_STATE';

export const getHomeAction = (token) => {
    return (dispatch) => {
        dispatch({ type: HOME_ACTION });
        axios.get(CONSTANT_URL.GET_HOME_API, { headers: headerToken(token) })
            .then((res) => {
                console.log('getHomeAction', res)
                dispatch({
                    type: HOME_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(HOME_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}