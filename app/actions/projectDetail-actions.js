import axios from 'axios';
import { CONSTANT_URL, headerToken, getErrorMessage } from '../utils/URLConstants';

export const PROJECT_DETAIL_ACTION = "PROJECT_DETAIL_ACTION";
export const PROJECT_DETAIL_SUCCESS = "PROJECT_DETAIL_SUCCESS";
export const PROJECT_DETAIL_FAILED = 'PROJECT_DETAIL_FAILED';

export const PROJECT_DELETE_ACTION = "PROJECT_DELETE_ACTION";
export const PROJECT_DELETE_SUCCESS = "PROJECT_DELETE_SUCCESS";
export const PROJECT_DELETE_FAILED = 'PROJECT_DELETE_FAILED';

export const CLEAR_STATE = 'CLEAR_STATE';

export const getProjectDetailAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: PROJECT_DETAIL_ACTION });
        axios.post(CONSTANT_URL.POST_PROJECT_DETAIL_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getProjectDetailAction', res)
                dispatch({
                    type: PROJECT_DETAIL_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(PROJECT_DETAIL_FAILED));
                console.log('error', error.response)
            });
    };
};

export const getProjectDeleteAction = (token, data) => {
    return (dispatch) => {
        dispatch({ type: PROJECT_DELETE_ACTION });
        axios.post(CONSTANT_URL.POST_MY_PROJECT_DELETE_URL, JSON.stringify(data), { headers: headerToken(token) })
            .then((res) => {
                console.log('getProjectDeleteAction', res)
                dispatch({
                    type: PROJECT_DELETE_SUCCESS,
                    payload: res.data
                });
            })
            .catch((error) => {
                dispatch(getErrorMessage(PROJECT_DELETE_FAILED));
                console.log('error', error.response)
            });
    };
};

export const onClearState = () => {
    return {
        type: CLEAR_STATE,
        payload: ''
    }
}