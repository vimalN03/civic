import React, { Component } from 'react';
import {
  View,
  Text,
  Image
} from 'react-native';
import { getHeaderKey,getNetInfo  } from '../../utils/URLConstants';
import _ from 'lodash';
import { connect } from 'react-redux';
import { getOfficialProfileAction, onClearElectedOfficial } from '../../actions/official-profile-action';

class SplashScreen extends Component {
    

    componentDidMount() {
    
        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            getHeaderKey().then((value) => {    
              console.log('aaaaaaaaRes',value)        
                if (!_.isEmpty(value)) {
                  console.log('aaaaaaaaResNotNull',value)
                    var data = {
                        id: value.data.id
                    }
                    this.props.dispatch(getOfficialProfileAction(value.data.token, data));
                    //this.props.navigation.navigate('HomeScreen', { activeTab: 0 })
                }else{
                  console.log('aaaaaaaaResNull',value)
                    this.props.navigation.navigate('LoginScreen', { email: '', pwd: ''})
                }
            }).catch((error) => {
                console.log('aaaaaaaaResErr',error.message)
            })
          }
        );
      }

    componentWillReceiveProps = (props) => {
        if(_.isEmpty(props.officialProfileResponse) === false){
            this.props.navigation.navigate('HomeScreen', { activeTab: 0 })
            this.props.dispatch(onClearElectedOfficial());
        }
    }

    componentWillUnmount(){
        this.willFocusSubscription.remove();
    }
    
    render(){
    return (
    <View style={styles.container}>
        <Image
            resizeMode="contain"
            source={require('../../resources/images/login/logo-Electra.png')}
            style={styles.logo}
        />
    </View>
  )
}
}

const styles = ({
    container: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100%'
    },
    logo: {
        alignSelf: 'center',
        width: 280,
        height: 360,
    },
  });

const mapStateToProps = (state) => {
    return {
      officialProfileResponse: state.ProfileOfficialR.officialProfileResponse,
    };
  };
  
  export default connect(mapStateToProps)(SplashScreen);