import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native';
import { Thumbnail, Text, Card } from 'native-base';
import { colors, normalize } from '../../../resources';
import { getUserProfileDummyUrl } from '../../../utils/URLConstants';

const PollsRecentItems = ({
  rowItem,
  onPress,
  onPressProfile
}) => (
    <TouchableOpacity activeOpacity={1} onPress={onPress}>
      <Card style={{ padding: 15, borderWidth: 1, borderColor: 'transparent', borderRadius: 5 }}>
        <View>
          <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(13), lineHeight: 20 }} numberOfLines={2} >{rowItem.question}</Text>
        </View>
        <View>
          <Text style={{ fontFamily: 'Nunito-Bold', color: '#b9bec2', marginLeft: 2, fontSize: normalize(12) }}>{rowItem.from_date + '-' + rowItem.end_date}</Text>
        </View>
        <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between', alignItems: 'center' }}>

        <TouchableOpacity onPress={ onPressProfile }>
          <View style={{ flexDirection: 'row' }}>
            <Thumbnail style={{ width: 25, height: 25 }} source={getUserProfileDummyUrl(rowItem.posted_user.profile_picture)} />
            <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 10, fontSize: normalize(12) }}>{rowItem.posted_user.name}</Text>
          </View>
        </TouchableOpacity>
          <View>
            <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>Posted: {rowItem.posted_at}</Text>
          </View>

        </View>
        <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'flex-end' }}>
          {/* <View>

            <View style={{ flexDirection: 'row' }}>
              <Text style={{ fontFamily: 'Nunito-Medium', marginLeft: 2, fontSize: normalize(10) }}>{`Tags: `}</Text>
              <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, marginLeft: 2, fontSize: normalize(10) }}>Improving Government, Elections</Text>
            </View>

          </View> */}
          <View>

            {<Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>{`${rowItem.total_votes} Votes`}</Text>}
          </View>
        </View>
      </Card>
    </TouchableOpacity>
  );

PollsRecentItems.prototype = {
  rowItem: PropTypes.object,
  onPress: PropTypes.func
};

export default PollsRecentItems;