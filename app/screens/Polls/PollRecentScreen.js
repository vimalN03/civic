import React, { Component } from 'react';
import { StyleSheet, RefreshControl, View, Text, FlatList } from 'react-native';
import { Container, Content } from 'native-base';
import Tabs from '../index';
import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile, getNetInfo } from '../../utils/URLConstants';
import PollsRecentItems from './items/PollsRecentItems';
import { colors, normalize } from '../../resources';

import {
  getpollsListAction, onClearPollsList
} from '../../actions/polls-action';


class PollRecentScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      activeTab: 0,
      loading: false,
      getListResponseData:[],
      errMsg: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        if(getNetInfo()){
          this.getRecentPollsApi();
        }else{
          this.setState({ refreshing: true, loading: false });
          alert('Check Network Connection!')
        } 
      }
    );
  }

  
  getRecentPollsApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      console.log('value got', value);
      if (!_.isEmpty(value)) {
        console.log('value got2');
        this.props.dispatch(getpollsListAction(value.data.token));
      }
    }).catch((error) => {
      console.log(error.message);
    })
  }

  componentWillReceiveProps = props => {
    if (_.isEmpty(props.error) === false) {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getResponseData) === false) {
      console.log('pollsResponse', props.getResponseData)
      if (props.getResponseData.status === 'success') {
        this.setState({ getListResponseData: props.getResponseData.data, errMsg: (props.getResponseData.data.length === 0)? 'No Result Found' : null, loading: false, refreshing: true })
        console.log('pollssData', props.getResponseData)
      } else if (props.getResponseData.status === 'error') {
        this.setState({ loading: false, errMsg: (props.getResponseData.data.length === 0)? 'No Result Found' : null, refreshing: true })
        // setTimeout(() => {
        //   alert(props.getResponseData.message)
        // }, 800); 
      }
      this.props.dispatch(onClearPollsList())
    }
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getRecentPollsApi();      
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
  }

  

  render() {
    console.log('rrrrRecent', this.state.getListResponseData)
    return (
      <Container style={{ flex: 1 }}>
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        <View style={{ flex: 7 }}>
          
            <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
              <FlatList
                data={this.state.getListResponseData}
                // ItemSeparatorComponent={() => <View style={{ height: 1, marginTop: 10, marginBottom: 5, backgroundColor: colors.litegray }} />}
                renderItem={({ item }) => <PollsRecentItems 
                  rowItem={item} 
                  onPress={() =>{ this.props.navigation.navigate('PollDetailScreen', { pollId: item }) }} 
                  onPressProfile = {()=>{ this.props.navigation.navigate('FollowerProfileScreen',{user_id: item.posted_user.id}) }} />
                }
                refreshControl={
                  <RefreshControl
                    refreshing={!this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
              />
            </View>
          
        </View>
        <Tabs
          activeTab={4}
          onPressHome={() => { this.props.navigation.navigate('HomeScreen') }}
          onPressCandidate={() => { this.props.navigation.navigate('CandidateScreen') }}
          onPressIssue={() => { this.props.navigation.navigate('IssueIdeaScreen') }}
          onPressOffial={() => { this.props.navigation.navigate('OfficialScreen') }}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    margin: 10
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


const mapStateToProps = (state) => {
  return {
    error: state.PollsR.error,
    getResponseData: state.PollsR.getResponseData,
    isLoading: state.PollsR.isLoading,
  };
};

export default connect(mapStateToProps)(PollRecentScreen);