import React, { Component } from 'react';
import { View } from 'react-native';
import { createMaterialTopTabNavigator } from 'react-navigation';
import PollRecentScreen from './PollRecentScreen';
import PollCompleteScreen from './PollCompleteScreen';
import { colors, normalize } from '../../resources';

export default class PollTab extends Component {
    render() {
        const MainTab = createMaterialTopTabNavigator({
            PollRecentScreen: {
                screen: PollRecentScreen,
                navigationOptions: {
                    tabBarLabel: 'Recent'
                }
            },
            PollCompleteScreen: {
                screen: PollCompleteScreen,
                navigationOptions: {
                    tabBarLabel: 'Completed',
                }
            },
        },
            {
                tabBarPosition: 'top',
                tabBarOptions: {
                    upperCaseLabel: false,
                    labelStyle: {
                        fontSize: normalize(16),
                        color: colors.primary,
                    },
                    style: {
                        backgroundColor: '#fff',
                    },
                    indicatorStyle: {
                        borderBottomColor: colors.primary,
                        borderBottomWidth: 2,
                    }
                }
            }
        );

        return (
            <View style={{ flex: 1 }}>
                <MainTab />
            </View>
        );
    }
}