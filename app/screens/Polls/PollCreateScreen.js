import React, { Component } from 'react';
import { StyleSheet, View, TextInput, BackHandler, Alert } from 'react-native';
import { Container, Root, Card, Content, Button, Text, Textarea, ActionSheet, Label  } from 'native-base';

import MyDatePicker from '../../components/DatePicker';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'

import { HeaderBackNativeBase } from '../../components/Headers/index';
import { colors , hp ,normalize}  from '../../resources/index';
import _ from 'lodash';
import moment from 'moment';
import { connect } from 'react-redux';
import Loader from '../../components/Loader/Loader';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import { pollsCreateAction, onClearPollCreate } from '../../actions/pollsCreation-action';

class PollCreateScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            pollTitle: '',
            pollQuestion:'',
            startingDate: '',
            toDate: '',
            option1:'',
            option2:'',
            option3:'',
            tagType:'-1',
            tagTypeValue: 'Select Private/Public',
            tagTypeData: [{ key: '0', value: 'Public' }, { key: '1', value: 'Private' }],
        }
    }

    componentWillMount(){
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
        this.getProfile();
    }

    getProfile() {
        getProfile().then((value) => {
            console.log('did mount value got', value);
            console.log("did mount vin===>",value.data.vin);
            if(value.data.vin === undefined || value.data.vin === null || value.data.vin === '') {
                alert('Update VIN to create poll!')
                this.props.navigation.navigate('PollScreen')
            } else {
                
            }
        },err=>{
            console.log('did mount error value',err)
        })
    }

    componentDidMount(){
        this.getCurrentDate();
    }

    componentWillReceiveProps = (props) => {
        if (props.isLoading === false) {
            setTimeout(() => { alert(props.error.message) }, 800);
            this.setState({ loading: false })
          }
          if (_.isEmpty(props.getPollsCreateResData) === false) {
            
            if (props.getPollsCreateResData.status === 'success') {
                this.setState({ getResponse: props.getPollsCreateResData.data, loading: false })    
                console.log('issueResponseData', props.getPollsCreateResData)
                setTimeout(() => { 
                    Alert.alert(
                      '',
                     props.getPollsCreateResData.message,
                      [
                        {text: 'OK', onPress: () => this.props.navigation.goBack()},
                      ],
                      { cancelable: false }
                    )
                    }, 800);
              } else if (props.getPollsCreateResData.status === 'error') {
                this.setState({ loading: false })
                setTimeout(() => {
                    alert(props.getPollsCreateResData.message)
                }, 800); 
              }
            }
    }

    getCurrentDate = () =>{
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd = '0'+dd
        } 
        if(mm<10) {
            mm = '0'+mm
        } 
        today = mm + '/' + dd + '/' + yyyy;
        this.setState({
            startingDate: moment(today).format("YYYY/MM/DD"),
            toDate: moment(today).format("YYYY/MM/DD"),
        });
    }

    onCreatePolls = () => {
        if (this.validateInputs()) {
          this.setState({ loading: true })
          getProfile().then((value) => {
            console.log('value got', value);

            if (!_.isEmpty(value)) {
              const data ={
                "poll_title": this.state.pollTitle, 
                "tag_type": this.state.tagTypeValue.toLowerCase(), 
                "question": this.state.pollQuestion, 
                "from_date": this.state.startingDate, 
                "end_date": this.state.toDate,
                "options" : [{"option": this.state.option1},{"option": this.state.option2},{"option": this.state.option3}]
            };
            
            this.props.dispatch(pollsCreateAction(data, value.data.token));
            }
          }).catch((error) => {
            console.log(error.message);
          })
    
        }
    
    }

    validateInputs = () => {
        if (this.state.pollTitle.trim() === ''){
            alert('Poll Title cannot be empty!!');
            return false;
        }else if(this.state.tagType === '-1') {
            alert('Please Select type Private/Public!!')
            return false;
        }else if (this.state.pollQuestion.trim() === ''){
            alert('Poll Question cannot be empty!!');
            return false;
        }else if (this.state.toDate <= this.state.startingDate){
            alert("Poll End Date must be in the future");
            return false;
        }
        else if (this.state.option1.trim() === ''){
            alert('Poll Option1 cannot be empty!!');
            return false;
        }else if (this.state.option2.trim() === ''){
            alert('Poll Option2 cannot be empty!!');
            return false;
        }else if (this.state.option3.trim() === ''){
            alert('Poll Option3 cannot be empty!!');
            return false;
        }
        return true;
    }

    componentWillUnmount() {
        ActionSheet.actionsheetInstance = null;
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
  
    handleBackButtonClick = () => {
      return this.props.navigation.goBack();
    }

    render() {
        return (
            <Root>
            <Container>
                <HeaderBackNativeBase
                    containerStyles={{ backgroundColor: '#f6f7f9' }}
                    leftIcon={require('../../resources/images/left-arrow.png')}
                    onPressLeft={() => {
                        this.props.navigation.navigate('PollScreen')
                    }}
                    title='Create Poll' />
                    {(this.state.loading) ? <Loader /> : null}
                <Content style={{ backgroundColor: '#f6f7f9' }}>
                    <View style={{ marginLeft: 15, marginRight: 15 }}>
                        <Card style={{ padding: 15, marginTop: 10 }}>
                            <View style={[styles.textInputContainer]}>
                                <TextInput
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Poll Title"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({ pollTitle: text }) }} />
                            </View>

                            <View style={[styles.textInputContainer]}>
                            <Label
                                adjustsFontSizeToFit={true}
                                style={{
                                    flex: 1, height: hp('6%'), padding: 10, fontSize: normalize(12),
                                    color: colors.black
                                }}
                                onPress={() => { this.selectTagType() }}>
                            {this.state.tagTypeValue}
                          </Label>
                            </View>

                            <View style={styles.textInputContainer}>
                                <Textarea rowSpan={4}
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Question"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({ pollQuestion: text }) }} />
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                <View
                                    style={{
                                        flex: 1, padding: 5, borderWidth: 0.5, borderRadius: 1, borderColor: '#dfe6f0',
                                        backgroundColor: '#eff4f8', paddingTop: 10
                                    }}>
                                    <MyDatePicker
                                        date={this.state.startingDate}
                                        style={{ width: '100%' }}
                                        placeholder={'From Date'}
                                        customStyles={{
                                            dateInput: { borderColor: "transparent", borderWidth: 1 },
                                            dateTouchBody: { borderColor: "transparent", borderWidth: 1 },
                                            dateIcon: { width: 22, height: 22 }
                                        }}
                                        onDateChange={(date) => { this.setState({ startingDate: date }) }}
                                    />
                                </View>
                                <View style={{ width: 10 }} />
                                <View
                                    style={{
                                        flex: 1, padding: 5, borderWidth: 0.5, borderRadius: 1, borderColor: '#dfe6f0',
                                        backgroundColor: '#eff4f8', paddingTop: 10
                                    }}>
                                    <MyDatePicker
                                        date={this.state.toDate}
                                        style={{ width: '100%' }}
                                        placeholder={'To Date'}
                                        customStyles={{
                                            dateInput: { borderColor: "transparent", borderWidth: 1 },
                                            dateTouchBody: { borderColor: "transparent", borderWidth: 1 },
                                            dateIcon: { width: 22, height: 22 }
                                        }}
                                        onDateChange={(date) => { this.setState({ toDate: date }) }}
                                    />
                                </View>
                            </View>
                            <View style={{ padding: 5 }}>
                            
                                <View style={[styles.textInputContainer]}>
                                <TextInput
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Option #1"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({option1:text})}} />
                                 </View>

                                 <View style={[styles.textInputContainer]}>
                                <TextInput
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Option #2"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({option2:text})}} />
                                 </View>

                                 <View style={[styles.textInputContainer]}>
                                <TextInput
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Option #3"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({option3:text})}} />
                                 </View>


                            </View>

                            <Button full
                                onPress={() => {this.onCreatePolls() }}
                                style={{
                                    marginTop: 10,
                                    borderWidth: 1,
                                    borderColor: '#018c31',
                                    borderRadius: 20,
                                    backgroundColor: '#018c31'
                                }}>
                                <Text>CREATE POLL</Text>
                            </Button>
                        </Card>
                    </View>
                </Content>
            </Container>
            </Root>
        );
    }

    selectTagType = () => {
        var tagTypeData = [{ key: 0, value: 'Public' }, { key: 1, value: 'Private' }];
        if (_.isEmpty(tagTypeData) === false) {
          let nameList = tagTypeData.map(function (item) {
            return item['value'];
          });
          nameList.push("Cancel")
          let CANCEL_INDEX = nameList.length - 1
          ActionSheet.show(
            {
              options: nameList,
              cancelButtonIndex: CANCEL_INDEX,
              title: "Select Public/Private"
            },
            buttonIndex => {
              if (buttonIndex < tagTypeData.length) {
                this.setState({
                tagType: tagTypeData[buttonIndex].key,
                tagTypeValue: tagTypeData[buttonIndex].value
                })
              }
            }
          )
        }else{
        }
      }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f6f7f9',
        padding: 20
    },
    instructions: {
        textAlign: 'center',
        marginBottom: 10
    },
    textInputContainer: {
        flexDirection: 'row',
        marginTop: 12,
        borderWidth: 0.5,
        borderRadius: 1,
        borderColor: '#dfe6f0',
        backgroundColor: '#eff4f8'
    }
});

const mapStateToProps = (state) => {
    return {
      error: state.PollsCreateR.error,
      getPollsCreateResData: state.PollsCreateR.getPollsCreateResData,
      isLoading: state.PollsCreateR.isLoading,
    };
  };
  
  export default connect(mapStateToProps)(PollCreateScreen);