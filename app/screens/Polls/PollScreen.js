import React, { Component } from 'react';
import { View } from 'react-native';

import Tabs from '../index';

class PollScreen extends Component {

    render() {
        return (
            <View style={{ flex: 1, flexDirection: 'column' }}>
                <Tabs
                    activeTab={4}
                    onPressHome={() => { this.props.navigation.navigate('HomeScreen') }}
                    onPressCandidate={() => { this.props.navigation.navigate('CandidateScreen') }}
                    onPressIssue={() => { this.props.navigation.navigate('IssueIdeaScreen') }}
                    onPressOffial={() => { this.props.navigation.navigate('OfficialScreen') }}
                />
            </View>
        );
    }
}


export default (PollScreen);