import React, { Component } from 'react';
import { StyleSheet, View, FlatList } from 'react-native';
import { Container, Content, Text } from 'native-base';
import { colors, normalize } from '../../resources';

import Tabs from '../index';

import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile } from '../../utils/URLConstants';
import PollsCompletedItems from './items/PollsCompletedItems';

import {
  getpollsCompletedAction
} from '../../actions/polls-completed-action';

class PollCompleteScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      activeTab: 0,
      loading: true,
      getListResponseData: [],
      errMsg: null
    }
  }

  componentWillMount() {
    getProfile().then((value) => {
      console.log('value got', value);
      if (!_.isEmpty(value)) {
        console.log('value got2');
        this.props.dispatch(getpollsCompletedAction(value.data.token));
      }
    }).catch((error) => {
      console.log(error.message);
    })
  }

  componentWillReceiveProps = props => {
    if (_.isEmpty(props.error) === false) {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getResponseData) === false) {
      console.log('pollsResponse', props.getResponseData)
      if (props.getResponseData.status === 'success') {
        this.setState({ getListResponseData: props.getResponseData.data, errMsg: (props.getResponseData.data.length === 0)? 'No Result Found' : null, loading: false })
        console.log('pollsResponseData', props.getResponseData)
      } else if (props.getResponseData.status === 'error') {
        this.setState({ loading: false, errMsg: (props.getResponseData.data.length === 0)? 'No Result Found' : null })
        // setTimeout(() => {
        //   alert(props.getResponseData.message)
        // }, 800); 
      }
    }

  }

  
  render() {

    return (
      <Container style={{ flex: 1 }}>
        {(this.state.loading) ? <Loader /> : null}
        <View style={{ flex: 7 }}>
          <Content>
            <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
              <FlatList
                data={this.state.getListResponseData}
                // ItemSeparatorComponent={() => <View style={{ height: 1, marginTop: 10, marginBottom: 5, backgroundColor: colors.litegray }} />}
                renderItem={({ item }) => <PollsCompletedItems rowItem={item} 
                  onPress={() => this.props.navigation.navigate('PollDetailScreen', { pollId: item })} 
                  onPressProfile = {()=>{ this.props.navigation.navigate('FollowerProfileScreen',{user_id: item.posted_user.id}) }}/>}
              />
            </View>
          </Content>
        </View>
        <Tabs
          activeTab={4}
          onPressHome={() => { this.props.navigation.navigate('HomeScreen') }}
          onPressCandidate={() => { this.props.navigation.navigate('CandidateScreen') }}
          onPressIssue={() => { this.props.navigation.navigate('IssueIdeaScreen') }}
          onPressOffial={() => { this.props.navigation.navigate('OfficialScreen') }}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    margin: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    error: state.PollsCompletedR.error,
    getResponseData: state.PollsCompletedR.getResponseData,
    isLoading: state.PollsCompletedR.isLoading,
  };
};

export default connect(mapStateToProps)(PollCompleteScreen);