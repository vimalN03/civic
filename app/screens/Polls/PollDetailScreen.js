import React, { Component } from 'react';
import { StyleSheet, View, BackHandler } from 'react-native';
import { Container, Thumbnail, Card, Content, Button, Text } from 'native-base';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button'
import { HeaderBackNativeBase } from '../../components/Headers/index';
import { colors, normalize, wp } from '../../resources';
import ProgressBarAnimated from 'react-native-progress-bar-animated';

import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';

import { pollsDetailListAction, onClearPollDetails } from '../../actions/polls-details-action';
import { pollsQuestionAction, onClearPollsQuestions } from '../../actions/pollsQuestionSubmit-actions';

class PollDetailScreen extends Component {

  constructor(props) {
    super(props);
    const { params } = props.navigation.state;

    console.log('pprpr', params.pollId)
    this.state = {
      loading: true,
      getListResponseData: { option: [], user: {} },
      prop_List: params.pollId,
      poll_id: 0,
      option_id: null
    }
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getPollsDetailApi();      
      }
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }
 
  getPollsDetailApi = () => {
    getProfile().then((value) => {
      // console.log('value got', value);
      if (!_.isEmpty(value)) {
        // console.log('value got2');
        // console.log('iiij', this.state.prop_List.id)
        this.props.dispatch(pollsDetailListAction({ "poll_id": this.state.prop_List.id }, value.data.token));
      }
    }).catch((error) => {
      console.log(error.message);
    })
  }

  componentWillReceiveProps = props => {
    if (props.isLoading === false) {
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getResponseData) === false) {
      if (props.getResponseData.status === 'success') {
        this.setState({ getListResponseData: props.getResponseData.data, poll_id: props.getResponseData.data.id, loading: false })
        console.log('detailsResponseData', props.getResponseData)
      } else {
        setTimeout(() => {
          this.setState({ loading: false })
          alert(props.getResponseData.message)
        }, 800);
      }
      this.props.dispatch(onClearPollDetails());
    }

    if (props.pollQuesisLoading === false) {
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getPollSubmitRes) === false) {
      if (props.getPollSubmitRes.status === 'success'){
        this.setState({ loading: false })
        setTimeout(() => {
          alert(props.getPollSubmitRes.message)
        }, 800);
        this.getPollsDetailApi();
      } else if (props.getPollSubmitRes.status === 'error') {
        this.setState({ loading: false })
        setTimeout(() => {
          alert(props.getPollSubmitRes.message)
        }, 800);
      }
      this.props.dispatch(onClearPollsQuestions());
    }
  }

  pollSubmit = () => {
    if(_.isEmpty(this.state.option_id)){
      alert('Please Select the Option');
    }else{
      this.setState({  loading: true }, function() {
          this.setState({  loading: true })
      });
      this.props.dispatch(onClearPollsQuestions());
      getProfile().then((value) => {
        if (!_.isEmpty(value)) {
          var data = {
            "poll_id": this.state.poll_id,
            "option_id": this.state.option_id
          }
          this.props.dispatch(pollsQuestionAction( data, value.data.token));
        }
      }).catch((error) => {
        console.log(error.message);
      })
    }
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  render() {
    const progressCustomStyles = {
      backgroundColor: colors.primary,
      borderRadius: 4
    };
    return (
      <Container>
        <HeaderBackNativeBase
          containerStyles={{ backgroundColor: '#f6f7f9' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => this.props.navigation.goBack()}
          title='Polls'
        />
        {(this.state.loading) ? <Loader /> : null}
        <Content style={{ backgroundColor: '#f6f7f9' }}>
          <View style={{ marginLeft: 15, marginRight: 15 }}>
            <Card style={{ padding: 15, borderWidth: 1, borderColor: 'transparent', borderRadius: 5 }}>
              <View>
                <Text style={{ fontSize: normalize(13) }} numberOfLines={2} >{this.state.getListResponseData.poll_title}</Text>
              </View>
              <View>
                <Text style={{ color: '#b9bec2', marginLeft: 2, fontSize: normalize(12) }}>{this.state.getListResponseData.from_date+' - '+this.state.getListResponseData.end_date}</Text>
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10, alignItems: 'center' }}>
                <View style={{ flexDirection: 'row' }}>
                  <Thumbnail source={getUserProfileDummyUrl(this.state.getListResponseData.user.profile_picture)} />
                </View>
                <View>
                <Text style={{ color: '#b9bec2', marginLeft: 8, fontSize: normalize(14) }}>{this.state.getListResponseData.user.name}</Text>
                </View>
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
                {/* <View>

                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ marginLeft: 2, fontSize: normalize(10) }}>{`Tags: `}</Text>
                    <Text style={{ color: colors.primary, marginLeft: 2, fontSize: normalize(10) }}>Improving Government, Elections</Text>
                  </View>

                </View> */}
                <View>
                  <Text style={{ color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>{`${this.state.getListResponseData.total_votes} Votes`}</Text>
                </View>
                <View>
                  <Text style={{ color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>Posted: {this.state.getListResponseData.posted_at} </Text>
                </View>
              </View>
            </Card>
            <Card style={{ padding: 15, marginTop: 10 }}>
              <View>
                <Text style={{ fontSize: normalize(14), fontWeight: '100', paddingBottom: 10 }}>Question 1 of 1</Text>
                <Text style={{ marginBottom:5, fontWeight: '500', fontSize: normalize(14) }}>{this.state.getListResponseData.question}
                </Text>
              </View>
              <View>
                {(parseInt(this.state.getListResponseData.poll_vote) === 1) ?
                <View>
                  {(this.state.getListResponseData.option.length !== 0)?
                  <View style={{ marginTop: 5, flexDirection: 'column' }}>
                  { this.state.getListResponseData.option.map((opt,i)=>{
                    return(
                    <View>
                      <Text style={{color: '#5c626e',fontSize: 12, marginBottom: 5,}}>{opt.option}</Text>
                      <View style={{ flex:1, marginBottom: 15, flexDirection: 'row', alignItems: 'center'}} key={i}>
                      <ProgressBarAnimated
                          {...progressCustomStyles}
                          width={wp('70%')}
                          height = {wp('7%')}
                          maxValue={50}
                          value={opt.option_rating.total_option_ratings}
                        />
                        <Text style={{ marginLeft:5, fontFamily: 'Nunito-Medium', color: colors.black, fontSize: normalize(9), marginStart: 3 }}>{Math.round(opt.option_rating.total_option_ratings)}%</Text>
                      </View>
                    </View>)
                    })
                  }
                  </View>
                  : null
                  }
                  </View>
                 :
          <View>
              {
                (this.state.getListResponseData.option.length !== 0)?
                  <RadioGroup
                    size={22}
                    thickness={2}
                    color={colors.primary}
                    selectedIndex={null}
                    onSelect={(index, value) => { 
                      this.setState({ option_id: value })
                    }}
                  >
                  {
                    this.state.getListResponseData.option.map((opt,i)=>{
                    return <RadioButton key={i} value={opt.option_id}>
                      <Text>{opt.option}</Text>
                    </RadioButton>
                    })
                  }
                  </RadioGroup>
              :
              null
              }
            </View>         
                }
              </View>
              {(parseInt(this.state.getListResponseData.poll_vote) !== 1) ?
              <Button full
                onPress={() => {this.pollSubmit()}}
                style={{
                  marginTop: 10,
                  borderWidth: 1,
                  borderColor: '#018c31',
                  borderRadius: 20,
                  backgroundColor: '#018c31'
                }}>
                <Text>SUBMIT</Text>
              </Button>
              : null 
              }
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#f6f7f9',
    padding: 20
  },
  instructions: {
    textAlign: 'center',
    marginBottom: 10
  },
  textInputContainer: {
    flexDirection: 'row',
    paddingLeft: 5,
    paddingBottom: 10,
    paddingTop: 10,
    marginTop: 12,
    borderWidth: 0.5,
    borderRadius: 3,
    borderColor: '#dfe6f0',
    backgroundColor: '#eff4f8'
  }
});


const mapStateToProps = (state) => {
  return {
    error: state.PollsDetailsR.error,
    getResponseData: state.PollsDetailsR.getResponseData,
    isLoading: state.PollsDetailsR.isLoading,
    pollQueserror: state.PollsQuestionR.pollQueserror,
    getPollSubmitRes: state.PollsQuestionR.getPollSubmitRes,
    pollQuesisLoading: state.PollsQuestionR.pollQuesisLoading,
  };
};

export default connect(mapStateToProps)(PollDetailScreen);