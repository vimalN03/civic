import React from 'react';
import { View, Text, Image, TextInput, Alert } from 'react-native';
import { Container, Button } from 'native-base';
import _ from 'lodash';
import { connect } from 'react-redux';

import { colors } from '../../resources';
import { HeaderBackNativeBase } from '../../components/Headers';

import { getForgotAction, onClearForgotState } from '../../actions/forgot-actions';
import Loader from '../../components/Loader/Loader';



class ForgotPasswordScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = { loading: false, forgotPasswordMailId: '' }
    }

    componentWillReceiveProps = (props) => {
        if (props.error.status === 'error') {
            if (props.error.status === 'error') {
                setTimeout(() => { alert(props.error.message) }, 800);
                this.props.dispatch(onClearForgotState());
            }
            this.setState({ loading: false })
        }
        if (_.isEmpty(props.forgotResponse) === false && props.error === '') {
            if (props.forgotResponse.status === 'success') {
                this.setState({ loading: false })
                setTimeout(() => { 
                    Alert.alert(
                        '',
                        props.forgotResponse.message,
                        [
                          {text: 'OK', onPress: () => this.props.navigation.navigate('LoginScreen', { email: '', pwd: ''})},
                        ],
                        { cancelable: false }
                      )
                    }, 800);
                this.props.dispatch(onClearForgotState());
            } else {
                this.setState({ loading: false })
                setTimeout(() => { alert(props.forgotResponse.message) }, 800);
            }
            
        }
    }

    /**
   * Validate and Post forgot password
   */
    onSubmit = () => {
        const { forgotPasswordMailId } = this.state;
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (reg.test(forgotPasswordMailId.trim()) === false) {
            alert('Please Enter a Valid Email ID')
            return false;
        } else {
            this.setState({ loading: true })
            const resetEmail = {
                email: forgotPasswordMailId
            };
            this.props.dispatch(onClearForgotState());
            this.props.dispatch(getForgotAction(resetEmail));
            console.log(resetEmail)
        }
    }

    render() {
        return (
            <Container>
                <HeaderBackNativeBase
                    containerStyles={{ backgroundColor: '#fff' }}
                    leftIcon={require('../../resources/images/left-arrow.png')}
                    onPressLeft={() => { this.props.navigation.goBack() }}
                    title='Forgot'
                />
                {(this.state.loading) ? <Loader /> : null}
                <View style={{ marginLeft: 15, marginRight: 15 }}>
                    <View style={styles.textInputContainer}>
                        <Image
                            resizeMode="contain"
                            style={{ alignSelf: 'center', height: '75%', width: '10%' }}
                            source={require('../../resources/images/Login-Register/Mail/Mail.png')}
                        />
                        <TextInput
                            value={this.state.forgotPasswordMailId}
                            style={{ flex: 1, padding: 10 }}
                            placeholder="Email Address"
                            underlineColorAndroid="transparent"
                            onChangeText={(text) => this.setState({ forgotPasswordMailId: text })}
                        />
                    </View>
                    <Button full
                        onPress={() => { this.onSubmit() }}
                        style={{
                            marginTop: 10,
                            borderWidth: 1,
                            borderColor: '#018c31',
                            borderRadius: 20,
                            backgroundColor: '#018c31'
                        }}>
                        <Text style={{ color: colors.white }}>SUBMIT</Text>
                    </Button>
                </View>
            </Container>
        );
    }
}

const styles = ({
    textInputContainer: {
        flexDirection: 'row',
        paddingLeft: 5,
        marginTop: 12,
        borderWidth: 0.5,
        borderRadius: 1,
        borderColor: '#dfe6f0',
        backgroundColor: '#eff4f8'
    }
});

const mapStateToProps = (state) => {
    return {
        error: state.ForgotPasswordR.error,
        isLoading: state.ForgotPasswordR.isLoading,
        forgotResponse: state.ForgotPasswordR.forgotResponse
    };
};


export default connect(mapStateToProps)(ForgotPasswordScreen);