import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Text, Thumbnail, Icon, Card, CardItem } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, dimens, hp } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import Share, {ShareSheet, Button} from 'react-native-share';
import {
    getOfficialProfileAction, onClearElectedOfficial
} from '../../actions/official-profile-action';


class FollowerProfileScreen extends Component {

    constructor(props) {
        super(props);
        
        const { user_id } = props.navigation.state.params
        console.log('111222----',user_id)
        this.state = {
            userId: user_id,
            loading: true,
            getProfileResponseData: { total_followers: 0 }
        }
    }

    componentDidMount() {
        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.getUserprofileApi();
          }
        );
    }

    getUserprofileApi = () => {
        getProfile().then((value) => {
            console.log('value got', value);
            if (!_.isEmpty(value)) {
                console.log('value got2')
                var data = {
                    id: this.state.userId
                }
                this.props.dispatch(getOfficialProfileAction(value.data.token, data));
            }
        }).catch((error) => {
            console.log(error.message);
        })
    }

    componentWillReceiveProps = props => {
        if (_.isEmpty(props.error) === false) {
            if (props.error.status === 'error') {
                setTimeout(() => { alert(props.error.message) }, 800);
            }
            this.setState({ loading: false })
            this.props.dispatch(onClearElectedOfficial())
        }

        if (_.isEmpty(props.officialProfileResponse) === false) {
            console.log('pResponse', props.officialProfileResponse)
            if (props.officialProfileResponse.status === 'success') {
                //alert(props.electedResponse.message)
                console.log('zzzzz', props.officialProfileResponse)
                this.setState({ getProfileResponseData: props.officialProfileResponse.data, loading: false })
            } else {
                this.setState({ loading: false })
                setTimeout(() => {
                    alert(props.officialProfileResponse.message)    
                }, 800);
            }
            this.props.dispatch(onClearElectedOfficial())
        }
    }

    componentWillUnmount(){
        this.willFocusSubscription.remove();
    }
    


    render() {
        console.log('1122====',this.state.getProfileResponseData)
        console.log('1122====1122',this.props)
        return (
            <Container>
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        <View style={{ height: hp('25%'), backgroundColor: 'grey' }}>
                            <ImageBackground source={require('../../resources/images/User_Profile/User-profile-Background.png')} style={{ width: '100%', height: '100%' }}>
                                <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
                                    leftIcon={require('../../resources/images/User_Profile/left-arrow/left-arrow@48.png')}
                                    onPressLeft={() => { this.props.navigation.goBack() }}
                                    title={this.state.getProfileResponseData.name} titleStyle={{ paddingLeft: 5, color: colors.white, fontSize: normalize(dimens.headerTitle) }}
                                />
                            </ImageBackground>
                        </View>
                        {(this.state.loading) ? <Loader /> : null}
                        <View style={{ flexGrow: 1, backgroundColor: 'white', alignItems: 'center' }}>
                            <View style={{ marginTop: 45, flexDirection: 'column' }}>

                                <View style={{ flexDirection: 'row', width: Dimensions.get('window').width, alignItems: 'center' }}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        
                                        <View style={{ marginTop: 5, backgroundColor: colors.primary, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                           <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FollowerScreen',{ user_id: this.state.getProfileResponseData.id})}}>
                                            <Text style={{ fontFamily: 'Nunito-Light', color: colors.white, paddingLeft: 5, paddingRight: 5, fontSize: normalize(12) }}>
                                                {`Followers(${this.state.getProfileResponseData.total_followers})`}
                                            </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', marginTop: 5 }}>
                                        <Text style={{ fontSize: normalize(18), textAlign: 'center' }}>{this.state.getProfileResponseData.name}</Text>
                                        
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', marginTop: 5 }} />
                                       
                                </View>


                                <View style={{ padding: 10, margin: 5 }}>
                                    <Card>
                                        <CardItem>
                                            <Text style={{ color: colors.primary, fontSize: normalize(14) }}>About</Text>
                                        </CardItem>
                                        <CardItem>
                                            <View>
                                                <Text style={{ fontFamily: 'Nunito-Light', lineHeight: 25, color: colors.gray, textAlign: 'justify', fontSize: normalize(12) }}>
                                                    {this.state.getProfileResponseData.description}
                                                </Text>
                                            </View>
                                        </CardItem>

                                        <CardItem bordered style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'stretch' }}>
                                            <Text style={{ color: colors.primary, alignItems: 'flex-start', fontSize: normalize(14) }}>Contact</Text>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                                <Text style={{ fontFamily: 'Nunito-Light', color: colors.gray, fontSize: normalize(12) }}>Phone</Text>
                                                <Text style={{ fontFamily: 'Nunito-Light', alignItems: 'flex-end', color: colors.black, fontSize: normalize(12) }}>{this.state.getProfileResponseData.mobile}</Text>
                                            </View>
                                            {(this.state.getProfileResponseData.website) ?
                                                (<View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                                    <Text style={{ fontFamily: 'Nunito-Light', color: colors.gray, fontSize: normalize(12) }}>Website</Text>
                                                    <Text style={{ fontFamily: 'Nunito-Light', alignItems: 'flex-end', color: colors.black, fontSize: normalize(12) }}>www.google.com</Text>
                                                </View>) : (null)
                                            }
                                            <View style={{ flexDirection: 'column', justifyContent: 'space-between', padding: 5 }}>
                                                <Text style={{ color: colors.gray, fontSize: normalize(12) }}>Address</Text>
                                                <Text style={{ fontFamily: 'Nunito-Light', alignItems: 'flex-end', color: colors.black, fontSize: normalize(12) }}>
                                                    {this.state.getProfileResponseData.address}
                                                </Text>
                                            </View>
                                        </CardItem>
                                    </Card>
                                </View>
                            </View>
                        </View>
                        <Thumbnail circle large
                            source={getUserProfileDummyUrl(this.state.getProfileResponseData.profile_picture)}
                            style={{
                                position: 'absolute',
                                top: hp('19%'),
                                left: Dimensions.get('window').width / 2 - 40,
                            }}
                        />
                    </View>
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});


const mapStateToProps = (state) => {
    return {
        error: state.ProfileOfficialR.error,
        officialProfileResponse: state.ProfileOfficialR.officialProfileResponse,
        isLoading: state.ProfileOfficialR.isLoading,
    };
};

export default connect(mapStateToProps)(FollowerProfileScreen);