import React, { Component } from 'react';
import { StyleSheet, RefreshControl, View, Text, FlatList } from 'react-native';
import { Container, Content } from 'native-base';
import ElectedOfficialsItems from './items/ElectedOfficialsItems';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import Tabs from '../index';
import { connect } from 'react-redux';
import _ from 'lodash';
import { normalize } from '../../resources';
import Loader from '../../components/Loader/Loader';
import { getProfile } from '../../utils/URLConstants';

import { getCandidateFollowAction, onClearState } from '../../actions/candidatelist-actions';

import {
  getOfficialListAction, onClearElectedOfficial
} from '../../actions/elected-official-action';

class OfficialScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      loading: true,
      getResponseData: [],
      isMy: false,
      errMsg: null,
      type: '',
      block_status: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getOfficialListApi();
      }
    );
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getOfficialListApi();      
  }

  componentWillReceiveProps = props => {
    if (_.isEmpty(props.error) === false) {
      if (props.error.status === 'error') {
        setTimeout(() => { alert(props.error.message) }, 800);
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getResponseData) === false) {
      console.log('electedResponse', props.getResponseData)
      if (props.getResponseData.status === 'success') {
        this.setState({ 
          refreshing: true,
          getResponseData: props.getResponseData.data, 
          errMsg: (props.getResponseData.data.length === 0)? 'No Result Found' : null, 
          loading: false 
        })
        console.log('officialdata resp', props.getResponseData)
      } else {
        this.setState({ 
          refreshing: true,
          loading: false,
          errMsg: (props.getResponseData.data.length === 0)? 'No Result Found' : null
        })
        // setTimeout(() => {
        //   alert(props.getResponseData.message)  
        // }, 800); 
      }
    }

    if (_.isEmpty(props.candidateFollowResponse) === false && props.errorCandidate === '') {
      if (props.candidateFollowResponse.status === 'success') {
        this.setState({ loading: false })
        console.log(" componentWillReceiveProps candidateFollowResponse", props.candidateFollowResponse)
      //  setTimeout(() => { alert(props.candidateFollowResponse.message) }, 800);
        this.getOfficialListApi();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.candidateFollowResponse.message) }, 800);
      }
    }
  }

  componentWillUnmount(){
      this.willFocusSubscription.remove();
  }

  /**
   * Api for Official Profiles
   */
  getOfficialListApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      console.log('value got', value);
      if(value.data.type === 'citizen'){
        this.setState({ isMy: true, type: value.data.type, block_status: parseInt(value.data.block_status)})
      }
      if (!_.isEmpty(value)) {
        console.log('value got2');
        this.props.dispatch(onClearElectedOfficial());
        this.props.dispatch(onClearState());
        this.props.dispatch(getOfficialListAction(value.data.token));
      }
    }).catch((error) => {
      console.log(error.message);
    })
  }

  /**
   * Move to Candidate Profile View
   */
  goCandidateProfile = (politician_id) => {
    console.log("clicked")
    this.props.navigation.navigate('CandidateProfileScreen', { politician_id: politician_id, isMy: this.state.isMy });
  }

  /**
   * Follow Politician Profile
   */
  followPolitician = (user_id) => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearElectedOfficial());
        this.props.dispatch(onClearState());
        this.props.dispatch(getCandidateFollowAction(value.data.token, { user_id: user_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }
  

  render() {
    return (
      <Container style={{ flex: 1 }}>
        <View style={{ flex: 9 }}>
          <HeaderBackNativeBase
            containerStyles={{ backgroundColor: '#f6f7f9' }}
            leftIcon={require('../../resources/images/Home_page/Menu_icon/Menu-icon.png')}
            onPressLeft={() => this.props.navigation.openDrawer()}
            title='Elected Officials'
          />
          {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
          
            <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
              <FlatList
                data={this.state.getResponseData}
                renderItem={({ item }) => <ElectedOfficialsItems rowItem={item} onPress={() => { this.goCandidateProfile(item.politician_id) }} onPressFollow={() => { this.followPolitician(item.politician_id) }} />}
                refreshControl={
                  <RefreshControl
                    refreshing={!this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
             />
            </View>
        </View>
        <Tabs
          activeTab={2}
          onPressHome={() => { this.props.navigation.navigate('HomeScreen') }}
          onPressCandidate={() => { this.props.navigation.navigate('CandidateScreen') }}
          onPressIssue={() => { this.props.navigation.navigate('IssueIdeaScreen') }}
          onPressPolls={() => { this.props.navigation.navigate('PollScreen',{ type: this.state.type, block_status: this.state.block_status }) }}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5FCFF',
    margin: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    error: state.ElectedOfficialR.error,
    getResponseData: state.ElectedOfficialR.getResponseData,
    isLoading: state.ElectedOfficialR.isLoading,
    errorCandidate: state.CandidateListR.error,
    candidateFollowResponse: state.CandidateListR.candidateFollowResponse
  };
};

export default connect(mapStateToProps)(OfficialScreen);
