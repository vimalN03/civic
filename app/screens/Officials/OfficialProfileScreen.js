import React, { Component } from 'react';
import { StyleSheet, View, Dimensions, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { Container, Text, Thumbnail, Icon, Card, CardItem } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, dimens, hp } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import Share, {ShareSheet, Button} from 'react-native-share';


class OfficialProfileScreen extends Component {

    constructor(props) {
        super(props);
        const { isMy, user_id } = props.navigation.state.params
        this.state = {
            userId: user_id,
            type: '',
            isMy: (isMy) ? true : false,
            loading: false,
            getProfileResponseData: { total_followers: 0 }
        }

        this.shareOptions = {
            title: "Civic",
            message: "Pasta on 20% discount, valid for the rest of time!",
            url: "http://facebook.github.io/react-native/",
            subject: "Civic" //  for email
        }
    }

    componentDidMount() {
        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.getUserprofileApi();
          }
        );
    }

    getUserprofileApi = () => {
        getProfile().then((value) => {
            console.log('value got', value);
            if (!_.isEmpty(value)) {
                console.log('value got2')
                this.setState({ type: value.data.type, getProfileResponseData: value.data })
            }
        }).catch((error) => {
            console.log(error.message);
        })
    }

   
    componentWillUnmount(){
        this.willFocusSubscription.remove();
    }
    
    /**
   * View User Activities like Profile, Contribution and Projects
   */
    viewActivities = (politician_id) => {
        this.props.navigation.navigate('CandidateProfileScreen', { politician_id: politician_id, isMy: true });
    }

    _share = (type) => {
        Share.shareSingle(Object.assign({},this.shareOptions, {
            "social": type
        })).catch(err => console.log(err));
    }

    render() {
        return (
            <Container>
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        <View style={{ height: hp('25%'), backgroundColor: 'grey' }}>
                            <ImageBackground source={require('../../resources/images/User_Profile/User-profile-Background.png')} style={{ width: '100%', height: '100%' }}>
                                <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
                                    leftIcon={require('../../resources/images/User_Profile/left-arrow/left-arrow@48.png')}
                                    onPressLeft={() => { this.props.navigation.goBack() }}
                                    title={this.state.getProfileResponseData.name} titleStyle={{ paddingLeft: 5, color: colors.white, fontSize: normalize(dimens.headerTitle) }}
                                    rightIcon={require('../../resources/images/User_Profile/Edit_icon/Edit-icon.png')}
                                    onPressRight={() => { this.props.navigation.navigate('ProfileScreenEdit', { profileData: this.state.getProfileResponseData }) }}
                                    rightVisibility={this.state.isMy} />
                            </ImageBackground>
                        </View>
                        {(this.state.loading) ? <Loader /> : null}
                        <View style={{ flexGrow: 1, backgroundColor: 'white', alignItems: 'center' }}>
                            <View style={{ marginTop: 45, flexDirection: 'column' }}>

                                <View style={{ flexDirection: 'row', width: Dimensions.get('window').width, alignItems: 'center' }}>
                                    <View style={{ flex: 1, alignItems: 'center' }}>
                                        {(this.state.getProfileResponseData.type == 'elected_official') ?
                                            (<TouchableOpacity onPress={() => { this.viewActivities(this.state.getProfileResponseData.id) }}>
                                                <View style={{ backgroundColor: colors.primary, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                                    <Text style={{ fontFamily: 'Nunito-Light', color: colors.white, paddingLeft: 5, paddingRight: 5, fontSize: normalize(12), alignItems: 'center' }}>
                                                        {'My Activities'}
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>) : (null)
                                        }
                                        <View style={{ marginTop: 5, backgroundColor: colors.primary, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                           <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FollowerScreen',{user_id: this.state.userId})}}>
                                            <Text style={{ fontFamily: 'Nunito-Light', color: colors.white, paddingLeft: 5, paddingRight: 5, fontSize: normalize(12) }}>
                                                {`Followers(${this.state.getProfileResponseData.total_followers})`}
                                            </Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', marginTop: 5 }}>
                                        <Text style={{ fontSize: normalize(18), textAlign: 'center' }}>{this.state.getProfileResponseData.name}</Text>
                                        <TouchableOpacity onPress={()=>{Share.open(this.shareOptions)}}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', padding: 5 }}>
                                            <Icon name='share' style={{ alignItems: 'center', color: colors.primary, fontSize: normalize(14) }} />
                                            <Text style={{ alignItems: 'center', color: colors.primary, marginLeft: 5, fontSize: normalize(12) }}>Share Profile</Text>
                                        </View>
                                        </TouchableOpacity>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                                        <Text style={{ color: colors.gray }}>Follow on</Text>
                                        <View style={{ flexDirection: 'row' }}>
                                        <TouchableOpacity onPress={() => this._share('facebook')}>
                                        <Thumbnail small style={{ margin: 5, width: 25, height: 25 }} source={require('../../resources/images/Candidate_Profile/logo-facebook/logo-facebook.png')} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this._share('twitter')}>
                                        <Thumbnail small style={{ margin: 5, width: 25, height: 25 }} source={require('../../resources/images/Candidate_Profile/Twitterbird/Twitterbird.png')} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this._share('googleplus')}>
                                        <Thumbnail small style={{ margin: 5, width: 25, height: 25 }} source={require('../../resources/images/Candidate_Profile/Google_Plus_logo/Google_Plus.png')} />
                                    </TouchableOpacity>
                                        </View>
                                    </View>
                                </View>


                                <View style={{ padding: 10, margin: 5 }}>
                                    <Card>
                                        
                                    {(this.state.type === 'citizen')? null :
                                        <View>
                                            <CardItem>
                                                <Text style={{ color: colors.primary, fontSize: normalize(14) }}>About</Text>
                                            </CardItem>
                                            <CardItem>
                                                <View>
                                                    <Text style={{ fontFamily: 'Nunito-Light', lineHeight: 25, color: colors.gray, textAlign: 'justify', fontSize: normalize(12) }}>
                                                        {this.state.getProfileResponseData.description}
                                                    </Text>
                                                </View>
                                            </CardItem>
                                        </View>
                                    }
                                        <CardItem bordered style={{ flexDirection: 'column', justifyContent: 'center', alignItems: 'stretch' }}>
                                            <Text style={{ color: colors.primary, alignItems: 'flex-start', fontSize: normalize(14) }}>Contact</Text>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                                <Text style={{ fontFamily: 'Nunito-Light', color: colors.gray, fontSize: normalize(12) }}>Phone</Text>
                                                <Text style={{ fontFamily: 'Nunito-Light', alignItems: 'flex-end', color: colors.black, fontSize: normalize(12) }}>{this.state.getProfileResponseData.mobile}</Text>
                                            </View>
                                            {(this.state.type === 'citizen')? null :
                                                (<View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 5 }}>
                                                    <Text style={{ fontFamily: 'Nunito-Light', color: colors.gray, fontSize: normalize(12) }}>Website</Text>
                                                    <Text style={{ fontFamily: 'Nunito-Light', alignItems: 'flex-end', color: colors.black, fontSize: normalize(12) }}>{this.state.getProfileResponseData.website}</Text>
                                                </View>)
                                            }
                                            <View style={{ flexDirection: 'column', justifyContent: 'space-between', padding: 5 }}>
                                                <Text style={{ color: colors.gray, fontSize: normalize(12) }}>Address</Text>
                                                <Text style={{ fontFamily: 'Nunito-Light', alignItems: 'flex-end', color: colors.black, fontSize: normalize(12) }}>
                                                    {((this.state.getProfileResponseData.address === 'null')) ? '' : this.state.getProfileResponseData.address }
                                                </Text>
                                            </View>
                                        </CardItem>
                                    </Card>
                                </View>
                            </View>
                        </View>
                        <Thumbnail circle large
                            source={getUserProfileDummyUrl(this.state.getProfileResponseData.profile_picture)}
                            style={{
                                position: 'absolute',
                                top: hp('19%'),
                                left: Dimensions.get('window').width / 2 - 40,
                            }}
                        />
                    </View>
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});

export default (OfficialProfileScreen);