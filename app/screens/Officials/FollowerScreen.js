import React, { Component } from 'react';
import { StyleSheet, RefreshControl, View, FlatList, TouchableOpacity } from 'react-native';
import { Container, Content, ListItem, Thumbnail, Text, Left, Body, Right, Card } from 'native-base';
import { connect } from 'react-redux';
import _ from 'lodash';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import { normalize, colors } from '../../resources';
import { getFollowerListAction, getCandidateFollowAction, onClearState } from '../../actions/candidatelist-actions';
import { getProfile, getDataList, consts, getUserProfileDummyUrl } from '../../utils/URLConstants';
import Loader from '../../components/Loader/Loader';

class FollowerListScreen extends Component {

  constructor(props) {
    super(props);
    
    this.state = {
      refreshing: true,
      loading: false,
      followerListRes: [],
      candidateFollowResponse: {},
      errMsg: null,
      stateList: [],
      state_value: null,
      lgaList: [],
      lga_value: null,
    }
  
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getCandidateListApi();
      }
    );
    getDataList(consts.STATE_LIST).then((value) => {
      if (!_.isEmpty(value)) {
        console.log('sssss',value)
        this.setState({
          stateList: value.data
        })
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getCandidateListApi();      
  }

  /**
   * Get Candidate List from API
   */
  getCandidateListApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        const { user_id } = this.props.navigation.state.params;
        this.props.dispatch(getFollowerListAction(value.data.token, {user_id: user_id}));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (_.isEmpty(props.error) === false) {
      if (props.error.status === 'error') {
        setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }


    if (_.isEmpty(props.followerListResponse) === false && props.error === '') {
      if (props.followerListResponse.status === 'success') {
        console.log(" componentWillReceiveProps followerListResponse", props.followerListResponse)
        this.setState({
          refreshing: true,
          followerListRes: props.followerListResponse.data, 
          loading: false,
          errMsg: (props.followerListResponse.data.length === 0)? 'No Followers for you' : null
        })
      } else {
        this.setState({ 
          refreshing: true,
          loading: false,
          errMsg: (props.followerListResponse.data.length === 0)? 'No Followers for you' : null
        })
        // setTimeout(() => { alert(props.followerListResponse.message) }, 800);
      }
    }

    if (_.isEmpty(props.candidateFollowResponse) === false && props.error === '') {
      if (props.candidateFollowResponse.status === 'success') {
        this.setState({ loading: false })
        console.log(" componentWillReceiveProps candidateFollowResponse", props.candidateFollowResponse)
      //  setTimeout(() => { alert(props.candidateFollowResponse.message) }, 800);
        this.getCandidateListApi();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.candidateFollowResponse.message) }, 800);
      }
    }
  }

  /**
   * Follow Unfollow Politician
   */
  followPolitician = (user_id) => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getCandidateFollowAction(value.data.token, { user_id: user_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillUnmount(){
      this.willFocusSubscription.remove();
  }


  _renderItem = (rowItem) => {

      return(
        <View>
        <Card>
            <ListItem thumbnail button onPress={()=>{this.props.navigation.navigate('FollowerProfileScreen',{user_id: rowItem.item.user_id})}}>
                <Left>
                    <Thumbnail circle source={getUserProfileDummyUrl(rowItem.item.profile_picture)} />
                </Left>
                <Body>
                    <Text>{rowItem.item.name}</Text>
                    {(_.isEmpty(rowItem.item.ward) || _.isEmpty(rowItem.item.lga) || _.isEmpty(rowItem.item.states))? <Text style={{padding:8}} /> : 
                    <Text note numberOfLines={1}>{`${rowItem.item.ward}, ${rowItem.item.lga}, ${rowItem.item.states}`}</Text>
                    }
                </Body>
                {/* <Right>
                    {
                        (rowItem.item.follow === 'unfollow') ?
                            (
                                <TouchableOpacity onPress={() => { this.followPolitician(rowItem.item.politician_id)}}>
                                    <View style={{ marginTop: 5, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                        <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, paddingLeft: 10, paddingRight: 10, fontSize: normalize(10) }}>
                                            Follow
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            ) :
                            (
                                <TouchableOpacity onPress={() => { this.followPolitician(rowItem.item.politician_id)}}>
                                    <View style={{ backgroundColor: colors.primary, marginTop: 5, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                        <Text style={{ fontFamily: 'Nunito-Medium', color: colors.white, paddingLeft: 10, paddingRight: 10, fontSize: normalize(10) }}>
                                            Following
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            )
                    }
                </Right> */}
            </ListItem>
        </Card>
    </View>
      )
  }

  
  render() {
    return (
      <Container style={{ flex: 1 }}>
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        <View style={{ flex: 9 }}>
          <HeaderBackNativeBase
            containerStyles={{ backgroundColor: colors.light_silver }}
            leftIcon={require('../../resources/images/left-arrow.png')}
            onPressLeft={() => { this.props.navigation.goBack() }}
            title='Followers'
          />
            <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
              <FlatList
                data={this.state.followerListRes}
                renderItem={this._renderItem}
                refreshControl={
                  <RefreshControl
                    refreshing={!this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
              />
            </View>
        </View>
        
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
    backgroundColor:'#fff'
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: colors.very_dark_gray,
    marginBottom: 5,
  },
});


const mapStateToProps = (state) => {
  return {
    error: state.CandidateListR.error,
    isLoading: state.CandidateListR.isLoading,
    followerListResponse: state.CandidateListR.followerListResponse,
    candidateFollowResponse: state.CandidateListR.candidateFollowResponse,
  };
};

export default connect(mapStateToProps)(FollowerListScreen);
