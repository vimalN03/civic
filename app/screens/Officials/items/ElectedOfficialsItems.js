import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native';
import { ListItem, Thumbnail, Text, Left, Body, Right, Card } from 'native-base';
import { Rating } from 'react-native-ratings';
import { colors, normalize } from '../../../resources';
import { getUserProfileDummyUrl } from '../../../utils/URLConstants';


const ElectedOfficialsItems = ({
    rowItem,
    onPress,
    onPressFollow
}) => (
        <View>
            <Card>
                <ListItem thumbnail button onPress={onPress}>
                    <Left>
                        <Thumbnail circle source={getUserProfileDummyUrl(rowItem.politician_details.profile_picture)} />
                    </Left>
                    <Body>
                        <Text>{rowItem.name}</Text>
                        <Text note numberOfLines={1}>{rowItem.politician_details.address}</Text>
                        <Rating
                            type='custom'
                            startingValue={rowItem.overallrating}
                            ratingColor={colors.primary}
                            ratingBackgroundColor={colors.grayishlimegreen}
                            ratingCount={5}
                            imageSize={15}
                            readonly
                            style={{ paddingVertical: 10 }}
                        />
                    </Body>
                    <Right>
                        {
                            (rowItem.follow === 'unfollow') ?
                                (
                                    <TouchableOpacity onPress={onPressFollow}>
                                        <View style={{ marginTop: 5, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                            <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, paddingLeft: 10, paddingRight: 10, fontSize: normalize(10) }}>
                                                Follow
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                ) :
                                (
                                    <TouchableOpacity onPress={onPressFollow}>
                                        <View style={{ backgroundColor: colors.primary, marginTop: 5, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                            <Text style={{ fontFamily: 'Nunito-Medium', color: colors.white, paddingLeft: 10, paddingRight: 10, fontSize: normalize(10) }}>
                                                Following
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                        }
                    </Right>
                </ListItem>
            </Card>
        </View>
    );

ElectedOfficialsItems.prototype = {
    rowItem: PropTypes.object,
    onPress: PropTypes.func,
    onPressFollow: PropTypes.func
};

export default ElectedOfficialsItems;