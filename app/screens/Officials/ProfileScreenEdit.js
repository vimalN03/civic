import React, { Component } from 'react';
import { StyleSheet, Alert, View, Platform, Modal, ScrollView, ImageBackground, TouchableOpacity , TextInput ,TouchableHighlight} from 'react-native';
import { Container, Text, Thumbnail, Card, CardItem ,Button ,Label ,ActionSheet } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, dimens, hp } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile, getHeaderKey, getDataList, consts, getUserProfileDummyUrl } from '../../utils/URLConstants';
import ImagePicker from 'react-native-image-picker';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';
import { changeUserTypeAction, onClearChangeUserState } from '../../actions/changeUserType-action'
import MyDatePicker from '../../components/DatePicker';
import moment from 'moment';
import {
    profileUpdateApi, onClearprofileUpdate,
    getLgaStateIdAction, onClearLgaStateId,
    getUserProfileAction, onClearUserOfficial
} from '../../actions/profile-edit-action';

import {
    getWardByStateLgaAction, onClearWardByStateLgaList
  } from '../../actions/registration-actions';

 
class ProfileScreenEdit extends Component{

    constructor(props) {
        super(props);
        const { profileData } = props.navigation.state.params
        console.log("====prf",profileData)
        this.state = {
            //: (isMy) ? true : false,
            modalPopup: false,
            getProfileResponseData: profileData,
            party_name: profileData.party_name,
            designation: profileData.designation,
            // name: profileData.name,
            name: profileData.first_name,
            lastname: profileData.last_name,
            userName:profileData.username,
            mobile:profileData.mobile,
            dob: profileData.dob,
            vin: profileData.vin,            
            email:profileData.email,
            description:profileData.description,
            gender: '',
            facebook: (profileData.facebook === 'null' || profileData.facebook === null) ? '' : profileData.facebook,
            instagram: (profileData.instagram === 'null' || profileData.instagram === null) ? '' : profileData.instagram,
            twitter: (profileData.twitter === 'null' || profileData.twitter === null) ? '' : profileData.twitter,
            skype: (profileData.skype === 'null' || profileData.skype === null) ? '' : profileData.skype,
            website: (profileData.website === 'null' || profileData.website === null) ? '' : profileData.website,
            profilePicture:profileData.profile_picture,
            address:(profileData.address === 'null' || profileData.address === null) ? '' : profileData.address,
            loading: true,
            upload_image: 'Upload Profile Image',
            avatarSource: null,
            allStateList: {},
            allLgaList: {},
            allWardList: {},
            lga: profileData.LGA,
            lga_value: 'Select LGA',
            ward: profileData.ward,
            ward_value: 'Select Ward',
            state_of_residence: profileData.state,
            state_of_residence_value: 'Select State Of Residence',
            imgRef: true,
            type: null,
            position: '',
            startingDate:'',
            toDate:''
        }
        this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
    }

      componentWillMount() {
        getDataList(consts.STATE_LIST).then((value) => {
          if (!_.isEmpty(value)) {
            console.log('sssss',value)
            var arr = value.data.filter(obj => {
              return obj.id === parseInt(this.state.state_of_residence)
            });
            this.setState({ 
              state_of_residence_value: (arr.length === 0)? 'Select State of Residence' : arr[0].state,
              allStateList: value
            })
          } else {
            this.setState({ loading: false })
          }
        }).catch((error) => {
          this.setState({ loading: false })
          console.log(error.message)
        })
      }

    componentDidMount(){
      if(!_.isEmpty(this.state.state_of_residence)){
          let data = { state_id: this.state.state_of_residence }
          this.props.dispatch(getLgaStateIdAction(data));
        // let data1 = { state_id: this.state.state_of_residence, lga_id: this.state.lga }
        // this.props.dispatch(getWardByStateLgaAction(data1));
      }
      this.getCurrentDate();
      getProfile().then((value) => {
        console.log('value got', value);
        if (!_.isEmpty(value)) {
         this.setState({ type: value.data.type })
        }
      })
    }

    getUserprofileApi = () => {
      this.setState({ loading: true })
      getProfile().then((value) => {            
        if (!_.isEmpty(value)) {
            var data = {
                id: value.data.id
            }
            this.props.dispatch(getUserProfileAction(value.data.token, data));
        }
      }).catch((error) => {
          console.log(error.message)
      })
  }


    componentWillReceiveProps = (props) => {
      if (_.isEmpty(props.getLgaStateRes) === false && props.error === '') {
        console.log('111222',props.getLgaStateRes)
        if (props.getLgaStateRes.status === 'success') {
          var arr1 = props.getLgaStateRes.data.filter(obj => {
            return obj.id === parseInt(this.state.lga)
          });
          this.setState({ 
            loading: false,
            lga_value: (arr1.length === 0)? 'Select LGA' : arr1[0].local_govt,
            allLgaList: props.getLgaStateRes
          })
        }else {
          this.setState({ loading: false, allLgaList: props.getLgaStateRes })
        //  setTimeout(() => { alert(props.allLgaResponse.message) }, 800);
        }
        this.props.dispatch(onClearLgaStateId());
      }

        if (_.isEmpty(props.allWardResponse) === false && props.error === '') {
          if (props.allWardResponse.status === 'success') {
            console.log('111222',props.allWardResponse)
            var arr2 = props.allWardResponse.data.filter(obj => {
              return obj.id === parseInt(this.state.ward)
            });
            this.setState({ 
              ward_value: (arr2.length === 0)? 'Select Ward' : arr2[0].ward,
              allWardList: props.allWardResponse 
            })
          } else {
            this.setState({ loading: false, allWardList: props.allWardResponse })
            setTimeout(() => { alert(props.allWardResponse.message) }, 800);
          }
          this.setState({ loading: false })
        this.props.dispatch(onClearWardByStateLgaList());
        }

        if (_.isEmpty(props.getProfileEditResponseData) === false && props.error === '') {
          if (props.getProfileEditResponseData.status === 'success') {
            this.setState({ loading: false })    
            setTimeout(() => { 
              Alert.alert(
                '',
                props.getProfileEditResponseData.message,
                [
                  {text: 'OK', onPress: () => this.getUserprofileApi()},
                ],
                { cancelable: false }
              )
              }, 800);        
            
          } else {
            this.setState({ loading: false })
            setTimeout(() => { alert(props.getProfileEditResponseData.message) }, 800);
          }
          this.props.dispatch(onClearprofileUpdate())
        }

        if(_.isEmpty(props.changeUserStatusErr) === false){
          setTimeout(() => {
            this.setState({ loading: false, modalPopup: false })
            alert(props.changeUserStatusErr.message)
          }, 800);
          this.props.dispatch(onClearChangeUserState());
        }

        if (_.isEmpty(props.changeUserStatusResponse) === false) {
          if (props.changeUserStatusResponse.status === 'success') {
            this.setState({ loading: false, modalPopup: false })
            setTimeout(() => { 
              Alert.alert(
                '',
                props.changeUserStatusResponse.message,
                [
                  {text: 'OK', onPress: () => this.props.navigation.navigate('LoginScreen', { email: '', pwd: ''})},
                ],
                { cancelable: false }
              )
              }, 800); 
          } else {
            this.setState({ loading: false, modalPopup: false })
            setTimeout(() => { alert(props.changeUserStatusResponse.message) }, 800);
          }
          this.props.dispatch(onClearChangeUserState());
        }    
        
        if(_.isEmpty(props.getUserDetails) === false){
          if(props.getUserDetails.status === 'success'){
            this.setState({ loading: false })
            console.log('1111112222221111',props.getUserDetails)
            this.props.navigation.goBack();
          }
          this.props.dispatch(onClearUserOfficial());
        }
       
      }

      getCurrentDate = () =>{
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth()+1; //January is 0!
        var yyyy = today.getFullYear();
        if(dd<10) {
            dd = '0'+dd
        } 
        if(mm<10) {
            mm = '0'+mm
        } 
        today = mm + '/' + dd + '/' + yyyy;
        this.setState({
            startingDate: moment(today).format("YYYY/MM/DD"),
            toDate: moment(today).format("YYYY/MM/DD"),
        });
    }


      selectStatetOfResidence = () => {
        this.setState({ lga: '-1', ward: '-1', lga_value: 'Select LGA', ward_value: 'Select Ward' });
        if (!_.isEmpty(this.state.allStateList)) {
          console.log("sts",this.state.allStateList.data)
          let nameList = this.state.allStateList.data.map(function (item) {
            return item['state'];
          });
          nameList.push("Cancel")
          let CANCEL_INDEX = nameList.length - 1
          ActionSheet.show(
            {
              options: nameList,
              cancelButtonIndex: CANCEL_INDEX,
              title: "State Of Residence"
            },
            buttonIndex => {
              if (buttonIndex < this.state.allStateList.data.length) {
                this.setState({
                  state_of_residence: this.state.allStateList.data[buttonIndex].id, state_of_residence_value: nameList[buttonIndex],
                  loading: true
                })
                let data = { state_id: this.state.allStateList.data[buttonIndex].id }
                this.props.dispatch(getLgaStateIdAction(data));
              }
            }
          );
        }
      }

      selectLga = () => {
        this.setState({ ward: '-1', ward_value: 'Select Ward' });
        if (!_.isEmpty(this.state.allLgaList)) {
          console.log(this.state.allLgaList.data)
          let nameList = this.state.allLgaList.data.map(function (item) {
            return item['local_govt'];
          });
          nameList.push("Cancel")
          let CANCEL_INDEX = nameList.length - 1
          ActionSheet.show(
            {
              options: nameList,
              cancelButtonIndex: CANCEL_INDEX,
              title: "Select LGA"
            },
            buttonIndex => {
              if (buttonIndex < this.state.allLgaList.data.length) {
                this.setState({
                  lga: this.state.allLgaList.data[buttonIndex].id, lga_value: nameList[buttonIndex],
                 
                })
                // let data = { state_id: this.state.state_of_residence, lga_id: this.state.allLgaList.data[buttonIndex].id }
                // this.props.dispatch(getWardByStateLgaAction(data));
              }
            }
          );
        }
      }
    
      selectWard = () => {
        if (!_.isEmpty(this.state.allWardList)) {
          
          let nameList = this.state.allWardList.data.map(function (item) {
            return item['ward'];
          });
          nameList.push("Cancel")
          let CANCEL_INDEX = nameList.length - 1
          ActionSheet.show(
            {
              options: nameList,
              cancelButtonIndex: CANCEL_INDEX,
              title: "Select Ward"
            },
            buttonIndex => {
              if (buttonIndex < this.state.allWardList.data.length) {
                this.setState({ ward: this.state.allWardList.data[buttonIndex].id, ward_value: nameList[buttonIndex] })
              }
            }
          );
        }
      }
      
      
      changeUserStatus = () => {
        if(this.state.type === 'candidate'){
          this.setState({ loading: false, modalPopup: true })
        }else {
          this.setState({ loading: true })
          getProfile().then((value) => {
            console.log('value got', value);
              if (!_.isEmpty(value)) {
                const changeStatus = {
                  type: 'candidate',
                  position: this.state.position,
                  from_date: this.state.startingDate,
                  to_date: this.state.toDate
                };
              this.props.dispatch(changeUserTypeAction(changeStatus, value.data.token));
            }
          })
        }
      }

      onPressStatus = () => {
        if (this.state.position.trim() === '') {
          alert('Please Enter Your Position')
          return false;
        }else if (this.state.toDate <= this.state.startingDate){
          alert("To Date must be in the future");
          return false;
        }else{
          this.setState({ loading: true })
          getProfile().then((value) => {
            console.log('value got', value);
              if (!_.isEmpty(value)) {
                const changeStatus = {
                  type: 'elected_official',
                  position: this.state.position,
                  from_date: this.state.startingDate,
                  to_date: this.state.toDate
                };
              this.props.dispatch(changeUserTypeAction(changeStatus, value.data.token));
            }
          })
        }        
      }
      
      
    selectPhotoTapped=()=> {
        console.log("clc","yes")
        const options = {
          quality: 1.0,
          storageOptions: {
            skipBackup: true,
            path: 'images',
          },
        };
    
        /**
        * The first arg is the options object for customization (it can also be null or omitted for default options)
        ** The second arg is the callback which sends object: response (more info in the API Reference)
        */
        ImagePicker.showImagePicker(options, (response) => {
          console.log('Response = ', response);
    
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = {
              uri: response.uri,
              type: response.type,
              name: response.fileName
              
            };
            
                
            // You can also display the image using data:
            // let source = { uri: 'data:image/jpeg;base64,' + response.data };
    
            this.setState({
              getProfileResponseData: {profile_picture: response.uri},
              avatarSource: source,
              upload_image: response.fileName,
              imgRef: false
            });
          }
        });
    }

    componentWillUnmount() {
        ActionSheet.actionsheetInstance = null;
    }

    render() {
      
        return (
            <Container>
                <ScrollView>
                    <View style={{ flex: 1 }}>
                        <View style={{ height: hp('25%'), backgroundColor: 'grey' }}>
                            <ImageBackground source={require('../../resources/images/User_Profile/User-profile-Background.png')} style={{ width: '100%', height: '100%' }}>
                                <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
                                    leftIcon={require('../../resources/images/User_Profile/left-arrow/left-arrow@48.png')}
                                    onPressLeft={() => { this.props.navigation.goBack() }} title="Profile" 
                                    titleStyle = {{ color: colors.white, paddingLeft: 5, fontSize: normalize(dimens.headerTitle) }}
                                    rightVisibility={(this.state.type === 'citizen')? false : true} 
                                    rightIcon={(this.state.type === 'elected_official') ? 
                                      require('../../resources/images/Login-Register/User/elected.png')
                                      :
                                      (this.state.type === 'candidate') ?
                                      require('../../resources/images/Login-Register/User/Candidate.png')
                                      :null
                                    }
                                    onPressRight={this.changeUserStatus}
                                />
                                <TouchableOpacity onPress={()=>{this.selectPhotoTapped()}} style={{justifyContent: 'center', alignItems: 'center', }}>
                                    
                                {(this.state.imgRef)?
                                  <Thumbnail circle large 
                                    source={getUserProfileDummyUrl(this.state.getProfileResponseData.profile_picture)}
                                  />
                                :
                                  <Thumbnail circle large 
                                    source={{uri: this.state.getProfileResponseData.profile_picture}}
                                  />
                                }
                                </TouchableOpacity>
                            </ImageBackground>
                        </View>
                        {(this.state.loading) ? <Loader /> : null}
                        <View style={{ flexGrow: 1, backgroundColor: 'white', alignItems: 'center' }}>
                            <View style={{ marginTop: 5, flexDirection: 'column' }}>
                                <View style={{ padding: 10, margin: 5 ,flexDirection:'row' ,justifyContent: 'space-between'}}>
                                    <Card>
                                    {(this.state.type === 'citizen')? null :
                                    <React.Fragment>
                                    <CardItem style={styles.cardItemStyle}>
                                        <View style={[styles.textInputContainer]}>
                                        <TextInput
                                            returnKeyType = { "go" }
                                            style={{ flex: 1, padding: 10 }}
                                            placeholder="Party Name"
                                            placeholderTextColor={colors.light_blue}
                                            underlineColorAndroid="transparent"
                                            value={this.state.party_name}
                                            onChangeText={(text) => { this.setState({party_name: text})}} />
                                        </View>
                                    </CardItem>
                                    <CardItem style={styles.cardItemStyle}>
                                      <View style={[styles.textInputContainer]}>
                                        <TextInput
                                            returnKeyType = { "go" }
                                            style={{ flex: 1, padding: 10 }}
                                            placeholder="Current Position/status"
                                            placeholderTextColor={colors.light_blue}
                                            underlineColorAndroid="transparent"
                                            value={this.state.designation}
                                            onChangeText={(text) => { this.setState({designation: text})}} />
                                        </View>
                                    </CardItem>
                                    </React.Fragment>
                                    }
                                    <CardItem style={styles.cardItemStyle}>
                                        <View style={[styles.textInputContainer]}>
                                        <TextInput
                                            returnKeyType = { "next" }
                                            onSubmitEditing={() => { this.lastnameInput.focus(); }}
                                            blurOnSubmit={false}
                                            style={{ flex: 1, padding: 10 }}
                                            placeholder="Name"
                                            placeholderTextColor={colors.light_blue}
                                            underlineColorAndroid="transparent"
                                            value={this.state.name}
                                            onChangeText={(text) => { this.setState({name: text})}} />
                                        </View>
                                    </CardItem>

                                    <CardItem style={styles.cardItemStyle}>
                                        <View style={[styles.textInputContainer]}>
                                        <TextInput
                                            returnKeyType = { "next" }
                                            onSubmitEditing={() => { this.mobileInput.focus(); }}
                                            blurOnSubmit={false}
                                            style={{ flex: 1, padding: 10 }}
                                            placeholder="Last Name"
                                            placeholderTextColor={colors.light_blue}
                                            underlineColorAndroid="transparent"
                                            value={this.state.lastname}
                                            onChangeText={(text) => { this.setState({lastname: text})}} />
                                        </View>
                                    </CardItem>

                                    <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                              ref={(input) => { this.mobileInput = input; }}
                                              returnKeyType = { "next" }
                                              onSubmitEditing={() => { this.emailInput.focus(); }}
                                              blurOnSubmit={false}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="Phone"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.mobile}
                                              onChangeText={(text) => {this.setState({mobile: text} )}} />
                                            </View>
                                        </CardItem>

                                        <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                              ref={(input) => { this.emailInput = input; }}
                                              returnKeyType = { "next" }
                                              onSubmitEditing={() => { this.vinInput.focus(); }}
                                              blurOnSubmit={false}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="Email"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.email}
                                              onChangeText={(text) => {this.setState({email: text} )}} />
                                            </View>
                                        </CardItem>

                                        <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                              ref={(input) => { this.vinInput = input; }}
                                              returnKeyType = { "next" }
                                              onSubmitEditing={() => { this.addressInput.focus(); }}
                                              blurOnSubmit={false}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="VIN"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.vin}
                                              onChangeText={(text) => {this.setState({vin: text} )}} />
                                            </View>
                                        </CardItem>



                                        <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                            returnKeyType={'next'}
                                              ref={(input) => { this.addressInput = input; }}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="Address"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.address}
                                              onChangeText={(text) => {this.setState({address:text})}} />
                                            </View>
                                        </CardItem>
                                        
{(this.state.type === 'citizen')? null :
<View>
{/* <CardItem style={styles.cardItemStyle}>
<View style={[styles.textInputContainer,{flex:1}]}>
<RadioGroup
  size={22}
  thickness={2}
  color={colors.primary}
  selectedIndex={(this.state.gender === 'male')?0:1}
  onSelect={(index, value) => { 
  this.setState({ gender: value })
  }}
>

<RadioButton value={'male'}>
<Text>Male</Text>
</RadioButton>

<RadioButton value={'female'}>
<Text>Female</Text>
</RadioButton>
</RadioGroup>
</View>
</CardItem> */}
                                        {/* <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                              returnKeyType = { "next" }
                                              onSubmitEditing={() => { this.facebookInput.focus(); }}
                                              blurOnSubmit={false}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="About"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.description}
                                              onChangeText={(text) => {this.setState({description: text}) }} />
                                            </View>
                                        </CardItem> */}

                                        <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                              ref={(input) => { this.facebookInput = input; }}
                                              returnKeyType = { "next" }
                                              onSubmitEditing={() => { this.twitterInput.focus(); }}
                                              blurOnSubmit={false}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="facebook"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.facebook}
                                              onChangeText={(text) => {this.setState({facebook: text}) }} />
                                            </View>
                                        </CardItem>

                                        <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                              ref={(input) => { this.twitterInput = input; }}
                                              returnKeyType = { "next" }
                                              onSubmitEditing={() => { this.skypeInput.focus(); }}
                                              blurOnSubmit={false}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="twitter"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.twitter}
                                              onChangeText={(text) => { this.setState({twitter: text})}} />
                                            </View>
                                        </CardItem>

                                        <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                              ref={(input) => { this.skypeInput = input; }}
                                              returnKeyType = { "next" }
                                              onSubmitEditing={() => { this.websiteInput.focus(); }}
                                              blurOnSubmit={false}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="Skype"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.skype}
                                              onChangeText={(text) => {this.setState({skype: text}) }} />
                                            </View>
                                        </CardItem>
                                        <CardItem style={styles.cardItemStyle}>
                                            <View style={[styles.textInputContainer]}>
                                            <TextInput
                                            returnKeyType={'next'}
                                              ref={(input) => { this.websiteInput = input; }}
                                              style={{ flex: 1, padding: 10 }}
                                              placeholder="Website"
                                              placeholderTextColor={colors.light_blue}
                                              underlineColorAndroid="transparent"
                                              value={this.state.website}
                                              onChangeText={(text) => {this.setState({website: text}) }} />
                                            </View>
                                        </CardItem>
</View>
}

                                        <CardItem style={styles.cardItemStyle}>
                                        <View style={styles.textInputContainer}>
                                           <Label
                                                adjustsFontSizeToFit={true}
                                                style={{ flex: 1, padding: 10, lineHeight: 25 }}
                                                onPress={() => { this.selectStatetOfResidence() }}>
                                                {this.state.state_of_residence_value}
                                            </Label>
                                        </View>
                                        </CardItem>
                                        <CardItem style={styles.cardItemStyle}>
                                            <View style={styles.textInputContainer}>
                                                <Label
                                                    adjustsFontSizeToFit={true}
                                                    style={{ flex: 1, padding: 10, lineHeight: 25 }}
                                                    onPress={() => { this.selectLga() }}>
                                                    {this.state.lga_value}
                                                </Label>
                                            </View>
                                        </CardItem>
                                        <CardItem style={[styles.cardItemStyle,{marginBottom: 10}]}>
                                            {/* <View style={styles.textInputContainer}>
                                                <Label
                                                    adjustsFontSizeToFit={true}
                                                    style={{ flex: 1, padding: 10, lineHeight: 25 }}
                                                    onPress={() => { this.selectWard() }}>
                                                    {this.state.ward_value}
                                                </Label>
                                              </View> */}
                                          </CardItem>
                                        <Button full
                                     onPress={this.onPressSubmit}
                                        style={{
                                        borderWidth: 1,
                                        borderColor: '#018c31',
                                        borderRadius: 20,
                                        backgroundColor: '#018c31'
                            }}>
                            <Text>Update</Text>
                        </Button>
                                        
                                    </Card>
                                </View>
                            </View>
                        </View>

                    </View>
                    
                </ScrollView>
                {(this.state.modalPopup)?
                <Modal
                  transparent={true}
                  animationType={'none'}
                  visible={this.state.modalPopup}
                  onRequestClose={() => {console.log('close modal')}}>
                  <View style={styles.modalBackground}>
                    <View style={styles.activityIndicatorWrapper}>
                        
                        <View style={{ flex: 1, flexDirection: 'column', margin: 15 }}>
                                                    
                                <View style={[styles.textInputContainer,{ marginTop:15, marginBottom:12}]}>
                                  <TextInput
                                    returnKeyType = { "go" }
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Position"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    value={this.state.position}
                                    onChangeText={(text) => { this.setState({position: text})}} />
                                </View>
                                
                                <View style={[styles.textInputContainer,{marginBottom:12}]}>
                                    <MyDatePicker
                                        date={this.state.startingDate}
                                        style={{ width: '100%', paddingBottom:5 }}
                                        placeholder={'From Date'}
                                        customStyles={{
                                            dateInput: { borderColor: "transparent", borderWidth: 1, padding: 8, justifyContent: 'flex-start',
                                            alignItems: 'flex-start', },
                                            dateTouchBody: { borderColor: "transparent", borderWidth: 1 },
                                            dateIcon: { width: 22, height: 22 }
                                        }}
                                        onDateChange={(date) => { this.setState({ startingDate: date }) }}
                                    />
                                  </View>
                                  <View style={[styles.textInputContainer,{marginBottom:15}]}>
                                    <MyDatePicker
                                        date={this.state.toDate}
                                        style={{ width: '100%', paddingBottom:5 }}
                                        placeholder={'To Date'}
                                        customStyles={{
                                            dateInput: { borderColor: "transparent", borderWidth: 1, padding: 8, justifyContent: 'flex-start',
                                            alignItems: 'flex-start', },
                                            dateTouchBody: { borderColor: "transparent", borderWidth: 1 },
                                            dateIcon: { width: 22, height: 22 }
                                        }}
                                        onDateChange={(date) => { this.setState({ toDate: date }) }}
                                    />
                                </View>    
                                <View style={{ flex: 1, flexDirection:'row', justifyContent:'space-evenly'}}>
                                  <Button small
                                    onPress={()=>this.setState({ modalPopup: false })}
                                    style={{
                                      borderWidth: 1,
                                      borderColor: '#018c31',
                                      borderRadius: 20,
                                      backgroundColor: '#018c31',
                                    }}
                                  >
                                    <Text>Close</Text>
                                  </Button>  
                                  <Button small
                                      onPress={this.onPressStatus}
                                      style={{
                                        borderWidth: 1,
                                        borderColor: '#018c31',
                                        borderRadius: 20,
                                        backgroundColor: '#018c31',
                                      }}
                                    >
                                      <Text>Submit</Text>
                                </Button>
                                </View> 
                                
                            </View>
                    </View>
                  </View>
                </Modal>
                :
                null
                }
            </Container>
        );
    }

    onPressSubmit = () => {
        if (this.validateInputs()) {
//            this.setState({ loading: true })
          getProfile().then((value) => {
            console.log('value got', value);
            if (!_.isEmpty(value)) {

              var formData = new FormData();
              // formData.append("name", this.state.name);
              formData.append("first_name", this.state.name);
              formData.append("last_name", this.state.lastname);
              formData.append("username", this.state.userName);
              formData.append("email", this.state.email);
              formData.append("upload_image", this.state.avatarSource);
              formData.append("dob", this.state.dob); //2018/11/20;
              formData.append("mobile", this.state.mobile);
              formData.append("vin", this.state.vin === null || this.state.vin === undefined || this.state.vin === '' ? '' : this.state.vin);
              formData.append("state_of_residence", this.state.state_of_residence);
              formData.append("lga", this.state.lga);
              // formData.append("ward", this.state.ward);
              formData.append("ward", '');
              formData.append("address", this.state.address);
              formData.append("platform", Platform.OS);
              if(value.data.type !== 'citizen'){
                formData.append("party_name", this.state.party_name);
                formData.append("gender", '');
                formData.append("website", this.state.website);
                formData.append("facebook", this.state.facebook);
                formData.append("twitter", this.state.twitter);
                formData.append("skype", this.state.skype);
                formData.append("instagram", '');
                formData.append("designation", this.state.designation);
                formData.append("description", this.state.description);
              }
              
              console.log('===prf',formData)
              this.setState({ loading: true })
             this.props.dispatch(profileUpdateApi(formData, value.data.token));
            }
          }).catch((error) => {
            console.log(error.message);
          })
        }
    }
    
    validateInputs = () => {
      let regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
      if (this.state.name.trim() === '') {
        alert('Please Enter a Valid Name')
        return false;
      } else  if (this.state.lastname.trim() === '') {
        alert('Please Enter a Valid Last Name')
        return false;
      } else if (this.state.mobile.trim() === '') {
        alert('Please Enter a Valid Mobile Number')
        return false;
      } else if (regEmail.test(this.state.email.trim()) === false) {
        alert('Please Enter a Valid Email ID')
        return false;
      } else if (this.state.state_of_residence === '-1') {
        alert('Please Select a Valid State Of Residence')
        return false;
      } else if (this.state.lga === '-1') {
        alert('Please Select a Valid LGA')
        return false;
      } 
      // else if (this.state.ward === '-1') {
      //   alert('Please Select a Valid Ward')
      //   return false;
      // }
        return true;
     
    }
      
        
} 

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    textInputContainer: {
        flexDirection: 'row',
        marginTop: 2,
        borderWidth: 0.5,
        borderRadius: 1,
        borderColor: '#dfe6f0',
        backgroundColor: '#eff4f8'
    },
    cardItemStyle:{
      paddingBottom:-30
    },
    modalBackground: {
      flex: 1,
      alignItems: 'center',
      flexDirection: 'column',
      justifyContent: 'space-around',
      backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
      borderWidth:1.5,
      borderColor:colors.primary,
      backgroundColor: '#fff',
      height: '48%',
      width: 300,
    }
});

const mapStateToProps = (state) => {
  
    return {
      error: state.ProfileEditR.error,
      getProfileEditResponseData: state.ProfileEditR.getProfileEditResponseData,
      isLoading: state.ProfileEditR.isLoading,
      getLgaStateRes: state.ProfileEditR.getLgaStateRes,
      allWardResponse: state.RegistrationR.allWardResponse,
      changeUserStatusErr: state.ChangeUserTypeR.changeUserStatusErr,
      changeUserStatusResponse: state.ChangeUserTypeR.changeUserStatusResponse,
      getUserDetails: state.ProfileEditR.getUserDetails
    };
  };
  
  export default connect(mapStateToProps)(ProfileScreenEdit);