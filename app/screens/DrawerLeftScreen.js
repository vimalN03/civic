import PropTypes from 'prop-types';
import React, { Component, PureComponent } from 'react';
import { AsyncStorage, ScrollView, Text, View, TouchableOpacity, Image } from 'react-native';
import { Icon, Thumbnail } from 'native-base';
import { connect } from 'react-redux';

import _ from 'lodash';
import { normalize } from '../resources';
import { getLogoutAction, onClearLoginState } from '../actions/login-actions';
import Loader from '../components/Loader/Loader';
import { getProfile, getUserProfileDummyUrl } from '../utils/URLConstants';

import {
    getAllStateListAction, onClearAllStateList,
    getLgaByStateIdAction, onClearLgaByStateId,
} from '../actions/registration-actions';

import { FBLogin, FBLoginManager } from 'react-native-facebook-login';

class DrawerLeftConfig extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            userId: null,
            userImage: '',
            activeTab: 0,
            loading: false,
            name: '',
            designation: '',
            state_of_residence: null,
            state_of_residence_value: null,
            lga: null,
            lga_value: null,
            politician_id: null,
            block_status: null,
            vin: null
        }
    }

    componentDidMount() {
       
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
              
                getProfile().then((value) => {
                    if (!_.isEmpty(value)) {
                        console.log('ddddd',value)
                        this.setState({
                            loading: false,
                            userId: value.data.id,
                            block_status: parseInt(value.data.block_status),
                            name: value.data.name,
                            designation: value.data.type,
                            state_of_residence: value.data.state,
                            lga: value.data.LGA,
                            userImage: getUserProfileDummyUrl(value.data.profile_picture),
                            politician_id: (value.data.type === 'elected_official') ? value.data.id : null,
                            vin: value.data.vin
                        })
                        this.props.dispatch(getAllStateListAction());
                        let data = { state_id: value.data.state }
                        this.props.dispatch(getLgaByStateIdAction(data));
                        
                    }
                }).catch((error) => {
                    // console.log(error.message)
                })
        });
    }

    componentWillReceiveProps = (props) => {

        getProfile().then((value) => {
            if (!_.isEmpty(value)) {
                console.log('111111222',value.data)
                this.setState({
                    loading: false,
                    userId: value.data.id,
                    block_status: parseInt(value.data.block_status),
                    name: value.data.name,
                    designation: value.data.type,
                    state_of_residence: value.data.state,
                    lga: value.data.LGA,
                    userImage: getUserProfileDummyUrl(value.data.profile_picture),
                    politician_id: (value.data.type === 'elected_official') ? value.data.id : null,
                })                
            }
        }).catch((error) => {
            // console.log(error.message)
        })

        console.log("props.error.ststus====>",props.error)

        if(props.error !== '') {
            console.log("prop.error undefined");
            if (props.error.status === 'error') {
                setTimeout(() => { alert(props.error.message) }, 800);
                this.props.navigation.navigate('LoginScreen', { email: '', pwd: ''});
            }else{
                this.props.navigation.navigate('LoginScreen', { email: '', pwd: ''});
            }
            this.setState({ loading: false })
        }
        
        

        if (_.isEmpty(props.logoutResponse) === false && props.error === '') {
            console.log("logout success===>",props.logoutResponse)
            if (props.logoutResponse.status === 'success') {
                this.setState({ loading: false })
                this.props.navigation.closeDrawer();
                this.props.navigation.navigate('SplashScreen');
                // this.props.navigation.navigate('LoginScreen');
            } else {
                this.setState({ loading: false })
                setTimeout(() => { alert(props.logoutResponse.message) }, 800);
            }
            this.props.dispatch(onClearLoginState());
        } else {
            console.log("logout error");
        }

        if (_.isEmpty(props.allStateResponse) === false) {
            if (props.allStateResponse.status === 'success') {
                console.log('33');
              var arr = props.allStateResponse.data.filter(obj => {
                return obj.id === parseInt(this.state.state_of_residence)
              });
              this.setState({ 
                loading: false,
                state_of_residence_value: (arr.length === 0)? null : arr[0].state,
              })
            } else {
                this.setState({ loading: false })
                setTimeout(() => { alert(props.allStateResponse.message) }, 800);
            }
            this.props.dispatch(onClearAllStateList())
        }

        if (_.isEmpty(props.allLgaResponse) === false) {
            if (props.allLgaResponse.status === 'success') {
                console.log('44');
                var arr1 = props.allLgaResponse.data.filter(obj => {
                    return obj.id === parseInt(this.state.lga)
                });
                this.setState({ 
                        loading: false,
                        lga_value: (arr1.length === 0)? null : arr1[0].local_govt,
                })              
            } else {
                this.setState({ loading: false })
                setTimeout(() => { alert(props.allLgaResponse.message) }, 800);
            }
            this.props.dispatch(onClearLgaByStateId())
        }        
    }

    onLogout = () => {
        
        getProfile().then((value) => {
            console.log('value got', value);
            if (!_.isEmpty(value)) {
                this.setState({ loading: true })
                this.props.dispatch(getLogoutAction(value.data.token));
                // FBLoginManager().logout();
            } else {
                this.setState({ loading: false })
            }
        }).catch((error) => {
            this.setState({ loading: false })
            console.log(error.message);
        })
    }

    newsFeed = () => {
        console.log("news feed");
        this.props.navigation.navigate('NewsScreen');
    }

    /**
   * Show User Profile and set isMy = true because reuse of screen can be done 
   */
    goToProfile = () => {
        this.props.navigation.navigate('OfficialProfileScreen', { isMy: true, user_id: this.state.userId });
    }

    render() {
        console.log('tttesttttttt',this.props.activeItemKey)
        if(this.props.activeItemKey === 'Payment'){
            this.drawerStatus = true;
        }else{
            this.drawerStatus = false;
        }

        // getProfile().then((value) => {
        //     if (!_.isEmpty(value)) {
        //         console.log('ddddd',value)
        //         this.setState({
        //             loading: false,
        //             userId: value.data.id,
        //             block_status: parseInt(value.data.block_status),
        //             name: value.data.name,
        //             designation: value.data.type,
        //             state_of_residence: value.data.state,
        //             lga: value.data.LGA,
        //             userImage: getUserProfileDummyUrl(value.data.profile_picture),
        //             politician_id: (value.data.type === 'elected_official') ? value.data.id : null,
        //             vin: value.data.vin
        //         })
        //         // this.props.dispatch(getAllStateListAction());
        //         // let data = { state_id: value.data.state }
        //         // this.props.dispatch(getLgaByStateIdAction(data));
                
        //     }
        // }).catch((error) => {
        //     console.log(error.message)
        // })
        


        return (
            <View style={styles.container}>
                {(this.state.loading) ? <Loader /> : null}
                <ScrollView>
                    <View style={{ padding: 30, backgroundColor: '#019235', flexDirection: 'row' }} >
                        <TouchableOpacity onPress={() => { this.goToProfile() }}>
                            <View style={{ flex: 5, flexDirection: 'row' }}>
                                <Thumbnail medium source={this.state.userImage} />
                                <View style={{ justifyContent: 'center', alignSelf: 'center', marginLeft: 15 }}>
                                    <Text style={{ fontFamily: 'Nunito-Medium', color: '#fff', fontSize: normalize(16) }}>{this.state.name}</Text>
                                    <Text style={{ fontFamily: 'Nunito-Medium', color: '#fff', fontSize: normalize(10) }}>{`${this.state.designation}`}</Text>
                                    <Text style={{ fontFamily: 'Nunito-Medium', color: '#fff', fontSize: normalize(10) }}>{`${this.state.lga_value}, ${this.state.state_of_residence_value}`}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View>
                        {(this.props.activeItemKey === 'Home') ? null :
                            <TouchableOpacity onPress={() => {
                                this.setState({ activeTab: 0 });
                                this.props.navigation.navigate('HomeScreen', { activeTab: 0 });
                            }}
                                style={{ padding: 8 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                    <Image
                                        source={(this.state.activeTab === 0) ?
                                            require('../resources/images/SideMenu/HomeIcon/Home-icon.png')
                                            :
                                            require('../resources/images/SideMenu/HomeIcon/Home-gray-icon.png')
                                        }
                                        style={{ marginRight: 8, marginLeft: 10, width: 25, height: 25 }}
                                    />
                                    <Text style={[styles.sectionHeadingStyle, { color: (this.state.activeTab === 0) ? '#f8cb00' : '#868b91' }]}>
                                        Home
                                    </Text>
                                </View>
                            </TouchableOpacity>
                        }
                        {(this.state.block_status === 0) ? null :
                        <TouchableOpacity onPress={() => {
                            this.setState({ activeTab: 1 });
                            if(this.state.vin === null || this.state.vin === undefined || this.state.vin === '') {
                                alert("Update VIN to access this feature!")
                            } else {
                                this.props.navigation.navigate('ChatScreen');
                            }
                        }}
                            style={{ padding: 8 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={(this.state.activeTab === 1) ?
                                        require('../resources/images/SideMenu/ChatIcon/Chat-icon-Yellow.png')
                                        :
                                        require('../resources/images/SideMenu/ChatIcon/Chat-icon.png')
                                    }
                                    style={{ marginRight: 8, marginLeft: 10, width: 25, height: 25 }}
                                />
                                <Text style={[styles.sectionHeadingStyle, { color: (this.state.activeTab === 1) ? '#f8cb00' : '#868b91' }]}>
                                    Chat
                                </Text>
                            </View>
                        </TouchableOpacity>
                        }
                        {(this.state.designation === 'elected_official')? 
                        <TouchableOpacity onPress={() => {
                            this.setState({ activeTab: 2 });
                            this.props.navigation.navigate('ElectedProfileScreen', { politician_id: this.state.politician_id, isMy: true });
                        }}
                            style={{ padding: 8 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={(this.state.activeTab === 2) ?
                                        require('../resources/images/SideMenu/Profile/Menu-yellow.png')
                                        :
                                        require('../resources/images/SideMenu/Profile/Menu-gray.png')
                                    }
                                    style={{ marginRight: 8, marginLeft: 10, width: 25, height: 25 }}
                                />
                                <Text style={[styles.sectionHeadingStyle, { color: (this.state.activeTab === 2) ? '#f8cb00' : '#868b91' }]}>
                                    Elected Official Profile
                                </Text>
                            </View>
                        </TouchableOpacity> : null
                        }

                        {
                        (this.state.designation === 'citizen')? null :
                        (this.state.block_status === 0) ?
                        null :
                        <TouchableOpacity onPress={() => {
                            this.setState({ activeTab: 3 });
                            // this.props.navigation.navigate('ForumScreen');
                            if(this.state.vin === null || this.state.vin === undefined || this.state.vin === '') {
                                alert("Update VIN to access this feature!")
                            } else {
                                this.props.navigation.navigate('ForumScreen');
                            }
                        }}
                            style={{ padding: 8 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={(this.state.activeTab === 3) ?
                                        require('../resources/images/SideMenu/MyFormsIcon/Form-icon-yellow.png')
                                        :
                                        require('../resources/images/SideMenu/MyFormsIcon/My-forms-icon.png')
                                    }
                                    style={{ marginRight: 8, marginLeft: 10, width: 25, height: 25 }}
                                />
                                <Text style={[styles.sectionHeadingStyle, { color: (this.state.activeTab === 3) ? '#f8cb00' : '#868b91' }]}>
                                    My Forums
                                </Text>
                            </View>
                        </TouchableOpacity>
                        }

                        <TouchableOpacity onPress={() => {
                            this.setState({ activeTab: 4 });
                            this.props.navigation.navigate('PaymentScreen');
                        }}
                            style={{ padding: 8 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={(this.state.activeTab === 4 && this.drawerStatus == true) ?
                                        require('../resources/images/SideMenu/Payment/Payment-icon-yellow.png')
                                        :
                                        require('../resources/images/SideMenu/Payment/Payment-icon.png')
                                    }
                                    style={{ marginRight: 8, marginLeft: 10, width: 25, height: 25 }}
                                />
                                <Text style={[styles.sectionHeadingStyle, { color: (this.state.activeTab === 4 && this.drawerStatus == true) ? '#f8cb00' : '#868b91' }]}>
                                    Payment
                                </Text>
                            </View>
                        </TouchableOpacity>

                        {/* <TouchableOpacity style={{ padding: 8 }} onPress={() => this.newsFeed()}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={require('../resources/images/SideMenu/Logout/Logout-icon.png')}
                                    style={{ marginRight: 8, marginLeft: 10, width: 25, height: 25 }}
                                />
                                <Text style={[styles.sectionHeadingStyle, { color: '#868b91' }]}>
                                    News
                                </Text>
                            </View>
                        </TouchableOpacity> */}

                        {/* <TouchableOpacity onPress={() => {
                            this.setState({ activeTab: 5 });
                            // this.props.navigation.navigate('PaymentScreen');
                            this.props.navigation.navigate('NewsScreen');
                        }}
                            style={{ padding: 8 }}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={(this.state.activeTab === 5) ?
                                        require('../resources/images/SideMenu/Payment/Payment-icon-yellow.png')
                                        :
                                        require('../resources/images/SideMenu/Payment/Payment-icon.png')
                                    }
                                    style={{ marginRight: 8, marginLeft: 10, width: 25, height: 25 }}
                                />
                                <Text style={[styles.sectionHeadingStyle, { color: (this.state.activeTab === 5) ? '#f8cb00' : '#868b91' }]}>
                                    News
                                </Text>
                            </View>
                        </TouchableOpacity> */}


                        <TouchableOpacity style={{ padding: 8 }} onPress={() => this.onLogout()}>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                <Image
                                    source={require('../resources/images/SideMenu/Logout/Logout-icon.png')}
                                    style={{ marginRight: 8, marginLeft: 10, width: 25, height: 25 }}
                                />
                                <Text style={[styles.sectionHeadingStyle, { color: '#868b91' }]}>
                                    Logout
                                </Text>
                            </View>
                        </TouchableOpacity>

                    </View>
                </ScrollView>
                <View style={styles.footerContainer}>
                    <Text style={{ fontFamily: 'Nunito-Medium', color: '#868b91' }}>Version 1.0.0</Text>
                </View>
            </View>
        );
    }
}

DrawerLeftConfig.propTypes = {
    navigation: PropTypes.object
};

const styles = ({
    container: {
        flex: 1,
        backgroundColor: '#2a2f33'
    },
    navItemStyle: {
        padding: 10
    },
    navSectionStyle: {
        backgroundColor: 'lightgrey'
    },
    sectionHeadingStyle: {
        fontFamily: 'Nunito-Medium',
        paddingVertical: 10,
        paddingHorizontal: 5,
    },
    footerContainer: {
        padding: 20,
        alignSelf: 'center'
    },
    leftIcon: { padding: 10 }
})

const mapStateToProps = (state) => {
    return {
        error: state.LoginR.error,
        isLoading: state.LoginR.isLoading,
        logoutResponse: state.LoginR.logoutResponse,
        allStateResponse: state.RegistrationR.allStateResponse,
        allLgaResponse: state.RegistrationR.allLgaResponse,
    };
};

export default connect(mapStateToProps)(DrawerLeftConfig);