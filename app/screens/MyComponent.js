import React, { Component } from 'react';
import BaseComponent from './BaseComponenet';

class MyComponent extends BaseComponent {

    constructor(props) {
      super(props);
    }
  
    _getState(props) {
      return {
        foo: props.foo || {},
        things: props.bar.map((item) => { return item.doodad; })
      };
    }
  
  }