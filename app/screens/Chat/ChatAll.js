import React, { Component } from 'react';
import { TouchableOpacity, RefreshControl, FlatList,Image } from 'react-native';
import { Container, View, Content, Fab, Card, List, Icon, ListItem, Button, Left, Body, Right, Thumbnail, Text } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import { Rating } from 'react-native-ratings';
import { colors, normalize, wp, hp } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import { getChatListAllAction, onChatAllClearState } from '../../actions/chatListAll-actions';
import Loader from '../../components/Loader/Loader';

class ChatAll extends Component {

  constructor(props){
    super(props);
    this.state={
      refreshing: true,
      loading : true,
      chatListData: [],
      errMsg: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getChatListApi();    
      }
    );
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getChatListApi();      
  }

  getChatListApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onChatAllClearState());
        this.props.dispatch(getChatListAllAction(value.data.token));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onChatAllClearState());
      }
      this.setState({ loading: false })
    }
    
    if (_.isEmpty(props.getChatListAllRes) === false) {
      if(props.getChatListAllRes.status === 'success'){
        this.setState({
          refreshing: true,
          loading: false,
          chatListData: props.getChatListAllRes.data,
          errMsg: (props.getChatListAllRes.data.length === 0)? 'No Result Found' : null
        })
      }else{
        this.setState({ refreshing: true, loading: false, errMsg: (props.getChatListAllRes.data.length === 0)? 'No Result Found' : null });
        // setTimeout(() => { alert(props.getChatListAllRes.message) }, 800);
      }
    }
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
  }

  _renderItem1 = ({ item, i }) => {
    return (
      <Card>
        <List>
          <ListItem avatar button onPress={() => this.props.navigation.navigate('FollowerProfileScreen',{user_id: item.id})}>
            <Left>
              <Thumbnail circle small source={getUserProfileDummyUrl(item.politician_details.profile_picture)} />
            </Left>
            <Body>
              <Text style={{ fontFamily: 'Nunito-Medium' }}>{item.name}</Text>
              <Text numberOfLines={1} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(10) }} note>
                {item.politician_details.address}
              </Text>
                <Rating
                    type='custom'
                    startingValue={item.overallrating}
                    ratingColor={colors.primary}
                    ratingBackgroundColor={colors.grayishlimegreen}
                    ratingCount={5}
                    imageSize={15}
                    readonly
                    style={{ paddingVertical: 10 }}
                />
            </Body>
            <Right style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity onPress={()=> this.props.navigation.navigate('ChatDetailScreen', { chatData: item, page: 'all' })}>
                <Image style={{ width: 40, height: 40 }} source={require('../../resources/images/Chat-video/MessageIcon/Message-icon.png')} />
              </TouchableOpacity>
              {/* <TouchableOpacity>
                <Image style={{ width: 40, height: 40 }} source={require('../../resources/images/Chat-video/facetime-buttonIcon/facetime-button-icon.png')} />
              </TouchableOpacity> */}
            </Right>
          </ListItem>
        </List>
      </Card>
    )
  }

  render() {
    return (
      <Container>
        <HeaderBackNativeBase
          containerStyles={{ backgroundColor: '#f6f7f9', alignItems: 'center' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Select Chat User' />
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        <View style={{backgroundColor:'#fff', marginBottom: 90}}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
            <FlatList
              data={this.state.chatListData}
              renderItem={this._renderItem1}
              refreshControl={
                <RefreshControl
                  refreshing={!this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            />
        </View>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      error: state.ChatListAllR.error,
      isLoading: state.ChatListAllR.isLoading,
      getChatListAllRes: state.ChatListAllR.getChatListAllRes
  };
};

export default connect(mapStateToProps)(ChatAll);