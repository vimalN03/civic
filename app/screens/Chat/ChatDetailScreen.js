import React, { Component } from 'react';
import { Platform, RefreshControl, StyleSheet, View, TextInput, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import { Card, Icon, Thumbnail, Text } from 'native-base';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import { colors, normalize } from '../../resources';
import _ from 'lodash';
import { connect } from 'react-redux';
import { getChatConversationAction, onChatConversationClearState} from '../../actions/chatConversation-actions';
import { getChatChooseMemberAction, onChatChooseMembClearState } from '../../actions/chatChooseMember-actions';
import { getChatPostMsgAction, onChatPostMsgClearState } from '../../actions/chatPostMsg-actions';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import Loader from '../../components/Loader/Loader';

class ChatDetailScreen extends Component {

  constructor(props) {
    super(props);
    const { chatData } = props.navigation.state.params;
    
    this.state = {
      token: '',
      conv_id: null,
      user_id: 0,
      loading: true,
      chatList: chatData,
      chatDetailResponse: [],
      message: ''
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getChat = setInterval(
          () => {
            this.props.dispatch(onChatConversationClearState());
            this.getchatMembers(); 
          },
          2000
        );     
      }
    );
  }

  _onRefresh = () => {
    this.setState({ loading: false });
    this.getchatMembers();      
  }
  
  getchatMembers = () => {
    const { page } = this.props.navigation.state.params;
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        if(page === 'all'){
          var data = { "member" : this.state.chatList.politician_id };
        }
        else {
          var data = { "member" : this.state.chatList.user.id };
        }
        this.setState({ user_id: value.data.id, token: value.data.token })
        this.props.dispatch(getChatChooseMemberAction(value.data.token, data));
      } 
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
  })
  }

   
  componentWillReceiveProps = (props) => {

    if (_.isEmpty(props.chatMembererror) === false) {
        if (props.chatMembererror.status === 'error') {
            setTimeout(() => { alert(props.chatMembererror.message) }, 800);
        }
        this.setState({ loading: false })
    }

    if (_.isEmpty(props.chatMsgerror) === false) {
      if (props.chatMsgerror.status === 'error') {
          setTimeout(() => { alert(props.chatMsgerror.message) }, 800);
      }
      this.setState({ loading: false })
    }


    if(_.isEmpty(props.getchatMemberRes) === false){
        if(props.getchatMemberRes.status === 'success'){
          // console.log('====123',props.getchatMemberRes.data)
          this.setState({ conv_id: props.getchatMemberRes.data.conversation_id})
          var data = { "conversation_id" : props.getchatMemberRes.data.conversation_id };
          this.props.dispatch(getChatConversationAction(this.state.token, data));
        }else{
            this.setState({ loading: false })
            setTimeout(() => { alert(props.getchatMemberRes.message) }, 800);
        }
        this.props.dispatch(onChatChooseMembClearState());
        this.props.dispatch(onChatConversationClearState());
    }
    
    if(_.isEmpty(props.getChatConversationRes) === false){
        console.log('====123',props.getChatConversationRes.data)
        if(props.getChatConversationRes.status === 'success'){
            this.setState({
                loading: false,
                chatDetailResponse: props.getChatConversationRes.data
            })
        }else{
            this.setState({ loading: false })
            setTimeout(() => { alert(props.getChatConversationRes.message) }, 800);
        }
        this.props.dispatch(onChatConversationClearState());
    }

     
      if(_.isEmpty(props.getChatMsgRes) === false){
          
          if(props.getChatMsgRes.status === 'success'){
                this.setState({ loading: true, message: '' })
                this.getchatMembers();
                // setTimeout(() => { alert(props.getChatMsgRes.message) }, 800);
          }else{
                this.setState({ loading: false })
                setTimeout(() => { alert(props.getChatMsgRes.message) }, 800);
          }
          this.props.dispatch(onChatPostMsgClearState());
      }

  }

  componentWillUnmount(){
    clearInterval(this.getChat);
    this.willFocusSubscription.remove();
  }

  onSendChat = () => {
    const { page } = this.props.navigation.state.params;
    if(_.isEmpty(this.state.message)){
        alert('Please type Your Query')
      }else{
        this.setState({ loading: true })
        getProfile().then((value) => {
          if (!_.isEmpty(value)) {
            this.props.dispatch(onChatPostMsgClearState());
            if(page === 'all'){
              var data= {
                "conversation_id": this.state.conv_id,
                "message": this.state.message
              }
            }else{
              var data= {
                "conversation_id": this.state.chatList.id,
                "message": this.state.message
              }
            }
            this.props.dispatch(getChatPostMsgAction(value.data.token, data));
          } else {
            this.setState({ loading: false })
          }
        }).catch((error) => {
          this.setState({ loading: false })
          console.log(error.message)
        })
      }
  }

  _renderItem = ({ item, i }) => {
    
    return (
      <View style={{ flex: 1 }}>
        {(parseInt(item.user_id) !== parseInt(this.state.user_id))? 
        <View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row', padding: 8 }}>
              <Thumbnail style={{ width: 30, height: 30 }} source={require('../../resources/images/Profile/avatar.jpg')} />
              <Text style={{ fontFamily: 'Nunito-Medium', alignSelf: 'flex-start', marginTop: 5, paddingLeft: 10, color: '#3e5964' }}>{item.replied_by}</Text>
            </View>
            <Text style={{ fontFamily: 'Nunito-Medium', paddingRight: 10, alignSelf: 'flex-end', color: '#87949c', fontSize: normalize(12), bottom: 5 }}>{item.replied_at}</Text>
          </View>
          <Card style={{ flex: 1, borderRadius: 8, marginLeft: 30, marginRight: 80, alignItems: 'flex-start', backgroundColor: '#ecf1f4', padding: 10 }}>
            <Text style={{ fontFamily: 'Nunito-Medium', color: '#3d5764', fontSize: normalize(12) }}>{item.reply_content}</Text>
          </Card>
        </View>
        :
        <View style={{ marginTop: 10, }}>
          <Text style={{ fontFamily: 'Nunito-Medium', paddingRight: 10, alignSelf: 'flex-end', color: '#87949c', fontSize: normalize(12), bottom: 5 }}>{item.replied_at}</Text>
          <Card style={{ flex: 1, borderRadius: 8, marginLeft: 80, marginRight: 10, alignItems: 'flex-end', backgroundColor: colors.primary, padding: 10 }}>
            <Text style={{ fontFamily: 'Nunito-Medium', alignSelf: 'flex-start', paddingRight: 10, color: colors.white, fontSize: normalize(12) }}>{item.reply_content}</Text>
          </Card>
        </View>
        }
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <HeaderBackNativeBase
          containerStyles={{ backgroundColor: '#f6f7f9', alignItems: 'center' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Chat' />
        {(this.state.loading) ? <Loader /> : null}
        <ScrollView style={{ marginLeft: 15, marginRight: 15, marginBottom: 70, backgroundColor: '#fff' }}
          ref={ref => this.scrollView = ref}
          
          onContentSizeChange={(contentWidth, contentHeight) => {
            this.scrollView.scrollToEnd({ animated: true });
          }}>
          {(this.state.chatDetailResponse.length === 0)? null :
            <FlatList
                data={this.state.chatDetailResponse}
                renderItem={this._renderItem}
                keyExtractor={(index) => index.toString()}
                
            />
          }
        </ScrollView>

        <View style={{
          flex: 1, flexDirection: 'row', position: 'absolute', left: 0, right: 0, bottom: 0, backgroundColor: '#fff', padding: 1,
          borderTopWidth: 2, borderTopColor: '#cad0dc', justifyContent:'space-around'
        }}>
          <View style={{ flex: 5 }}>
            <TextInput
              style={{ marginLeft:8, alignItems: 'center', textAlign: 'left', padding: 2 }}
              placeholder="Type your message..."
              underlineColorAndroid='transparent'
              onChangeText={(text) => this.setState({ message: text })}
              value={this.state.message}
            />{(Platform.OS === 'ios') ? <KeyboardSpacer /> : null}
          </View>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            {/* <TouchableOpacity
              style={{ padding: 5 }}
              onPress={() => { }}>
              <Image style={{ width: 30, height: 30 }} source={require('../../resources/images/ForumsDetail/AddIcon/Add-Icon.png')} />
            </TouchableOpacity> */}
            <TouchableOpacity
              style={{ padding: 5 }}
              onPress={() => {this.onSendChat()}}>
              <Image style={{ width: 30, height: 30 }} source={require('../../resources/images/ForumsDetail/SendIcon/Send-Icon.png')} />
            </TouchableOpacity>

          </View>

        </View>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    error: state.ChatConverstionR.error,
    isLoading: state.ChatConverstionR.isLoading,
    getChatConversationRes: state.ChatConverstionR.getChatConversationRes,
    chatMsgerror: state.ChatMsgPostR.chatMsgerror,
    chatMsgisLoading: state.ChatMsgPostR.chatMsgisLoading,
    getChatMsgRes: state.ChatMsgPostR.getChatMsgRes,
    chatMembererror: state.ChatMemberR.chatMembererror,
    chatMemberisLoading: state.ChatMemberR.chatMemberisLoading,
    getchatMemberRes: state.ChatMemberR.getchatMemberRes
  };
};

export default connect(mapStateToProps)(ChatDetailScreen);