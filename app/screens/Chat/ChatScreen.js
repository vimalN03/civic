import React, { Component } from 'react';
import { TouchableOpacity, FlatList, RefreshControl, Image } from 'react-native';
import { Container, View, Content, Fab, Card, List, Icon, ListItem, Button, Left, Body, Right, Thumbnail, Text } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import { Rating } from 'react-native-ratings';
import { colors, normalize, wp, hp } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import { getChatListAction, onChatClearState } from '../../actions/chatList-actions';
import Loader from '../../components/Loader/Loader';

class ChatScreen extends Component {

  constructor(props){
    super(props);
    this.state={
      refreshing: true,
      loading : true,
      chatListData: [],
      errMsg: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getChatListApi();    
      }
    );
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getChatListApi();      
  }

  getChatListApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onChatClearState());
        this.props.dispatch(getChatListAction(value.data.token));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onChatClearState());
      }
      this.setState({ loading: false })
    }
    
    if (_.isEmpty(props.getChatListRes) === false) {
      if(props.getChatListRes.status === 'success'){
        this.setState({
          refreshing: true,
          loading: false,
          chatListData: props.getChatListRes.data,
          errMsg: (props.getChatListRes.data.length === 0)? 'No Result Found' : null
        })
      }else{
        this.setState({ refreshing: true, loading: false, errMsg: (props.getChatListRes.data.length === 0)? 'No Result Found' : null });
        // setTimeout(() => { alert(props.getChatListRes.message) }, 800);
      }
    }
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
  }

  _renderItem1 = ({ item, i }) => {
    return (
      <Card>
        <List>
          <ListItem avatar button onPress={() => this.props.navigation.navigate('FollowerProfileScreen',{user_id: item.user.id})}>
            <Left>
              <Thumbnail circle small source={getUserProfileDummyUrl(item.user.profile_picture)} />
            </Left>
            <Body>
              <Text style={{ fontFamily: 'Nunito-Medium' }}>{item.user.name}</Text>
              <Text numberOfLines={1} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(10) }} note>
                {item.user.address}
              </Text>
                <Rating
                    type='custom'
                    startingValue={item.user.overallrating}
                    ratingColor={colors.primary}
                    ratingBackgroundColor={colors.grayishlimegreen}
                    ratingCount={5}
                    imageSize={15}
                    readonly
                    style={{ paddingVertical: 10 }}
                />
            </Body>
            <Right style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <TouchableOpacity onPress={()=> this.props.navigation.navigate('ChatDetailScreen', { chatData: item, page: 'recent' })}>
                <Image style={{ width: 40, height: 40 }} source={require('../../resources/images/Chat-video/MessageIcon/Message-icon.png')} />
              </TouchableOpacity>
              {/* <TouchableOpacity>
                <Image style={{ width: 40, height: 40 }} source={require('../../resources/images/Chat-video/facetime-buttonIcon/facetime-button-icon.png')} />
              </TouchableOpacity> */}
            </Right>
          </ListItem>
        </List>
      </Card>
    )
  }

  render() {
    return (
      <Container>
        <HeaderBackNativeBase
          containerStyles={{ backgroundColor: '#f6f7f9' }}
          leftIcon={require('../../resources/images/Home_page/Menu_icon/Menu-icon.png')}
          onPressLeft={() => this.props.navigation.openDrawer()}
          title='Chat'
        />
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        <View style={{backgroundColor:'#fff', marginBottom: 90}}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
            <FlatList
              data={this.state.chatListData}
              renderItem={this._renderItem1}
              refreshControl={
                <RefreshControl
                  refreshing={!this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            />
          </View>
          <Fab
            style={{ backgroundColor: colors.primary }}
            position="bottomLeft"
            onPress={() => { this.props.navigation.navigate('ChatAll')}}>
            <Icon name="contacts" />
          </Fab>
      </Container>
    );
  }
}

const mapStateToProps = (state) => {
  return {
      error: state.ChatListR.error,
      isLoading: state.ChatListR.isLoading,
      getChatListRes: state.ChatListR.getChatListRes
  };
};

export default connect(mapStateToProps)(ChatScreen);