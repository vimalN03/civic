import React, { Component } from 'react';
import { Platform, TextInput, Dimensions, TouchableOpacity, Image, View } from 'react-native';


class BaseComponent extends Component {
    constructor(props) {
        super(props);
        this.state = this._getState(props);
    }


    _shouldUpdateState() { return true; }

    _getState(props) {
        throw new Error('Method _getState needs to be defined in derived classes of BaseComponent');
    }

    componentWillReceiveProps(nextProps) {
        if (this._shouldUpdateState(nextProps)) {
            this.setState(this._getState(nextProps));
        }
    }

}

export default BaseComponent;