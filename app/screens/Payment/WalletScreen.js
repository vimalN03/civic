import React, { Component } from 'react';
import { StyleSheet, View, TextInput, ScrollView, Platform } from 'react-native';
import { Container, Card, Content, Button, Text } from 'native-base';

import { HeaderBackNativeBase } from '../../components/Headers/index';
import { colors, strings, normalize } from '../../resources/index';

import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile } from '../../utils/URLConstants';
import Loader from '../../components/Loader/Loader';
import {
    getOfficialProfileAction, onClearElectedOfficial
} from '../../actions/official-profile-action';
import { getWalletAction, onClearState } from '../../actions/wallet-actions';

import Rave from 'react-native-rave'; 

import { ViewPager } from 'rn-viewpager';

class WalletScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            cardNumber: null,
            expiryDate: null,
            cvv: null,
            cardName: null,
            amount: '',
            walletBalance: strings.nigerian_symbol + ' 0.00',
            email: '',
            firstname: '',
            lastname: '',
            currentPosition: 0,
            polid: '',
            token: ''
        }
    }

    componentDidMount() {
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
              this.getUserprofileApi();
            }
          );
    }

    onSuccess(data) {
        console.log("success", data);
        this.setState({
            currentPosition: 0,
        })
        if(data.status === 'success') {
            // this.props.navigation.navigate('HomeScreen');
            var data = {
                payment_mode: "wallet",
                payment_type:"wallet_deposit",
                cost : this.state.amount,
                politician_id: this.state.polid,
                plan_id: ''
            }
                this.props.dispatch(getWalletAction(this.state.token, data));
        } else {

        }
    }

    onFailure(data) {
        console.log("error", data);
    }

    onClose() {
        //navigate to the desired screen on rave close    
    }

    getUserprofileApi = () => {
        this.setState({ loading: false})
        getProfile().then((value) => {
            console.log('value got', value);
            this.setState({
                amount: '',
                walletBalance: strings.nigerian_symbol +' '+value.data.wallet_balance,
                email: value.data.email,
                firstname: value.data.username,
                lastname: value.data.name,
                polid: value.data.id,
                token: value.data.token
            })
            // if (!_.isEmpty(value)) {
            //     console.log('value got211')
            //     var data = {
            //         id: value.data.id
            //     }
            //     this.props.dispatch(getOfficialProfileAction(value.data.token, data));
            // }
        }).catch((error) => {
            console.log(error.message);
        })
    }

    componentWillReceiveProps = (props) => {
        if (_.isEmpty(props.error) === false) {
            if (props.error.status === 'error') {
                setTimeout(() => { alert(props.error.message) }, 800);
                this.props.dispatch(onClearState());
            }
            this.setState({ loading: false })
        }
        
        if (_.isEmpty(props.officialProfileResponse) === false) {
            console.log('pResponse', props.officialProfileResponse)
            if (props.officialProfileResponse.status === 'success') {
                //alert(props.electedResponse.message)
                console.log('zzzzz', props.officialProfileResponse)
                this.setState({ walletBalance : strings.nigerian_symbol + ' ' + props.officialProfileResponse.data.wallet_balance, loading: false })
            } else {
                this.setState({ loading: false })
                setTimeout(() => {
                    alert(props.officialProfileResponse.message)    
                }, 800);
            }
            this.props.dispatch(onClearElectedOfficial());
        }

        if (_.isEmpty(props.walletResponse) === false && props.error === '') {
            if (props.walletResponse.status === 'success') {
                console.log(" componentWillReceiveProps walletResponse", props.walletResponse)
                this.setState({
                    loading: false
                })
                var data = {
                    id:this.state.polid
                }
                this.props.dispatch(getOfficialProfileAction(this.state.token, data));
                this.props.navigation.navigate('HomeScreen');
            } else {
                setTimeout(() => { alert(props.walletResponse.message) }, 800);
                // this.props.navigation.navigate('HomeScreen');
            }
            this.setState({ loading: false })

        }
    }

    componentWillUnmount(){
        this.willFocusSubscription.remove();
    }

    /**
   * Validate fields and Walled creation
   */
    validateWalletCreation = () => {
        // var myRegExp =  /[0-9]{4} {0,1}[0-9]{4} {0,1}[0-9]{4} {0,1}[0-9]{4}/;
        // const expiryReg = /^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{4})$/mg;
        // const cNameReg = /^[a-zA-Z.]+$/
        // var numReg = /^[0-9]*$/;
        // if (!myRegExp.test(this.state.cardNumber)) {
        //    alert('Please Enter Valid card Number');
        //    return;
        // } else if (!expiryReg.test(this.state.expiryDate)) {
        //     alert('Please Enter Valid Expiry Date');
        //     return;
        // } else if (!numReg.test(this.state.cvv)) {
        //     alert('Please Enter Valid CVV');
        //     return;
        // } else if (!cNameReg.test(this.state.cardName)) {
        //     alert('Please Enter Valid Card Name');
        //     return;
        // } else if (!numReg.test(this.state.amount)) {
        //     alert('Please Enter Valid Amount');
        //     return;
        // } else {
        //     // this.setState({ loading: true })
        //     getProfile().then((value) => {
        //         if (!_.isEmpty(value)) {
        //            alert('PayStack Payment')
        //         } 
        //     }).catch((error) => {
        //         this.setState({ loading: false })
        //         console.log(error.message)
        //     })
        // }

        console.log("amount===>",this.state.amount)
        console.log("wallwet===>",this.state.walletBalance)
        console.log("email===>",this.state.email)
        console.log("fname===>",this.state.firstname)
        console.log("lname===>",this.state.lastname)
        if(this.state.amount === '' || this.state.amount === undefined || this.state.amount === null) {
            alert("Enter amount")
        } else {
            this.setState({ currentPosition: 1})
        }
        
    }

    renderViewPagerPage = (data) => {
        if (data === 0) {
            return (
                <Content style={{ backgroundColor: '#f6f7f9' }}>
                    <View style={{ marginLeft: 15, marginRight: 15 }}>
                        <Card style={{ padding: 15 }}>
                            <View style={styles.container}>
                                <Text style={styles.instructions}>Wallet Balance</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(25), fontWeight: 'bold', color: colors.primary }}>{this.state.walletBalance}</Text>
                                </View>
                            </View>
                        </Card>
                        <Card style={{ padding: 15, marginTop: 10 }}>
                        
                            {/* <View style={styles.textInputContainer}>
                                <TextInput
                                    maxLength={16}
                                    keyboardType={'numeric'}
                                    returnKeyType='done'
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Card Number"
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({ cardNumber: text }) }}
                                />
                            </View>
                            <View style={{ flex: 1, flexDirection: 'row', marginTop: 10 }}>
                                <TextInput
                                    maxLength={7}
                                    keyboardType={'phone-pad'}
                                    returnKeyType='done'
                                    style={{
                                        flex: 1.8, padding: 10, borderWidth: 0.5, borderRadius: 1,
                                        borderColor: '#dfe6f0', backgroundColor: '#eff4f8', padding: 10
                                    }}
                                    placeholder="Expiry Date(mm/yyyy)"
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({ expiryDate: text }) }}
                                />
                                <View style={{ width: 10 }} />
                                <TextInput
                                    keyboardType={'numeric'}
                                    returnKeyType='done'
                                    maxLength={4}
                                    style={{
                                        flex: .5, padding: 10, borderWidth: 0.5, borderRadius: 1,
                                        borderColor: '#dfe6f0', backgroundColor: '#eff4f8', padding: 10
                                    }}
                                    placeholder="CVV"
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({ cvv: text }) }}
                                />
                            </View>
                            <View style={styles.textInputContainer}>
                                <TextInput
                                    style={{ flex: 1, padding: 10 }}
                                    returnKeyType='done'
                                    placeholder="Card Holder Name"
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({ cardName: text }) }}
                                />
                            </View> */}
                            <View style={styles.textInputContainer}>
                                <TextInput
                                    keyboardType={'numeric'}
                                    returnKeyType='done'
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Enter Amount"
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => { this.setState({ amount: parseInt(text) }) }}
                                />
                            </View> 
                            <Button full
                                onPress={() => { this.validateWalletCreation() }}
                                style={{
                                    marginTop: 10,
                                    borderWidth: 1,
                                    borderColor: '#018c31',
                                    borderRadius: 20,
                                    backgroundColor: '#018c31'
                                }}>
                                {/* <Text>ADD AMOUNT</Text> */}
                                <Text>ADD AMOUNT</Text>
                            </Button> 
                        </Card>

                        
                    </View>
                </Content>
            )
        }else if(data === 1) {
            return(
                <View>
                    <Card style={{ padding: 15, marginTop: 10 }}>
                        <Rave
                            amount={this.state.amount}
                            country="NG"
                            currency="NGN"
                            email={this.state.email}
                            firstname={this.state.firstname}
                            lastname={this.state.lastname}
                            // email="test@mail.com"
                            // firstname="test"
                            // lastname="ltestname"

                            publickey="FLWPUBK_TEST-2720eb06efcb7658023a22b75d55ca94-X" 
                            encryptionkey="FLWSECK_TEST512a195b5771"
                            // meta={this.state.meta}
                            meta={[{ metaname: "color", metavalue: "red" }, { metaname: "storelocation", metavalue: "ikeja" }]}
                            production={false}
                            // production={true}
                            onSuccess={res => this.onSuccess(res)}
                            onFailure={e => this.onFailure(e)}
                            onClose={e => this.onClose(e)}
                            
                            
                        />
                    </Card>
                </View>
            )
        }
    }
    

    render() {
        return (
            <Container>
                <HeaderBackNativeBase
                    containerStyles={{ backgroundColor: '#f6f7f9' }}
                    leftIcon={require('../../resources/images/Home_page/Menu_icon/Menu-icon.png')}
                    onPressLeft={() => this.props.navigation.openDrawer()}
                    title='Wallet'
                />
                {(this.state.loading) ? <Loader /> : null}
                {(Platform.OS == 'ios') ? 
                    <View>
                        <ScrollView>
                            { this.renderViewPagerPage(this.state.currentPosition) }
                        </ScrollView>
                    </View>
                :
                <ViewPager
                        style={{ flexGrow: 1 }}
                        onPageSelected={(page) => { this.setState({ currentPosition: page.position }) }}>
                        <ScrollView>
                        { this.renderViewPagerPage(this.state.currentPosition) }
                        </ScrollView>
                    </ViewPager>
                }
                {/* <Content style={{ backgroundColor: '#f6f7f9' }}>
                    <View style={{ marginLeft: 15, marginRight: 15 }}>
                        <Card style={{ padding: 15 }}>
                            <View style={styles.container}>
                                <Text style={styles.instructions}>Wallet Balance</Text>
                                <View style={{ flexDirection: 'row' }}>
                                    <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(25), fontWeight: 'bold', color: colors.primary }}>{this.state.walletBalance}</Text>
                                </View>
                            </View>
                        </Card>
                        <Card style={{ padding: 15, marginTop: 10 }}>
                             <Rave
                                amount={this.state.amount}
                                country="NG"
                                currency="NGN"
<<<<<<< HEAD
                                email={this.state.email}
                                firstname={this.state.firstname}
                                lastname={this.state.lastname}
                                publickey="FLWPUBK_TEST-2720eb06efcb7658023a22b75d55ca94-X" 
                                encryptionkey="FLWSECK_TEST512a195b5771"
                                // publickey="FLWPUBK-80f215eae880efc5e4a4a96688502041-X" 
                                // encryptionkey="c07a8c8f38e50803c695b99f"
                                meta={this.state.meta}
                                production={true}
=======
                                // email={this.state.email}
                                // firstname={this.state.firstname}
                                // lastname={this.state.lastname}
                                email="test@mail.com"
                                firstname="test"
                                lastname="ltestname"

                                publickey="FLWPUBK_TEST-2720eb06efcb7658023a22b75d55ca94-X" 
                                encryptionkey="FLWSECK_TEST512a195b5771"
                                // meta={this.state.meta}
                                meta={[{ metaname: "color", metavalue: "red" }, { metaname: "storelocation", metavalue: "ikeja" }]}
                                production={false}
                                // production={true}
>>>>>>> 46a019dc127071e708203a4d01d1939cee3e918a
                                onSuccess={res => this.onSuccess(res)}
                                onFailure={e => this.onFailure(e)}
                                onClose={e => this.onClose(e)}
                                
                                
                            />
                        </Card>
                    </View>
                </Content> */}
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#f6f7f9',
        padding: 20
    },
    instructions: {
        fontFamily: 'Nunito-Medium',
        textAlign: 'center',
        marginBottom: 10
    },
    textInputContainer: {
        fontFamily: 'Nunito-Medium',
        flexDirection: 'row',
        paddingLeft: 5,
        marginTop: 12,
        borderWidth: 0.5,
        borderRadius: 1,
        borderColor: '#dfe6f0',
        backgroundColor: '#eff4f8'
    }
});

const mapStateToProps = (state) => {
    return {
        error: state.WalletR.error,
        isLoading: state.WalletR.isLoading,
        walletResponse: state.WalletR.walletResponse,
        officialProfileResponse: state.ProfileOfficialR.officialProfileResponse,
    };
};

export default connect(mapStateToProps)(WalletScreen);