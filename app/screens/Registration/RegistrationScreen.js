import React, { Component } from 'react';
import { StyleSheet, View, ScrollView, TouchableOpacity, Image, TextInput, Platform, Alert } from 'react-native';
import { Text, Container, Textarea, Button, Icon, CheckBox, Root, Label, ActionSheet } from "native-base";
import ImagePicker from 'react-native-image-picker';
import { ViewPager, IndicatorViewPager } from 'rn-viewpager';
import StepIndicator from 'react-native-step-indicator';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';
import MyDatePicker from '../../components/DatePicker/index';
import { connect } from 'react-redux';
import moment from 'moment';
import _ from 'lodash';
import { RadioGroup, RadioButton } from 'react-native-flexi-radio-button';

import { HeaderBackNativeBase } from '../../components/Headers/index';
import { colors, normalize, hp } from '../../resources';
import Loader from '../../components/Loader/Loader';

import {
  getRegistrationAuthAction, onClearRegistrationState,
  getAllStateListAction, onClearAllStateList,
  getLgaByStateIdAction, onClearLgaByStateId,
  getWardByStateLgaAction, onClearWardByStateLgaList
} from '../../actions/registration-actions';

const secondIndicatorStyles = {
  stepIndicatorSize: 30,
  currentStepIndicatorSize: 30,
  separatorStrokeWidth: 2,
  currentStepStrokeWidth: 3,
  stepStrokeCurrentColor: '#018c31',
  stepStrokeWidth: 3,
  stepStrokeFinishedColor: '#018c31',
  stepStrokeUnFinishedColor: '#67738b',
  separatorFinishedColor: '#018c31',
  separatorUnFinishedColor: '#67738b',
  stepIndicatorFinishedColor: '#018c31',
  stepIndicatorUnFinishedColor: '#67738b',
  stepIndicatorCurrentColor: '#018c31',
  stepIndicatorLabelFontSize: normalize(14),
  currentStepIndicatorLabelFontSize: normalize(14),
  stepIndicatorLabelCurrentColor: '#67738b',
  stepIndicatorLabelFinishedColor: '#ffffff',
  stepIndicatorLabelUnFinishedColor: '#67738b',
  labelColor: '#999999',
  labelSize: 14,
  currentStepLabelColor: '#67738b'
}


const getStepIndicatorIconConfig = ({ position, stepStatus }) => {
  const iconConfig = {
    name: 'done',
    color: stepStatus === 'finished' ? '#ffffff' : '#fff',
    size: 15,
  };
  return iconConfig;
};

class RegistrationScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      allStateList: {},
      allLgaList: {},
      allWardList: {},
      currentPage: 0,
      markedDates: {},
      calendarShow: false,
      checked: false,
      email: props.navigation.state.params.email === '' ? '' : props.navigation.state.params.email,
      userName: '',
      password: '',
      confirmPassword: '',
      firstName: '',
      lastName: '',
      role: -1,
      role_value: 'Select Role',
      lga: '-1',
      lga_value: 'Select LGA',
      ward: '-1',
      ward_value: 'Select Ward',
      state_of_residence: '-1',
      state_of_residence_value: 'Select State Of Residence',
      surname: '',
      voterVerificationNumber: '',
      mobile: '',
      dob: '',
      upload_image: 'Upload Profile Image',
      avatarSource: null,
      address: '',
      party_name: '',
      position_vying : '',
      gender: '',
      selectRadio: 0,
      about: '',
      fromDate: '',
      toDate: '',
      socialemail: props.navigation.state.params.email === '' ? '' : props.navigation.state.params.email,
    }
    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
  }

  
  componentDidMount(){
    console.log("reg did mount===>",this.state.email);
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        // this.props.dispatch(onClearAllStateList())
        // this.props.dispatch(onClearLgaByStateId());
        // this.props.dispatch(onClearWardByStateLgaList());
        console.log("will focus")
        this.props.dispatch(getAllStateListAction());
      }
    );
    
  }

  componentWillReceiveProps = (props) => {
    if (_.isEmpty(props.error) === false) {
      if (props.error.status === 'error') {
        setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearRegistrationState());
      }
      this.setState({ loading: false })
    }

    if (_.isEmpty(props.registrationResponse) === false && props.error === '') {
      if (props.registrationResponse.status === 'success') {
        this.setState({ loading: false })
        setTimeout(() => { 
          Alert.alert(
            '',
           props.registrationResponse.message,
            [
              {text: 'OK', onPress: () => this.props.navigation.navigate('OtpScreen',{ mobileNo: this.state.mobile, email: this.state.email, pwd: this.state.password })},
            ],
            { cancelable: false }
          )
          }, 800);
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.registrationResponse.message) }, 800);
      }
      this.props.dispatch(onClearRegistrationState());
    }

    if (_.isEmpty(props.allStateResponse) === false && props.error === '') {
      if (props.allStateResponse.status === 'success') {
        this.setState({ allStateList: props.allStateResponse, loading: false })
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.allStateResponse.message) }, 800);
      }
      
    }

    if (_.isEmpty(props.allLgaResponse) === false && props.error === '') {
      if (props.allLgaResponse.status === 'success') {
        this.setState({ loading: false, allLgaList: props.allLgaResponse })
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.allLgaResponse.message) }, 800);
      }
      
    }
    if (_.isEmpty(props.allWardResponse) === false && props.error === '') {
      if (props.allWardResponse.status === 'success') {
        this.setState({ loading: false, allWardList: props.allWardResponse })
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.allWardResponse.message) }, 800);
      }
      
    }
  }

  selectRoleType = () => {
    var tagRoleData = [{ key: 0, value: 'Citizen' }, { key: 1, value: 'Candidate' }, { key: 2, value: 'Elected Official' }];
    if (_.isEmpty(tagRoleData) === false) {
      let nameList = tagRoleData.map(function (item) {
        return item['value'];
      });
      nameList.push("Cancel")
      console.log("name list--->",nameList);
      let CANCEL_INDEX = nameList.length - 1
      ActionSheet.show(
        {
          options: nameList,
          cancelButtonIndex: CANCEL_INDEX,
          title: "Select Role"
        },
        buttonIndex => {
          if (buttonIndex < tagRoleData.length) {
            
            this.setState({
            role: tagRoleData[buttonIndex].key,
            role_value: tagRoleData[buttonIndex].value
            })
          }
        }
      )
    }else{
    }
  }

  /**
   * Select Photo to Upload as Profile
   */
  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    /**
    * The first arg is the options object for customization (it can also be null or omitted for default options)
    ** The second arg is the callback which sends object: response (more info in the API Reference)
    */
    ImagePicker.showImagePicker(options, (response) => {
     console.log('=======',response.uri)
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        var fileExtension = response.fileName.replace(/^.*\./, '');
        var fileName = `${this.state.userName}.${fileExtension}`;
        let source = {
          uri: response.uri,
          type: `image/${fileExtension}`,
          name: fileName
        };
        console.log('======',source)
        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
          upload_image: fileName
        });
      }
    });
  }

  componentWillUnmount(){
    ActionSheet.actionsheetInstance = null;
    this.willFocusSubscription.remove();
  }

  render() {
    
    return (
      <Root>
        <Container>
          <HeaderBackNativeBase
            containerStyles={{ backgroundColor: '#fff' }}
            leftIcon={require('../../resources/images/left-arrow.png')}
            onPressLeft={() => { this.props.navigation.goBack() }}
            title='Sign up' />
            {(this.state.loading) ? <Loader /> : null}
          <View style={styles.container}>
            <View style={styles.stepIndicator}>
              <StepIndicator
                stepCount={3}
                renderStepIndicator={this.renderStepIndicator}
                customStyles={secondIndicatorStyles}
                currentPosition={this.state.currentPage}
                labels={["General Info", "More Info", "Voter Details"]} />
            </View>
            {(Platform.OS == 'ios') ? 
              <View>
                <View>
                  {this.renderViewPagerPage(this.state.currentPage)}
                </View>
              </View>
            :
            // <View>
            <ViewPager
              style={{ flexGrow: 1 }}
              ref={(viewPager) => { this.viewPager = viewPager }}
              horizontalScroll={false} 
              scrollEnabled={false}
              onPageSelected={(page) => { this.setState({ currentPage: page.position }) }}> */}
              <View>
                  {this.renderViewPagerPage(this.state.currentPage)}
              </View>
            </ViewPager>
            // </View>
            }
          </View>
        </Container>
      </Root>
    );
  }

  /**
   * Validate Page One
   */
  validatePageOne = () => {
    let regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;

    if (this.state.role === -1) {
      alert('Please Select Role Type')
      return false;
    } else if (this.state.role === 2){


        // if(this.state.fromDate.trim() === ''){
        //   alert('Please Select From Date')
        //   return false;
        // }else if(this.state.toDate.trim() === ''){
        //   alert('Please Select To Date')
        //   return false;
        // }

        var fromdate = (this.state.fromDate).split("/");
        var todate = (this.state.toDate).split("/");
       
    
        if(fromdate[1] === undefined || fromdate[2] === undefined || todate[1] === undefined || todate[2] === undefined) {
          alert('Please check fromDate and toDate. ex date format: yyyy/mm/dd');
          return false;
        } else if (fromdate[1] < 0 || fromdate[1] > 12 || todate[1] < 0 || todate[1] > 12) {
          alert("Please check your month in fromDate and toDate. it should be between 1 to 12");
          return false;
        } else if(fromdate[2] < 0 || fromdate[2] > 31 || todate[2] < 0 || todate[2] > 31) {
          alert("Please check your day in fromDate and toDate. it should be between 1 to 31");
          return false;
        } else if (this.state.toDate <= this.state.fromDate){
          alert("To Date must be in the future");
          return false;
        }
    } else if (regEmail.test(this.state.email.trim()) === false) {
      alert('Please Enter a Valid Email ID')
      return false;
    } else if (this.state.userName.trim() === '') {
      alert('Please Enter a Valid UserName')
      return false;
    } else if (this.state.password.trim() === '') {
      alert('Please Enter a Valid Password')
      return false;
    } else if (this.state.password.trim().length < 6) {
      alert('Password must be atleast 6 characters ');
      return false;
    } else if (this.state.confirmPassword.trim() === '') {
      alert("Password and Confirm Password don't match!")
      return false;
    } else if (this.state.confirmPassword.trim() !== this.state.password.trim()) {
      alert("Password and Confirm Password don't match!")
      return false;
    }
    return true;
  }

  /**
   * Validate Page Two
   */
  validatePageTwo = () => {
    if (this.state.firstName.trim() === '') {
      alert('Please Enter a Valid First Name')
      return false;
    } else  if (this.state.lastName.trim() === '') {
      alert('Please Enter a Valid Last Name')
      return false;
    }
    else if (this.state.state_of_residence === '-1') {
      alert('Please Select a Valid State Of Residence')
      return false;
    } 
    return true;
  }

  /**
   * Validate Page Three
   */
  validatePageThree = () => {
    console.log(this.state.dob);
    var dateob = (this.state.dob).split("/");
    console.log(dateob[1])

    if(dateob[1] === undefined || dateob[2] === undefined) {
      alert('Please enter proper date. ex date format: yyyy/mm/dd');
    } else if (dateob[1] < 0 || dateob[1] > 12) {
      alert("enter proper month. it should be between 1 to 12");
    } else if(dateob[2] < 0 || dateob[2] > 31) {
      alert("enter proper day. it should be between 1 to 31")
    }

    var enteredAge = this.getAge(this.state.dob);
    if (this.state.lga === '-1') {
      alert('Please Select a Valid LGA')
      return false;
    }
    // else if (this.state.ward === '-1') {
    //   alert('Please Select a Valid Ward')
    //   return false;
    // } 
    // else if (this.state.voterVerificationNumber.trim() === '') {
    //   alert('Please Enter a Valid Voter Verification Number')
    //   return false;
    // } 
    else if (this.state.mobile.trim() === '') {
      alert('Please Enter a Valid Mobile Number')
      return false;
    } else if (this.state.dob.trim() === '') {
      alert('Please Enter a Valid Date Of Birth')
      return false;
    } else if(dateob[1] === undefined || dateob[2] === undefined) {
      alert('Please enter proper date');
      return false;
    } else if (dateob[1] < 0 || dateob[1] > 12) {
      alert("enter proper month. it should be between 1 to 12");
      return false;
    } else if(dateob[2] < 0 || dateob[2] > 31) {
      alert("enter proper day. it should be between 1 to 31");
      return false;
    }
    else if (18 >= enteredAge) {
      alert('You are not 18 years.')
      return false;
    }
    else if (!this.state.checked) {
      alert('Please Accept Terms of Service & Privacy Policy')
      return false;
    }
    return true;
  }

  getAge = (dateString) => {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}

  /**
   * Call API for SignUp
   */
  onSignUpPress = () => {
    if (this.validatePageThree()) {
      this.setState({ loading: true },
        ()=>{
          this.setState({ loading: true })
        })

      console.log('onsignup', this.state.avatarSource);

      
      if(this.state.role === 0){ var roleType= 'citizen' }
      else if(this.state.role === 1){ var roleType= 'candidate' }
      else if(this.state.role === 2){ var roleType= 'elected_official' }

      
      let formData = new FormData();
      formData.append("type", roleType);
      // formData.append("name", this.state.firstName);
      formData.append("first_name", this.state.firstName);
      formData.append("last_name", this.state.lastName);
      formData.append("username", this.state.userName);
      formData.append("mobile", this.state.mobile);
      formData.append("email", this.state.email);
      formData.append("password", this.state.password);
      formData.append("vin", this.state.voterVerificationNumber === undefined || this.state.voterVerificationNumber === null || this.state.voterVerificationNumber === '' ? '' : this.state.voterVerificationNumber);
      formData.append("upload_image", this.state.avatarSource);
      formData.append("dob", this.state.dob);
      formData.append("state_of_residence", this.state.state_of_residence);
      formData.append("lga", this.state.lga);
      // formData.append("ward", this.state.ward);
      formData.append("ward",'');
      formData.append("address", this.state.address);
      formData.append("platform", Platform.OS);
      if(this.state.role !== 0){
        formData.append("party_name", this.state.party_name);
        formData.append("website", "");
        formData.append("designation", this.state.position_vying);
        formData.append("gender", this.state.gender);
        formData.append("description", this.state.about);
        formData.append("qualification", "");
      }
      if(this.state.role === 2){
        formData.append("from_date", this.state.fromDate);
        formData.append("to_date", this.state.toDate);
      }

      console.log(formData)

      this.props.dispatch(getRegistrationAuthAction(formData));
    }
  }


  /**
   * StateResidence picker
   */
  selectStatetOfResidence = () => {
    this.setState({ lga: '-1', ward: '-1', lga_value: 'Select LGA', ward_value: 'Select Ward' });
    if (!_.isEmpty(this.state.allStateList)) {
      console.log(this.state.allStateList.data)
      let nameList = this.state.allStateList.data.map(function (item) {
        return item['state'];
      });
      nameList.push("Cancel")
      let CANCEL_INDEX = nameList.length - 1
      ActionSheet.show(
        {
          options: nameList,
          cancelButtonIndex: CANCEL_INDEX,
          title: "State Of Residence"
        },
        buttonIndex => {
          if (buttonIndex < this.state.allStateList.data.length) {
            this.setState({
              state_of_residence: this.state.allStateList.data[buttonIndex].id, state_of_residence_value: nameList[buttonIndex],
              loading: true
            },()=>{
              this.setState({ loading: true })
            })
            // this.props.dispatch(onClearLgaByStateId());
            // this.props.dispatch(onClearWardByStateLgaList());
            let data = { state_id: this.state.allStateList.data[buttonIndex].id }
            this.props.dispatch(getLgaByStateIdAction(data));
          }
        }
      );
    }
  }

  /**
   * LGA picker
   */
  selectLga = () => {
    this.setState({ ward: '-1', ward_value: 'Select Ward' });
    if (!_.isEmpty(this.state.allLgaList)) {
      console.log(this.state.allLgaList.data)
      let nameList = this.state.allLgaList.data.map(function (item) {
        return item['local_govt'];
      });
      nameList.push("Cancel")
      let CANCEL_INDEX = nameList.length - 1
      ActionSheet.show(
        {
          options: nameList,
          cancelButtonIndex: CANCEL_INDEX,
          title: "Select LGA"
        },
        buttonIndex => {
          if (buttonIndex < this.state.allLgaList.data.length) {
            this.setState({
              lga: this.state.allLgaList.data[buttonIndex].id, lga_value: nameList[buttonIndex],
              loading: true
            },()=>{
              this.setState({ loading: true })
            })
            // this.props.dispatch(onClearWardByStateLgaList());
            let data = { state_id: this.state.state_of_residence, lga_id: this.state.allLgaList.data[buttonIndex].id }
            this.props.dispatch(getWardByStateLgaAction(data));
          }
        }
      );
    }
  }

  /**
   * Ward Picker
   */
  selectWard = () => {
    if (!_.isEmpty(this.state.allWardList)) {
      console.log(this.state.allWardList.data)
      let nameList = this.state.allWardList.data.map(function (item) {
        return item['ward'];
      });
      nameList.push("Cancel")
      let CANCEL_INDEX = nameList.length - 1
      ActionSheet.show(
        {
          options: nameList,
          cancelButtonIndex: CANCEL_INDEX,
          title: "Select Ward"
        },
        buttonIndex => {
          if (buttonIndex < this.state.allWardList.data.length) {
            this.setState({ ward: this.state.allWardList.data[buttonIndex].id, ward_value: nameList[buttonIndex] })
          }
        }
      );
    }
  }

  /**
   * Render Pages for signup
   */
  renderViewPagerPage = (data) => {
    if (data === 0) {
      return (
        <ScrollView ref={ref => this.listView2 = ref}
        onContentSizeChange={() => {
          this.listView2.scrollTo({y: 0})
        }}>
        <View style={styles.page}>
          <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Parliment/Parliment.png')}
              />
              <Label
                adjustsFontSizeToFit={true}
                style={{ flex: 1, height: hp('6%'), padding: 10, fontSize: normalize(12), color: colors.dark_grayish_blue }}
                onPress={() => { this.selectRoleType() }}>
                {this.state.role_value}
              </Label>
            </View>
            {(this.state.role !== 0) ?
            <React.Fragment>
              
            <View style={styles.textInputContainer}>

              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/User/User.png')}
              />
              {(this.state.role === 1) ?
              <TextInput
                // value={this.state.position_vying}
                value={this.state.role}
                style={{ flex: 1, padding: 10 }}
                // placeholder="Current position/status"
                placeholder="Aspiring position"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ position_vying: text })}
              /> : 
                <TextInput
                // value={this.state.position_vying}
                value={this.state.role}
                style={{ flex: 1, padding: 10 }}
                // placeholder="Current position/status"
                placeholder="Position"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ position_vying: text })}
              />
              }
            </View> 
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/User/User.png')}
              />
              <TextInput
                value={this.state.party_name}
                style={{ flex: 1, padding: 10 }}
                placeholder="Political party"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ party_name: text })}
              />
            </View>
            </React.Fragment>
            :null
            }

            {(this.state.role === 2)?
            <React.Fragment>
              <View style={styles.textInputContainer}>
                <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '50%', width: '10%', marginLeft: 8 }}
                source={require('../../resources/images/05b_create_poll/Calendar/Calendar.png')} />

                 <TextInput
                    returnKeyType={'done'}
                    ref={(input) => { this.toDateInput = input; }}
                    style={{ flex: 1, padding: 10 }}
                    value={this.state.fromDate}
                    placeholder="YYYY/MM/DD"
                    underlineColorAndroid="transparent"
                    onChangeText={(text) => this.setState({ fromDate: text })}
                />
              </View>

              <View style={styles.textInputContainer}>
                <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '50%', width: '10%', marginLeft: 8 }}
                source={require('../../resources/images/05b_create_poll/Calendar/Calendar.png')} />

                 <TextInput
                    returnKeyType={'done'}
                    // ref={(input) => { this.mobileInput = input; }}
                    style={{ flex: 1, padding: 10 }}
                    value={this.state.toDate}
                    placeholder="YYYY/MM/DD"
                    underlineColorAndroid="transparent"
                    onChangeText={(text) => this.setState({ toDate: text })}
                />
              </View>

            {/* <View style={styles.textInputContainer}>
               <MyDatePicker
                date={this.state.fromDate}
                style={{ width: '95%' }}
                placeholder={'From Date'}
                customStyles={{
                  dateInput: {
                    borderWidth: 0,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    paddingTop: 14,
                  },
                  dateTouchBody: { borderColor: "transparent", borderWidth: 1 },
                  dateIcon: { width: 22, height: 22, color: colors.primary }
                }}
                onDateChange={(date) => { this.setState({ fromDate: date }) }}
              /> 
            </View> 
            <View style={styles.textInputContainer}>
               <MyDatePicker
                date={this.state.toDate}
                style={{ width: '95%' }}
                placeholder={'To Date'}
                customStyles={{
                  dateInput: {
                    borderWidth: 0,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    paddingTop: 14,
                  },
                  dateTouchBody: { borderColor: "transparent", borderWidth: 1 },
                  dateIcon: { width: 22, height: 22, color: colors.primary }
                }}
                onDateChange={(date) => { this.setState({ toDate: date }) }}
              />
          </View> */}
          </React.Fragment>
  :
  null
            }
            
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Mail/Mail.png')}
              />
              <TextInput
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.userInput.focus(); }}
                blurOnSubmit={false}
                value={this.state.email}
                style={{ flex: 1, padding: 10 }}
                placeholder="Email Address"
                textContentType='emailAddress'
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ email: text })}
              />
            </View>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/User/User.png')}
              />
              <TextInput
                ref={(input) => { this.userInput = input; }}
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.pwdInput.focus(); }}
                blurOnSubmit={false}
                value={this.state.userName}
                style={{ flex: 1, padding: 10 }}
                placeholder="Username"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ userName: text })}
              />
            </View>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Password/Password.png')}
              />
              <TextInput
                ref={(input) => { this.pwdInput = input; }}
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.cpwdInput.focus(); }}
                blurOnSubmit={false}
                secureTextEntry
                value={this.state.password}
                style={{ flex: 1, padding: 10 }}
                placeholder="Password"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ password: text })}
              />
            </View>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Password/Password.png')}
              />
              <TextInput
                ref={(input) => { this.cpwdInput = input; }}
                returnKeyType={'go'}
                secureTextEntry
                value={this.state.confirmPassword}
                style={{ flex: 1, padding: 10 }}
                placeholder="Confirm Password"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ confirmPassword: text })}
              />
            </View>
                        
            <Button full
              onPress={() => {
               if (this.validatePageOne()) {
                  this.setState({ currentPage: 1 })
               }
              }}
              style={{
                marginTop: 10,
                borderWidth: 1,
                borderColor: '#018c31',
                borderRadius: 20,
                backgroundColor: '#018c31'
              }}>
              <Text>NEXT</Text>
            </Button>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: '45%', top: 19, borderTopWidth: 2, borderTopColor: '#b7b7b7' }} />
              <Text style={{ fontFamily: 'Nunito-Medium', color: '#b8b8b8', padding: 8 }}>or</Text>
              <View style={{ width: '45%', top: 19, borderTopWidth: 2, borderTopColor: '#b7b7b7' }} />
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Button iconLeft style={{
                flex: 1,
                borderWidth: 1,
                borderColor: '#365494',
                borderRadius: 20,
                backgroundColor: '#365494',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}>
                <Icon style={{ fontSize: normalize(14) }} type="FontAwesome" name="facebook" />
                <Text uppercase={false} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(9) }}>Sign in with Facebook</Text>
              </Button>
              <View style={{ width: '2%' }} />
              <Button iconLeft style={{
                flex: 1,
                borderWidth: 1,
                borderColor: '#de4b39',
                borderRadius: 20,
                backgroundColor: '#de4b39',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}>
                <Icon style={{ fontSize: normalize(14) }} type="FontAwesome" name="google" />
                <Text uppercase={false} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(9) }}>Sign in With Google</Text>
              </Button>
            </View>
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 15, marginBottom: 25 }}>
            <Text style={{ color: '#adadad' }}> Already have an account? </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('LoginScreen', { email: '', pwd: ''})
              }}>
              <Text style={{ fontFamily: 'Nunito-Medium', color: '#439f5c' }}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
        </ScrollView>
      )
    } else if (data === 1) {
      return (
        <ScrollView ref={ref => this.listView1 = ref}
        onContentSizeChange={() => {
          this.listView1.scrollTo({y: 0})
        }}>  
        <View style={styles.page}>
          <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/User/User.png')}
              />
              <TextInput
                
                returnKeyType={'go'}
                ref={(input) => { this.firstNameInput = input; }}
                value={this.state.firstName}
                style={{ flex: 1, padding: 10 }}
                placeholder="First Name"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ firstName: text })}
              />
            </View>

            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/User/User.png')}
              />
              <TextInput
                
                returnKeyType={'go'}
                ref={(input) => { this.lastNameInput = input; }}
                value={this.state.lastName}
                style={{ flex: 1, padding: 10 }}
                placeholder="Last Name"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ lastName: text })}
              />
            </View>
            {(this.state.role !== 0) ?
            <React.Fragment>
            <View style={styles.textInputContainer}>
              <RadioGroup
                  selectedIndex={this.state.selectRadio}
                  size={22}
                  thickness={2}
                  color={colors.primary}
                  onSelect={(index, value) => { 
                    this.setState({ gender: value, selectRadio: (value === 'female')? 1 : 0 })
                  }}
                >

              <RadioButton value={'male'}>
              <Text>Male</Text>
              </RadioButton>

              <RadioButton value={'female'}>
              <Text>Female</Text>
              </RadioButton>
              </RadioGroup>
            </View>    
            <View style={styles.textInputContainer}>
                            
              <Textarea 
                returnKeyType={'go'}
                placeholderTextColor={colors.light_blue}
                value={this.state.about}
                style={{ flex: 1, paddingTop: 20, paddingBottom: 10,paddingLeft: 10,paddingRight: 10 }}
                placeholder="About"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ about: text })}
              />
            </View>
            </React.Fragment>
            : null 
            }
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', padding: 10, height: '80%', width: '12%' }}
                source={require('../../resources/images/Login-Register/Ward/Ward.png')}
              />
              
              <Textarea 
              returnKeyType={'go'}
              ref={(input) => { this.addressInput = input; }}
              placeholderTextColor={colors.light_blue}
              value={this.state.address}
              style={{ flex: 1, paddingTop: 20, paddingBottom: 10,paddingLeft: 10,paddingRight: 10 }}
              placeholder="Address"
              underlineColorAndroid="transparent"
              onChangeText={(text) => this.setState({ address: text })}
                  
                />
            </View>
            <View style={styles.textInputContainer}>
              <Text numberOfLines={1}
                adjustsFontSizeToFit={true}
                style={{ flex: 1, height: hp('6%'), padding: 10, fontSize: normalize(12), color: colors.dark_grayish_blue }}>
                {this.state.upload_image}
              </Text>
              <Button style={{ backgroundColor: colors.primary, height: '100%' }} onPress={() => { this.selectPhotoTapped() }}>
                <Text style={{ fontFamily: 'Nunito-Medium' }}>Browse</Text>
              </Button>
            </View>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Flag/Flag.png')}
              />
              <Label
                adjustsFontSizeToFit={true}
                style={{ flex: 1, height: hp('6%'), padding: 10, fontSize: normalize(12), color: colors.dark_grayish_blue }}
                onPress={() => { this.selectStatetOfResidence() }}>
                {this.state.state_of_residence_value}
              </Label>
            </View>
            
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <Button iconLeft
                onPress={() => { this.setState({ currentPage: 0 }) }}
                style={{
                  marginTop: 10,
                  borderWidth: 1,
                  borderColor: '#018c31',
                  borderRadius: 5,
                  backgroundColor: '#018c31'
                }}>
                <Icon name='arrow-back' />
                <Text>Back</Text>
              </Button>
              <Button iconRight
                onPress={() => {
                 if (this.validatePageTwo()) {
                    this.setState({ currentPage: 2 })
                 }
                }}
                style={{
                  marginTop: 10,
                  borderWidth: 1,
                  borderColor: '#018c31',
                  borderRadius: 5,
                  backgroundColor: '#018c31'
                }}>
                <Text>NEXT</Text>
                <Icon name='arrow-forward' />
              </Button>
            </View>

            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: '45%', top: 19, borderTopWidth: 2, borderTopColor: '#b7b7b7' }} />
              <Text style={{ fontFamily: 'Nunito-Medium', color: '#b8b8b8', padding: 8 }}>or</Text>
              <View style={{ width: '45%', top: 19, borderTopWidth: 2, borderTopColor: '#b7b7b7' }} />
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Button iconLeft style={{
                flex: 1,
                borderWidth: 1,
                borderColor: '#365494',
                borderRadius: 20,
                backgroundColor: '#365494',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}>
                <Icon style={{ fontSize: normalize(14), }} type="FontAwesome" name="facebook" />
                <Text uppercase={false} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(9) }}>Sign in with Facebook</Text>
              </Button>
              <View style={{ width: '2%' }} />
              <Button iconLeft style={{
                flex: 1,
                borderWidth: 1,
                borderColor: '#de4b39',
                borderRadius: 20,
                backgroundColor: '#de4b39',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}>
                <Icon style={{ fontSize: normalize(14) }} type="FontAwesome" name="google" />
                <Text uppercase={false} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(9) }}>Sign in With Google</Text>
              </Button>
            </View>
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 15, marginBottom: 25 }}>
            <Text style={{ fontFamily: 'Nunito-Medium', color: '#adadad' }}> Already have an account? </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('LoginScreen', { email: '', pwd: ''})
              }}>
              <Text style={{ fontFamily: 'Nunito-Medium', color: '#439f5c' }}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
        </ScrollView>
      )
    } else if (data === 2) {
      return (
        <ScrollView ref={ref => this.listView = ref}
        onContentSizeChange={() => {
          this.listView.scrollTo({y: 0})
        }}>
        <View style={styles.page}>
          <View style={{ marginLeft: 20, marginRight: 20, marginTop: 10 }}>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Parliment/Parliment.png')}
              />
              <Label
                adjustsFontSizeToFit={true}
                style={{ flex: 1, height: hp('6%'), padding: 10, fontSize: normalize(12), color: colors.dark_grayish_blue }}
                onPress={() => { this.selectLga() }}>
                {this.state.lga_value}
              </Label>
            </View>

            {/* <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Ward/Ward.png')}
              />
              <Label
                adjustsFontSizeToFit={true}
                style={{ flex: 1, height: hp('6%'), padding: 10, fontSize: normalize(12), color: colors.dark_grayish_blue }}
                onPress={() => { this.selectWard() }}>
                {this.state.ward_value}
              </Label>
            </View> */}

            {/* <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '90%', width: '15%' }}
                source={require('../../resources/images/Login-Register/User/User.png')}
              />
              <TextInput
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.vinInput.focus(); }}
                blurOnSubmit={false}
                style={{ flex: 1, padding: 10 }}
                value={this.state.surname}
                placeholder="Surname"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ surname: text })}
              />
            </View> */}
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '70%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Voter-Id/Voter-Id.png')}
              />
              <TextInput
                returnKeyType={'next'}
                ref={(input) => { this.vinInput = input; }}
                returnKeyType = { "next" }
                onSubmitEditing={() => { this.mobileInput.focus(); }}
                blurOnSubmit={false}
                style={{ flex: 1, padding: 10 }}
                value={this.state.voterVerificationNumber}
                placeholder="Voter Verification Number"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ voterVerificationNumber: text })}
              />
            </View>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '70%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Mobile-icon/Mobile-icon.png')}
              />
              <TextInput
                returnKeyType={'next'}
                ref={(input) => { this.mobileInput = input; }}
                style={{ flex: 1, padding: 10 }}
                value={this.state.mobile}
                placeholder="Mobile Number"
                underlineColorAndroid="transparent"
                onChangeText={(text) => this.setState({ mobile: text })}
              />
            </View>
            <View style={styles.textInputContainer}>
              <Image
                resizeMode="contain"
                style={{ alignItems: 'center', height: '70%', width: '15%' }}
                source={require('../../resources/images/Login-Register/Date-Birth/Date-Birth.png')} />

                 <TextInput
                    returnKeyType={'done'}
                    ref={(input) => { this.mobileInput = input; }}
                    style={{ flex: 1, padding: 10 }}
                    value={this.state.dob}
                    placeholder="YYYY/MM/DD"
                    underlineColorAndroid="transparent"
                    onChangeText={(text) => this.setState({ dob: text })}
                />
              {/* <MyDatePicker
                date={this.state.dob}
                style={{ width: '85%' }}
                customStyles={{
                  dateInput: {
                    borderWidth: 0,
                    justifyContent: 'flex-start',
                    alignItems: 'flex-start',
                    paddingTop: 14,
                  },
                  dateTouchBody: { borderColor: "transparent", borderWidth: 1 },
                  dateIcon: { width: 22, height: 22, color: colors.primary }
                }}
                onDateChange={(date) => { this.setState({ dob: date }) }}
              /> */}
            </View>

            <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
              <CheckBox color={colors.primary}
                checked={this.state.checked}
                onPress={() => this.setState({ checked: !this.state.checked })}
              />
              <Text style={{ marginLeft: 15, fontFamily: 'Nunito-Medium', fontSize: normalize(11), color: '#717171', }}>
                Joining civic means that you're okay with our</Text>
            </View>
            <View style={{ flexDirection: 'row', marginStart: 35 }}>
              <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(11), color: '#019235', }}>{`Terms of Service `}</Text>
              <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(11), color: '#717171', }}>and</Text>
              <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(11), color: '#019235', }}>{` Privacy Policy`}</Text>
            </View>
            <Button iconLeft
              onPress={() => { this.setState({ currentPage: 1 }) }}
              style={{
                marginTop: 10,
                borderWidth: 1,
                borderColor: '#018c31',
                borderRadius: 5,
                backgroundColor: '#018c31'
              }}>
              <Icon name='arrow-back' />
              <Text>Back</Text>
            </Button>

            <Button full
              onPress={() => {
                this.onSignUpPress()
              }}
              style={{
                marginTop: 10,
                borderWidth: 1,
                borderColor: '#018c31',
                borderRadius: 20,
                backgroundColor: '#018c31'
              }}>
              <Text>Sign Up</Text>
            </Button>
            <View style={{ flexDirection: 'row' }}>
              <View style={{ width: '45%', top: 19, borderTopWidth: 2, borderTopColor: '#b7b7b7' }} />
              <Text style={{ fontFamily: 'Nunito-Medium', color: '#b8b8b8', padding: 8 }}>or</Text>
              <View style={{ width: '45%', top: 19, borderTopWidth: 2, borderTopColor: '#b7b7b7' }} />
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <Button iconLeft style={{
                flex: 1,
                borderWidth: 1,
                borderColor: '#365494',
                borderRadius: 20,
                backgroundColor: '#365494',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}>
                <Icon style={{ fontSize: normalize(14), }} type="FontAwesome" name="facebook" />
                <Text uppercase={false} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(9) }}>Sign in with Facebook</Text>
              </Button>
              <View style={{ width: '2%' }} />
              <Button iconLeft style={{
                flex: 1,
                borderWidth: 1,
                borderColor: '#de4b39',
                borderRadius: 20,
                backgroundColor: '#de4b39',
                justifyContent: 'space-around',
                alignItems: 'center'
              }}>
                <Icon style={{ fontSize: normalize(14) }} type="FontAwesome" name="google" />
                <Text uppercase={false} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(9) }}>Sign in With Google</Text>
              </Button>
            </View>
          </View>
          <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 15, marginBottom: 25 }}>
            <Text style={{ fontFamily: 'Nunito-Medium', color: '#adadad' }}> Already have an account? </Text>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('LoginScreen', { email: '', pwd: ''})
              }}>
              <Text style={{ fontFamily: 'Nunito-Medium', color: '#439f5c' }}>Login</Text>
            </TouchableOpacity>
          </View>
        </View>
        </ScrollView>
      )
    }
  }

  /**
   * Date Picker
   */
  onDayPress = (day) => {
    console.log(day)
    const d = moment(day.dateString).format("YYYY-MM-DD")
    if (!this.state.markedDates[d]) {
      const color = '#018c31'
      const markedDates = {
        [d]: {
          selected: true,
          selectedColor: color
        }
      }
      this.setState({ markedDates, calendarShow: false, dob: day.dateString })
    } else {
      const markedDates = {
        [d]: {
          ...this.state.markedDates[d],
          selected: false
        }
      }
      this.setState({ markedDates }, () => {
        const color = this.state.markedDates[d].selectedColor == '#018c31' ? '#018c31' : '#018c31'
        const markedDates = {
          [d]: {
            selected: true,
            selectedColor: color
          }
        }
        this.setState({ markedDates, calendarShow: false, dob: day.dateString })
      })
    }
  }

  /**
   * Indicator for SignUp Progress
   */
  renderStepIndicator = params => (
    <MaterialIcon {...getStepIndicatorIconConfig(params)} />
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  stepIndicator: {
    marginVertical: 2,
  },
  page: {
    flex: 1,
    padding: 10
  },
  textInputContainer: {
    flexDirection: 'row',
    paddingLeft: 5,
    marginTop: 12,
    borderWidth: 0.5,
    borderRadius: 1,
    alignItems: 'center',
    borderColor: '#dfe6f0',
    backgroundColor: '#eff4f8'
  },
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    borderWidth: 1,
    borderColor: 'transparent',
    backgroundColor: '#fff',
    height: '60%',
    width: '80%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around'
  }
});


const mapStateToProps = (state) => {
  
  return {
    error: state.RegistrationR.error,
    isLoading: state.RegistrationR.isLoading,
    registrationResponse: state.RegistrationR.registrationResponse,
    allStateResponse: state.RegistrationR.allStateResponse,
    allLgaResponse: state.RegistrationR.allLgaResponse,
    allWardResponse: state.RegistrationR.allWardResponse,
  };
};

export default connect(mapStateToProps)(RegistrationScreen);