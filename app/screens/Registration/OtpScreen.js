/**
 * Created by dungtran on 8/20/17.
 */
import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  Alert,
  Platform
} from 'react-native';
import { Button } from 'native-base';
import DeviceInfo from 'react-native-device-info';
import { HeaderBackNativeBase } from '../../components/Headers';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import _ from 'lodash';
import CodeInput from 'react-native-confirmation-code-input';
import Loader from '../../components/Loader/Loader';
import { getOtpAction, onClearOtpState } from '../../actions/optSender-action';
import { getOtpVerifyAction, onClearOtpVerifyState } from '../../actions/otpVerify-action';
import { getLoginAuthAction, onClearLoginState } from '../../actions/login-actions';

const hyt = Dimensions.get('window').height/2;

class OtpScreen extends Component {
  constructor(props) {
    super(props);
    const { params } = props.navigation.state;
    console.log('123456',params)
    this.state = {
        loading: true,
        code: null,
        mobile: '8438374626',
        Dummy_otp: null,
        // email: 'test@gmail.com',
        // password: '123456', 
        email: params.email,
        password: params.pwd,  
       // mobile: params.mobileNo
    };
  }
  
    componentDidMount() {
        
        this.willFocusSubscription = this.props.navigation.addListener(
        'willFocus',
        () => {
            const resetmobile = {
                mobile: this.state.mobile
            };
            this.props.dispatch(getOtpAction(resetmobile));
        }
        )
    }

    componentWillReceiveProps = (props) => {
        
        if(_.isEmpty(props.otpResponse) === false){
            if(props.otpResponse.status === 'success'){
                this.setState({ loading: false, Dummy_otp: props.otpResponse.data })
            }else{
                this.setState({ loading: false })
                setTimeout(() => {
                    alert(props.otpResponse.message)
                }, 800);
            }
            this.props.dispatch(onClearOtpState())
        }

        if(_.isEmpty(props.otpVerifyResponse) === false){
            if(props.otpVerifyResponse.status === 'success'){
                this.setState({ loading: false })
                const loginAuth = {
                    email: this.state.email,
                    password: this.state.password,
                    device_name: Platform.OS,
                    device_id: DeviceInfo.getUniqueID()
                };
                this.props.dispatch(getLoginAuthAction(loginAuth));
                
            }else{
                this.setState({ loading: false })
                setTimeout(() => {
                    alert(props.otpVerifyResponse.message)
                }, 800);
            }
            this.props.dispatch(onClearOtpVerifyState())
        }

        if (_.isEmpty(props.loginResponse) === false) {
            if (props.loginResponse.status === 'success') {
                this.setState({ loading: false })
                this.props.dispatch(onClearLoginState());
                const resetAction = StackActions.reset({
                    index: 0,
                    actions: [NavigationActions.navigate({ routeName: 'HomeScreen', activeTab: 0 })],
                });
                this.props.navigation.dispatch(resetAction);
            }             
        }
    }
  
    componentWillUnmount(){
        this.willFocusSubscription.remove();
    }

  _onFinishCheckingCode1(isValid ,codeTxt) {
    this.setState({ code: codeTxt, })
  }
  
  onVerifyClick = () => {
        this.setState({ loading: true })
        const verifyOtp = {
            mobile: this.state.mobile,
            otp : this.state.code
        };
        this.props.dispatch(getOtpVerifyAction(verifyOtp));
  }
    
  render() {
    
    return (
        <View>
            <HeaderBackNativeBase
                    containerStyles={{ backgroundColor: '#fff' }}
                    leftIcon={require('../../resources/images/left-arrow.png')}
                    onPressLeft={() => { this.props.navigation.goBack() }}
                    title='Otp'
                />
        {(this.state.loading) ? <Loader /> : null}
      <View style={styles.container}>
             
          <View style={styles.inputWrapper2}>
            <Text style={styles.inputLabel2}>Enter Valid OTP</Text>
            <CodeInput
                ref="codeInputRef2"
                keyboardType="numeric"
                codeLength={5}
                className='border-circle'
                compareWithCode= {this.state.Dummy_otp}
                activeColor='rgba(49, 180, 4, 1)'
                inactiveColor='rgba(49, 180, 4, 1.3)'
                autoFocus={true}
                codeInputStyle={{ fontWeight: '800' }}
                onFulfill={(isValid, code) => this._onFinishCheckingCode1(isValid, code)}
            />
            
          </View>
          <Button full
                onPress={this.onVerifyClick}
                style={{
                    marginLeft: 20,
                    marginRight: 20,
                    borderWidth: 1,
                    borderColor: '#018c31',
                    borderRadius: 20,
                    backgroundColor: '#018c31'
                }}>
                <Text style={{color:'#fff'}} >VERIFY OTP</Text>
            </Button>
      </View>
      <Text>{this.state.Dummy_otp}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

  inputWrapper2: {
    marginTop: hyt,
    paddingVertical: 50,
    paddingHorizontal: 20,
    marginBottom: 80
  },
  inputLabel2: {
    color: '#31B404',
    fontSize: 14,
    fontWeight: '800',
    textAlign: 'center'
  },
});

const mapStateToProps = (state) => {
    return {
        error: state.OtpSendR.error,
        isLoading: state.OtpSendR.isLoading,
        otpResponse: state.OtpSendR.otpResponse,
        otpVerifyResponse: state.OtpVerifyR.otpVerifyResponse,
        loginResponse: state.LoginR.loginResponse
    };
};

export default connect(mapStateToProps)(OtpScreen);