import React, {Component} from 'react';
import {View, WebView} from 'react-native';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import Loader from '../../components/Loader/Loader';

class News extends Component {
  constructor(props) {
    super(props);
    this.state = {
      newsurl : props.navigation.state.params.newsurl,
      loading: true
    }
  }
  componentDidMount() {
    console.log("news urlllll===>",this.state.newsurl);
    setTimeout(() => {
      this.setState({
        loading: false
      })
    }, 3000);
    
  }
  render() {
    return (
        <View style={{flex:1}}>
             {/* <HeaderBackNativeBase
                    containerStyles={{ backgroundColor: '#f6f7f9' }}
                    leftIcon={require('../../resources/images/Home_page/Menu_icon/Menu-icon.png')}
                    onPressLeft={() => this.props.navigation.openDrawer()}
                    title='News'
                /> */}

            <HeaderBackNativeBase
              containerStyles={{ backgroundColor: '#f6f7f9' }}
              leftIcon={require('../../resources/images/left-arrow.png')}
              onPressLeft={() => {
                  this.props.navigation.navigate('HomeScreen')
              }}
            title='News' />

            {(this.state.loading) ? <Loader /> : null}

            <WebView
                source={{uri: this.state.newsurl}}
                // style={{marginTop: 60}}
            />
        </View>

    );
  }
}

export default News;