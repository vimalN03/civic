import React, { Component } from 'react';
import { Platform, TextInput, Dimensions, TouchableOpacity, Alert, Image, View } from 'react-native';
import { Container, Icon, Button, Text, Content } from 'native-base';
import DeviceInfo from 'react-native-device-info';
import { NavigationActions, StackActions } from 'react-navigation';
import { connect } from 'react-redux';
import _ from 'lodash';

import { getLoginAuthAction, onClearLoginState } from '../../actions/login-actions';
import { normalize } from '../../resources';
import Loader from '../../components/Loader/Loader';
import { getProfile,getNetInfo  } from '../../utils/URLConstants';
import {
    getOfficialProfileAction, onClearElectedOfficial
  } from '../../actions/official-profile-action';
  import { getGoogleAction, onClearState,getFbAction } from '../../actions/social-action';


import { FBLogin, FBLoginManager } from 'react-native-facebook-login';
import { GoogleSignin, GoogleSigninButton, statusCodes } from 'react-native-google-signin';

const dimensions = Dimensions.get('window');
const imageWidth = dimensions.width;

class LoginScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: false,
            email: '',
            password: '',
            socialemail: ''
        }    
    }

    componentDidMount() {
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
              this.getUserprofileApi();
              this.configureGoogleSignIn();
            }
          );
      }
 
      getUserprofileApi = () => {
        getProfile().then((value) => {
            console.log("value====>",value)
            if (!_.isEmpty(value)) {
                var data = {
                    id: value.data.id
                }
                this.props.dispatch(getOfficialProfileAction(value.data.token, data));
            }
        }).catch((error) => {
            console.log(error.message)
        })
    }

    configureGoogleSignIn() {
        GoogleSignin.configure({
          webClientId: '773294256259-tkq5po380re3uutr4fa73nfs8q8d727c.apps.googleusercontent.com',
          offlineAccess: false,
          forceConsentPrompt: true,
          iosClientId: '773294256259-igiq2oqhlt2t8guk8vl3fh5jih1prjdf.apps.googleusercontent.com'
        });
      }

    componentWillReceiveProps = (props) => {
        if (_.isEmpty(props.error) === false && props.isLoggedIn === false) {
            if (props.error.status === 'error') {
                // alert('error');
                setTimeout(() => { alert(props.error.message) }, 800);
                this.props.dispatch(onClearLoginState());
            }
            this.setState({ loading: false })
        }
        if (_.isEmpty(props.loginResponse) === false && props.error === '') {
            // alert('login response')
            console.log("login resposense====>",props.loginResponse);
            if (props.loginResponse.status === 'success') {
                if (props.isLoggedIn === true) {
                    this.setState({ loading: false })
                    var data = {
                        id: props.loginResponse.data.profile.id
                    }
                    this.props.dispatch(getOfficialProfileAction(props.loginResponse.data.token, data));                    
                }
            } else {
                this.setState({ loading: false })
                setTimeout(() => { alert(props.loginResponse.message) }, 800);
            }
            this.props.dispatch(onClearLoginState());
        }

        if (_.isEmpty(props.officialProfileResponse) === false) {
            // alert('official profile response')
            console.log('pResponse', props.officialProfileResponse)
            if (props.officialProfileResponse.status === 'success') {
                    this.setState({ loading: false, email: '', password: '', })
                    const resetAction = StackActions.reset({
                        index: 0,
                        actions: [NavigationActions.navigate({ routeName: 'HomeScreen', activeTab: 0 })],
                    });
                    this.props.navigation.dispatch(resetAction);
            } else {
                this.setState({ loading: false })
                setTimeout(() => {
                    alert(props.officialProfileResponse.message)    
                }, 800);
            }
            
          }

        if(_.isEmpty(props.googleResponse) === false) {
            // alert('google response')
            console.log("googlereceive respose====>",props.googleResponse);
            if(props.googleResponse.status === 'success') {
                console.log("goto login page");
                alert(props.googleResponse.message);
                console.log(props.googleResponse.data);
                var gdata = {
                    id: props.googleResponse.data.profile.id
                }
                this.props.dispatch(getOfficialProfileAction(props.googleResponse.data.token, gdata));
            } else {
                console.log("goto registration page");
                if(props.googleResponse.message === 'Please wait until Admin gave you access to your account') {
                    alert(props.googleResponse.message);
                    this.props.dispatch(onClearState());
                } else {
                    alert(props.googleResponse.message)
                    this.props.navigation.navigate('RegistrationScreen', {email: this.state.socialemail});
                    this.props.dispatch(onClearState());
                }
                
            }
        } else {
            console.log("googel error response")
        }

        if(_.isEmpty(props.fbResponse) === false) {
            // alert('fb response');

            console.log("fb receive respose====>",props.fbResponse);
            if(props.fbResponse.status === 'success') {
                console.log("goto login page")
                alert(props.fbResponse.message);
                console.log(props.fbResponse.data);
                var fdata = {
                    id: props.fbResponse.data.profile.id
                }
                this.props.dispatch(getOfficialProfileAction(props.fbResponse.data.token, fdata));
            } else {
                console.log("goto registration page");
                if(props.fbResponse.message === 'Please wait until Admin gave you access to your account') {
                    alert(props.fbResponse.message);
                    this.props.dispatch(onClearState());
                } else {
                    alert(props.fbResponse.message);
                    // this.props.navigation.navigate('RegistrationScreen');
                    this.props.navigation.navigate('RegistrationScreen', {email: this.state.socialemail});
                    this.props.dispatch(onClearState());
                }
                // this.props.navigation.navigate('RegistrationScreen');
            }
        } else {
            console.log("fb error response")
        }

    }

    /**
   * Login API
   */
    onLoginPress = () => {
        
        if (this.validateLogin()) {
            this.setState({ loading: true })
            
            this.props.dispatch(onClearLoginState());
            const loginAuth = {
                email: this.state.email,
                password: this.state.password,
                device_name: Platform.OS,
                device_id: DeviceInfo.getUniqueID()
            };
            
            if(getNetInfo()){
                this.props.dispatch(getLoginAuthAction(loginAuth));
            }else{
                // setTimeout(() => { alert('Check Network Connection!') }, 800);  
            }
           
        }
    }

     /**
   * Validate Login fields
   */
    validateLogin = () => {
        let regEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
        if (regEmail.test(this.state.email.trim()) === false) {
            alert('Please Enter a Valid Email ID')
            return false;
        } else if (this.state.password.trim() === '') {
            alert('Please Enter a Valid Password')
            return false;
        }
        return true;
    }

    /**fb login */

    fbLogin = async () => {
        console.log("fb login hete")

        var _this = this
        // FBLoginManager.setLoginBehavior(FBLoginManager.LoginBehaviors.Web);
        if(Platform.OS !== 'ios'){
            FBLoginManager.setLoginBehavior(FBLoginManager.LoginBehaviors.WebView); // defaults to Native
          }
          console.log("fb login step1");
        FBLoginManager.loginWithPermissions(["email","user_friends"], function(error, data){
            console.log("fb login step2");
            if(!error) {
                console.log("fb login successs===>", data.credentials);
                console.log(data)
                if(Platform.OS === 'ios') {
                    console.log('ios platform');
                    if(data.credentials.userId === '' || data.credentials.userId === null || data.credentials.userId === undefined) {
                        console.log('ios if-===>')
                        
                    } else {
                        console.log('else==>')
                        var api = 'https://graph.facebook.com/v2.8/' + data.credentials.userId +
                        '?fields=name,email&access_token=' + data.credentials.token;
                    fetch(api)
                        .then((response) => response.json())
                        .then( (responseData) => {
                                console.log('ios email====>',responseData.email);
                                _this.setState ({
                                    socialemail: responseData.email
                                })
            
                                var facebkdata = {
                                    email: responseData.email
                                }
                                _this.props.dispatch(getFbAction(facebkdata));
                        })
                    .done();
                    }
                } else {
                    if(data.type === 'success') {
                        // alert("logged in successfully");
                        console.log(data.profile);
                        let fbdata = JSON.parse(data.profile);
                        console.log("fbdata===>",fbdata);
                        console.log('email=====>',fbdata.email);
                        console.log(data.credentials);
                        console.log(data.credentials.token);
                        _this.setState ({
                            socialemail: fbdata.email
                        })
    
                        var facebkdata = {
                            email: fbdata.email
                        }
                        _this.props.dispatch(getFbAction(facebkdata));
                    }
                }
               

            } else {
                console.log("fb login failed===>",error);
                console.log(error);
            }
        })
    }
    /** google sign in */

    googleLogin = async () => {
        console.log("google signin")
        try {
            await GoogleSignin.hasPlayServices();
            const userInfo = await GoogleSignin.signIn();
            console.log("socailresponse", userInfo)
            // alert("Logged in successfully");
            this.setState ({
                socialemail: userInfo.user.email
            })
            var googledata = {
                email: userInfo.user.email
            }
            this.props.dispatch(getGoogleAction(googledata));
          } catch (error) {
            console.log('userInfoErr', error)
          }
    }
    render() {
        return (
            <Container>
                <Image
                    resizeMode="stretch"
                    style={{ height: '35%', width: imageWidth }}
                    source={require('../../resources/images/login/loginImage.png')}
                />
                <Image
                    resizeMode="contain"
                    style={{ alignSelf: 'center', height: '6%', width: imageWidth / 3 }}
                    source={require('../../resources/images/login/logo-Electra.png')}
                />
                {(this.state.loading) ? <Loader /> : null}
                <Content>
                    <View style={{ marginLeft: 30, marginRight: 30, marginTop: 10 }}>
                        <View style={styles.textInputContainer}>
                            <Image
                                resizeMode="contain"
                                style={{ alignSelf: 'center', height: '60%', width: '10%' }}
                                source={require('../../resources/images/Login-Register/User/User.png')}
                            />
                            <TextInput
                                returnKeyType = { "done" }
                                value={this.state.email}
                                style={{ flex: 1, padding: 10 }}
                                placeholder="Email"
                                textContentType='emailAddress'
                                underlineColorAndroid="transparent"
                                onChangeText={(text) => this.setState({ email: text })}
                            />
                        </View>
                        <View style={styles.textInputContainer}>
                            <Image
                                resizeMode="contain"
                                style={{ alignSelf: 'center', height: '75%', width: '10%' }}
                                source={require('../../resources/images/Login-Register/Password/Password.png')}
                            />
                            <TextInput
                                returnKeyType={'done'}
                                
                                autoCapitalize={true}
                                value={this.state.password}
                                style={{ flex: 1, padding: 10 }}
                                secureTextEntry={true}
                                textContentType='password'
                                placeholder="Password"
                                underlineColorAndroid="transparent"
                                onChangeText={(text) => this.setState({ password: text, })}
                            />
                        </View>
                        <TouchableOpacity onPress={() => this.props.navigation.navigate('ForgotScreen')}
                            style={{ marginTop: 10, marginBottom: 10 }}>
                            <Text style={{ fontFamily: 'Nunito-Medium', color: '#9399a7', alignSelf: 'flex-end' }}>Forgot Password?</Text>
                        </TouchableOpacity>
                        <Button full
                            onPress={this.onLoginPress}
                            style={{
                                borderWidth: 1,
                                borderColor: '#018c31',
                                borderRadius: 20,
                                backgroundColor: '#018c31'
                            }}>
                            <Text>LOG IN</Text>
                        </Button>
                        
                        <View style={{ flexDirection: 'row' }}>
                            <View style={{ width: '45%', top: 19, borderTopWidth: 2, borderTopColor: '#b7b7b7' }} />
                            <Text style={{ fontFamily: 'Nunito-Medium', color: '#b8b8b8', padding: 8 }}>or</Text>
                            <View style={{ width: '45%', top: 19, borderTopWidth: 2, borderTopColor: '#b7b7b7' }} />
                        </View>
                        <View style={{ flexDirection: 'row' }}>
                            <Button iconLeft style={{
                                flex: 1,
                                borderWidth: 1,
                                borderColor: '#365494',
                                borderRadius: 20,
                                backgroundColor: '#365494',
                                justifyContent: 'space-around'
                            }} onPress={()=>{ this.fbLogin() }}>
                                <Icon style={{ fontSize: normalize(14), }} type="FontAwesome" name="facebook" />
                                <Text uppercase={false} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(9.5) }}>Sign in with Facebook</Text>
                            </Button>
                            <View style={{ width: '2%' }} />
                            <Button iconLeft style={{
                                flex: 1,
                                borderWidth: 1,
                                borderColor: '#de4b39',
                                borderRadius: 20,
                                backgroundColor: '#de4b39',
                                justifyContent: 'space-around'
                            }}
                            onPress={()=>{this.googleLogin()}}>
                                <Icon style={{ fontSize: normalize(14) }} type="FontAwesome" name="google" />
                                <Text uppercase={false} style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(9.5) }}>Sign in With Google</Text>
                            </Button>
                        </View>
                        <View style={{ justifyContent: 'center', alignItems: 'center', flexDirection: 'row', marginTop: 10, marginBottom: 25 }}>
                            <Text style={{ fontFamily: 'Nunito-Medium', color: '#adadad' }}> Don't have an account? </Text>
                            <TouchableOpacity
                                onPress={() => {
                                    this.props.navigation.navigate('RegistrationScreen',{email: ''})
                                }}>
                                <Text style={{ fontFamily: 'Nunito-Medium', color: '#439f5c' }}>Sign up</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = ({
    textInputContainer: {
        flexDirection: 'row',
        paddingLeft: 5,
        marginTop: 12,
        borderWidth: 0.5,
        borderRadius: 1,
        borderColor: '#dfe6f0',
        backgroundColor: '#eff4f8'
    }
});


const mapStateToProps = (state) => {
    return {
        error: state.LoginR.error,
        isLoading: state.LoginR.isLoading,
        isLoggedIn: state.LoginR.isLoggedIn,
        loginResponse: state.LoginR.loginResponse,
        officialProfileResponse: state.ProfileOfficialR.officialProfileResponse,
        googleResponse: state.SocialLoginR.googleResponse,
        fbResponse: state.fbR.fbResponse,
    };
};

export default connect(mapStateToProps)(LoginScreen);