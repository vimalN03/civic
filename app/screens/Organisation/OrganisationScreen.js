import React, {Component} from 'react';
import {Platform, StyleSheet, View} from 'react-native';
import { Container, Header, Content, Button, Text } from 'native-base';

export default class ContributionScreen extends Component {
  render() {
    return (
        <Container>
            <Header />
            <Content>
            <View style={styles.container}>
                <Text style={styles.welcome}>Welcome to Contribution Screen!</Text>
            </View> 
            </Content>
        </Container>    
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
