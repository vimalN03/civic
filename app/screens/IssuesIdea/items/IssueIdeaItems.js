import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native';
import { ListItem, Thumbnail, Text, Left, Body, Right, Card, CardItem } from 'native-base';
import { colors, normalize } from '../../../resources';
import { getUserProfileDummyUrl } from '../../../utils/URLConstants';


const IssueIdeaItems = ({
    rowItem,
    onPress,
    onPressProfile
}) => (
        <TouchableOpacity activeOpacity={1}>
            <Card style={{ flexDirection: 'column' }}>
                <CardItem>
                    <Text style={{ lineHeight: 20, fontFamily: 'Nunito-Bold', fontSize: normalize(13) }} numberOfLines={2} >{rowItem.title}</Text>
                </CardItem>
                <ListItem button thumbnail noBorder={true}>
                    <Left>
                        {(rowItem.posted_user === null)? null :
                        <TouchableOpacity onPress={ onPressProfile }>
                            <Thumbnail circle small source={getUserProfileDummyUrl(rowItem.posted_user.profile_picture)} />
                        </TouchableOpacity>
                        }
                    </Left>
                    <Body>
                    {(rowItem.posted_user === null)? null :
                        <TouchableOpacity onPress={ onPressProfile }>
                        <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(12) }}>{rowItem.posted_user.name}</Text>
                        </TouchableOpacity>
                    }
                    </Body>
                    <Right>
                        {
                            (rowItem.key != 'b') ?
                                (
                                    <TouchableOpacity onPress={onPress}>
                                        <View style={{ marginTop: 5, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                            <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, paddingLeft: 10, paddingRight: 10, fontSize: normalize(10) }}>See Details</Text>
                                        </View>
                                    </TouchableOpacity>
                                ) :
                                (
                                    <TouchableOpacity onPress={onPress}> 
                                        <View style={{ backgroundColor: colors.primary, marginTop: 5, borderWidth: 1, borderRadius: 5, borderColor: colors.primary, padding: 5 }}>
                                            <Text style={{ fontFamily: 'Nunito-Medium', color: colors.white, paddingLeft: 10, paddingRight: 10, fontSize: normalize(10) }}>See Details</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                        }
                    </Right>
                </ListItem>
                <CardItem bordered={false} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                    <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>
                        {rowItem.posted_at}
                    </Text>
                    <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>
                        {/* {(rowItem.type === 'issue') ? null : `${rowItem.overall_ratings.total_votes} Votes | `}{rowItem.type} */}
                        {rowItem.type}
                    </Text>
                </CardItem>
            </Card>
        </TouchableOpacity>
    );

IssueIdeaItems.prototype = {
    rowItem: PropTypes.object,
    onPress: PropTypes.func
};

export default IssueIdeaItems;