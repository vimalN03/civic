import React from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity } from 'react-native';
import { Thumbnail, Text, } from 'native-base';
import { Rating } from 'react-native-ratings';
import { colors } from '../../../resources';
import _ from 'lodash';
import { getUserProfileDummyUrl } from '../../../utils/URLConstants';

const IssueItems = ({ onPress, rowItem }) => {

    return (<TouchableOpacity activeOpacity={1} onPress={onPress} style={{ marginTop: 10 }}>
            <View style={{ flexDirection: 'column' }}>
                <View style={{ flexDirection: 'row' }}>
                    {(rowItem.user.user_details !== null) ?
                    <Thumbnail circle small source={getUserProfileDummyUrl(rowItem.user.user_details.profile_picture)} />
                    :
                    null
                    }
                    <View style={{ marginStart: 10 }}>
                        <Text style={{ fontFamily: 'Nunito-Medium', fontSize: 14 }}>{rowItem.user.name}</Text>
                        
                    </View>
                </View>
                <View>
                    <Text note style={{ fontFamily: 'Nunito-Medium', fontSize: 15, lineHeight: 15 }} numberOfLines={3} ellipsizeMode='tail'>
                        {rowItem.comment}
                    </Text>
                </View>
            </View>
        </TouchableOpacity>)
    };

IssueItems.prototype = {
    rowItem: PropTypes.object,
    onPress: PropTypes.func
};

export default IssueItems;