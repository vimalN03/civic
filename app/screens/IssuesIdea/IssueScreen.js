import React, { Component } from 'react';
import { StyleSheet, View, FlatList, Keyboard, TextInput, BackHandler } from 'react-native';
import { Container, Content, Button, Text, Card, ListItem, Thumbnail, Left, Body, CardItem, Textarea } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize } from '../../resources';
import IssueItems from './items/IssueItems';
import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile, BASE_URL, getUserProfileDummyUrl } from '../../utils/URLConstants';

import {
  issueDetailsApi, onClearIssueIdeas
} from '../../actions/issue-screen-action';

import {
  issuePostApi, onClearIssue
} from '../../actions/issue-post-action';

import {
  onClearCreateIssue
} from '../../actions/issue-create-action';

class IssueScreen extends Component {

  constructor(props) {
    super(props);
    const { issueId } = this.props.navigation.state.params

    this.state = {
      
      loading: false,
      issueId: issueId,
      suggestion: '',
      loading: true,
      getListResponseData: { issue: { user: {} } },
      getIssueResponse: {}
    }
  }

  componentDidMount(){
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.props.dispatch(onClearCreateIssue());
        this.getIdeaApi();      
      }
    ); 
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  
  getIdeaApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      console.log('value got', value);
      if (!_.isEmpty(value)) {
        console.log('value got2');
        this.props.dispatch(issueDetailsApi({ "issue_id": this.state.issueId }, value.data.token));
      }
    }).catch((error) => {
      console.log(error.message);
    })
  }


  componentWillReceiveProps = props => {
    if (props.isLoading === false) {
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getResponseData) === false) {
      console.log('issueResponse', props.getResponseData)
      if (props.getResponseData.status === 'success') {
        this.setState({ getListResponseData: props.getResponseData.data, loading: false })
        console.log('issueResponseData', props.getResponseData)
      } else {
        this.setState({ loading: false })
        setTimeout(() => {
          alert(props.getResponseData.message)  
        }, 800);        
      }
      this.props.dispatch(onClearIssueIdeas())
    }
    

    if (_.isEmpty(props.getIssuePostResponseData) === false) {
      if (props.getIssuePostResponseData.status === 'success') {
        this.setState({ loading: false, suggestion: '' })
        console.log('postRes', props.getIssuePostResponseData)
        this.getIdeaApi();  
        // setTimeout(() => { alert(props.getIssuePostResponseData.message) }, 800);
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.getIssuePostResponseData.message) }, 800);
      }
      this.props.dispatch(onClearIssue())
    }
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  render() {
    return (
      <Container>
        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent', alignItems: 'center' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Issue' />
        {(this.state.loading) ? <Loader /> : null}
        <Content>
        <View style={{ marginStart: 10, marginEnd: 10 }}>
            <Card style={{ flexDirection: 'column' }}>
              <CardItem>
              <Text style={{ fontFamily: 'Nunito-Medium', fontWeight: "500", lineHeight: 15 }}>{this.state.getListResponseData.issue.title}</Text>
              </CardItem>
              <ListItem thumbnail noBorder={true}>
                <Left>
                  {<Thumbnail circle small source={getUserProfileDummyUrl(this.state.getListResponseData.issue.user.profile_picture)} />}
                </Left>
                <Body>
                  {(this.state.getListResponseData.issue.user.name === null)? null :
                  <Text style={{ fontFamily: 'Nunito-Medium', fontWeight: "300" }} note>{this.state.getListResponseData.issue.user.name}</Text>
                  }
                </Body>
              </ListItem>
              <Text numberOfLines={4} style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', fontWeight: "300", padding:10, lineHeight: 15 }}>
                {this.state.getListResponseData.issue.description}
              </Text>
              <CardItem bordered={false} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(10) }}>
                  {this.state.getListResponseData.issue.created_at}
                </Text>
                {/* <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(10) }}>{`${this.state.getListResponseData.total_votes}`} Votes</Text> */}
              </CardItem>
            </Card>
            <Card>
              <CardItem style={{ alignSelf: 'center' }}><Text style={{ fontFamily: 'Nunito-Medium' }}>Drop Your Suggestion</Text></CardItem>
              <View style={{
                flexDirection: 'column', borderRadius: 1,
                borderColor: '#dfe6f0',
                backgroundColor: '#eff4f8', marginStart: 10, marginEnd: 10
              }}>
              <TextInput 
                multiline
                placeholder="Suggestion"
                returnKeyLabel='Done' returnKeyType='done' onSubmitEditing={Keyboard.dismiss}
                  value={this.state.suggestion}
                  style={{ paddingTop: 10, flex: 1, height: 100, }}
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => this.setState({ suggestion: text, userNameErr: '' })}
              />
                
              </View>
              <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-around', alignSelf: 'center' }}>
                <Button small
                  onPress={() => this.onPostIssueApi()}
                  style={{ backgroundColor: colors.primary, marginEnd: 10, borderRadius: 5 }}>
                  <Text style={{ fontFamily: 'Nunito-Medium', color: colors.white }}>Submit</Text>
                </Button>
                <Button small style={{ backgroundColor: colors.gray, marginStart: 10, borderRadius: 5 }}>
                  <Text style={{ fontFamily: 'Nunito-Medium', color: colors.white }}>Cancel</Text>
                </Button>
              </View>
              <CardItem>
                <FlatList
                  data={this.state.getListResponseData.issue.issue_user_comments}
                  ItemSeparatorComponent={() => <View style={{ height: 1, marginTop: 10, marginBottom: 5, backgroundColor: colors.litegray }} />}
                  renderItem={({ item }) => <IssueItems rowItem={item} onPress={() => console.log('zzzz', item)} />}
                  
               />
              </CardItem>
            </Card>
        </View>
        </Content>
      </Container>
    );
  }


  onPostIssueApi = () => {
    if (this.validateInputs()) {
      this.setState({ loading: true })
      getProfile().then((value) => {
        console.log('value got', value);
        if (!_.isEmpty(value)) {
          console.log('value got2', this.state.suggestion);
          this.props.dispatch(issuePostApi({ "issue_id": this.state.issueId, "comment": this.state.suggestion }, value.data.token));
        }
      }).catch((error) => {
        console.log(error.message);
      })
    }
  }

  validateInputs = () => {
    if (this.state.suggestion.trim() === '') {
      alert('Comments cannot be empty!!')
      console.log('buttonPressed', 'yes')
      return false;
    }
    return true;
  }
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInputContainer: {
    flexDirection: 'column',
    flex: 1,
    paddingLeft: 5,
    marginTop: 12,
    borderWidth: 0.5,
    borderRadius: 1,
    borderColor: '#dfe6f0',
    backgroundColor: '#eff4f8',
    marginStart: 10, marginEnd: 10
  }
});


const mapStateToProps = (state) => {
  return {
    error: state.IssueR.error,
    getResponseData: state.IssueR.getResponseData,
    getIssuePostResponseData: state.IssuePostR.getIssuePostResponseData,
    isLoading: state.IssueR.isLoading,
  };
};

export default connect(mapStateToProps)(IssueScreen);
