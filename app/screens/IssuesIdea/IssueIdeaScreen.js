import React, { Component } from 'react';
import { StyleSheet, Platform, RefreshControl, View, FlatList, Image, TouchableOpacity } from 'react-native';
import { Container, Header, Root, Content, Text, Left, Right, Title, Body } from 'native-base';
import { colors, normalize, dimens } from '../../resources';
import IssueIdeaItems from './items/IssueIdeaItems';
import { getHeaderTitle, getNetInfo } from '../../utils/URLConstants';
import Tabs from '../index';
import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile } from '../../utils/URLConstants';

import {
  getIssueIdeasListAction
} from '../../actions/issue-ideas-action';

class IssueIdeaScreen extends Component {


  constructor() {
    super();
    this.state = {
      type: '',
      block_status: null,
      refreshing: true,
      loading: true,
      getListResponseData: [],
      errMsg: null,
      vin: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        if(getNetInfo()){
          this.setState({ loading: true })
          this.getIssueIdeaListApi();  
          // setTimeout(()=>{
          //   alert(this.state.type);
          //   alert(this.state.block_status);
          // },5000)
        }else{
          this.setState({ refreshing: true, loading: false });
          alert('Check Network Connection!')
        }    
      }
    );
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getIssueIdeaListApi();      
  }

  componentWillReceiveProps = props => {
    if (props.isLoading === false) {
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getResponseData) === false) {
      console.log('issueIdeasResponse', props.getResponseData)
      if (props.getResponseData.status === 'success') {
        this.setState({ 
          refreshing: true,
          getListResponseData: props.getResponseData.data, 
          errMsg: (props.getResponseData.data.length === 0)? 'No Result Found' : null, 
          loading: false 
        })
        console.log('issueIdeasResponseData', props.getResponseData)
      } else {
        this.setState({ 
          refreshing: true,
          loading: false, 
          errMsg: (props.getResponseData.data.length === 0)? 'No Result Found' : null 
        })
        // setTimeout(() => {
        //   alert(props.getResponseData.message)
        // }, 800); 
      }
    }

  }

  getIssueIdeaListApi = () => {
    getProfile().then((value) => {
      console.log('value got', value);
      if (!_.isEmpty(value)) {
        console.log('value got2===>',value);
        this.setState({ type: value.data.type, block_status: parseInt(value.data.block_status), vin: value.data.vin})
        this.props.dispatch(getIssueIdeasListAction(value.data.token));
      }
    }).catch((error) => {
      console.log(error.message);
    })
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
  }


  render() {
    return (
      <Root>
      <Container style={{ flex: 1 }}>
        <View style={{ flex: 9 }}>
          <Header transparent={false} style={{ backgroundColor: colors.white, alignItems: 'center' }} androidStatusBarColor={'#019235'} iosBarStyle="light-content">
            <Left style={{ flex: 0.2, marginLeft: 5 }}>
              <TouchableOpacity onPress={() => { this.props.navigation.openDrawer() }}>
                <Image style={{ width: 25, height: 25 }} source={require('../../resources/images/Home_page/Menu_icon/Menu-icon.png')} />
              </TouchableOpacity>
            </Left>
            {(this.state.loading) ? <Loader /> : null}
            <Body style={{flex: 2,justifyContent:'flex-start', marginLeft:10, alignItems:'flex-start'}}>
              <Title style={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }}>Issues/Ideas</Title>
            </Body>
            <Right>
              <TouchableOpacity onPress={() => {
                if(this.state.vin === undefined || this.state.vin === null || this.state.vin === '') {
                  alert("Update VIN to create Issues / Ideas")
                } else {
                  this.props.navigation.navigate('IssuesIdeaCreateScreen')
                }
              }
                // this.props.navigation.navigate('IssuesIdeaCreateScreen')
              }>
              {(this.state.type === 'citizen' && this.state.block_status === 1) ? <View style={{
                  flexDirection: 'row',
                  backgroundColor: colors.primary, borderRadius: 5, justifyContent: 'space-between', alignItems: 'center', padding: 5
                }}>
                  <Image style={{ margin: 5, width: 15, height: 15 }} source={require('../../resources/images/04IssueIdea/Edit-Icon/Edit-Icon.png')} />
                  <Text uppercase={false} style={{ margin: 5, color: colors.white, fontSize: normalize(dimens.headerButtonText) }}>Create Issue/Idea</Text>
                </View> : null
              }
              </TouchableOpacity>
            </Right>
          </Header>
          {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
          <View style={{ marginStart: 10, marginEnd: 10, backgroundColor:'#fff', marginBottom:(Platform.OS === 'ios')? 100 : 0 }}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
              <FlatList
                data={this.state.getListResponseData}
                renderItem={({ item }) => <IssueIdeaItems
                  onPress={() => {
                    (item.type === 'idea') ? (this.props.navigation.navigate('IdeaScreen', { issueId: item.id })) : (this.props.navigation.navigate('IssueScreen', { issueId: item.id }))
                  }}
                  rowItem={item}
                  onPressProfile = {()=> this.props.navigation.navigate('OfficialProfileScreen', { isMy: true, user_id: item.posted_user.id })}
              />
                }
                refreshControl={
                  <RefreshControl
                    refreshing={!this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
              />
            </View>
        </View>
        <Tabs
          activeTab={3}
          onPressCandidate={() => { this.props.navigation.navigate('CandidateScreen') }}
          onPressOffial={() => { this.props.navigation.navigate('OfficialScreen') }}
          onPressHome={() => { this.props.navigation.navigate('HomeScreen') }}
          onPressPolls={() => { this.props.navigation.navigate('PollScreen',{ type: this.state.type, block_status: this.state.block_status }) }}
        />
      </Container>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


const mapStateToProps = (state) => {
  return {
    error: state.IssueIdeasR.error,
    getResponseData: state.IssueIdeasR.getResponseData,
    isLoading: state.IssueIdeasR.isLoading,
  };
};

export default connect(mapStateToProps)(IssueIdeaScreen);