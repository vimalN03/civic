import React, { Component } from 'react';
import { ProgressBarAndroid, ProgressViewIOS, Platform, StyleSheet, View, BackHandler } from 'react-native';
import { Container, Content, Button, Text, Card, ListItem, Thumbnail, Left, Body, CardItem } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, wp } from '../../resources';
import ProgressBarAnimated from 'react-native-progress-bar-animated';

import { connect } from 'react-redux';
import _ from 'lodash';
import Loader from '../../components/Loader/Loader';
import { getProfile, BASE_URL, getUserProfileDummyUrl } from '../../utils/URLConstants';

import {
  ideaDetailsApi,
  postRatingIdeaApi,
  onClearIssueIdeas,
  onClearRatingIdeas
} from '../../actions/idea-screen-action';
import {
  onClearCreateIssue
} from '../../actions/issue-create-action';


class IdeaScreen extends Component {

  constructor(props) {
    super(props);
    const { issueId } = this.props.navigation.state.params

    this.state = {
      loading: true,
      projectTitle: '',
      uploadPhoto: '',
      projectDescription: '',
      opposeStyle: styles.buttonDefaultStyle,
      unsureStyle: styles.buttonDefaultStyle,
      supportStyle: styles.buttonDefaultStyle,
      opposeTextStyle: styles.buttonTextDefaultStyle,
      unsureTextStyle: styles.buttonTextDefaultStyle,
      supportTextStyle: styles.buttonTextDefaultStyle,
      unsureProgressCustomized: 60,
      getListResponseData: { issue: { user: {} } },
      getRatingIdeaResponse: {},
      issueId: issueId,
      loading: true,
      selectValue: ''
    }
    console.log('ideaId', this.props.navigation.state.params)
  }

  componentWillMount() {
    this.props.dispatch(onClearCreateIssue());
    this.ideaDetails_Api();
  }

  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  ideaDetails_Api = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      console.log('value got', value);
      if (!_.isEmpty(value)) {
        console.log('value got2');
        console.log('zzz', ({ "issue_id": this.state.issueId }, value.data.token));
        this.props.dispatch(ideaDetailsApi({ "issue_id": this.state.issueId }, value.data.token));
      }
    }).catch((error) => {
      console.log(error.message);
    })
  }

  componentWillReceiveProps = props => {
    if (props.isLoading === false) {
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getResponseData) === false) {
      
      if (props.getResponseData.status === 'success') {
        this.setState({ getListResponseData: props.getResponseData.data, loading: false })
        console.log('ideaResponseData', props.getResponseData)
      } else {
        this.setState({ loading: false })
        alert(props.getResponseData.message)
      }
      this.props.dispatch(onClearIssueIdeas())
    }

    //Rating response
    if (_.isEmpty(props.getRatingResponseData) === false) {
      if (props.getRatingResponseData.status === 'success') {
        this.setState({ getRatingIdeaResponse: props.getRatingResponseData },()=>{
          this.ideaDetails_Api();
        })        
        console.log('RatingData111', props.getRatingResponseData)
       // setTimeout(() => { alert(props.getRatingResponseData.message) }, 800);
      } else if (props.getRatingResponseData.status === 'error') {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.getRatingResponseData.message) }, 800);
      }
      this.props.dispatch(onClearRatingIdeas())
    }


  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  onChangeIdea = (selectedType) => {
    console.log(selectedType)
    switch (selectedType) {
      case 'Oppose':
        this.setState({
          opposeStyle: styles.buttonSelectedStyle, opposeTextStyle: styles.buttonTextSelectedTextStyle,
          unsureStyle: styles.buttonDefaultStyle, unsureTextStyle: styles.buttonTextDefaultStyle,
          supportStyle: styles.buttonDefaultStyle, supportTextStyle: styles.buttonTextDefaultStyle
        })


        this.postRatingFun("oppose")


        break;
      case 'Unsure':
        this.setState({
          opposeStyle: styles.buttonDefaultStyle, opposeTextStyle: styles.buttonTextDefaultStyle,
          unsureStyle: styles.buttonSelectedStyle, unsureTextStyle: styles.buttonTextSelectedTextStyle,
          supportStyle: styles.buttonDefaultStyle, supportTextStyle: styles.buttonTextDefaultStyle
        })

        this.postRatingFun("unsure")

        break;
      case 'Support':
        this.setState({
          opposeStyle: styles.buttonDefaultStyle, opposeTextStyle: styles.buttonTextDefaultStyle,
          unsureStyle: styles.buttonDefaultStyle, unsureTextStyle: styles.buttonTextDefaultStyle,
          supportStyle: styles.buttonSelectedStyle, supportTextStyle: styles.buttonTextSelectedTextStyle
        })

        this.postRatingFun("support")
        break;
      default:
        this.setState({
          opposeStyle: styles.buttonDefaultStyle, opposeTextStyle: styles.buttonTextDefaultStyle,
          unsureStyle: styles.buttonDefaultStyle, unsureTextStyle: styles.buttonTextDefaultStyle,
          supportStyle: styles.buttonDefaultStyle, supportTextStyle: styles.buttonTextDefaultStyle
        })
    }
  }

  postRatingFun(str) {
    getProfile().then((value) => {
      console.log('value got', value);
      if (!_.isEmpty(value)) {
        console.log('value got2');
        this.setState({ loading: true })
        this.props.dispatch(postRatingIdeaApi({ "issue_id": this.state.issueId, "rating": str }, value.data.token));
      }
    }).catch((error) => {
      console.log(error.message);
    })
  }


  render() {
    const progressCustomStyles = {
      backgroundColor: 'red',
      borderRadius: 0
    };
    return (
      <Container>
        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Ideas' />
        {(this.state.loading) ? <Loader /> : null}
        <Content>
          <View style={{ marginStart: 10, marginEnd: 10 }}>
            <Card style={{ flexDirection: 'column' }}>
              <CardItem>
                <Text style={{ fontFamily: 'Nunito-Medium', fontWeight: "500", lineHeight: 15 }}>
                  {this.state.getListResponseData.issue.title}</Text>
              </CardItem>
              <ListItem thumbnail noBorder={true}>
                <Left>
                  {<Thumbnail circle small source={getUserProfileDummyUrl(this.state.getListResponseData.issue.user.profile_picture)} />}
                </Left>
                <Body>
                {(this.state.getListResponseData.issue.user.name === null) ? null :
                  <Text style={{ fontFamily: 'Nunito-Medium', fontWeight: "300" }}>{this.state.getListResponseData.issue.user.name}</Text>
                }
                </Body>
              </ListItem>
              <Text numberOfLines={4} style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', fontWeight: "300", padding:10, lineHeight: 15 }}>
                {this.state.getListResponseData.issue.description}
              </Text>
              <CardItem bordered={false} style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <Text numberOfLines={2} ellipsizeMode='tail' style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(11) }}>{this.state.getListResponseData.issue.created_at}</Text>
                <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(11) }}>{`${this.state.getListResponseData.total_votes}`} Votes</Text>
              </CardItem>
            </Card>
          </View>
          <View style={{ marginStart: 10, marginEnd: 10 }}>
            <Card style={{ flexDirection: 'column' }}>
              <CardItem style={{ justifyContent: 'center', alignItems: 'center', alignSelf: 'center' }}>
                <Text style={{ fontFamily: 'Nunito-Medium', textAlign: 'center' }}>What's your stance on this idea?</Text>
              </CardItem>
              <CardItem style={{ justifyContent: 'space-between' }}>
                <View style={{ flex: 1, flexDirection: 'column', marginEnd: 4 }}>
                  <Button style={this.state.opposeStyle} onPress={() => this.onChangeIdea('Oppose')}><Text style={this.state.opposeTextStyle}>Oppose</Text></Button>
                  <View style={{ marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
                  {(this.state.getListResponseData.oppose === undefined) ? null :
                    
                    <View>
                      {
                          ( Platform.OS === 'android' )
                          ?
                            ( 
                              <ProgressBarAndroid
                                style={{width: 70, transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }],}}
                                color='red'
                                styleAttr="Horizontal"
                                indeterminate={false}
                                progress={this.state.getListResponseData.oppose}
                              /> )
                          :
                            ( 
                              <ProgressBarAnimated
                                width={60}
                                value={this.state.getListResponseData.oppose}
                                backgroundColor="red"
                              />
                              )
                      }
                    </View>
                  }
                  
                    <Text style={{ fontFamily: 'Nunito-Medium', color: colors.black, fontSize: normalize(9), marginStart: 3 }}>{this.state.getListResponseData.oppose}%</Text>
                  </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'column', marginEnd: 4 }}>
                  <Button style={this.state.unsureStyle} onPress={() => this.onChangeIdea('Unsure')}><Text style={this.state.unsureTextStyle}>Unsure</Text></Button>
                  <View style={{ marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
                  {(this.state.getListResponseData.unsure === undefined) ? null :
                    <View>
                    {
                        ( Platform.OS === 'android' )
                        ?
                          ( 
                            <ProgressBarAndroid
                              style={{width: 70, transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }],}}
                              color='red'
                              styleAttr="Horizontal"
                              indeterminate={false}
                              progress={this.state.getListResponseData.unsure}
                            /> )
                        :
                          ( <ProgressBarAnimated
                            width={60}
                            value={this.state.getListResponseData.unsure}
                            backgroundColor="red"
                          /> )
                    }
                  </View>
                  }
                 
                    <Text style={{ fontFamily: 'Nunito-Medium', color: colors.black, fontSize: normalize(9), marginStart: 3 }}>{this.state.getListResponseData.unsure}%</Text>
                  </View>
                </View>
                <View style={{ flex: 1, flexDirection: 'column' }}>
                  <Button style={this.state.supportStyle} onPress={() => this.onChangeIdea('Support')}><Text style={this.state.supportTextStyle}>Support</Text></Button>
                  <View style={{ marginTop: 5, flexDirection: 'row', alignItems: 'center' }}>
                  {(this.state.getListResponseData.support === undefined) ? null :
                    <View>
                    {
                        ( Platform.OS === 'android' )
                        ?
                          ( 
                            <ProgressBarAndroid
                            style={{width: 70, transform: [{ scaleX: 1.0 }, { scaleY: 2.5 }],}}
                              color='red'
                              styleAttr="Horizontal"
                              indeterminate={false}
                              progress={this.state.getListResponseData.support}
                            /> )
                        :
                          ( <ProgressBarAnimated
                            width={60}
                            value={this.state.getListResponseData.support}
                            backgroundColor="red"
                          /> )
                    }
                  </View>
                  }
                    <Text style={{ fontFamily: 'Nunito-Medium', color: colors.black, fontSize: normalize(9), marginStart: 3 }}>{this.state.getListResponseData.support}%</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ alignSelf: 'center' }}>
                <Text style={{ fontFamily: 'Nunito-Medium', textAlign: 'center', fontSize: normalize(12) }}>
                  Result may not add up as upto 100% due to rounding
                </Text>
              </CardItem>
              <CardItem>
                <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', textAlign: 'justify', fontSize: normalize(15), lineHeight: 20 }}>
                  {this.state.getListResponseData.description}
                </Text>
              </CardItem>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  buttonDefaultStyle: {
    backgroundColor: colors.light_silver, borderColor: colors.liteblack, borderWidth: 1, flex: 1
  },
  buttonSelectedStyle: {
    backgroundColor: colors.primary_light, flex: 1
  },
  buttonTextDefaultStyle: {
    color: colors.black,
    fontFamily: 'Nunito-Medium',
    fontSize:14
  },
  buttonTextSelectedTextStyle: {
    color: colors.white,
    fontFamily: 'Nunito-Medium',
  }
});


const mapStateToProps = (state) => {
  return {
    error: state.IdeaR.error,
    getResponseData: state.IdeaR.getResponseData,
    getRatingResponseData: state.IdeaR.getRatingResponseData,
    isLoading: state.IdeaR.isLoading,
  };
};

export default connect(mapStateToProps)(IdeaScreen);