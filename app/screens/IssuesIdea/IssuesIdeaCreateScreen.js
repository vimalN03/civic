import React, { Component } from 'react';
import { StyleSheet, View, TextInput, Alert } from 'react-native';
import { Container, Content, Button, Text, Card, CardItem, Textarea, ActionSheet, Label, Root } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, hp, normalize } from '../../resources';
import ImagePicker from 'react-native-image-picker';
import _ from 'lodash';

import { connect } from 'react-redux';
import Loader from '../../components/Loader/Loader';
import { getProfile } from '../../utils/URLConstants';

import {
  issueCreateApi, onClearCreateIssue
} from '../../actions/issue-create-action';

class IssuesIdeaCreateScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      createType: 'Issue/Idea',
      loading: false,
      issueTitle: '',
      uploadPhoto: '',
      issueDescription: '',
      issueidea: '-1',
      issueIdeaValue: 'Select Issue/Idea',
      issueIdeaData: [{ key: '0', value: 'Issue' }, { key: '1', value: 'Idea' }],
      issueideaType: '-1',
      issueTypeValue: 'Select Type Public/Private',
      issueTypeData: [{ key: '0', value: 'Public' }, { key: '1', value: 'Private' }],
      getResponse: {},
      upload_image: 'Upload Profile Image',
      avatarSource: null
    }
    this.selectPhotoTapped = this.selectPhotoTapped.bind(this);
  }


   /**
   * Issue creation API
   */
  onCreateIssue = () => {
    if (this.validateInputs()) {
      this.setState({ loading: true })
      getProfile().then((value) => {
        console.log('value got', value);
        if (!_.isEmpty(value)) {
          //console.log('value got2', this.state.suggestion);
          
          const data ={
              "type": this.state.issueIdeaValue.toLowerCase(), 
              "tag_type": this.state.issueTypeValue.toLowerCase(), 
              "issue": this.state.issueTitle, 
              "reason": this.state.issueDescription
          };
          console.log("asdf",data)
          this.props.dispatch(issueCreateApi(data, value.data.token));
        }
      }).catch((error) => {
        console.log(error.message);
      })

    }

  }

   /**
   * Validate Input Fields
   */
  validateInputs = () => {
    if(this.state.issueidea === '-1') {
      alert('Please Select issue/idea!!')
      return false;
    }else if(this.state.issueideaType === '-1') {
      alert('Please Select Public/Private!!')
      return false;
    }
    else if (this.state.issueTitle.trim() === ''){
      alert('Title cannot be empty!!');
      return false;
    }else if (this.state.issueDescription.trim() === ''){
      alert('Description cannot be empty!!');
      return false;
    }
    return true;
  }

  componentWillReceiveProps = props => {
    if (props.isLoading === false) {
      setTimeout(() => { alert(props.error.message) }, 800);
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.getResponseData) === false) {
      
      if (props.getResponseData.status === 'success') {
        this.setState({ getResponse: props.getResponseData.data, loading: false })
        setTimeout(() => { 
          Alert.alert(
            '',
           this.state.createType+' created successfully',
            [
              {text: 'OK', onPress: () => this.props.navigation.goBack()},
            ],
            { cancelable: false }
          )
          }, 800);
        console.log('issueResponseData', props.getResponseData)
        
      } else {
        this.setState({ loading: false })
        setTimeout(() => {
          alert(props.getResponseData.message)
        }, 800); 
      }
      this.props.dispatch(onClearCreateIssue());
    }
  }
  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    /**
    * The first arg is the options object for customization (it can also be null or omitted for default options)
    ** The second arg is the callback which sends object: response (more info in the API Reference)
    */
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = {
          uri: 'data:image/jpeg;base64,' + response.data,
          type: response.type,
          name: response.fileName
        };

        // You can also display the image using data:
        // let source = { uri: 'data:image/jpeg;base64,' + response.data };

        this.setState({
          avatarSource: source,
          upload_image: response.fileName
        });
      }
    });
  }

  selectIssueIdea = () => {
    var issueIdeaData = [{ key: '0', value: 'Issue' }, { key: '1', value: 'Idea' }];
    if (!_.isEmpty(issueIdeaData)) {

      let nameList = issueIdeaData.map(function (item) {
        return item['value'];
      });
      nameList.push("Cancel")
      let CANCEL_INDEX = nameList.length - 1
      ActionSheet.show(
        {
          options: nameList,
          cancelButtonIndex: CANCEL_INDEX,
          title: "Select Issue/Idea"
        },
        buttonIndex => {
          if (buttonIndex < issueIdeaData.length) {
            this.setState({
              createType: (parseInt(issueIdeaData[buttonIndex].key) === 0) ? 'Issue' : 'Idea', 
              issueidea: issueIdeaData[buttonIndex].key,
              issueIdeaValue: issueIdeaData[buttonIndex].value
            })
            console.log('==',issueIdeaData[buttonIndex].value)
          }
        }
      )
    }else{
    }
  }

  selectIssueIdeaType = () => {
    var issueTypeData= [{ key: '0', value: 'Public' }, { key: '1', value: 'Private' }];
    if (!_.isEmpty(issueTypeData)) {

      let nameList = issueTypeData.map(function (item) {
        return item['value'];
      });
      nameList.push("Cancel")
      let CANCEL_INDEX = nameList.length - 1
      ActionSheet.show(
        {
          options: nameList,
          cancelButtonIndex: CANCEL_INDEX,
          title: "Select Type Public/Private"
        },
        buttonIndex => {
          if (buttonIndex < issueTypeData.length) {
            this.setState({
              issueideaType: issueTypeData[buttonIndex].key,
              issueTypeValue: issueTypeData[buttonIndex].value
            })
            console.log('==',issueTypeData[buttonIndex].value)
          }
        }
      )
    }else{
    }
  }

  componentWillUnmount() {
    ActionSheet.actionsheetInstance = null;
  }

  render() {
    return (
      <Root>
        <Container>
          <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent', alignItems: 'center' }}
            leftIcon={require('../../resources/images/left-arrow.png')}
            onPressLeft={() => { this.props.navigation.goBack() }}
            title='Create Issue/Idea' />
          {(this.state.loading) ? <Loader /> : null}
          <Content>
            <View style={{ marginStart: 15, marginEnd: 15 }}>
              <Card padder style={{ flexDirection: 'column', padding: 10 }}>
                <View style={styles.textInputContainer}>
                  <Label
                    adjustsFontSizeToFit={true}
                    style={{flex: 1, padding: 10, fontSize: normalize(12),
                    color: colors.dark_grayish_blue}}
                    onPress={() => { this.selectIssueIdea() }}>
                    {this.state.issueIdeaValue}
                  </Label>
                </View>
                <View style={styles.textInputContainer}>
                  <Label
                    adjustsFontSizeToFit={true}
                    style={{flex: 1, padding: 10, fontSize: normalize(12),
                    color: colors.dark_grayish_blue}}
                    onPress={() => { this.selectIssueIdeaType() }}>
                    {this.state.issueTypeValue}
                  </Label>
                </View>
                <View style={styles.textInputContainer}>
                  <TextInput
                    value={this.state.issueTitle}
                    style={{ flex: 1, padding: 10, fontSize: normalize(12)}}
                    placeholder= {`${this.state.createType} Title`}
                    placeholderTextColor={colors.light_blue}
                    underlineColorAndroid="transparent"
                    onChangeText={(text) => this.setState({ issueTitle: text, userNameErr: '' })}
                  />
                </View>
                {/* <View style={styles.textInputContainer}>
                    <Label
                  style={{ flex: 1, padding: 10, fontSize: normalize(12)}}
                  adjustsFontSizeToFit={true}
                  placeholder="Upload a Photo"
                  placeholderTextColor={colors.light_blue}>
                  {this.state.upload_image}
                  </Label>
                  <Button style={{ backgroundColor: colors.primary }} onPress={() => { this.selectPhotoTapped() }}>
                    <Text style={{ fontFamily: 'Nunito-Medium' }}>Browse</Text>
                  </Button>
                </View> */}
                <View style={styles.textInputContainer}>
                  <Textarea
                    rowSpan={5}
                    placeholder= {`${this.state.createType} Description`}
                    placeholderTextColor={colors.light_blue}
                    value={this.state.issueDescription}
                    style={{ flex: 1, padding: 10 , fontSize: normalize(12)}}
                    underlineColorAndroid="transparent"
                    onChangeText={(text) => this.setState({ issueDescription: text, userNameErr: '' })}
                  />
                </View>
                <CardItem style={{ justifyContent: 'center', alignSelf: 'center' }}>
                  <Button rounded style={{ backgroundColor: colors.primary }}  onPress={() => { this.onCreateIssue() }}>
                    <Text style={{ fontFamily: 'Nunito-Medium' }}>{`Create ${this.state.createType}`}</Text>
                  </Button>
                </CardItem>
              </Card>
            </View>
          </Content>
        </Container>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInputContainer: {
    flexDirection: 'row',
    paddingLeft: 5,
    marginTop: 12,
    borderWidth: 0.5,
    borderRadius: 1,
    borderColor: '#dfe6f0',
    backgroundColor: '#eff4f8',
    marginStart: 10, marginEnd: 10
  }
});


const mapStateToProps = (state) => {
  return {
    error: state.CreateIssueR.error,
    getResponseData: state.CreateIssueR.getResponseData,
    isLoading: state.CreateIssueR.isLoading,
  };
};

export default connect(mapStateToProps)(IssuesIdeaCreateScreen);