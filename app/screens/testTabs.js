import React, { Component, YellowBox } from 'react';
import { Text, TouchableOpacity, Dimensions, Image, View, Button } from 'react-native';

import HomeScreen from './Home/HomeScreen';
import CandidateScreen from './Candidates/CandidateListScreen';
import OfficialScreen from './Officials/OfficialScreen';
import IssueIdeaScreen from './IssuesIdea/IssueIdeaScreen';
import PollScreen from './Polls/PollScreen';

console.disableYellowBox = true;

var tabWidth = Dimensions.get('window').width/5;

export default class App extends Component {

  constructor(props){
    super(props);
    this.state={
      activeTab: this.props.activeTab
    }
  }

    render() {
        return (
            <View style={{flex: 1.5,backgroundColor:'#fff'}}>
                <View style={{height: 20, backgroundColor: 'transparent'}}></View>
                <View style={{flexGrow: 1.5, backgroundColor: '#019235', borderTopColor:'yellow',borderTopWidth:3 }}>
                    <View style={{flexDirection:'row',}}>
                    {(this.state.activeTab === 0)? 
                        <View style={{width: tabWidth, alignItems:'center', paddingTop:6}}>
                        <Image
                            source={require('../resources/images/Home_page/HomeTabIcon/Home.png')}
                            style={{width:25,height:25,alignSelf:'center'}}
                        />
                        <Text style={{textAlign:'center',color:'transparent'}}>Home</Text>
                        </View>
                        :
                        <TouchableOpacity
                            onPress={ this.props.onPressHome }
                            style={{ alignItems:'center', width: tabWidth, paddingTop:6}}
                        >
                            <Image
                                source={require('../resources/images/Home_page/HomeTabIcon/Home.png')}
                                style={{width:25,height:25,alignSelf:'center'}}
                            />
                            <Text style={{ fontSize:12, textAlign:'center',color:'transparent'}}>Home</Text>
                        </TouchableOpacity>
                        }
                        {(this.state.activeTab === 1)? 
                        <View style={{width: tabWidth,}}>
                        <Image
                            source={require('../resources/images/Home_page/Candidates_icon/Candidates-icon.png')}
                            style={{width:30,height:30,alignSelf:'center'}}
                        />
                        <Text style={{textAlign:'center',color:'transparent'}}>Candidates</Text>
                        </View>
                        :
                        <TouchableOpacity
                            onPress={ this.props.onPressCandidate }
                            style={{ width: tabWidth, paddingTop:6, }}>
                            <Image
                                source={require('../resources/images/Home_page/Candidates_icon/Candidates-icon.png')}
                                style={{width:30,height:30,alignSelf:'center'}}
                            />
                            <Text style={{fontSize:12,textAlign:'center',color:'transparent'}}>Candidates</Text>
                        </TouchableOpacity>
                        }
                        {(this.state.activeTab === 2)? 
                        <View style={{ width: tabWidth, paddingTop:6}}>
                        <Image
                            source={require('../resources/images/Home_page/Officals_icon/Officals-icon.png')}
                            style={{width:30,height:30,alignSelf:'center'}}
                        />
                        <Text style={{textAlign:'center',color:'transparent'}}>Officials</Text>
                        </View>
                        :
                        <TouchableOpacity
                            onPress={ this.props.onPressOffial }
                            style={{ width: tabWidth, paddingTop:6}}
                        >
                            <Image
                                source={require('../resources/images/Home_page/Officals_icon/Officals-icon.png')}
                                style={{width:30,height:30,alignSelf:'center'}}
                            />
                            <Text style={{ fontSize:12, textAlign:'center',color:'transparent'}}>Officials</Text>
                        </TouchableOpacity>
                        }
                        {(this.state.activeTab === 3)? 
                        <View style={{ width: tabWidth, paddingTop:6}}>
                        <Image
                            source={require('../resources/images/Home_page/Issue_icon/Issue-icon.png')}
                            style={{width:30,height:30,alignSelf:'center'}}
                        />
                        <Text style={{textAlign:'center',color:'transparent'}}>Issue/Ideas</Text>
                        </View>
                        :
                        <TouchableOpacity
                            onPress={ this.props.onPressIssue }
                            style={{ width: tabWidth, paddingTop:6}}
                        >
                            <Image
                                source={require('../resources/images/Home_page/Issue_icon/Issue-icon.png')}
                                style={{width:30,height:30,alignSelf:'center'}}
                            />
                            <Text style={{ fontSize:12, textAlign:'center',color:'transparent'}}>Issue/Ideas</Text>
                        </TouchableOpacity>
                        }
                        {(this.state.activeTab === 4)? 
                        <View style={{ width: tabWidth, paddingTop:6}}>
                        <Image
                            source={require('../resources/images/Home_page/Polls_icon/Polls-icon.png')}
                            style={{width:30,height:30,alignSelf:'center'}}
                        />
                        <Text style={{textAlign:'center',color:'transparent'}}>Polls</Text>
                        </View>
                        :
                        <TouchableOpacity
                            onPress={ this.props.onPressPolls }
                            style={{ width: tabWidth, paddingTop:6}}
                        >
                            <Image
                                source={require('../resources/images/Home_page/Polls_icon/Polls-icon.png')}
                                style={{width:30,height:30,alignSelf:'center'}}
                            />
                            <Text style={{ fontSize:12, textAlign:'center',color:'transparent'}}>Polls</Text>
                        </TouchableOpacity>
                        }
                        
                                                                                
                    </View>
                </View>
                
                
                <View style={{ position: 'absolute',top: -10}}>
                    <View style={{flexDirection:'row'}}>
                    {(this.state.activeTab === 0)? 
                        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', width: tabWidth,}}>
                            <View>
                                <View style={{height: 20,}}></View>
                                <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>

                                <Image
                                    source={require('../resources/images/Home_page/HomeTabIcon/Home-active.png')}
                                    style={{
                                        position: 'absolute',
                                        bottom: -15,
                                        height: 60,
                                        width: 60,
                                        left: -10
                                    }}
                                />
                            </View>
                            <View style={{marginTop:72,}} >
                                <Text style={{color:'#fff',textAlign:'center'}}>Home</Text>
                            </View>
                        </View>:
                        <View style={{flexDirection:'row', justifyContent:'center', alignItems:'center', width: tabWidth, }}>
                        <View>
                            <View style={{height: 20,}}></View>
                            <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>
                        </View>
                            <TouchableOpacity
                                onPress={ this.props.onPressHome }
                                style={{marginTop:72,}}
                            >
                                <Text style={{ fontSize:12, color:'#fff',textAlign:'center'}}>Home</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    <View style={{height:40,backgroundColor:'#fff', width:0.5, marginLeft:0, marginTop:45}}/>
                    {(this.state.activeTab === 1)?
                        <View style={{width: tabWidth,justifyContent:'center', alignItems:'center',flexDirection:'row',}}>
                            <View>
                                <View style={{height: 20,}}></View>
                                <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>

                                <Image
                                    source={require('../resources/images/Candidates/CandidateOverIcon/Candidate-over-icon.png')}
                                    style={{
                                        position: 'absolute',
                                        bottom: -15,
                                        height: 60,
                                        width: 60,
                                        left: 0

                                    }}
                                />
                            </View>
                            <View style={{marginTop:72}} >
                                <Text style={{fontSize:12,color:'#fff',textAlign:'center'}}>Candidates</Text>
                            </View>
                        </View>:
                        <View style={{ width: tabWidth,justifyContent:'center', alignItems:'center',flexDirection:'row',}}>
                            <View>
                                <View style={{height: 20,}}></View>
                                <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>
                            </View>
                            <TouchableOpacity
                                onPress={ this.props.onPressCandidate }
                                style={{marginTop:72,}}
                            >
                                <Text style={{ fontSize:12, color:'#fff',textAlign:'center'}}>Candidates</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    <View style={{height:40,backgroundColor:'#fff', width:0.5, marginLeft:0, marginTop:45}}/>
                    {(this.state.activeTab === 2)?
                        <View style={{width: tabWidth, justifyContent:'center', alignItems:'center',flexDirection:'row', }}>
                            <View>
                                <View style={{height: 20,}}></View>
                                <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>

                                <Image
                                    source={require('../resources/images/Official_icon_over/Official_Icon/Official-icon.png')}
                                    style={{
                                        position: 'absolute',
                                        bottom: -15,
                                        height: 60,
                                        width: 60,
                                        left: -8

                                    }}
                                />
                            </View>
                            <View style={{marginTop:72,}} >
                                <Text style={{fontSize:12,color:'#fff',textAlign:'center'}}>Officials</Text>
                            </View>
                        </View>:
                        <View style={{width: tabWidth,justifyContent:'center', alignItems:'center', flexDirection:'row', }}>
                        <View>
                            <View style={{height: 20,}}></View>
                            <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>
                        </View>
                            <TouchableOpacity
                                onPress={ this.props.onPressOffial }
                                style={{marginTop:72,}}
                            >
                            <Text style={{ fontSize:12, color:'#fff',textAlign:'center'}}>Officials</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    <View style={{height:40,backgroundColor:'#fff', width:0.5, marginLeft:0, marginTop:45}}/>
                    {(this.state.activeTab === 3)?
                        <View style={{width: tabWidth, justifyContent:'center', alignItems:'center', flexDirection:'row', }}>
                            <View>
                                <View style={{height: 20,}}></View>
                                <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>

                                <Image
                                    source={require('../resources/images/04IssueIdea/Issues-icon/04-issues-idea.png')}
                                    style={{
                                        position: 'absolute',
                                        bottom: -15,
                                        height: 60,
                                        width: 60,
                                        left: 0

                                    }}
                                />
                            </View>
                            <View style={{marginTop:72,}} >
                                <Text style={{fontSize:12,color:'#fff',textAlign:'center'}}>Issue/Ideas</Text>
                            </View>
                        </View>:
                        <View style={{width: tabWidth, justifyContent:'center', alignItems:'center', flexDirection:'row', }}>
                        <View>
                            <View style={{height: 20,}}></View>
                            <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>
                        </View>
                            <TouchableOpacity
                                onPress={ this.props.onPressIssue }
                                style={{marginTop:72,}}
                            >
                            <Text style={{ fontSize:12, color:'#fff',textAlign:'center'}}>Issue/Ideas</Text>
                            </TouchableOpacity>
                        </View>
                    }
                    <View style={{height:40,backgroundColor:'#fff', width:0.5, marginLeft:0, marginTop:45}}/>
                    {(this.state.activeTab === 4)?
                        <View style={{width: tabWidth, justifyContent:'center', alignItems:'center', flexDirection:'row', }}>
                            <View>
                                <View style={{height: 20,}}></View>
                                <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>

                                <Image
                                    source={require('../resources/images/Polls/Polls/Polls.png')}
                                    style={{
                                        position: 'absolute',
                                        bottom: -18,
                                        height: 60,
                                        width: 60,
                                        left: -18
                                    }}
                                />
                            </View>
                            <View style={{marginTop:72,}} >
                                <Text style={{fontSize:12,color:'#fff',textAlign:'center'}}>Polls</Text>
                            </View>
                        </View>:
                        <View style={{width: tabWidth, justifyContent:'center', alignItems:'center', flexDirection:'row', }}>
                        <View>
                            <View style={{height: 20,}}></View>
                            <View style={{backgroundColor: 'black', alignItems: 'center',}}></View>
                        </View>
                            <TouchableOpacity
                                onPress={ this.props.onPressPolls }
                                style={{marginTop:72,}}
                            >
                                <Text style={{ fontSize:12, color:'#fff',textAlign:'center'}}>Polls</Text>
                            </TouchableOpacity>
                        </View>
                    }
                                            
                    </View>
                </View>                    
            </View>
        );
    }
}
