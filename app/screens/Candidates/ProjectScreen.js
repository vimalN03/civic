import React, { Component } from 'react';
import { StyleSheet, RefreshControl, Dimensions, BackHandler, View, TouchableOpacity, FlatList, ImageBackground, Image } from 'react-native';
import { Container, Header, Content, Text, Left, Body, Title, Right } from 'native-base';
import { colors, normalize, wp, dimens } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile, BASE_URL, getNetInfo } from '../../utils/URLConstants';
import { getProjectListAction, onClearState } from '../../actions/projectList-actions';
import Loader from '../../components/Loader/Loader';

class ProjectScreen extends Component {

  constructor(props) {
    super(props)
    const { politician_id, isMy } = props.navigation.state.params
    this.state = {
      refreshing: true,
      politician_id: politician_id,
      isMy: (isMy) ? true : false,
      loading: false,
      projectListResponse: [],
      errMsg: null,
      type:'',
      userId: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        if(getNetInfo()){
          this.setState({ projectListResponse: [] });
          this.getProjectListApi();
        }else{
          this.setState({ refreshing: true, loading: false });
          alert('Check Network Connection!')
        }
      }
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  
  /**
   * Get Project Lists
   */
  getProjectListApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      console.log(" getProjectListApi", value)
      if (!_.isEmpty(value)) {
        this.setState({ loading: true, userId: value.data.id, type: value.data.type })
        console.log(" getProjectListApi1", this.state.politician_id)
        this.props.dispatch(onClearState());
        this.props.dispatch(getProjectListAction(value.data.token, { politician_id: this.state.politician_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    console.log(" componentWillReceiveProps componentWillReceiveProps 0")
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
        this.setState({ loading: false })
      }      
    }
    
    if (_.isEmpty(props.projectListResponse) === false && props.error === '') {
      console.log(" componentWillReceiveProps componentWillReceiveProps 2")
      if (props.projectListResponse.status === 'success') {
        console.log(" componentWillReceiveProps projectListResponse", props.projectListResponse)
        this.setState({
          refreshing: true,
          projectListResponse: props.projectListResponse.data,
          loading: false,
          errMsg: (props.projectListResponse.data.length === 0)? 'No Result Found' : null
        })
      } else {
        this.setState({ 
          refreshing: true,
          loading: false,
          errMsg: (props.projectListResponse.data.length === 0)? 'No Result Found' : null
        })
        // setTimeout(() => { alert(props.projectListResponse.message) }, 800);
      }
      
    } else {
      console.log(" componentWillReceiveProps componentWillReceiveProps 3")
    }
  }


  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getProjectListApi();      
  }


  componentWillUnmount(){
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  renderItem = ({ item, index }) => {
    let url = BASE_URL.IMAGE_URL + item.project_image;
    
    return <TouchableOpacity onPress={() => { this.props.navigation.navigate('ProjectDetailScreen', { project_id: item.id, userId: this.state.userId, isMy: this.state.isMy }) }}
      activeOpacity={1} style={{ borderRadius: 5 }}>
      <View style={{
        flex: 1,
        margin: 5,
        minWidth: Dimensions.get('window').width / 2 - 20,
        maxWidth: Dimensions.get('window').width / 2 - 10,
        minHeight: Dimensions.get('window').width / 2 - 20,
        maxHeight: Dimensions.get('window').width / 2 - 10,
        backgroundColor: '#CCCC',
      }}>
        <ImageBackground style={{ width: '100%', height: '100%' }} source={{ uri: url }} resizeMode='cover' >
          <View style={{ marginStart: 2, marginEnd: 2, flexDirection: 'column', alignItems: 'flex-start', position: "absolute", bottom: 0, right: 0, left: 0, padding: 5 }}>
              <Text numberOfLines={1} ellipsizeMode='tail' style={{ textAlign: 'left', lineHeight: 18, fontFamily: 'Nunito-Regular', color: colors.white, fontSize: normalize(12) }}>
                {item.project_title}
              </Text>
              {/* <Text numberOfLines={1} ellipsizeMode='tail' style={{ lineHeight: 18, fontFamily: 'Nunito-Regular', flex: 1, color: colors.white, fontSize: normalize(10), textAlign: 'left' }}>
                {item.project_description}
              </Text> */}
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              
              <Text style={{ lineHeight: 18, fontFamily: 'Nunito-Regular', flex: 1, color: colors.white, fontSize: normalize(10), textAlign: 'left' }}>
                {item.updated_at}
              </Text>
            </View>
          </View>
        </ImageBackground>
      </View>
    </TouchableOpacity>
  }

  render() {
    return (
      <Container>
        <View>
          <Header transparent={false} style={{ backgroundColor: colors.white, alignItems: 'center' }} androidStatusBarColor={'#019235'} iosBarStyle="light-content">
            <Left style={{ flex: 0.2, marginLeft: 5 }}>
              <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                <Image style={{ width: 25, height: 25 }} source={require('../../resources/images/left-arrow.png')} />
              </TouchableOpacity>
            </Left>
            <Body style={{flex: 2,justifyContent:'flex-start', marginLeft:10, alignItems:'flex-start'}}>
              <Title style={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }}>Projects</Title>
            </Body>
            {(this.state.type === 'elected_official' && this.state.isMy) ?
              (
                <Right>
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('ProjectCreateScreen', { project_details: {} }) }}>
                    <View style={{
                      backgroundColor: colors.primary, borderRadius: 5,
                      padding: 5, alignItems: 'center'
                    }}>
                      <Text uppercase={false} style={{
                        margin: 5,
                        textAlignVertical: 'center', color: colors.white, fontSize: normalize(dimens.headerButtonText)
                      }}>Add Project</Text>
                    </View>
                  </TouchableOpacity>
                </Right>) : (null)
            }
          </Header>
        </View>
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
            <FlatList
              contentContainerStyle={styles.list}
              data={this.state.projectListResponse}
              renderItem={this.renderItem}
              refreshControl={
                <RefreshControl
                  refreshing={!this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  list: {
    //justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 8
  }
});


const mapStateToProps = (state) => {
  return {
    error: state.ProjectListR.error,
    isLoading: state.ProjectListR.isLoading,
    projectListResponse: state.ProjectListR.projectListResponse
  };
};

export default connect(mapStateToProps)(ProjectScreen);