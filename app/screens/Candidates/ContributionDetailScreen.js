import React, { Component } from 'react';
import { StyleSheet, View, Image, BackHandler } from 'react-native';
import { Container, Content, Button, Icon, Text, Card, CardItem } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, hp } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile, BASE_URL } from '../../utils/URLConstants';
import { getContributionDetailAction, getContributionDeleteAction, onClearState } from '../../actions/contributionDetail-actions';
import Loader from '../../components/Loader/Loader';
import Share, {ShareSheet} from 'react-native-share';

class ContributionDetailScreen extends Component {

  constructor(props) {
    super(props)
    const { contribution_id, isMy, type, userId } = props.navigation.state.params
    this.state = {
      userId: userId,
      type: type,
      contribution_id: contribution_id,
      isMy: (isMy) ? true : false,
      loading: false,
      contributionDetailResponse: {}
    }

    this.shareOptions = {
      title: "Civic",
      message: "Pasta on 20% discount, valid for the rest of time!",
      url: "http://facebook.github.io/react-native/",
      subject: "Civic" //  for email
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getContributionDetailApi();
      }
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  /**
   * Get Contribution Details from APi
   */
  getContributionDetailApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getContributionDetailAction(value.data.token, { contribution_id: this.state.contribution_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  /**
   * Delete Contribution 
   */
  postContributionDeleteApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getContributionDeleteAction(value.data.token, { contribution_id: this.state.contribution_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.contributionDetailResponse) === false && props.error === '') {
      if (props.contributionDetailResponse.status === 'success') {
        console.log(" componentWillReceiveProps contributionDetailResponse", props.contributionDetailResponse)
        this.setState({
          contributionDetailResponse: props.contributionDetailResponse.data, loading: false
        })
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.contributionDetailResponse.message) }, 800);
      }
    }
    if (_.isEmpty(props.contributionDeleteResponse) === false && props.error === '') {
      if (props.contributionDeleteResponse.status === 'success') {
        console.log(" componentWillReceiveProps contributionDeleteResponse", props.contributionDetailResponse)
        this.setState({ loading: false })
        this.props.navigation.goBack();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.contributionDeleteResponse.message) }, 800);
      }
    }
  }

  goToContributionDetailsEdit = () => {
    this.props.navigation.navigate('ContributionCreateScreen', { contribution_details: this.state.contributionDetailResponse })
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  render() {
    const url = BASE_URL.IMAGE_URL + this.state.contributionDetailResponse.image
    return (
      <Container>
        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Contribution Detail'
          rightIcon={require('../../resources/images/User_Profile/Edit_Green_icon/EditGreen.png')}
          rightVisibility={parseInt(this.state.userId) === parseInt(this.state.contributionDetailResponse.user_id)}
          onPressRight={() => { this.goToContributionDetailsEdit() }} />
        {(this.state.loading) ? <Loader /> : null}
        <Content>
          <View style={{ marginStart: 10, marginEnd: 10 }}>
            <Card>
              <View style={{ height: hp('35%'), backgroundColor: 'grey', marginTop: 2 }}>
                <Image style={{ width: '100%', height: '100%' }} source={{ uri: url }} />
              </View>
              <CardItem style={{ marginEnd: 5, marginStart: 5, flexDirection: 'column' }}>
                <View style={{ flex: 1, alignSelf: 'flex-start', marginTop: 2 }}>
                  <Text numberOfLines={2} style={{ fontFamily: 'Nunito-Medium', textAlign: 'left', lineHeight: 20 }}>{this.state.contributionDetailResponse.title}</Text>
                </View>
                <View style={{ flex: 1, alignSelf: 'flex-start', marginTop: 2 }}>
                  <Text style={{ fontSize: normalize(12), fontFamily: 'Nunito-Medium', color: colors.litegray, }}>{this.state.contributionDetailResponse.updated_at}</Text>
                </View>
              </CardItem>

              <CardItem style={{ marginEnd: 5, marginStart: 5, flexDirection: 'column' }}>
                <Text style={{ alignSelf:'flex-start', fontFamily: 'Nunito-Medium', color: colors.litegray, textAlign: 'left', lineHeight: 20 }}>
                  {this.state.contributionDetailResponse.description}
                </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                  <Button iconLeft style={{ backgroundColor: colors.primary, marginTop: 10, borderRadius: 5 }}
                    onPress={()=>{Share.open(this.shareOptions)}}
                  >
                    <Icon name='share' />
                    <Text style={{ fontFamily: 'Nunito-Medium' }}>Share</Text>
                  </Button>
                  {(parseInt(this.state.userId) === parseInt(this.state.contributionDetailResponse.user_id))?
                    (
                      <Button onPress={() => { this.postContributionDeleteApi() }} iconLeft style={{ backgroundColor: colors.primary, marginTop: 10, marginStart: 10, borderRadius: 5 }}>
                        <Icon name='trash-o' type='FontAwesome' />
                        <Text style={{ fontFamily: 'Nunito-Medium' }}>Delete</Text>
                      </Button>) : (null)
                  }
                </View>
              </CardItem>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


const mapStateToProps = (state) => {
  return {
    error: state.ContributionDetailR.error,
    isLoading: state.ContributionDetailR.isLoading,
    contributionDetailResponse: state.ContributionDetailR.contributionDetailResponse,
    contributionDeleteResponse: state.ContributionDetailR.contributionDeleteResponse,
  };
};

export default connect(mapStateToProps)(ContributionDetailScreen);