import React, { Component } from 'react';
import { StyleSheet, RefreshControl, Dimensions, View, TouchableOpacity, BackHandler, FlatList, Image } from 'react-native';
import { Container, Header, Content, Text, Card, Left, Body, Title, Right } from 'native-base';
import { colors, normalize, dimens } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile, getBillsDummyUrl } from '../../utils/URLConstants';
import { getBillListAction, onClearState } from '../../actions/billsList-action';
import Loader from '../../components/Loader/Loader';

class BillScreen extends Component {

  constructor(props) {
    super(props)
    const { politician_id, isMy } = props.navigation.state.params
    this.state = {
      refreshing: true,
      politician_id: politician_id,
      isMy: (isMy) ? true : false,
      loading: true,
      billListResponse: [],
      errMsg: null,
      type: '',
      userId: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.setState({ billListResponse: [] })
        this.getBillsApi();
      }
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  /**
   * Get List of Bill Api
   */
  getBillsApi = () => {
    
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.setState({ loading: true, userId: value.data.id, type: value.data.type })
        
        this.props.dispatch(onClearState());
        this.props.dispatch(getBillListAction(value.data.token, { politician_id: this.state.politician_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.billListResponse) === false && props.error === '') {
      if (props.billListResponse.status === 'success') {
        console.log(" componentWillReceiveProps billListResponse", props.billListResponse)
        this.setState({
          refreshing: true,
          billListResponse: props.billListResponse.data,
          loading: false,
          errMsg: (props.billListResponse.data.length === 0)? 'No Result Found' : null
        })
      } else {
        this.setState({ 
          refreshing: true,
          loading: false,
          errMsg: (props.billListResponse.data.length === 0)? 'No Result Found' : null
        })
       // setTimeout(() => { alert(props.billListResponse.message) }, 800);
      }
    }
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getBillsApi();      
  }


  componentWillUnmount(){
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  renderItem = ({ item, index }) => {
    
    return (<TouchableOpacity onPress={() => this.props.navigation.navigate('BillDetailScreen', { type: this.state.type, userId: this.state.userId, bill_id: item.id, isMy: this.state.isMy })} activeOpacity={1} style={{ borderRadius: 5 }}>
      <View style={{
        flex: 1,
        margin: 5,
        minWidth: Dimensions.get('window').width / 2 - 20,
        maxWidth: Dimensions.get('window').width / 2 - 10,
        minHeight: Dimensions.get('window').width / 2 - 20,
        maxHeight: Dimensions.get('window').width / 2 - 10
      }}>
        <Card style={{ flex: 1, flexDirection: 'column' }}>
          <Image style={{ flex: 2, width: '100%' }} resizeMode='cover' source={getBillsDummyUrl(item.document)}></Image>
          <View style={{ flex: 0.5, paddingStart: 10, paddingEnd: 10, backgroundColor: colors.yellow, alignItems: 'center' }}>
            <Text numberOfLines={1} ellipsizeMode='tail' style={{
              lineHeight: 20, fontFamily: 'Nunito-Medium', color: colors.black,
              fontSize: normalize(10), textAlign: 'left'
            }}>
              {item.title}
            </Text>
          </View>
        </Card>
      </View>
    </TouchableOpacity>)
  }

  render() {
    return (
      <Container>
        <View>
          <Header transparent={false} style={{ backgroundColor: colors.white }} androidStatusBarColor={'#019235'} iosBarStyle="light-content">
            <Left style={{ flex: 0.2, marginLeft: 5 }}>
              <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
                <Image style={{ width: 25, height: 25 }} source={require('../../resources/images/left-arrow.png')} />
              </TouchableOpacity>
            </Left>
            <Body style={{flex: 2, justifyContent:'flex-start', marginLeft:10, alignItems:'flex-start'}}>
              <Title style={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }}>Bills</Title>
            </Body>
            {
              (this.state.type === 'elected_official' && this.state.isMy) ?
                (<Right>
                  <TouchableOpacity onPress={() => { this.props.navigation.navigate('BillCreationScreen', { bill_details: {} }) }}>
                    <View style={{
                      backgroundColor: colors.primary, borderRadius: 5,
                      padding: 5, alignItems: 'center'
                    }}>
                      <Text uppercase={false} style={{
                        margin: 5,
                        textAlignVertical: 'center', color: colors.white, fontSize: normalize(dimens.headerButtonText)
                      }}>Upload</Text>
                    </View>
                  </TouchableOpacity>
                </Right>) : (null)
            }
          </Header>
        </View>
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
            <FlatList
              contentContainerStyle={styles.list}
              data={this.state.billListResponse}
              renderItem={this.renderItem}
              refreshControl={
                <RefreshControl
                  refreshing={!this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            />
        </View>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#fff',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  list: {
   // justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginLeft: 5
  }
});


const mapStateToProps = (state) => {
  return {
    error: state.BillsListR.error,
    isLoading: state.BillsListR.isLoading,
    billListResponse: state.BillsListR.billListResponse
  };
};

export default connect(mapStateToProps)(BillScreen);