import React, { Component } from 'react';
import { StyleSheet, View, TextInput, FlatList, BackHandler, TouchableOpacity } from 'react-native';
import { Container, Content, Button, Text, Card, CardItem, Right, Body, CheckBox, ListItem } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, strings, normalize } from '../../resources';


import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile } from '../../utils/URLConstants';
import { getWalletAction, onClearState } from '../../actions/wallet-actions';
import Loader from '../../components/Loader/Loader';
import {
  getOfficialProfileAction, onClearElectedOfficial
} from '../../actions/official-profile-action';

class DonateScreen extends Component {

  constructor(props) {
    super(props);
    const { politician_id } = props.navigation.state.params
    this.state = {
      politician_id: politician_id,
      loading: true,
      walletBalance: strings.nigerian_symbol + ' 0.00',
      uploadPhoto: '',
      projectDescription: '',
      amount: '',
      otherEnable: false,
      donationList: [{ key: 'a', value: 'Donate', amount: 10.0, checked: false }, { key: 'b', value: 'Donate', amount: 50.0, checked: false },
      { key: 'c', value: 'Donate', amount: 100.0, checked: false },
      { key: 'd', value: 'Other', amount: '', checked: false }]
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
        'willFocus',
        () => {
          this.getUserprofileApi();
        }
      );
      BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  getUserprofileApi = () => {
    getProfile().then((value) => {
          console.log('value got', value);
          if (!_.isEmpty(value)) {
              console.log('value got211')
              var data = {
                  id: value.data.id
              }
              this.props.dispatch(getOfficialProfileAction(value.data.token, data));
          }
      }).catch((error) => {
          console.log(error.message);
      })
  }

  
  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }

    if (_.isEmpty(props.officialProfileResponse) === false) {
      console.log('pResponse', props.officialProfileResponse)
      if (props.officialProfileResponse.status === 'success') {
          //alert(props.electedResponse.message)
          console.log('zzzzz', props.officialProfileResponse)
          this.setState({ walletBalance : strings.nigerian_symbol + ' ' + props.officialProfileResponse.data.wallet_balance, loading: false })
      } else {
          this.setState({ loading: false })
          setTimeout(() => {
              alert(props.officialProfileResponse.message)    
          }, 800);
      }
      this.props.dispatch(onClearElectedOfficial());
    }

    if (_.isEmpty(props.walletResponse) === false && props.error === '') {
      if (props.walletResponse.status === 'success') {
        console.log(" componentWillReceiveProps walletResponse", props.walletResponse)
        this.setState({
          loading: false, walletBalance: strings.nigerian_symbol + ' ' + props.walletResponse.data.wallet_balance
        })
        setTimeout(() => { alert(props.walletResponse.message) }, 800);
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.walletResponse.message) }, 800);
      }
      this.props.dispatch(onClearState());
    }
  }

  /**
   * Validate Wallet Creation
   */
  validateWalletCreation = () => {
    let checkedIndex = -1
    for (let i = 0; i < this.state.donationList.length; i++) {
      console.log(this.state.donationList[i].checked)
      if (this.state.donationList[i].checked) { checkedIndex = i }
    }
    console.log('checkedIndex', checkedIndex)
    if (checkedIndex == -1) {
      setTimeout(() => { alert('Please Select the Amount') }, 800);
    } else if (checkedIndex == this.state.donationList.length - 1) {
      if (this.state.amount === undefined || this.state.amount.trim() === '') {
        setTimeout(() => { alert('Please Enter the Amount') }, 800);
      } else {
        this.callApiDonation(this.state.amount)
      }
    } else {
      this.callApiDonation(this.state.donationList[checkedIndex].amount)
    }
  }

  /**
   * Call Api for Donation
   */
  callApiDonation(amount) {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getWalletAction(value.data.token, {
          cost: amount,
          payment_mode: "wallet",
          payment_type: "donation",
          politician_id: this.state.politician_id,
          poll_id: "",
          plan_id: "",
        }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  /**
   * Checkbox functionality
   */
  checkUpdateList = (index) => {
    console.log('index', index)
    let data = [...this.state.donationList];
    let item = { ...data[index] }
    for (let i = 0; i < this.state.donationList.length; i++) {
      console.log('item', item)
      if (i == index) {
        console.log('item', i, item)
        let checked = !item.checked
        item.checked = checked
        console.log('data', i, item, data)
        data[index] = item
        console.log('data', i, data)
      } else {
        let dataItem = data[i]
        let ch = false
        dataItem.checked = ch
        data[i] = dataItem
      }
    }
    if (this.state.donationList.length - 1 == index) {
      this.setState({ otherEnable: !this.state.donationList[index].checked })
    }
    this.setState({
      donationList: data
    })
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  renderItem(item, index) {
    return (<TouchableOpacity activeOpacity={1} onPress={() => { this.checkUpdateList(index) }}>
      <View style={{
        flex: 1,
        margin: 5
      }}>
        <Card style={{ flex: 1, flexDirection: 'row', padding: 10, alignItems: 'center' }}>
          <CheckBox onPress={() => { this.checkUpdateList(index) }} checked={item.checked} color={colors.gray} />
          <Body>
            <Text style={{ fontFamily: 'Nunito-Medium', color: colors.black, fontSize: normalize(12) }}>{item.value}</Text>
          </Body>
          <Right>
            <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, fontSize: normalize(14) }}>{strings.nigerian_symbol + item.amount}</Text>
          </Right>
        </Card>
      </View>
    </TouchableOpacity>
    )
  }

  render() {
    return (
      <Container>
        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Donate' />
        {(this.state.loading) ? <Loader /> : null}
        <Content>
          <View style={{ marginStart: 15, marginEnd: 15 }}>
            <Card padder style={{ padding: 10 }}>
              <CardItem style={{ justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, fontSize: normalize(18) }}>Donate</Text>
                <View>
                  <Button small bordered style={{ borderColor: colors.primary, borderRadius: 5 }}>
                    <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, fontSize: normalize(10) }}>Add Wallet</Text>
                  </Button>
                </View>
              </CardItem>
              <CardItem style={styles.textInputContainer}>
                <Text style={{ fontFamily: 'Nunito-Medium' }}>Wallet Balance</Text>
                <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(26), color: colors.primary, marginTop: 10 }}>{this.state.walletBalance}</Text>
              </CardItem>
            </Card>
            <Card>
              <FlatList
                data={this.state.donationList}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => this.renderItem(item, index)}
              />
              {(this.state.otherEnable) ?
                (<View style={{
                  borderWidth: 0.5,
                  borderRadius: 1, borderColor: '#dfe6f0',
                  backgroundColor: '#eff4f8',
                  marginStart: 15, marginEnd: 15,
                  padding: 5
                }}>
                  <TextInput
                    value={this.state.amount}
                    placeholderTextColor={colors.light_blue}
                    placeholder="Enter Amount"
                    underlineColorAndroid="transparent"
                    onChangeText={(text) => this.setState({ amount: text, userNameErr: '' })}
                  />
                </View>) : (null)
              }
              <CardItem style={{ alignSelf: 'center' }}>
                <Button rounded onPress={() => { this.validateWalletCreation() }} style={{ backgroundColor: colors.primary }}><Text style={{ fontFamily: 'Nunito-Medium', color: colors.white }}>Donate</Text></Button>
              </CardItem>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInputContainer: {
    flexDirection: 'column',
    padding: 15,
    marginTop: 12,
    borderWidth: 0.5,
    borderRadius: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#dfe6f0',
    backgroundColor: '#eff4f8',
    marginStart: 10, marginEnd: 10
  }
});

const mapStateToProps = (state) => {
  return {
    error: state.WalletR.error,
    isLoading: state.WalletR.isLoading,
    walletResponse: state.WalletR.walletResponse,
    officialProfileResponse: state.ProfileOfficialR.officialProfileResponse,
  };
};

export default connect(mapStateToProps)(DonateScreen);