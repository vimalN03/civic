import React, { Component } from 'react';
import { StyleSheet, RefreshControl, View, Text, FlatList } from 'react-native';
import { Container, Content } from 'native-base';
import { connect } from 'react-redux';
import _ from 'lodash';
import ElectedOfficialsItems from '../Officials/items/ElectedOfficialsItems';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import Tabs from '../index';
import { normalize, colors } from '../../resources';
import { getCandidateListAction, getCandidateFollowAction, onClearState } from '../../actions/candidatelist-actions';
import { getProfile } from '../../utils/URLConstants';
import Loader from '../../components/Loader/Loader';


class CandidateListScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      type: '',
      block_status: null,
      refreshing: true,
      loading: false,
      candidateListResponse: [],
      candidateFollowResponse: {},
      errMsg: null,
      vin: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getCandidateListApi();
      }
    );
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getCandidateListApi();      
  }

  /**
   * Get Candidate List from API
   */
  getCandidateListApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        console.log('value===>',value)
        this.setState({ type : value.data.type, vin: value.data.vin, block_status: parseInt(value.data.block_status)})
        this.props.dispatch(onClearState());
        this.props.dispatch(getCandidateListAction(value.data.token));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.candidateListResponse) === false && props.error === '') {
      if (props.candidateListResponse.status === 'success') {
        console.log(" componentWillReceiveProps candidateListResponse", props.candidateListResponse)
        this.setState({
          candidateListResponse: props.candidateListResponse.data,
          refreshing: true,
          loading: false,
          errMsg: (props.candidateListResponse.data.length === 0)? 'No Result Found' : null          
        })
      } else {
        this.setState({ 
          loading: false,
          refreshing: true,
          errMsg: (props.candidateListResponse.data.length === 0)? 'No Result Found' : null
        })
        // setTimeout(() => { alert(props.candidateListResponse.message) }, 800);
      }
      this.props.dispatch(onClearState());
    }

    if (_.isEmpty(props.candidateFollowResponse) === false && props.error === '') {
      if (props.candidateFollowResponse.status === 'success') {
        this.setState({ loading: false })
        console.log(" componentWillReceiveProps candidateFollowResponse", props.candidateFollowResponse)
      //  setTimeout(() => { alert(props.candidateFollowResponse.message) }, 800);
        this.getCandidateListApi();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.candidateFollowResponse.message) }, 800);
      }
      this.props.dispatch(onClearState());
    }
  }

  /**
   * View Candidate Profile
   */
  goCandidateProfile = (item) => {
    console.log("clicked")
    console.log(item);
    console.log(item.politician_details.vin);

    if(this.state.vin === null || this.state.vin === undefined || this.state.vin === '') {
      alert("Update your VIN to view candidate profile");
    } else {
      this.props.navigation.navigate('CandidateProfileScreen', { politician_id: item.politician_id });
    }
    // this.props.navigation.navigate('CandidateProfileScreen', { politician_id: politician_id });
  }

  /**
   * Follow Unfollow Politician
   */
  followPolitician = (user_id) => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getCandidateFollowAction(value.data.token, { user_id: user_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillUnmount(){
      this.willFocusSubscription.remove();
  }

  render() {
    return (
      <Container style={{ flex: 1 }}>
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        <View style={{ flex: 9 }}>
          <HeaderBackNativeBase
            containerStyles={{ backgroundColor: '#f6f7f9' }}
            leftIcon={require('../../resources/images/Home_page/Menu_icon/Menu-icon.png')}
            onPressLeft={() => this.props.navigation.openDrawer()}
            title='Candidates'
          />
          <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
              <FlatList
                data={this.state.candidateListResponse}
                // renderItem={({ item }) => <ElectedOfficialsItems rowItem={item} onPress={() => { this.goCandidateProfile(item.politician_id) }} onPressFollow={() => { this.followPolitician(item.politician_id) }} />}
                renderItem={({ item }) => <ElectedOfficialsItems rowItem={item} onPress={() => { this.goCandidateProfile(item) }} onPressFollow={() => { this.followPolitician(item.politician_id) }} />}
                refreshControl={
                  <RefreshControl
                    refreshing={!this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
              />
          </View>
        </View>
        <Tabs
          activeTab={1}
          onPressHome={() => { this.props.navigation.navigate('HomeScreen') }}
          onPressOffial={() => { this.props.navigation.navigate('OfficialScreen') }}
          onPressIssue={() => { this.props.navigation.navigate('IssueIdeaScreen') }}
          onPressPolls={() => { this.props.navigation.navigate('PollScreen',{ type: this.state.type, block_status: this.state.block_status }) }}
        />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.very_pale_mostly_white_blue,
    margin: 10,
    backgroundColor:'#fff'
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: colors.very_dark_gray,
    marginBottom: 5,
  },
});


const mapStateToProps = (state) => {
  return {
    error: state.CandidateListR.error,
    isLoading: state.CandidateListR.isLoading,
    candidateListResponse: state.CandidateListR.candidateListResponse,
    candidateFollowResponse: state.CandidateListR.candidateFollowResponse
  };
};

export default connect(mapStateToProps)(CandidateListScreen);
