import React, { Component } from 'react';
import { StyleSheet, View, Platform, BackHandler, ScrollView, ImageBackground, TouchableOpacity } from 'react-native';
import { Text, Thumbnail, Card, CardItem } from 'native-base';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { connect } from 'react-redux';
import _ from 'lodash';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, hp, wp, dimens } from '../../resources';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import { getCandidateDetailAction, onClearState, postCandidateDetailRatingAction } from '../../actions/candidateDetail-actions';
import Loader from '../../components/Loader/Loader';
import Share, {ShareSheet, Button} from 'react-native-share';

class CandidateProfileScreen extends Component {

    constructor(props) {
        super(props);
        const { politician_id, isMy } = props.navigation.state.params
        this.state = {
            politician_id: politician_id,
            isMy: (isMy) ? true : false,
            loading: false,
            candidateDetailResponse: { politician_details: {} },
            rating: 0,
            rate_status: false
        }

        this.shareOptions = {
            title: "Civic",
            message: "Pasta on 20% discount, valid for the rest of time!",
            url: "http://facebook.github.io/react-native/",
            subject: "Civic" //  for email
        }
    }

    componentDidMount() {
        
        this.willFocusSubscription = this.props.navigation.addListener(
            'willFocus',
            () => {
                this.getDetailApi();   
            }
          );
          BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    getDetailApi = () => {
        this.setState({ loading: true })
        getProfile().then((value) => {
            if (!_.isEmpty(value)) {
                this.props.dispatch(getCandidateDetailAction(value.data.token, { politician_id: this.state.politician_id }));
            } else {
                this.setState({ loading: false })
            }
        }).catch((error) => {
            this.setState({ loading: false })
            console.log(error.message)
        })
    }

    componentWillUnmount() {
        this.props.dispatch(onClearState());
        this.willFocusSubscription.remove();
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
  
    handleBackButtonClick = () => {
      return this.props.navigation.goBack();
    }

    componentWillReceiveProps = (props) => {
        if (props.error.status === 'error') {
            if (props.error.status === 'error') {
                // setTimeout(() => { alert(props.error.message) }, 800);
                this.props.dispatch(onClearState());
            }
            this.setState({ loading: false })
        }
        if (_.isEmpty(props.candidateDetailResponse) === false && props.error === '') {
            if (props.candidateDetailResponse.status === 'success') {
                console.log(" componentWillReceiveProps candidateDetailResponse", props.candidateDetailResponse)
                this.setState({
                    candidateDetailResponse: props.candidateDetailResponse.data,
                    rating: props.candidateDetailResponse.data.given_rating,
                    rate_status: true
                })
            } else {
                setTimeout(() => { alert(props.candidateDetailResponse.message) }, 800);
            }
            this.setState({ loading: false })
            this.props.dispatch(onClearState());
        }
        if (_.isEmpty(props.candidateDetailRatingResponse) === false && props.error === '') {
            if (props.candidateDetailRatingResponse.status === 'success') {
                console.log(" componentWillReceiveProps candidateDetailRatingResponse", props.candidateDetailRatingResponse)
               // setTimeout(() => { alert(props.candidateDetailRatingResponse.message) }, 800);
               this.getDetailApi();
            } else {
                this.setState({ loading: false })
                setTimeout(() => { alert(props.candidateDetailRatingResponse.message) }, 800);
            }
            
            this.props.dispatch(onClearState());
        }
    }

    ratingCompleted = (rating) => {
        console.log("Rating is: " + rating)
        this.setState({
            rating: rating,
            rate_status: false
        });
        this.setState({ loading: true })
        getProfile().then((value) => {
            if (!_.isEmpty(value)) {
                this.props.dispatch(onClearState());
                this.props.dispatch(postCandidateDetailRatingAction(value.data.token, { politician_id: this.state.politician_id, rating: rating }));
            } else {
                this.setState({ loading: false })
            }
        }).catch((error) => {
            this.setState({ loading: false })
            console.log(error.message)
        })
    }

    _share = (type) => {
        Share.shareSingle(Object.assign({},this.shareOptions, {
            "social": type
        })).catch(err => console.log(err));
    }
        

    render() {
        return (
            <ScrollView style={{ flex: 1 }}>
                {(this.state.loading) ? <Loader /> : null}
                <View style={{ height: hp('28%'), backgroundColor: colors.mostly_desaturated_dark_blue }}>
                    <ImageBackground resizeMode='cover' source={require('../../resources/images/Candidate_Profile/2a-candidate-Background.png')}
                        style={{ width: '100%', height: '100%' }}>
                        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
                            leftIcon={require('../../resources/images/User_Profile/left-arrow/left-arrow@48.png')}
                            onPressLeft={() => { this.props.navigation.goBack() }}
                            title={this.state.candidateDetailResponse.name} titleStyle={{ paddingLeft: 5, color: colors.white, fontSize: normalize(dimens.headerTitle) }} />
                    </ImageBackground>
                </View>
                <View style={{ flexGrow: 1, backgroundColor: 'white', alignItems: 'center' }}>
                    <View style={{ marginTop: dimens.candidateProfileScreenMarginTop, flexDirection: 'column' }}>

                        <View style={{ flexDirection: 'row', width: wp('100%'), alignItems: 'center' }}>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'flex-start', marginTop: 5, marginLeft: wp('12%') }}>
                                <Text style={{ marginTop:5, fontFamily: 'Nunito-Medium', fontSize: normalize(18) }}>{this.state.candidateDetailResponse.name}</Text>
                                <Text style={{ fontFamily: 'Nunito-Medium', color: colors.gray, fontSize: normalize(12) }}>{this.state.candidateDetailResponse.politician_details.address}</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center' }}>
                           {(!this.state.rate_status) ? null :
                           <View style={{ flex:1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginRight:17 }}>
                                <AirbnbRating
                                    count={5}
                                    defaultRating={parseInt(this.state.rating)}
                                    showRating={false}
                                    size={ (Platform.OS === 'ios')? 22 : 17 }
                                    onFinishRating={this.ratingCompleted}
                                />
                                <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, alignItems: 'center', justifyContent: 'center', fontSize: normalize(12), marginLeft: 5 }}>({this.state.rating})</Text>
                            </View>
                            
                           }
                                {/* <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                                    <Rating
                                        type='custom'
                                        ratingColor={colors.primary}
                                        startingValue={this.state.rating}
                                        ratingBackgroundColor={colors.grayishlimegreen}
                                        ratingCount={5}
                                        imageSize={15}
                                        style={{ paddingVertical: 10 }}
                                        onFinishRating={this.ratingCompleted}
                                    />
                                    <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, alignItems: 'center', justifyContent: 'center', fontSize: normalize(12), marginLeft: 5 }}>({this.state.rating})</Text>
                                </View> */}
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center',marginRight:15 }}>
                                    <Text style={{ fontFamily: 'Nunito-Medium', color: colors.gray, alignItems: 'center', justifyContent: 'center', fontSize: normalize(12) }}>Share on</Text>
                                    <TouchableOpacity onPress={() => this._share('facebook')}>
                                        <Thumbnail small style={{ margin: 5, width: 25, height: 25 }} source={require('../../resources/images/Candidate_Profile/logo-facebook/logo-facebook.png')} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this._share('twitter')}>
                                        <Thumbnail small style={{ margin: 5, width: 25, height: 25 }} source={require('../../resources/images/Candidate_Profile/Twitterbird/Twitterbird.png')} />
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={() => this._share('googleplus')}>
                                        <Thumbnail small style={{ margin: 5, width: 25, height: 25 }} source={require('../../resources/images/Candidate_Profile/Google_Plus_logo/Google_Plus.png')} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>


                        <View style={{ padding: 10, margin: 5 }}>
                            {
                                (this.state.candidateDetailResponse.type === 'elected_official') ?
                                    (
                                        <Card>
                                            <CardItem>
                                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'stretch' }}>
                                                    <TouchableOpacity onPress={()=>{Share.open(this.shareOptions)}}>
                                                        <View style={{ margin: 5, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                            <Thumbnail circle large
                                                                source={require('../../resources/images/Candidate_Profile/ShareWithContacts/Share-with-contacts.png')} />
                                                            <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(14), justifyContent: 'center', alignItems: 'center' }}>{"Share with \n Contacts"}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ProjectScreen', { politician_id: this.state.politician_id, isMy: this.state.isMy })}>
                                                        <View style={{ margin: 5, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                            <Thumbnail circle large
                                                                source={require('../../resources/images/Candidate_Profile/ProjectIcon/Project-icon.png')} />
                                                            <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(14), justifyContent: 'center', alignItems: 'center' }}>Projects</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('ContributionScreen', { politician_id: this.state.politician_id, isMy: this.state.isMy })}>
                                                        <View style={{ margin: 5, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                            <Thumbnail circle large
                                                                source={require('../../resources/images/Candidate_Profile/Contribution-icon/Contribution-icon.png')} />
                                                            <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(14), justifyContent: 'center', alignItems: 'center' }}>Contribution</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                </View>
                                            </CardItem>
                                            <CardItem>
                                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'stretch' }}>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('BillScreen', { politician_id: this.state.politician_id, isMy: this.state.isMy })}>
                                                        <View style={{ margin: 5, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                            <Thumbnail circle large
                                                                source={require('../../resources/images/Candidate_Profile/bills-icon/bills-icon.png')} />
                                                            <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(14), justifyContent: 'center', alignItems: 'center' }}>Bills</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={{ margin: 5 }}>
                                                        <View style={{ width: wp('20%') }} />
                                                    </View>
                                                    <View style={{ margin: 5 }}>
                                                        <View style={{ width: wp('20%') }} />
                                                    </View>
                                                </View>
                                            </CardItem>
                                        </Card>
                                    )
                                    :
                                    (
                                        <Card>
                                            <CardItem>
                                                <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'stretch' }}>
                                                    <TouchableOpacity onPress={()=>{Share.open(this.shareOptions)}}>
                                                        <View style={{ margin: 5, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                            <Thumbnail circle large
                                                                source={require('../../resources/images/Candidate_Profile/ShareWithContacts/Share-with-contacts.png')} />
                                                            <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(14), justifyContent: 'center', alignItems: 'center' }}>{"Share with \n Contacts"}</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('DonateScreen', { politician_id: this.state.politician_id })}>
                                                        <View style={{ margin: 5, flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                                            <Thumbnail circle large
                                                                source={require('../../resources/images/Candidate_Profile/Donating-icon/Donating-icon.png')} />
                                                            <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(14), justifyContent: 'center', alignItems: 'center' }}>Donating</Text>
                                                        </View>
                                                    </TouchableOpacity>
                                                    <View style={{ margin: 5 }}>
                                                        <View style={{ width: wp('20%') }} />
                                                    </View>
                                                </View>
                                            </CardItem>
                                        </Card>
                                    )
                            }
                        </View>
                    </View>
                </View>
                <Thumbnail circle large
                    source={getUserProfileDummyUrl(this.state.candidateDetailResponse.politician_details.profile_picture) }
                    style={{
                        position: 'absolute',
                        top: hp('21%'),
                        left: wp('10%'),
                    }}
                />
            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: normalize(20),
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});


const mapStateToProps = (state) => {
    return {
        error: state.CandidateDetailR.error,
        isLoading: state.CandidateDetailR.isLoading,
        candidateDetailResponse: state.CandidateDetailR.candidateDetailResponse,
        candidateDetailRatingResponse: state.CandidateDetailR.candidateDetailRatingResponse,
    };
};

export default connect(mapStateToProps)(CandidateProfileScreen);