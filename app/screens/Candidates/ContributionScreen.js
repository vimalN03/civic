import React, { Component } from 'react';
import { RefreshControl, BackHandler, StyleSheet, Image, View, TouchableOpacity, ImageBackground, FlatList, Dimensions } from 'react-native';
import { Container, Header, Left, Right, Title, Body, Content, Button, Text, Card, CardItem, Icon } from 'native-base';
import { connect } from 'react-redux';
import { colors, normalize, hp, dimens } from '../../resources';
import _ from 'lodash';
import { getProfile, BASE_URL } from '../../utils/URLConstants';
import { getContributionListAction, onClearState } from '../../actions/contributionList-actions';
import Loader from '../../components/Loader/Loader';
import Share, {ShareSheet} from 'react-native-share';

class ContributionScreen extends Component {

  constructor(props) {
    super(props)
    const { politician_id, isMy } = props.navigation.state.params
    this.state = {
      politician_id: politician_id,
      isMy: (isMy) ? true : false,
      loading: false,
      refreshing: true,
      contributionListResponse: [],
      errMsg: null,
      type: '',
      userId: null
    }
    this.shareOptions = {
      title: "Civic",
      message: "Pasta on 20% discount, valid for the rest of time!",
      url: "http://facebook.github.io/react-native/",
      subject: "Civic" //  for email
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.setState({ contributionListResponse: [] });
        this.getContributionListApi();
      }
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getContributionListApi();      
  }

  /**
   * Get List of Contribution
   */
  getContributionListApi = () => {
    
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.setState({ loading: true, userId: value.data.id, type: value.data.type })
        this.props.dispatch(onClearState());
        this.props.dispatch(getContributionListAction(value.data.token, { politician_id: this.state.politician_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.contributionListResponse) === false && props.error === '') {
      if (props.contributionListResponse.status === 'success') {
        console.log(" componentWillReceiveProps contributionListResponse", props.contributionListResponse)
        this.setState({
          refreshing: true,
          contributionListResponse: props.contributionListResponse.data, 
          loading: false,
          errMsg: (props.contributionListResponse.data.length === 0)? 'No Result Found' : null
        })
      } else {
        this.setState({ 
          refreshing: true,
          loading: false, 
          errMsg: (props.contributionListResponse.data.length === 0)? 'No Result Found' : null
        })
        // setTimeout(() => { alert(props.contributionListResponse.message) }, 800);
      }
     
    }
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  renderItem = ({ item, index }) => {
    let url = BASE_URL.IMAGE_URL + item.image
    return <TouchableOpacity onPress={() => { this.props.navigation.navigate('ContributionDetailScreen', { contribution_id: item.id, userId: this.state.userId, type: this.state.type, isMy: this.state.isMy }) }}
      activeOpacity={1} style={{ borderRadius: 5 }}>
      <View style={{
        margin: 5,
        flexDirection: 'column',
        height: hp('40%')
      }}>
        <Card style={{ flex: 1, flexDirection: 'column' }}>
          <ImageBackground style={{ flex: 2, width: '100%' }} source={{ uri: url }}>
            <View style={{ position: "absolute", bottom: 0, left: 0, paddingStart: 10, paddingEnd: 10, paddingBottom: 10 }}>
              <Text numberOfLines={2} ellipsizeMode='tail' style={{ lineHeight: 18, fontFamily: 'Nunito-Medium', color: colors.primary, fontSize: normalize(12), textAlign: 'left' }}>
                {item.title}
              </Text>
            </View>
            <View style={{ position: "absolute", top: 0, right: 10 }}>
              <Button small iconLeft style={{ backgroundColor: colors.primary, marginTop: 10, borderRadius: 5 }}
                onPress={()=>{Share.open(this.shareOptions)}}
              >
                <Icon style={{ fontSize: normalize(15) }} name='share' />
                <Text style={{ fontSize: normalize(10) }}>Share</Text>
              </Button>
            </View>
          </ImageBackground>
          <View style={{ padding: 10 }}>
            <Text numberOfLines={4} ellipsizeMode='tail' style={{ lineHeight: 20, fontFamily: 'Nunito-Medium', color: colors.black, fontSize: normalize(12), textAlign: 'left' }}>
              {item.description}
            </Text>
          </View>
        </Card>
      </View>
    </TouchableOpacity>
  }

  render() {
    return (
      <Container>
        <Header transparent={false} style={{ backgroundColor: colors.white, alignItems: 'center' }} androidStatusBarColor={'#019235'} iosBarStyle="light-content">
          <Left style={{ flex: 0.2, marginLeft: 5 }}>
            <TouchableOpacity onPress={() => { this.props.navigation.goBack() }}>
              <Image style={{ width: 25, height: 25 }} source={require('../../resources/images/left-arrow.png')} />
            </TouchableOpacity>
          </Left>
          <Body style={{flex: 2,justifyContent:'flex-start', marginLeft:10, alignItems:'flex-start'}}>
            <Title style={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }}>Contribution</Title>
          </Body>
          {(this.state.type === 'elected_official' && this.state.isMy) ?
            (<Right>
              <TouchableOpacity onPress={() => { this.props.navigation.navigate('ContributionCreateScreen', { contribution_details: {} }) }}>
                <View style={{
                  backgroundColor: colors.primary, borderRadius: 5,
                  padding: 5, alignItems: 'center'
                }}>
                  <Text uppercase={false} style={{
                    margin: 5,
                    textAlignVertical: 'center', color: colors.white, fontSize: normalize(dimens.headerButtonText)
                  }}>Add Contribution</Text>
                </View>
              </TouchableOpacity>
            </Right>) : (null)
          }
        </Header>
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        
        <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
          <FlatList
            data={this.state.contributionListResponse}
            renderItem={this.renderItem}
            refreshControl={
              <RefreshControl
                refreshing={!this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          />
          </View>
        
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }, list: {
    justifyContent: 'center',
    flexDirection: 'row',
    flexWrap: 'wrap',
  }
});



const mapStateToProps = (state) => {
  return {
    error: state.ContributionListR.error,
    isLoading: state.ContributionListR.isLoading,
    contributionListResponse: state.ContributionListR.contributionListResponse
  };
};

export default connect(mapStateToProps)(ContributionScreen);