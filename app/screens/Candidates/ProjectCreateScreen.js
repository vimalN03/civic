import React, { Component } from 'react';
import { StyleSheet, Platform, View, BackHandler, TextInput } from 'react-native';
import { Container, Content, Button, Text, Card, CardItem, Textarea } from 'native-base';
import ImagePicker from 'react-native-image-picker';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, dimens } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile } from '../../utils/URLConstants';
import { getProjectCreationAction, onClearState } from '../../actions/projectCreation-actions';
import Loader from '../../components/Loader/Loader';
import ImageResizer from 'react-native-image-resizer';

class ProjectCreateScreen extends Component {

  constructor(props) {
    super(props);
    const { project_details } = props.navigation.state.params
    console.log('=============',project_details)
    if(_.isEmpty(project_details.project_image) === false){
        var fName = project_details.project_image.split('/').pop();
    }
    this.state = {
      project_details: project_details,
      projectTitle: (_.isEmpty(project_details.project_title))? '' : project_details.project_title,
      uploadPhoto: (_.isEmpty(project_details.project_title))? '' : fName,
      projectDescription: project_details.project_description,
      avatarSource: ''
    }
  }

  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.projectCreationResponse) === false && props.error === '') {
      if (props.projectCreationResponse.status === 'success') {
        console.log(" componentWillReceiveProps projectCreationResponse", props.projectCreationResponse)
        this.setState({ loading: false })
        this.props.navigation.goBack();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.projectCreationResponse.message) }, 800);
      }
    }
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    /**
    * The first arg is the options object for customization (it can also be null or omitted for default options)
    ** The second arg is the callback which sends object: response (more info in the API Reference)
    */
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      this.setState({ loading: true },()=>{
        this.setState({ loading: true })
      })
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
if(Platform.OS !== 'ios'){
        ImageResizer.createResizedImage(response.uri, 800, 600, 'PNG', 30, 90)
          .then(({ uri }) => {
              
              var fileExtension = uri.replace(/^.*\./, '');
              var fileName = response.uri.split('/').pop()
              let source = {
                  uri: uri,
                  type: `image/${fileExtension}`,
                  name: fileName
              };
              this.setState({
                  loading: false,
                  avatarSource: source,
                  uploadPhoto: fileName
              },()=>{
                this.setState({
                  loading: false,
                  avatarSource: source,
                  uploadPhoto: fileName
                });
              });
              console.log('avatarSource', this.state.avatarSource);
          })
          .catch(err => {
              console.log('123456',err);
              this.setState({ loading: true })
          });
}else{
        var fileExtension = response.fileName.replace(/^.*\./, '');
        
        let source = {
          //uri: 'data:image/jpeg;base64,' + response.data,
          uri: response.uri,
          type: `image/${fileExtension}`,
          name: response.fileName
        };
        this.setState({
          loading:false,
          avatarSource: source,
          uploadPhoto: response.fileName
        });
        console.log('avatarSource', this.state.avatarSource);
}
        
      }
    });
  }

  /**
   * Validate Project Creation
   */
  validateProjectCreation = () => {
    if (this.state.projectTitle === undefined || this.state.projectTitle.trim() === '') {
      setTimeout(() => { alert('Please Enter the Project Title') }, 800);
    } else if (this.state.uploadPhoto.trim() === '') {
      setTimeout(() => { alert('Please Upload Photo') }, 800);
    } else if (this.state.projectDescription === undefined || this.state.projectDescription.trim() === '') {
      setTimeout(() => { alert('Please Enter Project Description') }, 800);
    } else {
      this.setState({ loading: true })
      getProfile().then((value) => {
        if (!_.isEmpty(value)) {
          var formData = new FormData()
          formData.append('project_title', this.state.projectTitle)
          formData.append('project_image', this.state.avatarSource)
          formData.append('project_description', this.state.projectDescription)
          if (!_.isEmpty(this.state.project_details)) {
            this.props.dispatch(onClearState());
            formData.append('project_id', this.state.project_details.id)
            this.props.dispatch(getProjectCreationAction(value.data.token, formData, true));
          } else {
            this.props.dispatch(onClearState());
            this.props.dispatch(getProjectCreationAction(value.data.token, formData, false));
          }
        } else {
          this.setState({ loading: false })
        }
      }).catch((error) => {
        this.setState({ loading: false })
        console.log(error.message)
      })
    }
  }
  

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  render() {
    
    return (
      <Container>
        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title= {(_.isEmpty(this.state.project_details.project_title) === true)? 'Create Project' : 'Edit Project' }
          titleStyle={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }} />
        {(this.state.loading) ? <Loader /> : null}
        <Content>
          <View style={{ marginStart: 15, marginEnd: 15 }}>
            <Card padder style={{ flexDirection: 'column', padding: 10 }}>
              <View style={styles.textInputContainer}>
                <TextInput
                  returnKeyType={'next'}
                  value={this.state.projectTitle}
                  style={{ flex: 1, padding: 10 }}
                  placeholder="Add Project Title"
                  placeholderTextColor={colors.light_blue}
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => this.setState({ projectTitle: text })}
                />
              </View>
              <View style={styles.textInputContainer}>
                <TextInput
                  editable={false}
                  value={this.state.uploadPhoto}
                  style={{ flex: 1, padding: 10 }}
                  placeholder="Upload a Photo"
                  placeholderTextColor={colors.light_blue}
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => this.setState({ uploadPhoto: text })}
                />
                <Button style={{ backgroundColor: colors.primary, height: '100%' }} 
                  onPress={() => {
                    if (this.state.projectTitle === undefined || this.state.projectTitle.trim() === '') {
                      setTimeout(() => { alert('Please Enter the Project Title') }, 800);
                    } else{
                      this.selectPhotoTapped()
                    }
                    
                  }}>
                  <Text style={{ fontFamily: 'Nunito-Medium' }}>Browse</Text>
                </Button>
              </View>
              <View style={styles.textInputContainer}>
                <Textarea rowSpan={5} placeholder="Project Description"
                  value={this.state.projectDescription}
                  placeholderTextColor={colors.light_blue}
                  style={{ flex: 1, padding: 10 }}
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => this.setState({ projectDescription: text })}
                />
              </View>
              <CardItem style={{ justifyContent: 'center', alignSelf: 'center' }}>
                <Button rounded style={{ backgroundColor: colors.primary }} onPress={() => { this.validateProjectCreation() }}>
                  <Text style={{ fontFamily: 'Nunito-Medium' }}>{(_.isEmpty(this.state.project_details.project_title) === true)? 'Create Project' : 'Save' }</Text>
                </Button>
              </CardItem>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInputContainer: {
    flexDirection: 'row',
    paddingLeft: 5,
    marginTop: 12,
    borderWidth: 0.5,
    borderRadius: 1,
    borderColor: '#dfe6f0',
    backgroundColor: '#eff4f8',
    marginStart: 10, marginEnd: 10
  }
});



const mapStateToProps = (state) => {
  return {
    error: state.ProjectCreationR.error,
    isLoading: state.ProjectCreationR.isLoading,
    projectCreationResponse: state.ProjectCreationR.projectCreationResponse
  };
};

export default connect(mapStateToProps)(ProjectCreateScreen);