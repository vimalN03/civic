import React, { Component } from 'react';
import { StyleSheet, PermissionsAndroid, Platform, View, TextInput, BackHandler } from 'react-native';
import { Container, Content, Button, Text, Card, CardItem, Textarea } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, dimens } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile } from '../../utils/URLConstants';
import { getBillCreationAction, onClearState } from '../../actions/billCreation-action';
import Loader from '../../components/Loader/Loader';
import ImagePicker from 'react-native-image-picker';
import { DocumentPicker, DocumentPickerUtil } from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import ImageResizer from 'react-native-image-resizer';
class BillCreationScreen extends Component {

    constructor(props) {
        super(props);
        const { bill_details } = props.navigation.state.params
        console.log(bill_details)
        if(_.isEmpty(bill_details.title) === false){
            var fName = bill_details.document.split('/').pop();
        }
        
        this.state = {
            bill_details: bill_details,
            billTitle: (_.isEmpty(bill_details.title))? '' : bill_details.title,
            uploadPhoto: (_.isEmpty(bill_details.document))? '' : fName,
            billCreationResponse: {},
            avatarSource: ''
        }
    }

    async componentDidMount(){
        if(Platform.OS !== 'ios'){
            try {
                const granted = await PermissionsAndroid.request(
                PermissionsAndroid.PERMISSIONS.READ_EXTERNAL_STORAGE,
                {
                    'title': 'Cool Photo AppCamera Permission',
                    'message': 'Cool Photo App needs access to your camera ' +
                            'so you can take awesome pictures.'
                }
                )
                if (granted === PermissionsAndroid.RESULTS.GRANTED) {
                console.log("You can use the camera")
                } else {
                console.log("Camera permission denied")
                }
            } catch (err) {
                console.warn(err)
            }
        }
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    }

    componentWillReceiveProps = (props) => {
        if (props.error.status === 'error') {
            if (props.error.status === 'error') {
                // setTimeout(() => { alert(props.error.message) }, 800);
                this.props.dispatch(onClearState());
            }
            this.setState({ loading: false })
        }
        if (_.isEmpty(props.billCreationResponse) === false && props.error === '') {
            if (props.billCreationResponse.status === 'success') {
                console.log(" componentWillReceiveProps billCreationResponse", props.billCreationResponse)
                this.setState({ loading: false })
                this.props.navigation.goBack();
            } else {
                this.setState({ loading: false })
                setTimeout(() => { alert(props.billCreationResponse.message) }, 800);
            }
        }
    }

    /**
   * Image and Pdf upload of bills
   */
    selectPhotoTapped() {
        const options = {
            quality: 1.0,
            storageOptions: {
                skipBackup: true,
                path: 'images',
            },
        };

        /**
        * The first arg is the options object for customization (it can also be null or omitted for default options)
        ** The second arg is the callback which sends object: response (more info in the API Reference)
        */
        ImagePicker.showImagePicker(options, (response) => {
            console.log('Response = ', response);
            this.setState({ loading: true },()=>{
                this.setState({ loading: true })
              })
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
if(Platform.OS === 'ios'){
                var fileExtension = response.fileName.replace(/^.*\./, '');
                
                let source = {
                //uri: 'data:image/jpeg;base64,' + response.data,
                    uri: response.uri,
                    type: `image/${fileExtension}`,
                    name: response.fileName
                };
                this.setState({
                    loading: false,
                    avatarSource: source,
                    uploadPhoto: response.fileName
                });

                console.log('avatarSource', this.state.avatarSource);
}else{
                ImageResizer.createResizedImage(response.uri, 800, 600, 'PNG', 30, 90)
                .then(({ uri }) => {
                    console.log('123456',uri)
                    var fileExtension = uri.replace(/^.*\./, '');
                    var fileName = response.uri.split('/').pop()
                    let source = {
                        uri: uri,
                        type: `image/${fileExtension}`,
                        name: fileName
                    };
                    this.setState({
                        loading: false,
                        avatarSource: source,
                        uploadPhoto: fileName
                    });
                    console.log('avatarSource', this.state.avatarSource);
                })
                .catch(err => {
                    console.log('123456',err);
                    this.setState({ loading: false })
                });
}
            }
        });
    }

    selectFiles = () => {
        DocumentPicker.show({
          filetype: [DocumentPickerUtil.allFiles()],
        },(error,res) => {
            console.log('====', res)
    if(res !== null){
        var fileExtension = res.fileName.replace(/^.*\./, '');
        if(fileExtension.toUpperCase() === 'JPEG' || fileExtension.toUpperCase() === 'JPG' || fileExtension.toUpperCase() === 'PNG' || fileExtension.toUpperCase() === 'PDF'){
            if(res !== null){
                console.log('=======pickerres',res);
                if (Platform.OS === 'ios') {
                    var filename = res.uri.replace('file:///private', '')
                } else {
                    var filename = res.uri
                }
                RNFetchBlob.fs.stat(filename)
                .then((stat) => {
                    console.log('===statres',stat)
                    let source = {
                        //uri: 'data:image/jpeg;base64,' + response.data,
                        uri: filename,
                        type: stat.type,
                        name: res.fileName
                    };
                    this.setState({
                        avatarSource: source,
                        uploadPhoto: res.fileName
                    });
                                          
                })
                .catch((err) => {
                    console.log('===staterr',err)
                })
            }
        }else{
            setTimeout(() => {
                alert('Only IMAGE and PDF files are allowed.');
            }, 800);
        }
    }
        });     
      }

    /**
   * Validate Bill Creation
   */
    validateBillCreation = () => {
        console.log('validateBillCreation', this.state.billTitle)
        if (this.state.billTitle === undefined || this.state.billTitle.trim() === '') {
            setTimeout(() => { alert('Please Enter the Bill Title') }, 800);
        } else if (this.state.uploadPhoto.trim() === '') {
            setTimeout(() => { alert('Please Upload Photo') }, 800);
        } else {
            this.setState({ loading: true })
            getProfile().then((value) => {
                if (!_.isEmpty(value)) {
                    var formData = new FormData()
                    formData.append('title', this.state.billTitle)
                    formData.append('bill', this.state.avatarSource)
                    if (!_.isEmpty(this.state.bill_details)) {
                        formData.append('bill_id', this.state.bill_details.id)
                        this.props.dispatch(onClearState());
                        this.props.dispatch(getBillCreationAction(value.data.token, formData, true));
                    } else {
                        this.props.dispatch(onClearState());
                        this.props.dispatch(getBillCreationAction(value.data.token, formData, false));
                    }
                } else {
                    this.setState({ loading: false })
                }
            }).catch((error) => {
                this.setState({ loading: false })
                console.log(error.message)
            })
        }
    }

    componentWillUnmount(){
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
    }
    
    handleBackButtonClick = () => {
        return this.props.navigation.goBack();
    }

    render() {
        return (
            <Container>
                <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
                    leftIcon={require('../../resources/images/left-arrow.png')}
                    onPressLeft={() => { this.props.navigation.goBack() }}
                    title= {(_.isEmpty(this.state.bill_details.title) === true)? 'Create Bill' : 'Edit Bill' }
                    titleStyle={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }} />
                {(this.state.loading) ? <Loader /> : null}
                <Content>
                    <View style={{ marginStart: 15, marginEnd: 15 }}>
                        <Card padder style={{ flexDirection: 'column', padding: 10 }}>
                            <View style={styles.textInputContainer}>
                                <TextInput
                                    value={this.state.billTitle}
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Add Bill Title"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => this.setState({ billTitle: text })}
                                />
                            </View>
                            <View style={styles.textInputContainer}>
                                <TextInput
                                    value={this.state.uploadPhoto}
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Upload a Photo"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => this.setState({ uploadPhoto: text })}
                                />
                                <Button style={{ backgroundColor: colors.primary, height: '100%' }} 
                                    onPress={() => {
                                        if (this.state.billTitle === undefined || this.state.billTitle.trim() === '') {
                                            setTimeout(() => { alert('Please Enter the Bill Title') }, 800);
                                        }else{
                                        this.selectPhotoTapped()
                                        }
                                    }}>
                                    <Text style={{ fontFamily: 'Nunito-Medium' }}>Browse</Text>
                                </Button>
                            </View>
                            <CardItem style={{ justifyContent: 'center', alignSelf: 'center' }}>
                                <Button rounded style={{ backgroundColor: colors.primary }} onPress={() => { this.validateBillCreation() }}>
                                    <Text style={{ fontFamily: 'Nunito-Medium' }}>{(_.isEmpty(this.state.bill_details.title) === true)? 'Create Bill' : 'Save' }</Text>
                                </Button>
                            </CardItem>
                        </Card>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: normalize(20),
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    textInputContainer: {
        flexDirection: 'row',
        paddingLeft: 5,
        marginTop: 12,
        borderWidth: 0.5,
        borderRadius: 1,
        borderColor: '#dfe6f0',
        backgroundColor: '#eff4f8',
        marginStart: 10, marginEnd: 10
    }
});


const mapStateToProps = (state) => {
    return {
        error: state.BillCreationR.error,
        isLoading: state.BillCreationR.isLoading,
        billCreationResponse: state.BillCreationR.billCreationResponse
    };
};

export default connect(mapStateToProps)(BillCreationScreen);