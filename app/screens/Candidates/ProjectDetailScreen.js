import React, { Component } from 'react';
import { StyleSheet, View, BackHandler, Image } from 'react-native';
import { Container, Content, Button, Icon, Text, Card, CardItem } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, dimens, hp } from '../../resources';
import Loader from '../../components/Loader/Loader';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProjectDetailAction, getProjectDeleteAction, onClearState } from '../../actions/projectDetail-actions';
import { BASE_URL, getProfile } from '../../utils/URLConstants';
import Share, {ShareSheet} from 'react-native-share';

class ProjectDetailScreen extends Component {

  constructor(props) {
    super(props)
    const { project_id, isMy, userId } = props.navigation.state.params
    this.state = {
      userId: userId,
      project_id: project_id,
      isMy: (isMy) ? true : false,
      loading: false,
      projectDetailResponse: {}
    }
    this.shareOptions = {
      title: "Civic",
      message: "Pasta on 20% discount, valid for the rest of time!",
      url: "http://facebook.github.io/react-native/",
      subject: "Civic" //  for email
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getProjectDetailApi();
      }
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
    
  }


  /**
   * Get Project Details
   */
  getProjectDetailApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getProjectDetailAction(value.data.token, { project_id: this.state.project_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }


  /**
   * Delete Project
   */
  postProjectDeleteApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getProjectDeleteAction(value.data.token, { project_id: this.state.project_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.projectDetailResponse) === false && props.error === '') {
      if (props.projectDetailResponse.status === 'success') {
        console.log(" componentWillReceiveProps projectDetailResponse", props.projectDetailResponse)
        this.setState({
          projectDetailResponse: props.projectDetailResponse.data, loading: false
        })
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.projectDetailResponse.message) }, 800);
      }
      
    }

    if (_.isEmpty(props.projectDeleteResponse) === false && props.error === '') {
      if (props.projectDeleteResponse.status === 'success') {
        console.log(" componentWillReceiveProps projectDeleteResponse", props.projectDeleteResponse)
        this.setState({ loading: false })
        this.props.navigation.goBack();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.projectDeleteResponse.message) }, 800);
      }
      
    }
  }

  /**
   * Edit Project Details
   */
  goToProjectDetailsEdit = () => {
    this.props.navigation.navigate('ProjectCreateScreen', { project_details: this.state.projectDetailResponse })
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  render() {
    const url = BASE_URL.IMAGE_URL + this.state.projectDetailResponse.project_image
    return (
      <Container>
        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Project Detail' titleStyle={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }}
          rightIcon={require('../../resources/images/User_Profile/Edit_Green_icon/EditGreen.png')}
          rightVisibility={parseInt(this.state.userId) === parseInt(this.state.projectDetailResponse.user_id)}
          onPressRight={() => {
            this.goToProjectDetailsEdit()
          }} />
        {(this.state.loading) ? <Loader /> : null}
        <Content>
          <View style={{ marginStart: 10, marginEnd: 10 }}>
            <Card>
              <View style={{ height: hp('35%'), marginTop: 2 }}>
                <Image style={{ width: '100%', height: '100%' }} source={{ uri: url }} />
              </View>
              <CardItem style={{ marginEnd: 5, marginStart: 5, flexDirection: 'column' }}>
                <View style={{ flex: 1, alignSelf: 'flex-start' }}>
                  <Text numberOfLines={2} style={{ fontFamily: 'Nunito-Medium', textAlign: 'left', lineHeight: 20 }}>
                    {this.state.projectDetailResponse.project_title}
                  </Text>
                </View>
                <View style={{ flex: 1, alignSelf: 'flex-start', marginTop: 2, justifyContent: 'flex-start' }}>
                  <Text style={{ fontSize: normalize(12), color: colors.litegray, fontFamily: 'Nunito-Medium', textAlign: 'left', alignContent: 'flex-start' }}>
                    {this.state.projectDetailResponse.updated_at}
                  </Text>
                </View>
              </CardItem>

              <CardItem style={{ marginEnd: 5, marginStart: 5, flexDirection: 'column' }}>
                <Text style={{ fontSize: normalize(12), alignSelf:'flex-start', fontFamily: 'Nunito-Medium', color: colors.litegray, textAlign: 'left', lineHeight: 15 }}>
                  {this.state.projectDetailResponse.project_description}
                </Text>
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
                  <Button iconLeft style={{ backgroundColor: colors.primary, marginTop: 10, borderRadius: 5 }}
                    onPress={()=>{Share.open(this.shareOptions)}}
                  >
                    <Icon name='share' />
                    <Text style={{ fontFamily: 'Nunito-Medium' }}>Share</Text>
                  </Button>
                  {(parseInt(this.state.userId) === parseInt(this.state.projectDetailResponse.user_id)) ?
                    (<Button onPress={() => { this.postProjectDeleteApi() }} iconLeft style={{ backgroundColor: colors.primary, marginTop: 10, marginStart: 10, borderRadius: 5 }}>
                      <Icon name='trash-o' type='FontAwesome' />
                      <Text style={{ fontFamily: 'Nunito-Medium' }}>Delete</Text>
                    </Button>) : (null)
                  }
                </View>
              </CardItem>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


const mapStateToProps = (state) => {
  return {
    error: state.ProjectDetailR.error,
    isLoading: state.ProjectDetailR.isLoading,
    projectDetailResponse: state.ProjectDetailR.projectDetailResponse,
    projectDeleteResponse: state.ProjectDetailR.projectDeleteResponse,
  };
};

export default connect(mapStateToProps)(ProjectDetailScreen);