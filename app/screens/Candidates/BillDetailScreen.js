import React, { Component } from 'react';
import { StyleSheet, View, Image, Platform, BackHandler } from 'react-native';
import { Container, Content, Button, Text, Card, CardItem } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, hp, normalize } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import PDFView from 'react-native-view-pdf';
import { getProfile, BASE_URL } from '../../utils/URLConstants';
import { getBillDetailAction, getBillDetailDeleteAction, onClearState } from '../../actions/billDetail-actions';
import Loader from '../../components/Loader/Loader';

class BillDetailScreen extends Component {

  constructor(props) {
    super(props)
    const { bill_id, isMy, type, userId } = props.navigation.state.params
    this.state = {
      userId: userId,
      type: type,
      bill_id: bill_id,
      isMy: (isMy) ? true : false,
      loading: false,
      billDetailResponse: {}
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getBillDetailApi();
      }
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  /**
   * Get Bill Details
   */
  getBillDetailApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getBillDetailAction(value.data.token, { bill_id: this.state.bill_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  /**
   * Delete Bill
   */
  getBillDetailDeleteApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getBillDetailDeleteAction(value.data.token, { bill_id: this.state.bill_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.billDetailResponse) === false && props.error === '') {
      if (props.billDetailResponse.status === 'success') {
        console.log(" componentWillReceiveProps billDetailResponse", props.billDetailResponse)
        this.setState({
          billDetailResponse: props.billDetailResponse.data, loading: false
        })
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.billDetailResponse.message) }, 800);
      }
    }

    if (_.isEmpty(props.billDetailDeleteResponse) === false && props.error === '') {
      if (props.billDetailDeleteResponse.status === 'success') {
        console.log(" componentWillReceiveProps billDetailDeleteResponse", props.billDetailDeleteResponse)
        this.setState({ loading: false });
        this.props.navigation.goBack();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.billDetailDeleteResponse.message) }, 800);
      }
    }
  }

  /**
   * Edit Bill details
   */
  goToBillDetailsEdit = () => {
    this.props.navigation.navigate('BillCreationScreen', { bill_details: this.state.billDetailResponse })
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  render() {
    const url = BASE_URL.IMAGE_URL + this.state.billDetailResponse.document;
        var fileExtension = url.replace(/^.*\./, '');
        if(fileExtension.toUpperCase() === 'JPEG' || fileExtension.toUpperCase() === 'JPG' || fileExtension.toUpperCase() === 'PNG' ){
          var fExt = true;
        }else{
          var fExt = false;
        }
    const resources = {
      url: url //'https://www.ets.org/Media/Tests/TOEFL/pdf/SampleQuestions.pdf',
    };
    const resourceType = 'url';
    return (
      <Container>
        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Bills'
          rightIcon={require('../../resources/images/User_Profile/Edit_Green_icon/EditGreen.png')}
          rightVisibility={parseInt(this.state.userId) === parseInt(this.state.billDetailResponse.user_id)}
          onPressRight={() => { this.goToBillDetailsEdit() }} />
        {(this.state.loading) ? <Loader /> : null}
        <Content>
          <View style={{ marginStart: 15, marginEnd: 15 }}>
            <Card style={{ flexDirection: 'column', padding: 10 }}>
              <CardItem style={{ flex: 1, height: hp('60%') }}>
                {(fExt)?
                <Image style={{ height: "100%", width: '100%' }} source={{ uri: url }} />
                :
                <PDFView
                  fadeInDuration={250.0}
                  style={{ flex: 1, height: "100%", width: '100%' }}
                  resource={resources[resourceType]}
                  resourceType={resourceType}
                  onLoad={() => console.log(`PDF rendered from ${resourceType}`)}
                  onError={(error) => console.log('Cannot render PDF', error)}
                />
                }
              </CardItem>
              <CardItem style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <Text style={{ flex: 2.5, fontFamily: 'Nunito-Medium', fontSize: normalize(12) }}>{this.state.billDetailResponse.title}</Text>
                {
                  (parseInt(this.state.userId) === parseInt(this.state.billDetailResponse.user_id)) ?
                    (
                      <Button onPress={() => { this.getBillDetailDeleteApi() }} transparent style={{ flex: 1 }}>
                        <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, fontSize: normalize(12) }}>Delete</Text>
                      </Button>)
                    :
                    (null)
                }
              </CardItem>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  }
});


const mapStateToProps = (state) => {
  return {
    error: state.BillDetailR.error,
    isLoading: state.BillDetailR.isLoading,
    billDetailResponse: state.BillDetailR.billDetailResponse,
    billDetailDeleteResponse: state.BillDetailR.billDetailDeleteResponse
  };
};

export default connect(mapStateToProps)(BillDetailScreen);