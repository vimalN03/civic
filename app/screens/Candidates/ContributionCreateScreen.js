import React, { Component } from 'react';
import { StyleSheet, View, Platform, TextInput, BackHandler } from 'react-native';
import { Container, Content, Button, Text, Card, CardItem, Textarea } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, dimens } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile } from '../../utils/URLConstants';
import { getContributionCreationAction, onClearState } from '../../actions/contributionCreation-actions';
import Loader from '../../components/Loader/Loader';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

class ContributionCreateScreen extends Component {

  constructor(props) {
    super(props);
    const { contribution_details } = props.navigation.state.params
    if(_.isEmpty(contribution_details.image) === false){
      var fName = contribution_details.image.split('/').pop();
    }
    
    this.state = {
      contribution_details: contribution_details,
      contributionTitle: (_.isEmpty(contribution_details.title))? '' : contribution_details.title,
      uploadPhoto: (_.isEmpty(contribution_details.title))? '' : fName,
      contributionDescription: contribution_details.description,
      avatarSource: ''
    }
  }

  componentDidMount(){
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  componentWillReceiveProps = (props) => {
    if (props.error.status === 'error') {
      if (props.error.status === 'error') {
        // setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.contributionCreationResponse) === false && props.error === '') {
      if (props.contributionCreationResponse.status === 'success') {
        console.log(" componentWillReceiveProps contributionCreationResponse", props.contributionCreationResponse)
        this.setState({ loading: false })
        //setTimeout(() => { alert(props.contributionCreationResponse.message) }, 800);
        this.props.navigation.goBack();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.contributionCreationResponse.message) }, 800);
      }
    }
  }

  selectPhotoTapped() {
    const options = {
      quality: 1.0,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };

    /**
    * The first arg is the options object for customization (it can also be null or omitted for default options)
    ** The second arg is the callback which sends object: response (more info in the API Reference)
    */
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      this.setState({ loading: true },()=>{
        this.setState({ loading: true })
      })
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {

if(Platform.OS === 'ios'){
        var fileExtension = response.fileName.replace(/^.*\./, '');
       
        let source = {
          //uri: 'data:image/jpeg;base64,' + response.data,
          uri: response.uri,
          type: `image/${fileExtension}`,
          name: response.fileName
        };
        this.setState({
          loading: false,
          avatarSource: source,
          uploadPhoto: response.fileName
        });
}else{
        ImageResizer.createResizedImage(response.uri, 800, 600, 'PNG', 30, 90)
        .then(({ uri }) => {
            
            var fileExtension = uri.replace(/^.*\./, '');
            var fileName = response.uri.split('/').pop()
            let source = {
                uri: uri,
                type: `image/${fileExtension}`,
                name: fileName
            };
            this.setState({
                loading: false,
                avatarSource: source,
                uploadPhoto: fileName
            });
            console.log('avatarSource', this.state.avatarSource);
        })
        .catch(err => {
            console.log('123456',err);
            this.setState({ loading: false })
        });
}
          
      }
    });
  }

  /**
   * Validation for Contribution Fields
   */
  validateContributionCreation = () => {
    if (this.state.contributionTitle === undefined || this.state.contributionTitle.trim() === '') {
      setTimeout(() => { alert('Please Enter the Contribution Title') }, 800);
    } else if (this.state.uploadPhoto.trim() === '') {
      setTimeout(() => { alert('Please Upload Photo') }, 800);
    } else if (this.state.contributionDescription === undefined || this.state.contributionDescription.trim() === '') {
      setTimeout(() => { alert('Please Enter Contribution Description') }, 800);
    } else {
      this.setState({ loading: true })
      getProfile().then((value) => {
        if (!_.isEmpty(value)) {
          var formData = new FormData()
          formData.append('contribution_title', this.state.contributionTitle)
          formData.append('contribution_image', this.state.avatarSource)
          formData.append('contribution_description', this.state.contributionDescription)
          if (!_.isEmpty(this.state.contribution_details)) {
            formData.append('contribution_id', this.state.contribution_details.id)
            this.props.dispatch(onClearState());
            this.props.dispatch(getContributionCreationAction(value.data.token, formData, true));
          } else {
            this.props.dispatch(onClearState());
            this.props.dispatch(getContributionCreationAction(value.data.token, formData, false));
          }
        } else {
          this.setState({ loading: false })
        }
      }).catch((error) => {
        this.setState({ loading: false })
        console.log(error.message)
      })
    }
  }

  componentWillUnmount(){
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return this.props.navigation.goBack();
  }

  render() {
    return (
      <Container>
        <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title= {(_.isEmpty(this.state.contribution_details.title) === true)? 'Create Contribution' : 'Edit Contribution' }
          titleStyle={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }} />
        {(this.state.loading) ? <Loader /> : null}
        <Content>
          <View style={{ marginStart: 15, marginEnd: 15 }}>
            <Card padder style={{ flexDirection: 'column', padding: 10 }}>
              <View style={styles.textInputContainer}>
                <TextInput
                  returnKeyType={'next'}
                  value={this.state.contributionTitle}
                  style={{ flex: 1, padding: 10 }}
                  placeholder="Add Contibution Title"
                  placeholderTextColor={colors.light_blue}
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => this.setState({ contributionTitle: text })}
                />
              </View>
              <View style={styles.textInputContainer}>
                <TextInput
                  value={this.state.uploadPhoto}
                  style={{ flex: 1, padding: 10 }}
                  placeholder="Upload a Photo"
                  placeholderTextColor={colors.light_blue}
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => this.setState({ uploadPhoto: text })}
                />
                <Button style={{ backgroundColor: colors.primary, height: '100%' }} 
                onPress={() => {
                  if (this.state.contributionTitle === undefined || this.state.contributionTitle.trim() === '') {
                    setTimeout(() => { alert('Please Enter the Contribution Title') }, 800);
                  } else{
                    this.selectPhotoTapped()
                  }
                  }}>
                  <Text style={{ fontFamily: 'Nunito-Medium' }}>Browse</Text>
                </Button>
              </View>
              <View style={styles.textInputContainer}>
                <Textarea returnKeyType={'done'} rowSpan={5} placeholder="Contribution Description"
                  placeholderTextColor={colors.light_blue}
                  value={this.state.contributionDescription}
                  style={{ flex: 1, padding: 10 }}
                  underlineColorAndroid="transparent"
                  onChangeText={(text) => this.setState({ contributionDescription: text })}
                />
              </View>
              <CardItem style={{ justifyContent: 'center', alignSelf: 'center' }}>
                <Button rounded style={{ backgroundColor: colors.primary }} onPress={() => { this.validateContributionCreation() }}>
                  <Text style={{ fontFamily: 'Nunito-Medium' }}> {(_.isEmpty(this.state.contribution_details.title) === true)? 'Create Contribution' : 'Save' }</Text>
                </Button>
              </CardItem>
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInputContainer: {
    flexDirection: 'row',
    paddingLeft: 5,
    marginTop: 12,
    borderWidth: 0.5,
    borderRadius: 1,
    borderColor: '#dfe6f0',
    backgroundColor: '#eff4f8',
    marginStart: 10, marginEnd: 10
  }
});


const mapStateToProps = (state) => {
  return {
    error: state.ContributionCreationR.error,
    isLoading: state.ContributionCreationR.isLoading,
    contributionCreationResponse: state.ContributionCreationR.contributionCreationResponse
  };
};

export default connect(mapStateToProps)(ContributionCreateScreen);