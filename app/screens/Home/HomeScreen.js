import React, { Component } from 'react';
import { StyleSheet, RefreshControl, TouchableOpacity, View, BackHandler, FlatList, Image, Platform } from 'react-native';
import { Container, Thumbnail, Content, Card, Text } from 'native-base';
import { colors, normalize } from '../../resources';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile, getHeaderKey, getUserProfileDummyUrl, getNetInfo, getImageDummyUrl } from '../../utils/URLConstants';
import { getHomeAction, onClearState } from '../../actions/home-actions';
import Loader from '../../components/Loader/Loader';
import Tabs from '../index';
import { getNewsAction } from '../../actions/news-action';

class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      loading: false,
      recentIssuesPollsResponse: [],
      newsResponse: [],
      errMsg: null,
      type:'',
      block_status: null
    }
    this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
  }

  componentDidMount() {
    
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        if(getNetInfo()){
          this.getRecentPollsListApi();
          this.props.dispatch(getNewsAction());
        }else{
          this.setState({ refreshing: true, loading: false });
          alert('Check Network Connection!')
        }
        // getNetInfo()
        // .then((data) => { console.log('netStatus',data) });
      }
    );
    BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);
  }


  /**
   * Recent Polls data from API
   */
  getRecentPollsListApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.setState({ type : value.data.type, block_status: parseInt(value.data.block_status)})
        console.log(" getRecentPollsListApi1", value.data)
        this.props.dispatch(onClearState());
        this.props.dispatch(getHomeAction(value.data.token));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    
    if (_.isEmpty(props.error) === false) {
      if (props.error.status === 'error') {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    
    if (_.isEmpty(props.recentIssuesPollsResponse) === false ) {
      if (props.recentIssuesPollsResponse.status === 'success') {
        console.log(" componentWillReceiveProps recentIssuesPollsResponse", props.recentIssuesPollsResponse)
        this.setState({
          loading: false,
          refreshing: true,
          recentIssuesPollsResponse: props.recentIssuesPollsResponse.data, 
          errMsg: (props.recentIssuesPollsResponse.data.length === 0)? 'No Result Found' : null
        })
      } else {
        this.setState({ 
          loading: false,
          refreshing: true,
          errMsg: (props.recentIssuesPollsResponse.data.length === 0)? 'No Result Found' : null
        })
        // setTimeout(() => { alert(props.recentIssuesPollsResponse.message) }, 800);
      }
      this.props.dispatch(onClearState());
    } 

    if (_.isEmpty(props.newsFeedResponse) === false ) {
      console.log("news response success===>", props.newsFeedResponse.articles);
      console.log(props.newsFeedResponse);
      this.setState({
        newsResponse: props.newsFeedResponse.articles
      })
    } else {
      console.log("news response error");
    }
    
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getRecentPollsListApi();      
  }


  componentWillUnmount(){
      this.willFocusSubscription.remove();
      BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick = () => {
    return true;
  }

  render() {
    return (
      <Container style={{ flex: 1 }}>
        <View style={{ flex: 9 }}>
        <HeaderBackNativeBase
            containerStyles={{ backgroundColor: '#f6f7f9' }}
            leftIcon={require('../../resources/images/Home_page/Menu_icon/Menu-icon.png')}
            onPressLeft={() => this.props.navigation.openDrawer()}
            title='Home'
          />
          {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
          <View style={styles.container}>
            <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', 
            marginTop:10 
            }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
              <FlatList
                // data={this.state.recentIssuesPollsResponse}
                data={this.state.newsResponse}
                showsVerticalScrollIndicator={false}
                renderItem={this._renderItem}
                refreshControl={
                  <RefreshControl
                    refreshing={!this.state.refreshing}
                    onRefresh={this._onRefresh}
                  />
                }
              />
          </View>
        </View>
        <Tabs
          activeTab={0}
          onPressCandidate={() => { this.props.navigation.navigate('CandidateScreen') }}
          onPressOffial={() => { this.props.navigation.navigate('OfficialScreen') }}
          onPressIssue={() => { this.props.navigation.navigate('IssueIdeaScreen') }}
          onPressPolls={() => { this.props.navigation.navigate('PollScreen',{ type: this.state.type, block_status: this.state.block_status }) }}
        />
      </Container>
    );
  }

  onRouteType = (item) => {
    if(item.type === 'issue'){
        this.props.navigation.navigate('IssueScreen', { issueId: item.id })
    }else if(item.type === 'poll'){
        this.props.navigation.navigate('PollDetailScreen', { pollId: item }) 
    }else{
        this.props.navigation.navigate('IdeaScreen', { issueId: item.id })
    }
  }

  showNews = (newsurl) => {
    console.log("news url===>", newsurl);
    this.props.navigation.navigate('NewsScreen', {newsurl: newsurl})
  }

  _renderItem = ({ item, index }) => {
        
    return (
      <View>
        {/* <TouchableOpacity activeOpacity={1} onPress={()=>{this.onRouteType(item)}}> */}
        <TouchableOpacity activeOpacity={1} onPress={()=>{this.showNews(item.url)}}>
          {/* <Card style={{ padding: 20, borderWidth: 1, borderColor: 'transparent', borderRadius: 5 }}> */}
          <Card style={{ paddingTop: 8, paddingBottom: 10, paddingLeft: 10, borderWidth: 1, borderColor: 'transparent', borderRadius: 5 }}>
            <View style={{flex:1, flexDirection: 'row', width: '100%'}}>
            <View style={{width: '20%',}}>
              <Image style={styles.userimg} source={getImageDummyUrl(item.urlToImage)}></Image>
            </View>
            
            <View style={{width: '79%'}}>
              <Text>{item.title}</Text>
            </View>
            
            </View>
            {/* <View>
              {(item.type === 'poll')?
              <Text style={{ lineHeight: 20, fontFamily: 'Nunito-Bold', fontSize: normalize(13) }} numberOfLines={2} >{item.question}</Text>
              :
              <Text style={{ lineHeight: 20, fontFamily: 'Nunito-Bold', fontSize: normalize(13) }} numberOfLines={2} >{item.title}</Text>
              }
            </View>
            <View>
              {(item.follow_status) ?
                <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>{item.posted_at}</Text>
                : null}
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
            <TouchableOpacity onPress={() => this.props.navigation.navigate('FollowerProfileScreen',{user_id: item.posted_user.id})}>
              <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                  <Thumbnail small source={getUserProfileDummyUrl(item.posted_user.profile_picture)} />
                  <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 10, fontSize: normalize(12) }}>{item.posted_user.name}</Text>
              </View>
            </TouchableOpacity>
              
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
              <View>
                {(item.follow_status) ?
                  <View style={{ flexDirection: 'row' }}>
                    <Text style={{ fontFamily: 'Nunito-Medium', marginLeft: 2, fontSize: normalize(10) }}>{`Tags: `}</Text>
                    <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, marginLeft: 2, fontSize: normalize(10) }}>Improving Government, Elections</Text>
                  </View>
                  :
                  <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>Posted: {item.posted_at}</Text>
                }
              </View>
              <View>
                {(item.type === 'poll') ? 
                 <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>{`${item.total_votes} Votes | ${item.type}`}</Text>
                : <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>{item.type}</Text>
                }
              </View>
            </View> */}
          </Card>
        </TouchableOpacity>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 10,
    marginRight: 10,
    // marginBottom: 10,
    backgroundColor:'#fff'
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  userimg: {
    height: 50,
    width: 50,
    borderRadius: (Platform.OS === 'ios') ? 25 : 40,
    // position: 'absolute',
    marginTop: 5
},
});

const mapStateToProps = (state) => {
  return {
    error: state.HomeR.error,
    isLoading: state.HomeR.isLoading,
    recentIssuesPollsResponse: state.HomeR.recentIssuesPollsResponse,
    newsFeedResponse: state.NewsR.newsFeedResponse
  };
};

export default connect(mapStateToProps)(HomeScreen);