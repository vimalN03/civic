import React, { Component } from 'react';
import { StyleSheet, View, TextInput } from 'react-native';
import { Container, Content, Button, Text, Card, CardItem } from 'native-base';
import { HeaderBackNativeBase } from '../../components/Headers';
import { colors, normalize, dimens } from '../../resources';
import { connect } from 'react-redux';
import _ from 'lodash';
import { getProfile } from '../../utils/URLConstants';
import { getForumCreationAction, onClearState } from '../../actions/forumCreation-actions';
import Loader from '../../components/Loader/Loader';
import SectionedMultiSelect from '../../components/SectionedMultiSelect';
import { getChatListAllAction, onChatAllClearState } from '../../actions/chatListAll-actions';

class ForumCreationScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            forumTitle: '',
            politicianListData: [],
            selectedItemsPolitical: []
        }
    }

    componentDidMount() {
        this.willFocusSubscription = this.props.navigation.addListener(
          'willFocus',
          () => {
            this.getChatListApi();    
          }
        );
      }
    
      getChatListApi = () => {
        this.setState({ loading: true })
        getProfile().then((value) => {
          if (!_.isEmpty(value)) {
            
            this.props.dispatch(getChatListAllAction(value.data.token));
          } else {
            this.setState({ loading: false })
          }
        }).catch((error) => {
          this.setState({ loading: false })
          console.log(error.message)
        })
      }

      onSelectedItemsChangeIndustry = (selectedItems) => {
        this.setState({ selectedItemsPolitical: selectedItems });
    }

    componentWillReceiveProps = (props) => {
        if (props.error.status === 'error') {
            if (props.error.status === 'error') {
                setTimeout(() => { alert(props.error.message) }, 800);
                this.props.dispatch(onClearState());
            }
            this.setState({ loading: false })
        }

        if (_.isEmpty(props.getChatListAllRes) === false) {
            if(props.getChatListAllRes.status === 'success'){
              this.setState({
                loading: false,
                politicianListData: props.getChatListAllRes.data,
              })
            }else{
              this.setState({ loading: false, errMsg: (props.getChatListAllRes.data.length === 0)? 'No Result Found' : null });
              // setTimeout(() => { alert(props.getChatListAllRes.message) }, 800);
            }
            this.props.dispatch(onChatAllClearState());
        }

        if (_.isEmpty(props.forumCreationResponse) === false && props.error === '') {
            if (props.forumCreationResponse.status === 'success') {
                console.log(" componentWillReceiveProps forumCreationResponse", props.forumCreationResponse)
                this.setState({ loading: false })
                this.props.navigation.goBack();
            } else {
                this.setState({ loading: false })
                setTimeout(() => { alert(props.forumCreationResponse.message) }, 800);
            }
            this.props.dispatch(onClearState());
        }
    }

    componentWillUnmount(){
        this.willFocusSubscription.remove();
    }

    /**
   * Validate Forum Creation
   */
    validateForumCreation = () => {
        console.log(this.state.selectedItemsPolitical)
        if (this.state.forumTitle === undefined || this.state.forumTitle.trim() === '') {
            setTimeout(() => { alert('Please Enter the Forum Title') }, 800);
        } else if(this.state.selectedItemsPolitical.length === 0){
            setTimeout(() => { alert('Please Select Members') }, 800);
        } else {
            this.setState({ loading: true },()=>{
                this.setState({ loading: true })
            })
            getProfile().then((value) => {
                if (!_.isEmpty(value)) {
                    this.props.dispatch(onClearState());
                    this.props.dispatch(getForumCreationAction(value.data.token, {
                        forum_topic: this.state.forumTitle,
                        members: this.state.selectedItemsPolitical
                    }));
                } else {
                    this.setState({ loading: false })
                }
            }).catch((error) => {
                this.setState({ loading: false })
                console.log(error.message)
            })
        }
    }

    render() {
        return (
            <Container>
                <HeaderBackNativeBase containerStyles={{ backgroundColor: 'transparent' }}
                    leftIcon={require('../../resources/images/left-arrow.png')}
                    onPressLeft={() => { this.props.navigation.goBack() }}
                    title='Create forum' titleStyle={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }} />
                {(this.state.loading) ? <Loader /> : null}
                <Content>
                    <View style={{ marginStart: 15, marginEnd: 15 }}>
                        <Card padder style={{ flexDirection: 'column', padding: 10 }}>
                            <View style={styles.textInputContainer}>
                                <TextInput
                                    value={this.state.forumTitle}
                                    style={{ flex: 1, padding: 10 }}
                                    placeholder="Forum Topic Title"
                                    placeholderTextColor={colors.light_blue}
                                    underlineColorAndroid="transparent"
                                    onChangeText={(text) => this.setState({ forumTitle: text })}
                                />
                            </View>
                            <View style={styles.textInputContainer}>
                                <SectionedMultiSelect
                                    items={this.state.politicianListData}
                                    uniqueKey='id'
                                    displayKey="name"
                                    selectText='Select Members'
                                    readOnlyHeadings={false}
                                    confirmText='Save'
                                    showRemoveAll={true}
                                    onSelectedItemsChange={this.onSelectedItemsChangeIndustry}
                                    selectedItems={this.state.selectedItemsPolitical} 
                                />
                            </View>
                            <CardItem style={{ justifyContent: 'center', alignSelf: 'center' }}>
                                <Button rounded style={{ backgroundColor: colors.primary }} onPress={() => { this.validateForumCreation() }}>
                                    <Text style={{ fontFamily: 'Nunito-Medium' }}>Create Forum</Text>
                                </Button>
                            </CardItem>
                        </Card>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: normalize(20),
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    textInputContainer: {
        flexDirection: 'row',
        paddingLeft: 5,
        marginTop: 12,
        borderWidth: 0.5,
        borderRadius: 1,
        borderColor: '#dfe6f0',
        backgroundColor: '#eff4f8',
        marginStart: 10, marginEnd: 10
    }
});


const mapStateToProps = (state) => {
    return {
        error: state.ForumCreationR.error,
        isLoading: state.ForumCreationR.isLoading,
        forumCreationResponse: state.ForumCreationR.forumCreationResponse,
        getChatListAllRes: state.ChatListAllR.getChatListAllRes
    };
};

export default connect(mapStateToProps)(ForumCreationScreen);