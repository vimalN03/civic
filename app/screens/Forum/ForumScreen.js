import React, { Component } from 'react';
import { StyleSheet, RefreshControl, TouchableOpacity, View, FlatList, Image } from 'react-native';
import { Container, Thumbnail, Content, Card, Text, Header, Left, Right, Body, Title } from 'native-base';
import _ from 'lodash';
import { colors, normalize, wp, dimens } from '../../resources';
import { connect } from 'react-redux';
import { getForumListAction, onClearState } from '../../actions/forumList-actions';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import Loader from '../../components/Loader/Loader';

class ForumScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      refreshing: true,
      loading: false,
      forumListResponse: [],
      type:'',
      errMsg: null
    }
  }

  componentDidMount() {
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getForumListApi();
      }
    );
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getForumListApi();      
  }

  /**
   * Fetch Forum list of data
   */
  getForumListApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.setState({ 
          type: value.data.type
        })
        this.props.dispatch(onClearState());
        this.props.dispatch(getForumListAction(value.data.token));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  componentWillReceiveProps = (props) => {
    if (_.isEmpty(props.error) === false) {
      if (props.error.status === 'error') {
        setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.forumListResponse) === false && props.error === '') {
      if (props.forumListResponse.status === 'success') {
        console.log(" componentWillReceiveProps forumListResponse", props.forumListResponse)
        this.setState({
          refreshing: true,
          forumListResponse: props.forumListResponse.data, 
          loading: false,
          errMsg: (props.forumListResponse.data.length === 0)? 'No Result Found' : null
        })
      } else {
        this.setState({ refreshing: true, loading: false, errMsg: (props.forumListResponse.data.length === 0)? 'No Result Found' : null })
        // setTimeout(() => { alert(props.forumListResponse.message) }, 800);
      }
    }
  }

  componentWillUnmount(){
    this.willFocusSubscription.remove();
  }

  render() {
    return (
      <Container>
        <Header transparent={false} style={{ backgroundColor: colors.light_silver, alignItems: 'center' }} androidStatusBarColor={'#019235'} iosBarStyle="light-content">
          <Left style={{ flex: 0.2, marginLeft: 5 }}>
            <TouchableOpacity onPress={() => { this.props.navigation.openDrawer() }}>
              <Image style={{ width: 25, height: 25 }} source={require('../../resources/images/Home_page/Menu_icon/Menu-icon.png')} />
            </TouchableOpacity>
          </Left>
          <Body style={{flex: 2,justifyContent:'flex-start', marginLeft:10, alignItems:'flex-start'}}>
              <Title style={{ color: colors.black, fontSize: normalize(dimens.headerTitle) }}>Forums</Title>
            </Body>
          <Right>
            {
              (this.state.type === 'citizen')? null :            
            <TouchableOpacity onPress={() => { this.props.navigation.navigate('ForumCreationScreen') }}>
              <View style={{
                backgroundColor: colors.primary, borderRadius: 5,
                padding: 5, alignItems: 'center'
              }}>
                <Text uppercase={false} style={{
                  margin: 5,
                  textAlignVertical: 'center', color: colors.white, fontSize: normalize(dimens.headerButtonText)
                }}>Add Forum</Text>
              </View>
            </TouchableOpacity>
            }
          </Right>
        </Header>
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
        <View style={styles.container}>
          <View style={{ backgroundColor:'#fff', alignSelf:'center', justifyContent: 'center', marginTop:20 }}>
              <Text style={{ alignItems:'center', fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(18) }}>{this.state.errMsg}</Text>
            </View>
            <FlatList
              data={this.state.forumListResponse}
              showsVerticalScrollIndicator={false}
              renderItem={this._renderItem}
              refreshControl={
                <RefreshControl
                  refreshing={!this.state.refreshing}
                  onRefresh={this._onRefresh}
                />
              }
            />
          </View>
      </Container>
    );
  }

  _renderItem = ({ item, index }) => {
    return (
      <View>
        <TouchableOpacity activeOpacity={1} onPress={() => this.props.navigation.navigate('ForumDetailScreen', { forum_topic_id: item.id })}>
          <Card style={{ padding: 15, borderWidth: 1, borderColor: 'transparent', borderRadius: 5 }}>
            <View>
              <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(13) }} numberOfLines={2} >{item.topic_title}</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row' }}>
                <Thumbnail style={{ width: 25, height: 25 }} source={getUserProfileDummyUrl(item.profile_picture)} />
                <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 10, fontSize: normalize(12) }}>{item.created_user}</Text>
              </View>
              {(item.status === 'closed') ?
                <View style={{ borderWidth: 1, borderRadius: 5, borderColor: '#808b9d', backgroundColor: '#808b9d', padding: 5 }}>
                  <Text style={{ fontFamily: 'Nunito-Medium', color: colors.white, paddingLeft: 10, paddingRight: 10, fontSize: normalize(12) }}>Closed</Text>
                </View>
                :
                <View style={{ borderWidth: 1, borderRadius: 5, borderColor: colors.primary, backgroundColor: colors.primary, padding: 5 }}>
                  <Text style={{ fontFamily: 'Nunito-Medium', color: colors.white, paddingLeft: 10, paddingRight: 10, fontSize: normalize(12) }}>Active</Text>
                </View>
              }
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
              <View>
                <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>Created On: {item.created_at}</Text>
              </View>
              <View>
                <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>{`${item.total_members} Members`}</Text>
              </View>
            </View>
          </Card>
        </TouchableOpacity>
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    margin: 10,
    backgroundColor:'#fff'
  },
  welcome: {
    fontSize: normalize(20),
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});


const mapStateToProps = (state) => {
  return {
    error: state.ForumListR.error,
    isLoading: state.ForumListR.isLoading,
    forumListResponse: state.ForumListR.forumListResponse,
  };
};

export default connect(mapStateToProps)(ForumScreen);