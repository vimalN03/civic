import React, { Component } from 'react';
import { Platform, RefreshControl, StyleSheet, View, TextInput, TouchableOpacity, Image, ScrollView, FlatList } from 'react-native';
import { Card, Icon, Thumbnail, Text } from 'native-base';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import { HeaderBackNativeBase } from '../../components/Headers/index';
import { colors, normalize } from '../../resources';

import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import _ from 'lodash';
import { connect } from 'react-redux';
import { getForumPostAction, onForumPostClearState } from '../../actions/forumPost-actions';
import { getForumDetailAction, getForumChangeStatusAction, onClearState } from '../../actions/forumDetail-actions';
import { getProfile, getUserProfileDummyUrl } from '../../utils/URLConstants';
import Loader from '../../components/Loader/Loader';

class ForumDetailScreen extends Component {

  constructor(props) {
    super(props);
    const { forum_topic_id } = props.navigation.state.params
    this.state = {
      refreshing: true,
      user_id: 0,
      forum_topic_id: forum_topic_id,
      loading: false,
      forumDetailResponse: { user: { politician_details: {} } },
    }
  }

  componentDidMount() {
    this.getForumDetailApi();
  }

  _onRefresh = () => {
    this.setState({ refreshing: false, loading: false });
    this.getForumDetailApi();      
  }

   /**
   * Fetch Forum Details from API
   */
  getForumDetailApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.setState({ user_id: value.data.id});
        this.props.dispatch(onClearState());
        this.props.dispatch(getForumDetailAction(value.data.token, { forum_topic_id: this.state.forum_topic_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

   /**
   * Change status of forum from Active and Close
   */
  getForumChangeStatusApi = () => {
    this.setState({ loading: true })
    getProfile().then((value) => {
      if (!_.isEmpty(value)) {
        this.props.dispatch(onClearState());
        this.props.dispatch(getForumChangeStatusAction(value.data.token, { forum_topic_id: this.state.forum_topic_id }));
      } else {
        this.setState({ loading: false })
      }
    }).catch((error) => {
      this.setState({ loading: false })
      console.log(error.message)
    })
  }

  onSendForum = () => {
    if(_.isEmpty(this.state.message)){
      alert('Please type Your Query')
    }else{
      this.setState({ loading: true },() =>
        this.setState({ loading: true })
      )
      getProfile().then((value) => {
        if (!_.isEmpty(value)) {
          this.props.dispatch(onForumPostClearState());
          var data= {
            "forum_topic_id": this.state.forum_topic_id,
            "comment": this.state.message
          }
          this.props.dispatch(getForumPostAction(value.data.token, data));
        } else {
          this.setState({ loading: false })
        }
      }).catch((error) => {
        this.setState({ loading: false })
        console.log(error.message)
      })
    }
  }

  componentWillReceiveProps = (props) => {
    if (_.isEmpty(props.error) === false) {
      if (props.error.status === 'error') {
        setTimeout(() => { alert(props.error.message) }, 800);
        this.props.dispatch(onClearState());
      }
      this.setState({ loading: false })
    }
    if (_.isEmpty(props.forumDetailResponse) === false && props.error === '') {
      if (props.forumDetailResponse.status === 'success') {
        console.log("componentWillReceiveProps forumDetailResponse", props.forumDetailResponse)
        this.setState({
          loading: false, refreshing: true,
          forumDetailResponse: props.forumDetailResponse.data, 
        })
      } else {
        this.setState({refreshing: true, loading: false })
        setTimeout(() => { alert(props.forumDetailResponse.message) }, 800);
      }
      
    }

    if (_.isEmpty(props.forumChangeStatusResponse) === false && props.error === '') {
      if (props.forumChangeStatusResponse.status === 'success') {
        console.log(" componentWillReceiveProps forumChangeStatusResponse", props.forumChangeStatusResponse)
        this.setState({
          loading: false, 
        })
       // setTimeout(() => { alert(props.forumChangeStatusResponse.message) }, 800);
        this.getForumDetailApi();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.forumChangeStatusResponse.message) }, 800);
      }
      
    }

    if (_.isEmpty(props.getForumPostRes) === false) {
      if (props.getForumPostRes.status === 'success') {
        this.setState({ message: '', loading: false })
       // setTimeout(() => { alert(props.getForumPostRes.message) }, 800);
        this.getForumDetailApi();
      } else {
        this.setState({ loading: false })
        setTimeout(() => { alert(props.getForumPostRes.message) }, 800);
      }
      this.props.dispatch(onForumPostClearState());
    }
  }

  _renderItem = ({ item, i }) => {
    
    return (
      <View style={{ flex: 1 }}>
        {(parseInt(item.user_id) !== parseInt(this.state.user_id))? 
        <View>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
            <View style={{ flexDirection: 'row', padding: 8 }}>
              <Thumbnail style={{ width: 30, height: 30 }} source={require('../../resources/images/Profile/avatar.jpg')} />
              <Text style={{ fontFamily: 'Nunito-Medium', alignSelf: 'flex-start', marginTop: 5, paddingLeft: 10, color: '#3e5964' }}>{item.replied_by}</Text>
            </View>
            <Text style={{ fontFamily: 'Nunito-Medium', paddingRight: 10, alignSelf: 'flex-end', color: '#87949c', fontSize: normalize(12), bottom: 5 }}>{item.replied_at}</Text>
          </View>
          <Card style={{ flex: 1, borderRadius: 8, marginLeft: 30, marginRight: 80, alignItems: 'flex-start', backgroundColor: '#ecf1f4', padding: 10 }}>
            <Text style={{ fontFamily: 'Nunito-Medium', color: '#3d5764', fontSize: normalize(12), lineHeight: 15 }}>{item.reply_content}</Text>
          </Card>
        </View>
        :
        <View style={{ marginTop: 10, }}>
          <Text style={{ fontFamily: 'Nunito-Medium', paddingRight: 10, alignSelf: 'flex-end', color: '#87949c', fontSize: normalize(12), bottom: 5 }}>{item.replied_at}</Text>
          <Card style={{ flex: 1, borderRadius: 8, marginLeft: 80, marginRight: 10, alignItems: 'flex-end', backgroundColor: colors.primary, padding: 10 }}>
            <Text style={{ fontFamily: 'Nunito-Medium', alignSelf: 'flex-start', paddingRight: 10, color: colors.white, fontSize: normalize(12),lineHeight: 15 }}>{item.reply_content}</Text>
          </Card>
        </View>
        }
      </View>
    );
  }

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }}>
        <HeaderBackNativeBase
          containerStyles={{ backgroundColor: '#f6f7f9', alignItems: 'center' }}
          leftIcon={require('../../resources/images/left-arrow.png')}
          onPressLeft={() => { this.props.navigation.goBack() }}
          title='Forums' />
        {(this.state.loading && this.state.refreshing) ? <Loader /> : null}
   
        <ScrollView style={{ marginLeft: 15, marginRight: 15, marginBottom: 70, backgroundColor: '#fff' }}
          ref={ref => this.scrollView = ref}
          onContentSizeChange={(contentWidth, contentHeight) => {
            this.scrollView.scrollToEnd({ animated: true });
          }}>
               <KeyboardAwareScrollView
               enableOnAndroid extraScrollHeight={ (Platform.OS === 'ios') ? 0 : -220}
  >
          <Card style={{ padding: 15, borderWidth: 1, borderColor: 'transparent', borderRadius: 5 }}>
            <View>
              <Text style={{ fontFamily: 'Nunito-Medium', fontSize: normalize(13) }} numberOfLines={2} >{this.state.forumDetailResponse.topic_title}</Text>
            </View>
            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
              <View style={{ flexDirection: 'row' }}>
                <Thumbnail style={{ width: 25, height: 25 }} source={getUserProfileDummyUrl(this.state.forumDetailResponse.user.politician_details.profile_picture)} />
                <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 10, fontSize: normalize(12) }}>
                  {this.state.forumDetailResponse.user.name}</Text>
              </View>

              <TouchableOpacity onPress={() => { this.getForumChangeStatusApi() }}>
                <View style={{
                  flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderRadius: 5, borderColor: colors.primary,
                  backgroundColor: colors.white, padding: 5
                }}>
                  <Text style={{ fontFamily: 'Nunito-Medium', color: colors.primary, paddingLeft: 10, paddingRight: 10, fontSize: normalize(12) }}>
                    {(this.state.forumDetailResponse.status === 'closed') ? 'Closed' : 'Open'}
                  </Text>
                  <Icon name='chevron-down' type='FontAwesome' style={{ fontSize: normalize(10), color: colors.primary }} />
                </View>
              </TouchableOpacity>

            </View>
            <View style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between' }}>
              <View>
                <View style={{ flexDirection: 'row' }}>
                  <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>
                    Created on: {this.state.forumDetailResponse.created_at}</Text>
                </View>

              </View>
              <View>
                <Text style={{ fontFamily: 'Nunito-Medium', color: '#b9bec2', marginLeft: 2, fontSize: normalize(10) }}>
                  {`${this.state.forumDetailResponse.total_members} Members`}
                </Text>
              </View>
            </View>
          </Card>

          <FlatList
            data={this.state.forumDetailResponse.replies}
            renderItem={this._renderItem}
            keyExtractor={(index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={!this.state.refreshing}
                onRefresh={this._onRefresh}
              />
            }
          />{(Platform.OS === 'ios') ? <KeyboardSpacer /> : null}
          </KeyboardAwareScrollView>
        </ScrollView>
        {(this.state.forumDetailResponse.status === 'closed') ? null : 
        <View style={{
          flex: 1, flexDirection: 'row', position: 'absolute', left: 0, right: 0, bottom: 0, backgroundColor: '#fff', padding: 1,
          borderTopWidth: 2, borderTopColor: '#cad0dc'
        }}>
          <View style={{ flex: 4.1 }}>
            <TextInput
            onFocus={(event) => {
              this.scrollView.scrollToEnd({ animated: true });
            }}
              style={{ alignItems: 'center', textAlign: 'left', padding:(Platform.OS === 'ios') ? 10 : 2 }}
              placeholder="Type your message..."
              underlineColorAndroid='transparent'
              onChangeText={(text) => this.setState({ message: text })}
              value={this.state.message}
            />{(Platform.OS === 'ios') ? <KeyboardSpacer /> : null}
          </View>
          <View style={{ flex: 1.5, flexDirection: 'row' }}>
            <TouchableOpacity
              style={{ padding: 5 }}
              onPress={() => { }}>
              <Text style={{width:40}} />
              {/* <Image style={{ width: 30, height: 30 }} source={require('../../resources/images/ForumsDetail/AddIcon/Add-Icon.png')} /> */}
            </TouchableOpacity>
            <TouchableOpacity
              style={{ padding: 5 }}
              onPress={() => {this.onSendForum()}}>
              <Image style={{ width: 30, height: 30 }} source={require('../../resources/images/ForumsDetail/SendIcon/Send-Icon.png')} />
            </TouchableOpacity>

          </View>

        </View>
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});

const mapStateToProps = (state) => {
  return {
    error: state.ForumDetailR.error,
    isLoading: state.ForumDetailR.isLoading,
    forumDetailResponse: state.ForumDetailR.forumDetailResponse,
    forumChangeStatusResponse: state.ForumDetailR.forumChangeStatusResponse,
    forumError: state.ForumPostR.forumError,
    forumisLoading: state.ForumPostR.forumisLoading,
    getForumPostRes: state.ForumPostR.getForumPostRes
  };
};

export default connect(mapStateToProps)(ForumDetailScreen);