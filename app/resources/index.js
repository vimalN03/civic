import colors from './colors'
import styles from './styles';
import dimens from './dimens';
import strings from './strings';
import normalize from './normalizeText';
import {
    widthPercentageToDP as wp,
    heightPercentageToDP as hp,
    listenOrientationChange as loc,
    removeOrientationListener as rol
} from './responsiveScreen';

export {
    styles, colors, dimens, strings, normalize, wp, hp,
    loc, rol
};