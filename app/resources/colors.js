/**
 * Created by Yuvaraj on 6/23/17.
 */

const colors = {
  primary: '#019235',
  primary_light: '#29c152',
  softcyan: '#4acbf5',
  verylightgray: '#eeeeee',
  verypalemostlywhiteblue: '#fcfdff',
  grayishlimegreen: '#ced0ce',
  gray: '#777777',
  drakblue: '#003f5b',
  white: '#FFFFFF',
  liteblack: '#3a3a3a',
  grayblue: '#003f5b',
  liteblue: '#0D80C1',
  black: '#000',
  drakskyblue: '#007aff',
  litebrown: '#CCCCCC',
  red: '#DA4336',
  litered: '#e83309',
  skyblue: '#3498db',
  orange: '#f2780c',
  blue: '#1291D1',
  btnblue: '#0DA1D9',
  green: '#39AF39',
  litegray: '#ACB1B2',
  darkgray: '#999999',
  litebrownsecond: '#BCBCBC',
  darkgreen: '#05721c',
  yellow: '#FFC515',
  darkbluelite: '#18628c',
  Vividblue: '#14aaff',
  Darkgrayishblue: '#94989A',
  Verydarkgrayishblue: '#565759',
  Darkgrayishcyan: '#848787',
  verylitebrown: '#d7d7d7',
  verydarkgray: '#343434',
  verydarkgraybrown: '#797979',
  whatsapp_color_code: '#34af23',
  light_silver: '#f6f7f9',
  light_blue: '#808b9d',
  very_pale_mostly_white_blue: '#f5fcff',
  very_dark_gray: '#333333',
  mostly_desaturated_dark_blue: '#445b61',
  dark_grayish_blue: '#94979a'
}


export default colors;