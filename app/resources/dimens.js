import { Platform } from 'react-native';
import {widthPercentageToDP as wp} from './responsiveScreen';
/**
 * Created by Yuvaraj on 6/21/17.
 */
export default {
  margin_small: 8,
  margin_medium: 16,
  margin_large: 24,
  text_size_button: 18,
  text_size_label: 20,
  headerTitle: (Platform.OS === 'ios') ? 16 : 20,
  headerButtonText: 10,
  candidateProfileScreenMarginTop: (Platform.OS === 'ios') ? wp('7%') : wp('5%'),
}