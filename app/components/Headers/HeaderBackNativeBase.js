import React from 'react';
import { Image, TouchableOpacity } from 'react-native';
import { View, Header, Title, Right, Body, Left } from "native-base";
import { colors, wp, normalize, dimens } from '../../resources';
import { getHeaderMainTitle } from '../../utils/URLConstants';

const HeaderBackNativeBase = ({ containerStyles, transparent = false, androidStatusBarColor = '#019235', title = "",
  onPressLeft, leftIcon, titleStyle = { color: colors.black, paddingLeft: 5, fontSize: normalize(dimens.headerTitle) },
  onPressRight, rightIcon, rightVisibility = false, }) => (
    <View>
      <Header transparent={transparent} style={containerStyles} androidStatusBarColor={androidStatusBarColor} iosBarStyle="light-content">
        <Left style={{ flex: 0.2, marginLeft: 5 }}>
          <TouchableOpacity onPress={onPressLeft}>
            <Image style={{ width: 25, height: 25 }} source={leftIcon} />
          </TouchableOpacity>
        </Left>
        {/* <Body style={getHeaderMainTitle()}>
          <Title style={titleStyle}>{title}</Title>
        </Body> */}
        <Body style={{flex: 2,justifyContent:'flex-start', marginLeft:10, alignItems:'flex-start'}}>
          <Title style={titleStyle}>{title}</Title>
        </Body>
        <Right>
          {(rightVisibility) ?
            (<TouchableOpacity onPress={onPressRight}>
              <Image style={{ width: 25, height: 25 }} source={rightIcon} />
            </TouchableOpacity>) : null
          }
        </Right>
      </Header>
    </View>
  );


export default HeaderBackNativeBase;
