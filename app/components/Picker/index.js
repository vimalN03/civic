import React from 'react';
import { Platform, View, Picker } from 'react-native';
import IOSPicker from 'react-native-ios-picker';

const CustomPicker = (props) => (
  <View>
        {(Platform.OS === 'ios') ?
            <View style={{ borderWidth: 1, margin: 20, borderRadius: 5, borderColor: '#06c4a2' }}>
                <IOSPicker 
                    mode='modal'
                    selectedValue={props.selectedValue}
                    onValueChange={props.onValueChange}
                >
                { props.children }
                </IOSPicker>   
            </View> 
        :
            <View style={{ borderWidth: 1, margin: 20, borderRadius: 5, borderColor: '#06c4a2' }}>
                <Picker
                    selectedValue={props.selectedValue}
                    onValueChange={props.onValueChange}    
                >
                    {props.children}
                </Picker>
            </View>
        }
        
  </View>
);

export default CustomPicker;