import React, { Component } from 'react'
import DatePicker from 'react-native-datepicker'
import moment from 'moment';
import { normalize } from '../../resources';

export default class MyDatePicker extends Component {
  constructor(props){
    super(props)
  }

 
  render(){
    return (
      <DatePicker
        {...this.props}
        mode="date"
        format="YYYY/MM/DD"
        confirmBtnText="Confirm"
        // customStyles = {{
        //         dateInput: {borderColor:"transparent", borderWidth:1},
        //         dateTouchBody: {borderColor:"lightgray", borderWidth:3, borderRadius:20} 
        // }}
        iconSource={require('../../resources/images/05b_create_poll/Calendar/Calendar.png')}
        cancelBtnText="Cancel"
        onDateChange={this.props.onDateChange}
      />
    )
  }
}